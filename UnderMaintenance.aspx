<%@ Page Language="C#" MasterPageFile="~/MasterPages/Minor.master" AutoEventWireup="true" Title="Customer.SunbeltRentals.com (Under Maintenance)" %>

<script runat="server">
//<![CDATA[

	protected void Page_Load(object sender, EventArgs e)
	{
		HyperLink1.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
	}

//]]>	
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="MainBodyContent" Runat="Server">
    <div>
	<table style="width: 100%;">
		<tr>
			<td style="text-align: center;">
				<table style="width: 620px;">
					<tr>
						<td colspan="2" style="font-weight: normal; font-size: 11pt; text-align: center; border-top: #999999 1px solid; border-bottom: #999999 1px solid; padding-top: 10px; padding-bottom: 10px;">The 
							Sunbelt Rentals Customer site is being updated.  It will be back momentarily.</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center; padding-top: 10px;">
							<asp:Image ID="Image1" runat="server" ImageUrl="~/images/down.png" /></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;"></td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center; vertical-align: bottom;">
							<asp:HyperLink ID="HyperLink1" runat="server" EnableViewState="False"
								NavigateUrl="#">Return to SunbeltRentals.com</asp:HyperLink>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
    </div>
</asp:Content>
