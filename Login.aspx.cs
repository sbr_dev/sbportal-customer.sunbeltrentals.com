using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;

public partial class Login : Sunbelt.Web.WebPage
{
	private enum PageState { Login, ResetPassword, Error }
	private PageState State
	{
		get { return (PageState)GetViewState("State", PageState.Login); }
		set { SetViewState("State", value); }
	}
	private string UserName
	{
		get { return (string)GetViewState("UserName", ""); }
		set { SetViewState("UserName", value); }
	}
	private string GotoUrl
	{
		get { return (string)GetViewState("GotoUrl", ""); }
		set { SetViewState("GotoUrl", value); }
	}
	protected override void OnPreRender(EventArgs e)
	{
		//This will set the form action to match the pages url
		//(ie. "action=https://.../login.aspx" not just "action=login.aspx").
		string s = Page.Request.Url.ToString();

		Page.Form.Action = s;

		base.OnPreRender(e);
	}

	protected override void OnLoad(EventArgs e)
	{
		if (!IsPostBack)
		{
			if (ConfigurationSettings.AppSettings["httpsRequired"] == "1")
			{
				//This code will force the login page in to https mode.
				if (Request.Url.Scheme == "http")
				{
					string url = Request.Url.AbsoluteUri.Replace("http:", "https:");
					Response.Redirect(url, true);
				}
			}
		}

		base.OnLoad(e);
	}
	
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
        {
			if (!User.Identity.IsAuthenticated)
				((SBNetMasterPage)Master).ShowLeftImage("~/Images/keypiece_75.gif");
			
			if (Request.QueryString["gotourl"] != null)
			{
                GotoUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(Request.QueryString["gotourl"]);
                GotoUrl = GotoUrl.Replace("https:", "http:");
			}
			else
			if (Request.QueryString["src"] != null && Request.QueryString["src"] == "WEB")
			{
				string webRoot = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
				string returnURL = Request.QueryString["returnURL"];
				if (returnURL.Contains("/SunbeltRentals"))
					webRoot = webRoot.Replace("/SunbeltRentals", "");
				GotoUrl = webRoot + returnURL;
			}
		}

		Master.LeftNavVisible = false;
		Master.TopSearchVisible = false;
		Master.UpdateUserNavControls();
		UpdateDisplay();

		UPLogin1.NewUserNavigateUrl = ResolveUrl("~/AddUser.aspx");

		UPLogin1.UserErrorRasied += new EventHandler<UserErrorEvent>(UPLogin1_UserErrorRasied);
		UPLogin1.UserLoggedIn += new EventHandler<UserEvent>(UPLogin1_UserLoggedIn);
		//UPLogin1.NewUserClicked += new EventHandler<UserEvent>(UPLogin1_NewUserClicked);
		UPLogin1.ForgotPasswordClicked += new EventHandler<UserEvent>(UPLogin1_ForgotPasswordClicked);
		UPLogin1.TooManyLoginAttempts += new EventHandler<UserEvent>(UPLogin1_TooManyLoginAttempts);

		UPResetPassword1.LogMeInClicked += new EventHandler<UserEvent>(UPResetPassword1_LogMeInClicked);
		UPResetPassword1.UserErrorRasied += new EventHandler<UserErrorEvent>(UPLogin1_UserErrorRasied);
	}

	private void UpdateDisplay()
	{
		divLogin.Visible = false;
		divResetPassword.Visible = false;
		divError.Visible = false;

		switch (State)
		{
			case PageState.Login:
				divLogin.Visible = true;
				break;
			case PageState.ResetPassword:
				divResetPassword.Visible = true;
				break;
			default:
				divError.Visible = true;
				break;
		}
	}

	void UPResetPassword1_LogMeInClicked(object sender, UserEvent e)
	{
		State = PageState.Login;
		UpdateDisplay();
	}

	void UPLogin1_UserLoggedIn(object sender, UserEvent e)
	{
		UserEvent ue = (UserEvent)e;

		//Login user.
		if (string.IsNullOrEmpty(GotoUrl))
		{
			SBNet.SBNetSqlMembershipProvider.LoginUser(ue.UserName, true);
		}
		else
		{
			SBNet.SBNetSqlMembershipProvider.LoginUser(ue.UserName, false);
			Response.Redirect(ResolveUrl(GotoUrl));
		}
		
		//Update master page.
		Master.UpdateUserNavControls();
	}

	void UPLogin1_ForgotPasswordClicked(object sender, UserEvent e)
	{
		UPResetPassword1.TooManyLoginAttempts = false;
		State = PageState.ResetPassword;
		UpdateDisplay();
	}

	void UPLogin1_TooManyLoginAttempts(object sender, UserEvent e)
	{
		//Treat this like a password reset. Tell UPResetPassword
		//to display extra info.
		UPResetPassword1.TooManyLoginAttempts = true;
		State = PageState.ResetPassword;
		UpdateDisplay();
	}

	//void UPLogin1_NewUserClicked(object sender, UserEvent e)
	//{
	//    Response.Redirect("AddUser.aspx");
	//}

	void UPLogin1_UserErrorRasied(object sender, UserErrorEvent e)
	{
		UPValidationSummary1.DisplayError(e.Message);
	}
}
