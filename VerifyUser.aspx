<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="VerifyUser.aspx.cs" Inherits="VerifyUser" Title="Verify User" %>

<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ Register Src="SiteControls/UserProfile/UPValidationSummary.ascx" TagName="UPValidationSummary" TagPrefix="uc2" %>
<%@ Register Src="SiteControls/UserProfile/UPSendVerifyEmail.ascx" TagName="UPSendVerifyEmail" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
	Account Activation
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<uc2:UPValidationSummary ID="UPValidationSummary1" runat="server" />
<cc1:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpUserProfile">
	<div id="divUserProfile">
	<%----CONTENT START HERE----%>
		<div id="divActivated" runat="server" visible="false">
			Your account has been activated.<br /><br />
			<asp:HyperLink ID="hlLogin" runat="server" NavigateUrl="~/Login.aspx">Log me in!</asp:HyperLink><br />
		</div>
		<div id="divExpired" runat="server" visible="false">
			Your activation email has expired.<br /><br />
			<asp:LinkButton ID="btnResendEmail" runat="server" OnClick="btnResendEmail_Click">Send me a new activation email</asp:LinkButton><br />
			Contact Customer Service.<br />
		</div>
		<div id="divResend" runat="server" visible="false">
			<uc1:UPSendVerifyEmail ID="UPSendVerifyEmail1" runat="server" />
		</div>
		<div id="divError" runat="server" visible="true">
			There was an error tring to activate your account.<br /><br />
			Contact Customer Service.<br />
		</div>
		<asp:HyperLink ID="hlHomePage" runat="server" NavigateUrl="~/Default.aspx">Go to the home page</asp:HyperLink><br />
	<%----CONTENT END HERE----%>
	</div>
</cc1:RoundedCornerPanel>
<script type="text/javascript">
//<![CDATA[

if(typeof(BI_WatchAnchors) == "function")
	BI_WatchAnchors(document.getElementById("divUserProfile"));

//]]>
</script>
</asp:Content>

