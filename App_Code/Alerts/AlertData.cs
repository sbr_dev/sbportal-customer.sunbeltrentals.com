using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Common.DAL;

/// <summary>
/// Summary description for AlertData
/// </summary>
namespace Alerts
{
    public sealed class AlertData
	{
        //private static string CnStr_AlertsInfo
        //{
        //    get { return ConfigurationManager.ConnectionStrings["CnStr_ReportingInfo"].ConnectionString; }
        //}

        public static DataTable GetAlertsByUser(Guid UserId, int AccountNumber, bool IsCorpLinkAccount)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_SelAlertsByUserId", conn))
            {
                sp.AddParameterWithValue("UserId", SqlDbType.UniqueIdentifier, 4, ParameterDirection.Input, UserId);
                sp.AddParameterWithValue("AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                sp.AddParameterWithValue("IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetAlertByAlertId(int AlertId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_SelAlertById", conn))
            {
                sp.AddParameterWithValue("AlertId", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                return sp.ExecuteDataTable();
            }
        }

        public static int AddAlert(
            Guid UserId,
            int AccountNumber,
            bool IsCorpLinkAccount,
            string AlertName,
            int TypeId,
            string ReferenceValue,
            string ParameterValue,
            string Email,
            bool NotifyByEmail,
            string PhoneNumber,
            bool NotifyByPhoneNumber,
            string FaxNumber,
            bool NotifyByFaxNumber,
            bool IsActive,
            string CreatedBy,
			int NotifyPhoneCarrierID )
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_AddAlert", conn))
            {
                sp.AddParameterWithValue("UserId", SqlDbType.UniqueIdentifier, 4, ParameterDirection.Input, UserId);
                sp.AddParameterWithValue("AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                sp.AddParameterWithValue("IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                sp.AddParameterWithValue("AlertName", SqlDbType.VarChar, 250, ParameterDirection.Input, AlertName);
                sp.AddParameterWithValue("TypeId", SqlDbType.Int, 4, ParameterDirection.Input, TypeId);
                sp.AddParameterWithValue("ReferenceValue", SqlDbType.VarChar, 250, ParameterDirection.Input, ReferenceValue);
                sp.AddParameterWithValue("ParameterValue", SqlDbType.VarChar, 250, ParameterDirection.Input, ParameterValue);
                sp.AddParameterWithValue("IsNotifyByEmail", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByEmail);
                if (Email == null)
                    sp.AddParameterWithValue("NotifyEmail", SqlDbType.VarChar, 250, ParameterDirection.Input, DBNull.Value);
                else
                    sp.AddParameterWithValue("NotifyEmail", SqlDbType.VarChar, 250, ParameterDirection.Input, Email);
                sp.AddParameterWithValue("IsNotifyByPhoneNumber", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByPhoneNumber);
                sp.AddParameterWithValue("NotifyPhoneNumber", SqlDbType.VarChar, 10, ParameterDirection.Input, PhoneNumber);
                sp.AddParameterWithValue("IsNotifyByFaxNumber", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByFaxNumber);
                sp.AddParameterWithValue("NotifyFaxNumber", SqlDbType.VarChar, 10, ParameterDirection.Input, FaxNumber);
                sp.AddParameterWithValue("IsActive", SqlDbType.Bit, 1, ParameterDirection.Input, IsActive);
                sp.AddParameterWithValue("CreatedBy", SqlDbType.VarChar, 250, ParameterDirection.Input, CreatedBy);
				if( NotifyPhoneCarrierID != 0)
					sp.AddParameterWithValue("NotifyPhoneCarrierID", SqlDbType.Int, 4, ParameterDirection.Input, NotifyPhoneCarrierID);
                sp.AddParameter("AlertID", SqlDbType.Int, 4, ParameterDirection.Output);
				
				sp.ExecuteNonQuery();

                return Convert.ToInt32(sp.Parameters["AlertID"].Value);
            }
        }

        public static int AddAlertFrequency(int AlertId, int FrequencyId, int FrequencyParameterId, int Time ,int TimeZoneId)
        {
            if (AlertId == 0)
            {
                throw new Exception("Alert Id 0 does not exist.");
            }
            else
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("Alerts.sb_AddAlertFrequency", conn))
                {
                    sp.AddParameterWithValue("AlertID", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                    sp.AddParameterWithValue("FrequencyID", SqlDbType.Int, 4, ParameterDirection.Input, FrequencyId);
                    if (FrequencyParameterId != 0)
                        sp.AddParameterWithValue("FrequencyParameterID", SqlDbType.Int, 4, ParameterDirection.Input, FrequencyParameterId);
                    else
                        sp.AddParameterWithValue("FrequencyParameterID", SqlDbType.Int, 4, ParameterDirection.Input, DBNull.Value);
                    sp.AddParameterWithValue("Time", SqlDbType.Int, 4, ParameterDirection.Input, Time);
                    sp.AddParameterWithValue("TimeZoneID", SqlDbType.Int, 4, ParameterDirection.Input, TimeZoneId);
                    return sp.ExecuteNonQuery();
                }
            }
        }

        public static int UpdateAlert(
            int AlertId,
            string AlertName,
            int TypeId,
            string ReferenceValue,
            string ParameterValue,
            string Email,
            bool NotifyByEmail,
            string PhoneNumber,
            bool NotifyByPhoneNumber,
            string FaxNumber,
            bool NotifyByFaxNumber,
            bool IsActive,
			int NotifyPhoneCarrierID )
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_UpdateAlert", conn))
            {
                sp.AddParameterWithValue("AlertId", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                sp.AddParameterWithValue("AlertName", SqlDbType.VarChar, 250, ParameterDirection.Input, AlertName);
                sp.AddParameterWithValue("ReferenceValue", SqlDbType.VarChar, 250, ParameterDirection.Input, ReferenceValue);
                sp.AddParameterWithValue("ParameterValue", SqlDbType.VarChar, 250, ParameterDirection.Input, ParameterValue);
                sp.AddParameterWithValue("IsNotifyByEmail", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByEmail);
                if (Email == null)
                   sp.AddParameterWithValue("NotifyEmail", SqlDbType.VarChar, 250, ParameterDirection.Input, DBNull.Value);
                else
                    sp.AddParameterWithValue("NotifyEmail", SqlDbType.VarChar, 250, ParameterDirection.Input, Email);
                sp.AddParameterWithValue("IsNotifyByPhoneNumber", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByPhoneNumber);
                sp.AddParameterWithValue("NotifyPhoneNumber", SqlDbType.VarChar, 10, ParameterDirection.Input, PhoneNumber);
                sp.AddParameterWithValue("IsNotifyByFaxNumber", SqlDbType.Bit, 1, ParameterDirection.Input, NotifyByFaxNumber);
                sp.AddParameterWithValue("NotifyFaxNumber", SqlDbType.VarChar, 10, ParameterDirection.Input, FaxNumber);
                sp.AddParameterWithValue("IsActive", SqlDbType.Bit, 1, ParameterDirection.Input, IsActive);
				if( NotifyPhoneCarrierID > 0 )
					sp.AddParameterWithValue("NotifyPhoneCarrierID", SqlDbType.Int, 4, ParameterDirection.Input, NotifyPhoneCarrierID);
				

                return sp.ExecuteNonQuery();
            }
        }

        public static int DeleteAlertById(int AlertId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_DeleteAlertById", conn))
            {
                sp.AddParameterWithValue("AlertId", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                return sp.ExecuteNonQuery();
            }
        }

        public static int DeleteAlertFrequenciesByAlertId(int AlertId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_DelAlertFrequenciesByAlertID", conn))
            {
                sp.AddParameterWithValue("AlertId", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                return sp.ExecuteNonQuery();
            }
        }

        public static DataTable GetTypes( bool excludeSystemAlerts )
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_SelTypes", conn))
            {
                DataTable dt;
                dt = sp.ExecuteDataTable();

                if( excludeSystemAlerts )
                {
                    DataColumn[] AlertKey = new DataColumn[1];
                    AlertKey[0] = dt.Columns["TypeID"];
                    dt.PrimaryKey = AlertKey;

                    DataRow[] rowSet = dt.Select("SystemAlert=true");
                    foreach (DataRow myRow in rowSet)
                    {
                        System.Data.DataRow matchingRow = dt.Rows.Find(myRow["TypeID"]);
                        dt.Rows.Remove(matchingRow);
                    }
                }
                return dt;
            }
        }

        public static DataTable GetReferenceByTypeId(int TypeId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("Alerts.sb_SelReferenceInfoByTypeId", conn))
            {
                sp.AddParameterWithValue("TypeId", SqlDbType.Int, 4, ParameterDirection.Input, TypeId);
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetSPWithAccountNumberParameter(string sp, int AccountNumber, bool IsCorpLinkAccount)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData spData = new SPData(sp, conn))
            {
                spData.AddParameterWithValue("@AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                spData.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                return spData.ExecuteDataTable();
            }
        }

        public static DataTable GetFrequencies()
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData spData = new SPData("Schedule.sb_SelAlertFrequencies", conn))
            {
                return spData.ExecuteDataTable();
            }
        }
        
        public static DataTable GetFrequencyParameters(int FrequencyId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData spData = new SPData("Schedule.sb_SelAlertFrequencyParameters", conn))
            {
                spData.AddParameterWithValue("FrequencyID", SqlDbType.Int, 4, ParameterDirection.Input, FrequencyId);
                return spData.ExecuteDataTable();
            }
        }

        public static DataTable GetAlertFrequenciesByAlertID(int AlertId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData spData = new SPData("Alerts.sb_SelAlertFrequenciesByAlertID", conn))
            {
                spData.AddParameterWithValue("AlertId", SqlDbType.Int, 4, ParameterDirection.Input, AlertId);
                return spData.ExecuteDataTable();
            }
        }

        public static DataTable GetAlertTimeZones()
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData spData = new SPData("Schedule.sb_SelAlertTimeZones", conn))
            {
                return spData.ExecuteDataTable();
            }
        }
		public static DataTable GetSMSCarriers()
		{
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("sms.sb_SelCarriers", conn))
			{
				return sp.ExecuteDataTable();
			}
		}



	}
}
