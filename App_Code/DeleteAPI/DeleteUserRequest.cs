﻿using Newtonsoft.Json;
//using System;

namespace DeleteAPI
{
    public class DeleteUserRequest
    {
        //Required
        [JsonProperty("email")]
        public string Email { get; set; }

        //Optional
        [JsonProperty("oktaUserId", NullValueHandling = NullValueHandling.Ignore)]
        public string OktaUserId { get; set; }

        //Optional
        [JsonProperty("companyId")]
        public string CompanyId { get; set; }

        //For logging
        [JsonProperty("userGuid")]
        public string UserGuid { get; set; }

    }
}