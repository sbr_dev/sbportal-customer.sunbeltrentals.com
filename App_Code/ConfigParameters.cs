﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Common.DAL;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PDFView
/// </summary>
public class ConfigParameters
{
	public static string GetConfigParameters( string ParameterName )
	{
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
		using (SPData sp = new SPData("config.sb_GetSiteParameters", conn))
		{
			sp.AddParameterWithValue("@ParameterName", SqlDbType.VarChar, 32, ParameterDirection.Input, ParameterName);

			try
			{
				Sunbelt.Log.LogActivity("ConfigParameters",
                    "ConfigParameters: GetConfigParameters database call for: " + ParameterName );
				DataTable dt = sp.ExecuteDataTable();
				return dt.Rows[0]["Value"].ToString();
			}
			catch (Exception ex)
			{
                Sunbelt.Log.LogError(ex, "ConfigParameters",
					"ConfigParameters: GetConfigParameters Error thrown ");
			}
			return null;
		}
	}
}
