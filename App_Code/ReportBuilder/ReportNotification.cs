using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Reports;
using Sunbelt;

/// <summary>
/// Summary description for ReportNotification
/// </summary>

namespace Reports
{
    public sealed class ReportNotification
    {
        private MembershipUser user = Membership.GetUser();

        public static DataTable GetUserReportData(int UserReportId, bool IsCorpLinkAccount, Guid UserId, bool ShowTotals, bool ShowSummary,
            int AccountNumber, List<SqlParameter> plist, DataTable dtColumns, string Sorting)
        {
            DataTable dtAllData = new DataTable();
            int ReportID = 0;
            DataTable dtUser = ReportData.GetUserReportByID(Convert.ToInt32(UserReportId));
            ReportID = Convert.ToInt32(dtUser.Rows[0]["ReportId"].ToString());

            DataTable dtReport = Reports.ReportData.GetReportById(ReportID);
            string sp = dtReport.Rows[0]["ProcedureFullName"].ToString();
            bool blnCustomerNumberRequired = Convert.ToBoolean(dtReport.Rows[0]["CustomerNumberRequired"].ToString());

            if (blnCustomerNumberRequired == true || AccountNumber > 0)
            {
                dtAllData = ReportData.GetDynamicReportByCustomerNumber(sp, plist, Sorting, AccountNumber, IsCorpLinkAccount);
            }
            else
                dtAllData = ReportData.GetDynamicReport(sp, plist, Sorting);

            bool blnExists = false;
            DataTable dt = ReportData.GetReportDetailsByReportId(ReportID);
            int iCount = 0;
            for (int i = 0; i < dtAllData.Columns.Count + iCount; i++)
            {
                //Check columns to see if they were selected.  if no rows then default to all columns.
                if (dtColumns != null)
                {
                    foreach (DataRow dr in dtColumns.Rows)
                    {
                        if (dr[0].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                            blnExists = true;
                    }
                }

                //Rename the column headers.
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j]["Header"] != null && dt.Rows[j]["ColumnName"].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                        dtAllData.Columns[i - iCount].ColumnName = dt.Rows[j]["Header"].ToString();
                }
                if (dtColumns != null)
                {
                    if (blnExists == false)
                    {
                        dtAllData.Columns.RemoveAt(i - iCount);
                        iCount++;
                    }
                    else
                        blnExists = false;
                }

            }

            //Get the total amount for each column if the checkbox show totals is selected
            if (ShowTotals == true)
            {
                dtAllData = ReportData.GetTotals(dtAllData, ReportID);
            }

            return dtAllData;
        }

        public static DataTable GetReportData(int ReportID, bool IsCorpLinkAccount, Guid UserId, bool ShowTotals, bool ShowSummary,
            int AccountNumber, List<SqlParameter> plist, DataTable dtColumns, string Sorting)
        {
            DataTable dtAllData = new DataTable();

            DataTable dtReport = Reports.ReportData.GetReportById(ReportID);
            string sp = dtReport.Rows[0]["ProcedureFullName"].ToString();
            bool blnCustomerNumberRequired = Convert.ToBoolean(dtReport.Rows[0]["CustomerNumberRequired"].ToString());

            if (blnCustomerNumberRequired == true || AccountNumber > 0)
                dtAllData = ReportData.GetDynamicReportByCustomerNumber(sp, plist, Sorting, AccountNumber, IsCorpLinkAccount);
            else
                dtAllData = ReportData.GetDynamicReport(sp, plist, Sorting);

            bool blnExists = false;
            DataTable dt = ReportData.GetReportDetailsByReportId(ReportID);

            int iCount = 0;
            for (int i = 0; i < dtAllData.Columns.Count + iCount; i++)
            {
                //Check columns to see if they were selected.  if no rows then default to all columns.
                if (dtColumns != null)
                {
                    foreach (DataRow dr in dtColumns.Rows)
                    {
                        if (dr[0].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                            blnExists = true;
                    }
                }

                //Rename the column headers.  // put in the datatypes so we can format.
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j]["Header"] != null && dt.Rows[j]["ColumnName"].ToString() == dtAllData.Columns[i - iCount].ColumnName)
                    {
                        dtAllData.Columns[i - iCount].ColumnName = dt.Rows[j]["Header"].ToString();
                    }
                }

                //if that column does not exist then get rid of it.
                if (dtColumns != null)
                {
                    if (blnExists == false)
                    {
                        dtAllData.Columns.RemoveAt(i - iCount);
                        iCount++;
                    }
                    else
                        blnExists = false;
                }
            }

            //Get the total amount for each column if the checkbox show totals is selected
            if (ShowTotals == true)
            {
                dtAllData = ReportData.GetTotals(dtAllData, ReportID);
            }

            return dtAllData;
        }

        public static void OpenReportInExcel(DataTable dt, bool ShowAllPages, int PageCount, string Title)
        {
            GridView gv = new GridView();
            if (ShowAllPages)
            {
                gv.AllowPaging = false;
                gv.DataSource = dt;
                gv.DataBind();
            }

            GridViewExportUtil.Export(Title + ".xls", gv, "", false);
        }

        public static void OpenReportAsCSV(DataTable dt, string Title)
        {
            if (Title.Contains(" "))
                Title = Title.Replace(" ", "");

            int iColCount = dt.Columns.Count;
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", Title + ".csv"));
            HttpContext.Current.Response.ContentType = "text/comma-separated-values";//"application/vnd.ms-excel";//"application/x-msexcel";

            StringBuilder sb = SBNet.Data.DataTableToCSV(dt);
            HttpContext.Current.Response.Write(sb.ToString());
            HttpContext.Current.Response.End();
        }

        public static void OpenReportAsXML(DataTable dt, string fileName)
        {
            if (dt.Rows.Count > 0)
            {
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.AddHeader(
                    "content-disposition", string.Format("attachment; filename={0}", fileName + ".xml"));
                HttpContext.Current.Response.ContentType = "text/xml";

                StringBuilder sb = SBNet.Data.DataTableToXML(dt, fileName);
                HttpContext.Current.Response.Write(sb.ToString());
                HttpContext.Current.Response.End();
            }
        }

        public static void EmailReportAsExcel(DataTable dt, bool ShowAllPages, int PageCount, string Title, string EmailAddress)
        {
            GridView gv = new GridView();
            if (ShowAllPages)
            {
                gv.AllowPaging = false;
                gv.DataSource = dt;
                gv.DataBind();
            }

            GridViewExportUtil.Export(Title + ".xls", gv, EmailAddress, true);
        }

        public static void EmailReportAsXML(DataTable dt, string Title, string EmailAddress)
        {
            //Remove any characters that will cause problems when saving.
            if (Title.Contains("\\"))
                Title = Title.Replace("\\", "");
            else if (Title.Contains("/"))
                Title = Title.Replace('/', ' ');

            using (StringWriter sw = new StringWriter())
            {
                FileInfo file = new FileInfo(HttpContext.Current.Server.MapPath("~/Temp/" + Title + ".xml"));
                StringBuilder sb = SBNet.Data.DataTableToXML(dt, Title);
                if (file.Exists)
                {
                    file.Delete();
                }
                using (StreamWriter swWriteFile = file.CreateText())
                {
                    swWriteFile.Write(sb.ToString());
                }

                Email.SendEmail(EmailAddress, ConfigurationManager.AppSettings["SunbeltEmail"].ToString(), null, null, 
					file.Name, "You have been sent a report", true, file.FullName, Email.CleanupType.AttachmentsOnly);
            }
        }

        public static void EmailReportAsCSV(DataTable dt, string Title, string EmailAddress, string SentFrom)
        {
            //Remove any characters that will cause problems when saving.
            if (Title.Contains("\\"))
                Title = Title.Replace("\\", "");
            else if (Title.Contains("/"))
                Title = Title.Replace('/', ' ');

			DirectoryInfo dir = new DirectoryInfo(ConfigurationManager.AppSettings["WEB_TEMP"].ToString());
			string subDirectoryPath = "";
			if (dir.Exists)
				subDirectoryPath = dir.FullName + Guid.NewGuid().ToString();
			else
				Sunbelt.Log.LogError(new Exception("Unable to create subdirectory"),
					dir.FullName + " does not exist!");

			if (!string.IsNullOrEmpty(subDirectoryPath))
			{
				Directory.CreateDirectory(subDirectoryPath);
				DirectoryInfo subDir = new DirectoryInfo(subDirectoryPath);

				FileInfo file = new FileInfo(subDir.FullName + "\\" + Title + ".csv");
				StringBuilder sb = SBNet.Data.DataTableToCSV(dt);

				if (file.Exists)
					File.Delete(file.FullName);

				using (StreamWriter swWriteFile = file.CreateText())
				{
					swWriteFile.Write(sb.ToString());
				}

				Sunbelt.Email.SendEmail(EmailAddress, ConfigurationManager.AppSettings["SunbeltEmail"].ToString(), "", "", file.Name,
					"The attached report was sent from www.sunbeltrentals.com by " + SentFrom, true, file.FullName, 
					Email.CleanupType.AttachmentsAndFolder);

			}
        }
    }
}
