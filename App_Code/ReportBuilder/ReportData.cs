using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Common.DAL;
using System.Text.RegularExpressions;

namespace Reports
{
    public sealed class ReportData
    {
        private Guid UserID;
        public enum SubscriptionType { Alert = 0, Report };

        public ReportData(Guid userID)
        {
            UserID = userID;
        }
        public ReportData()
        {
        }

        #region PreDefined Report Data

        public static DataTable GetReportById(int ID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelReportMetadataByReportID", conn))
                {
                    sp.AddParameterWithValue("@ReportId", SqlDbType.Int, 4, ParameterDirection.Input, ID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public DataTable GetSPWithCustomerNumberParamter(string sp, int AccountNumber, bool IsCorpLinkAccount)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData(sp, conn))
                {
                    spData.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                    spData.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                    returnVal = spData.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetReportDetailsByReportId(int ReportId)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelReportColumnMetadata", conn))
                {
                    sp.AddParameterWithValue("@ReportId", SqlDbType.Int, 4, ParameterDirection.Input, ReportId);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetReportsByCategoryId(int CategoryId, Guid UserID, int AccountNumber)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelReportMetadataByCatID", conn))
                {
                    sp.AddParameterWithValue("@ReportCategoryId", SqlDbType.Int, 4, ParameterDirection.Input, CategoryId);
                    sp.AddParameterWithValue("@UserID", SqlDbType.UniqueIdentifier, 4, ParameterDirection.Input, UserID);
                    sp.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public DataTable GetSPWithNoParameters(string sp)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData(sp, conn))
                {
                    returnVal = spData.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetEnabledColumns(int ReportID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelColumnsWithEnabledSelect", conn))
                {
                    sp.AddParameterWithValue("@ReportID", SqlDbType.Int, 4, ParameterDirection.Input, ReportID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetColumnsThatAllowGrouping(int ReportID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelColumnsWithGrouping", conn))
                {
                    sp.AddParameterWithValue("@ReportID", SqlDbType.Int, 4, ParameterDirection.Input, ReportID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetReportColumnByReportColumnID(int ReportColumnID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelReportColumnMetatdataByReportColumnId", conn))
                {
                    sp.AddParameterWithValue("@ReportColumnID", SqlDbType.Int, 4, ParameterDirection.Input, ReportColumnID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetReportColumnFilters(int ReportId, bool blnGetJustDefaultFilter)
        {
            DataTable returnVal = null;

            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportInfo.sb_SelReportColumnFilters", conn))
                {
                    sp.AddParameterWithValue("ReportID", SqlDbType.Int, 4, ParameterDirection.Input, ReportId);
                    sp.AddParameterWithValue("DefaultFiltersOnly", SqlDbType.Bit, 1, ParameterDirection.Input, blnGetJustDefaultFilter);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetDynamicReport(string sp, List<SqlParameter> paramList, string OrderBy)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData(sp, conn))
                {
                    spData.Timeout = 180;
                    foreach (SqlParameter p in paramList)
                    {
                        spData.AddParameterWithValue(p.ParameterName, p.SqlDbType, p.Size, p.Direction, p.Value);
                    }
                    if (OrderBy != "")
                        spData.AddParameterWithValue("OrderBy", SqlDbType.VarChar, 1000, ParameterDirection.Input, OrderBy);
                    returnVal = spData.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetDynamicReportByCustomerNumber(string sp, List<SqlParameter> paramList, string OrderBy, int AccountNumber, bool IsCorpLinkAccount)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData(sp, conn))
                {
                    spData.Timeout = 180;
                    if (paramList != null)
                    {
                        foreach (SqlParameter p in paramList)
                        {
                            spData.AddParameterWithValue(p.ParameterName, p.SqlDbType, p.Size, p.Direction, p.Value);
                        }
                    }
                    spData.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                    spData.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                    if (OrderBy == "" || OrderBy == null)
                    {
                        spData.AddParameterWithValue("OrderBy", SqlDbType.VarChar, 1000, ParameterDirection.Input, DBNull.Value);
                    }
                    else
                        spData.AddParameterWithValue("OrderBy", SqlDbType.VarChar, 1000, ParameterDirection.Input, OrderBy);
                    returnVal = spData.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }

        #endregion

        #region User Report Data

        public static DataTable GetUserReportByID(int UserReportID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_SelReport", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }
        public static DataTable GetUserReportColumnsByUserReportID(int UserReportId)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_SelReportColumns", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportId);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }

        public static DataTable GetUserReportFiltersByUserReportID(int UserReportID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_SelReportFilters", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }

        public static DataSet GetAlertsAndReportsByEmail(string email)
        {
            DataSet returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("config.sb_GetAlertsReports_byEmail", conn))
                {
                    sp.AddParameterWithValue("email", SqlDbType.VarChar, 64, ParameterDirection.Input, email);
                    returnVal = sp.ExecuteDataSet();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }

        public static void UnSubscribe(int ID, string email, string NewRecipients, SubscriptionType subscriptionType)
        {
            try
            {
                if (subscriptionType == SubscriptionType.Alert)
                {
					using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
					using (SPData sp = new SPData("Alerts.sb_Unsubscribe", conn))
                    {
                        sp.AddParameterWithValue("@AlertID", SqlDbType.Int, 4, ParameterDirection.Input, ID);
                        sp.AddParameterWithValue("@email", SqlDbType.VarChar, 64, ParameterDirection.Input, email);
                        if (NewRecipients == "")
                            sp.AddParameterWithValue("@NewRecipients", SqlDbType.VarChar, 255, ParameterDirection.Input, DBNull.Value);
                        else
                            sp.AddParameterWithValue("@NewRecipients", SqlDbType.VarChar, 255, ParameterDirection.Input, NewRecipients);
                        sp.ExecuteNonQuery();
                    }
                }
                else
                {
					using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
					using (SPData sp = new SPData("ReportSpec.sb_Unsubscribe", conn))
                    {
                        sp.AddParameterWithValue("@UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, ID);
                        sp.AddParameterWithValue("@email", SqlDbType.VarChar, 64, ParameterDirection.Input, email);
                        if (NewRecipients == "")
                            sp.AddParameterWithValue("@NewRecipients", SqlDbType.VarChar, 255, ParameterDirection.Input, DBNull.Value);
                        else
                            sp.AddParameterWithValue("@NewRecipients", SqlDbType.VarChar, 1024, ParameterDirection.Input, NewRecipients);
                        sp.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable GetUserReportAggregate(int UserReportID)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_SelReportAggregates", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem retrieving report from SQL.");
                throw ex;
            }
            return returnVal;
        }

        public static int AddCustomerReport(Guid UserID, int AccountNumber, bool IsCorpLink, int ReportID, string ReportTitle, string Description, bool ShowTotals, int Pagination)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_AddReport", conn))
                {
                    sp.AddParameterWithValue("@UserId", SqlDbType.UniqueIdentifier, 4, ParameterDirection.Input, UserID);
                    sp.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                    sp.AddParameterWithValue("@IsCorpLink", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLink);
                    sp.AddParameterWithValue("@ReportID", SqlDbType.Int, 4, ParameterDirection.Input, ReportID);
                    sp.AddParameterWithValue("@Title", SqlDbType.VarChar, 1000, ParameterDirection.Input, ReportTitle);
                    sp.AddParameterWithValue("@Description", SqlDbType.VarChar, 1000, ParameterDirection.Input, Description);
                    sp.AddParameterWithValue("@ShowTotals", SqlDbType.Int, 4, ParameterDirection.Input, ShowTotals);
                    sp.AddParameterWithValue("@RowsPerPage", SqlDbType.Int, 4, ParameterDirection.Input, Pagination);
                    sp.AddParameter("@UserReportID", SqlDbType.Int, 4, ParameterDirection.Output);
                    sp.ExecuteNonQuery();
                    returnVal = Convert.ToInt32(sp.Parameters["@UserReportID"].Value);
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem adding custom report to SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int AddReportColumns(int UserReportID, int ColumnID, int SelectIndex, string Description, int GroupIndex, int SortIndex)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_AddReportColumn", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    sp.AddParameterWithValue("ReportColumnID", SqlDbType.Int, 4, ParameterDirection.Input, ColumnID);
                    sp.AddParameterWithValue("SelectIndex", SqlDbType.Int, 4, ParameterDirection.Input, SelectIndex);
                    sp.AddParameterWithValue("Description", SqlDbType.VarChar, 1000, ParameterDirection.Input, Description);
                    sp.AddParameterWithValue("GroupIndex", SqlDbType.Int, 4, ParameterDirection.Input, GroupIndex);
                    sp.AddParameterWithValue("SortIndex", SqlDbType.Int, 4, ParameterDirection.Input, SortIndex);
                    sp.AddParameterWithValue("UserReportColumnID", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 0);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem adding custom report to SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int AddReportFilter(int UserReportID,
            int ColumnID,
            string Filter,
            string Filter_List,
            string Filter_Like,
            string Filter_Range_Begin,
            string Filter_Range_End)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_AddReportFilter", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    sp.AddParameterWithValue("ReportColumnID", SqlDbType.Int, 4, ParameterDirection.Input, ColumnID);
                    sp.AddParameterWithValue("Filter", SqlDbType.VarChar, 1000, ParameterDirection.Input, (Filter == null) ? DBNull.Value.ToString() : Filter);
                    sp.AddParameterWithValue("Filter_List", SqlDbType.VarChar, 1000, ParameterDirection.Input, (Filter_List == null) ? DBNull.Value.ToString() : Filter_List);
                    sp.AddParameterWithValue("Filter_Like", SqlDbType.VarChar, 1000, ParameterDirection.Input, (Filter_Like == null) ? DBNull.Value.ToString() : Filter_Like);
                    sp.AddParameterWithValue("Filter_Range_Begin", SqlDbType.VarChar, 1000, ParameterDirection.Input, (Filter_Range_Begin == null) ? DBNull.Value.ToString() : Filter_Range_Begin);
                    sp.AddParameterWithValue("Filter_Range_End", SqlDbType.VarChar, 1000, ParameterDirection.Input, (Filter_Range_End == null) ? DBNull.Value.ToString() : Filter_Range_End);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem adding custom report to SQL server.");
                throw ex;
            }
            return returnVal;
        }

        public static int UpdateUserEmail(int UserReportId, string RecipentEmail)
        {
            int returnVal = 0;
            if (RecipentEmail.Contains("\r\n"))
            {
                RecipentEmail = RecipentEmail.Replace("\r\n", "; ");
            }
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_SetEmailRecipient", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportId);
                    if (RecipentEmail != null)
                        sp.AddParameterWithValue("EmailRecipient", SqlDbType.NVarChar, 1024, ParameterDirection.Input, RecipentEmail);
                    else
                        sp.AddParameterWithValue("EmailRecipient", SqlDbType.NVarChar, 1024, ParameterDirection.Input, DBNull.Value);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem updating user email on SQL server.");
                throw ex;
            }
            return returnVal;
        }

        //Adds the aggregate function to be applied to a certain column when the user saves a report with grouping.
        public static int AddReportAggregates(int UserReportID,
            int ReportColumnID,
            string Description,
            int AggregateIndex,
            string AggregateFunction)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_AddReportAggregate", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    sp.AddParameterWithValue("ReportColumnID", SqlDbType.Int, 4, ParameterDirection.Input, ReportColumnID);
                    sp.AddParameterWithValue("Description", SqlDbType.VarChar, 1000, ParameterDirection.Input, Description);
                    sp.AddParameterWithValue("AggregateIndex", SqlDbType.Int, 4, ParameterDirection.Input, AggregateIndex);
                    sp.AddParameterWithValue("AggregateFunction", SqlDbType.VarChar, 1000, ParameterDirection.Input, AggregateFunction);
                    sp.AddParameterWithValue("UserReportAggregateID", SqlDbType.Int, 4, ParameterDirection.ReturnValue, 0);

                    returnVal = Convert.ToInt32(sp.Parameters["UserReportAggregateID"].Value);
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem adding report aggregate data to SQL server.");
                throw ex;
            }
            return returnVal;
        }

        public static int UpdateReport(int UserReportID, string Title, string Description, bool ShowTotals, int RowsPerPage)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_UpdateReport", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    sp.AddParameterWithValue("Title", SqlDbType.VarChar, 1000, ParameterDirection.Input, Title);
                    sp.AddParameterWithValue("Description", SqlDbType.VarChar, 1000, ParameterDirection.Input, Description);
                    sp.AddParameterWithValue("ShowTotals", SqlDbType.Bit, 4, ParameterDirection.Input, ShowTotals);

                    if (RowsPerPage != 0)
                        sp.AddParameterWithValue("RowsPerPage", SqlDbType.Int, 4, ParameterDirection.Input, RowsPerPage);
                    else
                        sp.AddParameterWithValue("RowsPerPage", SqlDbType.Int, 1, ParameterDirection.Input, DBNull.Value);

                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem updating report data on SQL server.");
                throw ex;
            }
            return returnVal;
        }

        //This will delete everything for the report including columns and filters
        public static int DeleteReport(int UserReportID)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_DeleteReport", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem deleting report on SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int DeleteReportColumn(int UserReportID)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_DeleteAllReportColumns", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem deleting report column on SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int DeleteReportFilterID(int UserReportID)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_DeleteAllReportFilters", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem deleting report filter on SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int DeleteReportAggregateID(int UserReportID)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_DeleteAllReportFilters", conn))
                {
                    sp.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportID);
                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem deleting report aggregate SQL server.");
                throw ex;
            }
            return returnVal;
        }
        public static int AddReportFrequency(int UserReportId, int FrequencyId, int FrequencyParameterId, int Time, int TimeZoneId, bool EmailAttachReport, bool EmailEmbedReport)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("ReportSpec.sb_AddReportFrequency", conn))
                {
                    sp.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportId);
                    sp.AddParameterWithValue("FrequencyID", SqlDbType.Int, 4, ParameterDirection.Input, FrequencyId);
                    if (FrequencyParameterId != 0)
                        sp.AddParameterWithValue("FrequencyParameterID", SqlDbType.Int, 4, ParameterDirection.Input, FrequencyParameterId);
                    else
                        sp.AddParameterWithValue("FrequencyParameterID", SqlDbType.Int, 4, ParameterDirection.Input, DBNull.Value);
                    sp.AddParameterWithValue("Time", SqlDbType.Int, 4, ParameterDirection.Input, Time);
                    sp.AddParameterWithValue("TimeZoneID", SqlDbType.Int, 4, ParameterDirection.Input, TimeZoneId);
                    sp.AddParameterWithValue("EmailAttachReport", SqlDbType.Bit, 1, ParameterDirection.Input, EmailAttachReport);
                    sp.AddParameterWithValue("EmailEmbedReport", SqlDbType.Bit, 1, ParameterDirection.Input, EmailEmbedReport);

                    returnVal = sp.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem adding report frequency to SQL server.");
                throw ex;
            }
            return returnVal;
        }

        public static DataTable GetReportFrequenciesByUserReportID(int UserReportId)
        {
            DataTable returnVal = null;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData("ReportSpec.sb_SelReportFrequenciesByUserReportId", conn))
                {
                    spData.AddParameterWithValue("UserReportId", SqlDbType.Int, 4, ParameterDirection.Input, UserReportId);
                    returnVal = spData.ExecuteDataTable();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem accessing report data on SQL server.");
                throw ex;
            }
            return returnVal;
        }

        public static int DeleteReportFrequenciesByUserReportId(int UserReportId)
        {
            int returnVal = 0;
            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData spData = new SPData("ReportSpec.sb_DeleteAllReportFrequencies", conn))
                {
                    spData.AddParameterWithValue("UserReportID", SqlDbType.Int, 4, ParameterDirection.Input, UserReportId);
                    returnVal = spData.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Sunbelt.Log.LogError(ex, SBNet.Error.Sections.Reports.ToString(), "Problem deleting report requencies on SQL server.");
                throw ex;
            }
            return returnVal;
        }

        #endregion

        #region Static Report Data

        public static DataTable GetAccountSummary(int AccountNumber, bool IsCorpLinkAccount)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("SpecialReports.sb_AccountSummary", conn))
            {
                sp.AddParameterWithValue("AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, AccountNumber);
                sp.AddParameterWithValue("IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLinkAccount);
                return sp.ExecuteDataTable();
            }
        }

        #endregion

        public static DataTable GroupData(DataTable CurrentView, DataTable GroupingAndFunction, string GroupByColumnName, int UserReport)
        {
            DataTable dtFinalDataTable = new DataTable();
            bool blnCalculateAverage = false;
            bool blnCalculateSum = false;
            bool blnCalculateCount = false;
            bool blnCalculateMin = false;
            bool blnCalculateMax = false;

            //This is the Grouped By Column Name.
            dtFinalDataTable.Columns.Add(GroupByColumnName);

            //Loop through creating the datacolumns for our new DataTable.
            int j = 0;
            foreach (DataRow dr in GroupingAndFunction.Rows)
            {
                string s = "";
                //if (UserReport == 0 || UserReport == -1)
                s = GroupingAndFunction.Rows[j][1].ToString().ToLower();
                //else
                //    s = GroupingAndFunction.Rows[j]["AggregateKey"].ToString().ToLower();

                string[] GroupFunctionAndColumnName = s.Split(',');
                GroupFunctionAndColumnName[0] = GroupFunctionAndColumnName[0].TrimEnd(' ');

                if (GroupFunctionAndColumnName[0] == "count")
                {
                    DataColumn dc = new DataColumn("Number of " + GroupFunctionAndColumnName[1] + " Per " + GroupByColumnName, Type.GetType("System.Int32"));
                    dtFinalDataTable.Columns.Add(dc);
                }
                else if (GroupFunctionAndColumnName[0] == "avg")
                {
                    DataColumn dc = new DataColumn(GroupFunctionAndColumnName[1] + "Average ", Type.GetType("System.Decimal"));
                    dtFinalDataTable.Columns.Add(dc);
                }
                else if (GroupFunctionAndColumnName[0] == "sum")
                {
                    DataColumn dc = new DataColumn(GroupFunctionAndColumnName[1] + " Total", Type.GetType("System.Decimal"));
                    dtFinalDataTable.Columns.Add(dc);
                }
                else if (GroupFunctionAndColumnName[0] == "min")
                {
                    DataColumn dc = new DataColumn("Min Value For " + GroupFunctionAndColumnName[1]);
                    dtFinalDataTable.Columns.Add(dc);
                }
                else if (GroupFunctionAndColumnName[0] == "max")
                {
                    DataColumn dc = new DataColumn("Max Value For " + GroupFunctionAndColumnName[1]);
                    dtFinalDataTable.Columns.Add(dc);
                }
                j += 1;
            }

            string GroupColumnData = "";
            int CountNumberOfCols = 0;

            j = 0;
            foreach (DataRow dr in GroupingAndFunction.Rows)
            {
                string s = "";
                //if (UserReport == 0 || UserReport == -1)
                s = GroupingAndFunction.Rows[j][1].ToString().ToLower();
                //else
                //    s = GroupingAndFunction.Rows[j]["AggregateKey"].ToString().ToLower();

                string[] GroupFunctionAndColumnName = s.Split(',');
                GroupFunctionAndColumnName[0] = GroupFunctionAndColumnName[0].TrimEnd(' ');

                if (GroupFunctionAndColumnName[0] == "count")
                    blnCalculateCount = true;
                else if (GroupFunctionAndColumnName[0] == "sum")
                    blnCalculateSum = true;
                else if (GroupFunctionAndColumnName[0] == "avg")
                {
                    blnCalculateAverage = true;
                    blnCalculateCount = true;
                    blnCalculateSum = true;
                }
                else if (GroupFunctionAndColumnName[0] == "max")
                    blnCalculateMax = true;
                else if (GroupFunctionAndColumnName[0] == "min")
                    blnCalculateMin = true;

                int Count = 0;
                double dSum = 0;
                double dAvg = 0;
                double dMinValue = 0;
                double dMaxValue = 0;
                DateTime dateMinValue = DateTime.MaxValue;
                DateTime dateMaxValue = DateTime.MinValue;

                int NewRowNumber = 0;
                for (int i = 0; i < CurrentView.Rows.Count; i++)
                {
                    DataRow newDataRow = dtFinalDataTable.NewRow();

                    if (CurrentView.Rows[i][GroupByColumnName].ToString() == GroupColumnData)
                    {
                        if (blnCalculateCount == true)
                            Count += 1;
                        if (blnCalculateSum == true)
                        {
                            dSum += Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                        }
                        if (blnCalculateAverage == true)
                        {//Dont need to do anything since we are calculating the sum and count above.
                        }
                        if (blnCalculateMax == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMinValue < Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMaxValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            }
                            else
                            {
                                if (dMaxValue < Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMaxValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            }
                        }
                        if (blnCalculateMin == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMinValue > Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMinValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            }
                            else
                            {
                                if (dMinValue > Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMinValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            }
                        }
                    }
                    else if (i == 0)
                    {
                        GroupColumnData = CurrentView.Rows[i][GroupByColumnName].ToString();

                        if (blnCalculateSum == true)
                            dSum += Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                        if (blnCalculateCount == true)
                            Count += 1;

                        if (blnCalculateMin == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                                dateMinValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            else
                                dMinValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                        }

                        if (blnCalculateMax == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                                dateMaxValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                            else
                                dMaxValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                        }
                    }
                    else if (i == CurrentView.Rows.Count - 1)
                    {
                        if (blnCalculateCount == true && blnCalculateAverage == false)
                        {
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                newDataRow[1] = Count;
                            }
                            else
                                dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = Count;
                        }
                        if (blnCalculateSum == true && blnCalculateAverage == false)
                        {
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                newDataRow[1] = dSum;
                            }
                            else
                                dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dSum;
                        }
                        if (blnCalculateAverage == true)
                        {
                            dAvg = dSum / Count;
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                newDataRow[1] = dAvg;
                            }
                            else
                                dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dAvg;
                        }
                        if (blnCalculateMin == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMinValue > Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMinValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dateMinValue;
                                }
                                else
                                    dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dateMinValue;
                            }
                            else
                            {
                                if (dMinValue > Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMinValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dMinValue;
                                }
                                else
                                    dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dMinValue;
                            }
                        }
                        if (blnCalculateMax == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMaxValue < Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMaxValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dateMaxValue;
                                }
                                else
                                    dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dateMaxValue;
                            }
                            else
                            {
                                if (dMaxValue < Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMaxValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dMaxValue;
                                }
                                else
                                    dtFinalDataTable.Rows[NewRowNumber - 1][CountNumberOfCols + 1] = dMaxValue;
                            }
                        }
                    }
                    //Start a new Row
                    else
                    {
                        GroupColumnData = CurrentView.Rows[i][GroupByColumnName].ToString();
                        if (blnCalculateCount == true && blnCalculateAverage == false)
                        {
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i - 1][GroupByColumnName].ToString();
                                newDataRow[1] = Count;
                            }
                            else
                                dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = Count;
                        }
                        if (blnCalculateSum == true && blnCalculateAverage == false)
                        {
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i - 1][GroupByColumnName].ToString();
                                newDataRow[1] = dSum;
                            }
                            else
                            {
                                dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dSum;
                                dSum = Convert.ToDouble(CurrentView.Rows[i][GroupByColumnName].ToString());
                            }
                        }
                        if (blnCalculateAverage == true)
                        {
                            dAvg = dSum / Count;
                            if (CountNumberOfCols == 0)
                            {
                                newDataRow[0] = CurrentView.Rows[i - 1][GroupByColumnName].ToString();
                                newDataRow[1] = dAvg;
                            }
                            else
                                dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dAvg;
                        }
                        if (blnCalculateMin == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMinValue > Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMinValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dateMinValue;
                                }
                                else
                                {
                                    dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dateMinValue;
                                    dateMinValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                                }
                            }
                            else
                            {
                                if (dMinValue > Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMinValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dMinValue;
                                }
                                else
                                {
                                    dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dMinValue;
                                    dMinValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                                }
                            }
                        }
                        if (blnCalculateMax == true)
                        {
                            if (CurrentView.Columns[GroupFunctionAndColumnName[1]].DataType == Type.GetType("System.DateTime"))
                            {
                                if (dateMaxValue < Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dateMaxValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dateMaxValue;
                                }
                                else
                                {
                                    dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dateMaxValue;
                                    dateMaxValue = Convert.ToDateTime(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                                }
                            }
                            else
                            {
                                if (dMaxValue < Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString()))
                                    dMaxValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());

                                if (CountNumberOfCols == 0)
                                {
                                    newDataRow[0] = CurrentView.Rows[i][GroupByColumnName].ToString();
                                    newDataRow[1] = dMaxValue;
                                }
                                else
                                {
                                    dtFinalDataTable.Rows[NewRowNumber][CountNumberOfCols + 1] = dMaxValue;
                                    dMaxValue = Convert.ToDouble(CurrentView.Rows[i][GroupFunctionAndColumnName[1]].ToString());
                                }
                            }
                        }

                        Count = 1;
                        dAvg = 0;

                        //Insert created row

                        if (CountNumberOfCols == 0)
                            dtFinalDataTable.Rows.Add(newDataRow);
                        else
                            NewRowNumber += 1;
                    }
                }
                CountNumberOfCols += 1;
                blnCalculateAverage = false;
                blnCalculateCount = false;
                blnCalculateMax = false;
                blnCalculateSum = false;
                blnCalculateMin = false;
                j += 1;
            }
            return dtFinalDataTable;
        }
        public static DataTable GetTotals(DataTable CurrentView, int ReportId)
        {
            DataTable dtAllowedTotals = ReportData.GetReportDetailsByReportId(ReportId);

            DataRow drTotals = CurrentView.NewRow();
            foreach (DataColumn dc in CurrentView.Columns)
            {
                foreach (DataRow drFindTotals in dtAllowedTotals.Rows)
                {
                    if (drFindTotals["Header"].ToString() == dc.ColumnName)
                    {
                        if (ConvertTools.ToBoolean(drFindTotals["EnableTotal"], false) == true)
                        {
                            if (dc.DataType.Name == "Int32")
                            {
                                int intTotals = 0;
                                foreach (DataRow dr in CurrentView.Rows)
                                {
                                    intTotals += ConvertTools.ToInt32(dr[dc]);
                                }
                                drTotals[dc] = intTotals;
                            }
                            else if (dc.DataType.Name == "Decimal")
                            {
                                decimal dTotals = 0;
                                foreach (DataRow dr in CurrentView.Rows)
                                {
                                    dTotals += ConvertTools.ToDecimal(dr[dc]);
                                }
                                drTotals[dc] = dTotals;
                            }
                            else if (dc.DataType.Name == "Int16")
                            {
                                int intTotals = 0;
                                foreach (DataRow dr in CurrentView.Rows)
                                {
                                    intTotals = ConvertTools.ToInt32(dr[dc]);
                                }
                                drTotals[dc] = intTotals;
                            }
                        }
                    }
                }
            }
            CurrentView.Rows.Add(drTotals);
            return CurrentView;
        }
    }
}