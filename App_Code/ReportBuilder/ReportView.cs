﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for ReportView
/// </summary>


namespace Reports
{
    /// <summary>
    /// This class will hold all the information needed to view a report
    /// </summary>
    public class ReportView
    {
        private int reportId = 0;
        private int userReportId = 0;
        private int accountNumber = 0;
        private bool isCorpLink;
        private List<SqlParameter> parameterList;
        private DataTable columns;
        private string agingDays = "";
        private string sorting;
        private bool showSummary;
        private bool showTotals;
        private int paging = 0;
        private Guid userId;
        private string filtersApplied;

        public int ReportId
        {
            set { reportId = value; }
            get { return reportId; }
        }
        public int UserReportId
        {
            set { userReportId = value; }
            get { return userReportId; }
        }
        public int AccountNumber
        {
            set { accountNumber = value; }
            get { return accountNumber; }
        }
        public bool IsCorpLink
        {
            set { isCorpLink = value; }
            get { return isCorpLink; }
        }
        public List<SqlParameter> ParameterList
        {
            set { parameterList = value; }
            get { return parameterList; }
        }
        public DataTable Columns
        {
            set { columns = value; }
            get { return columns; }
        }
        public string AgingDays
        {
            set { agingDays = value; }
            get { return agingDays; }
        }
        public string Sorting
        {
            set { sorting = value; }
            get { return sorting; }
        }
        public bool ShowSummary
        {
            set { showSummary = value; }
            get { return showSummary; }
        }
        public bool ShowTotals
        {
            set { showTotals = value; }
            get { return showTotals; }
        }
        public int Paging
        {
            set { paging = value; }
            get { return paging; }
        }
        public Guid UserId
        {
            set { userId = value; }
            get { return userId; }
        }
        public string FiltersApplied
        {
            set { filtersApplied = value; }
            get { return filtersApplied; }
        }
    }
}