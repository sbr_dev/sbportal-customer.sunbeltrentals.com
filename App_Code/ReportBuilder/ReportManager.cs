using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;
using SunbeltExtensions;

namespace Reports
{
	/// <summary>
	/// Summary description for ReportManager
	/// </summary>
	public class ReportManager
	{
        public int ReportID
        {
            set { WebTools.SetSessionCache("ReportID", value, new TimeSpan(0, 5, 0)); }
            get { return (int)WebTools.GetSessionCache("ReportID", 0); }
        }

		public List<ReportFilter> FilterList
		{
			set { WebTools.SetSessionCache("FilterList", value, new TimeSpan(0, 5, 0)); }
			get { return (List<ReportFilter>)WebTools.GetSessionCache("FilterList", null); }
		}
        public List<ReportColumns> ColumnList
        {
			set { WebTools.SetSessionCache("ColumnList", value, new TimeSpan(0, 5, 0)); }
			get { return (List<ReportColumns>)WebTools.GetSessionCache("ColumnList", null); }
        }

        public int AccountNumber
        {
            set { WebTools.SetSessionCache("ReportManagerAccountNumber", value, new TimeSpan(0, 5, 0)); }
            get { return (int)WebTools.GetSessionCache("ReportManagerAccountNumber", 0); }
        }

		//public List<FilterControlBase> ControlList = null;

		private Page MyPage;
		private ReportData ReportData;

		public ReportManager(Page myPage, ReportData reportData)
		{
			MyPage = myPage;
			ReportData = reportData;
		}

		public void Clear()
		{
			ReportID = 0;
			FilterList = new List<ReportFilter>();
		}

		public void InitReport(int reportID, int accountNumber, bool IsCorpLink)
		{
			if (reportID > 0)
			{
				bool isNewReport = (reportID != ReportID || accountNumber != AccountNumber);

				if (isNewReport || FilterList == null)
				{
					FilterList = ReportReader.LoadFilters(reportID, accountNumber, IsCorpLink);
                    ColumnList = ReportReader.LoadColumns(reportID);
				}
				//ControlList = ReportReader.BuidFilterControls(MyPage, FilterList);
			}
			ReportID = reportID;
            AccountNumber = accountNumber;
		}

		//public void LoadReportData()
		//{
		//    if (ControlList != null)
		//    {
		//        foreach (FilterControlBase fcb in ControlList)
		//        {
		//            fcb.LoadData(ReportData);
		//        }
		//    }
		//}

        public ReportFilter GetFilterListByReportColumnID(int ReportColumnID)
        {
            foreach (ReportFilter rf in FilterList)
            {
                if (rf.ReportColumnID == ReportColumnID)
                    return rf;
            }
            return null;
        }

        public ReportColumns GetColumnListByReportColumnID(int ReportColumnID)
        {
            foreach (ReportColumns rc in ColumnList)
            {
                if (rc.ReportColumnID == ReportColumnID)
                    return rc;
            }
            return null;
        }
	}
}