﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sunbelt.Security;
using Sunbelt.Tools;
using SBNet.Extensions;

namespace Reports
{
/// <summary>
/// Globals for Reporting.
/// </summary>
public class ReportingGlobals
{
    public static string BuildReportViewSessionCacheAndWCAN(ReportView rv)
    {
        //Create a unique time stamp.
        string timeStamp = DateTimeTools.TimeStamp();

        //Set the wcan secure string passed in the url
        SecureQueryString sqs = new SecureQueryString();
        sqs.Add("ts", timeStamp);
        string wcan = sqs.ToString();

        //Save the ReportView object in session cache. NOTE: it must be named
        //'"ReportView" + timeStamp'. It will be used from the ReportView page. 
        WebTools.SetSessionCache("ReportView" + timeStamp, rv, new TimeSpan(0, 20, 0));
		if(HttpContext.Current.Request.Browser.Browser == "Firefox")
		{
			string message = string.Format("TimeStamp {0}<br>, wcan {1}<br>, Production: {2} <br><br>{3}", timeStamp, wcan, ConfigurationManager.AppSettings["AS400Development"].ToString() == "false", rv.DumpToHtml(true));
 
			
			
			Sunbelt.Log.LogActivity("Reports", message);

			string to, cc, bcc, from;
			to = "michael.evanko@sunbeltrentals.com";
			cc = "";
			bcc = "";
			from = ConfigurationManager.AppSettings["SunbeltEmail"].ToString();

			Sunbelt.Email.SendEmail(to, from, cc, bcc, "Firefox view report View, Production: " + (ConfigurationManager.AppSettings["AS400Development"].ToString() == "false").ToString() + " User: " +Membership.GetUser().UserName, message, true, null, Sunbelt.Email.CleanupType.None);
		}



        return wcan;

    }
    
    /// <summary>
    /// Returns the report id for equipment on rent.
    /// </summary>
    public static int EquipmentOnRentId
    {
        get { return 6; }
    }

    /// <summary>
    /// Returns the default columns for equipment on rent.
    /// </summary>
    public static DataTable EquipmentOnRentDefaultColumns
    {
        get
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Columns");
            dt.Columns.Add("ColumnId");
            dt.Columns.Add("HyperlinkType");

            DataTable dtColumns = Reports.ReportData.GetEnabledColumns(EquipmentOnRentId);
            
            foreach (DataRow dr in dtColumns.Rows)
            {
                if (ConvertTools.ToBoolean(dr["DefaultColumn"], false) == true)
                {
                    if (Convert.ToBoolean(dr["DefaultColumn"].ToString()) && !string.IsNullOrEmpty(Convert.ToString(dr["HyperlinkType"])))
                        dt.Rows.Add(dr["Header"], dr["ReportColumnId"], dr["HyperlinkType"]);
                    else
                        dt.Rows.Add(dr["Header"], dr["ReportColumnId"], null);
                }
            }
            return dt;
        }
    }

    /// <summary>
    /// Returns the default columns for equipment on rent.
    /// </summary>
    public static List<SqlParameter> GetDefaultFilter(int ReportId)
    {
        DataTable dt = ReportData.GetReportColumnFilters(ReportId, true);
        List<SqlParameter> listParam = new List<SqlParameter>();
        if (dt.Rows.Count > 0)
        {
            bool FilterEqual = ConvertTools.ToBoolean(dt.Rows[0]["FilterEqual"], false);
            bool FilterList = ConvertTools.ToBoolean(dt.Rows[0]["FilterList"], false);
            bool FilterLike = ConvertTools.ToBoolean(dt.Rows[0]["FilterLike"], false);
            bool FilterRange = ConvertTools.ToBoolean(dt.Rows[0]["FilterRange"], false);
            string DataType = ConvertTools.ToString(dt.Rows[0]["DataType"], "").ToLower();
            
            if (DataType == "int" || DataType == "numeric" || DataType == "money" || DataType == "varchar" || DataType == "char")
            {
                if (FilterEqual == true && FilterList == false && FilterLike == false)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterEqualParameter"]), dt.Rows[0]["FilterDefaultValue"].ToString()));
                else if (FilterEqual == true && FilterList == true && FilterLike == false)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterListParameter"]), dt.Rows[0]["FilterDefaultValue"].ToString()));
                else if (FilterRange == true)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterRangeParameter"]), dt.Rows[0]["FilterDefaultValue"].ToString()));
            }
            else if (DataType == "datetime")
            {
                if (FilterEqual == true && FilterRange == false)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterRangeParameter"]), ConvertTools.ToDateTime(dt.Rows[0]["FilterDefaultValue"])));
                else if (FilterList == true)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterListParameter"]), ConvertTools.ToDateTime(dt.Rows[0]["FilterDefaultValue"])));
                else if (FilterLike == true)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterLikeParameter"]), ConvertTools.ToDateTime(dt.Rows[0]["FilterDefaultValue"])));
                else if (FilterRange == true)
                    listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterEqualParameter"]), ConvertTools.ToDateTime(dt.Rows[0]["FilterDefaultValue"])));
            }
            else if (DataType == "bit" && FilterEqual == true)
            {
                listParam.Add(new SqlParameter(ConvertTools.ToString(dt.Rows[0]["FilterEqualParameter"]), ConvertTools.ToBoolean(dt.Rows[0]["FilterDefaultValue"])));
            }
        }
        return listParam;
    }

    /// <summary>
    /// Returns the report id for invoice history.
    /// </summary>
    public static int InvoiceHistoryId
    {
        get { return 8; }
    }

    /// <summary>
    /// Returns the default columns for Invoice History.
    /// </summary>
    public static DataTable InvoiceHistoryDefaultColumns
    {
        get
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Columns");
            dt.Columns.Add("ColumnId");
            dt.Columns.Add("HyperlinkType");

            DataTable dtColumns = Reports.ReportData.GetEnabledColumns(InvoiceHistoryId);

            foreach (DataRow dr in dtColumns.Rows)
            {
                if (ConvertTools.ToBoolean(dr["DefaultColumn"], false) == true)
                {
                    if (Convert.ToBoolean(dr["DefaultColumn"].ToString()) && !string.IsNullOrEmpty(Convert.ToString(dr["HyperlinkType"])))
                        dt.Rows.Add(dr["Header"], dr["ReportColumnId"], dr["HyperlinkType"]);
                    else
                        dt.Rows.Add(dr["Header"], dr["ReportColumnId"], null);
                }
            }
            return dt;
        }
    }

    /// <summary>
    /// Returns the report id for equipment on rent.
    /// </summary>
    public static int InvoiceAgingId
    {
        get { return 15; }
    }

    /// <summary>
    /// Returns the default columns for Invoice Aging.
    /// </summary>
    public static DataTable InvoiceAgingDefaultColumns
    {
        get
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Columns");
            dt.Columns.Add("ColumnId");
            dt.Columns.Add("HyperlinkType");

            DataTable dtColumns = Reports.ReportData.GetEnabledColumns(InvoiceAgingId);

            foreach (DataRow dr in dtColumns.Rows)
            {
                if (Convert.ToBoolean(dr["DefaultColumn"].ToString()) && !string.IsNullOrEmpty(Convert.ToString(dr["HyperlinkType"])))
                    dt.Rows.Add(dr["Header"], dr["ReportColumnId"], dr["HyperlinkType"]);
                else
                    dt.Rows.Add(dr["Header"], dr["ReportColumnId"], null);
            }
            return dt;
        }
    }
}
}
