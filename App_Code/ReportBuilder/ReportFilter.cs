using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;
using System.Runtime.Serialization;
using SunbeltExtensions;

namespace Reports
{
	/// <summary>
	/// Summary description for ReportFilter
	/// </summary>
	[Serializable()]
	public class ReportFilter
	{
        public int ReportColumnID;
		public string ColumnName;
        public string ColumnHeader;
		public string DataType;
		public bool EnableSelect;
		public bool EnableGrouping;
		public bool FilterEqual;
		public string FilterEqualParameter;
		public bool FilterList;
		public string FilterListParameter;
		public string FilterListProcedure;
		public string FilterListProcedure_KeyField;
		public string FilterListProcedure_DataField;
		public bool FilterListProcedure_CustomerNumberRequired;
		public bool FilterLike;
		public string FilterLikeParameter;
		public bool FilterRange;
		public string FilterRangeParameter;
        public string Control;
        public int AccountNumber;
        public bool IsCorpLink;
        public string FilterDefaultValue;
        public bool DefaultColumn;
        //public string FilterHelp;
		public bool FilterGreaterThan;
		public string FilterGreaterThanParameter;

        public ReportFilter(DataRow dr, int accountNumber, bool isCorpLink)
		{
			InitControl(dr);
            AccountNumber = accountNumber;
            IsCorpLink = isCorpLink;
		}

        public static class ControlType
        {
            public static string fDropDown = "DropDown";
            public static string fBoolean = "Boolean";
            public static string fDateTime = "DateTime";
            public static string fDateTimeRange = "DateTimeRange";
            public static string fIntRange = "IntRange";
            public static string fListBox = "ListBox";
            public static string fString = "String";
            public static string fStringEqual = "StringEqual";
            public static string fInt = "Int";
			public static string fMoney = "Money";
		}

		public void InitControl(DataRow dr)
		{
            ReportColumnID = ConvertTools.ToInt32(dr["ReportColumnID"].ToString());
            ColumnHeader = ConvertTools.ToString(dr["Header"]);
            ColumnName = ConvertTools.ToString(dr["ColumnName"]);
			DataType = ConvertTools.ToString(dr["DataType"]);
			EnableSelect = ConvertTools.ToBoolean(dr["EnableSelect"]);
			EnableGrouping = ConvertTools.ToBoolean(dr["EnableGrouping"]);
			FilterEqual = ConvertTools.ToBoolean(dr["FilterEqual"]);
			FilterEqualParameter = ConvertTools.ToString(dr["FilterEqualParameter"]);
			FilterList = ConvertTools.ToBoolean(dr["FilterList"]);
			FilterListParameter = ConvertTools.ToString(dr["FilterListParameter"]);
			FilterListProcedure = ConvertTools.ToString(dr["FilterListProcedure"]);
			FilterListProcedure_KeyField = ConvertTools.ToString(dr["FilterListProcedure_KeyField"]);
			FilterListProcedure_DataField = ConvertTools.ToString(dr["FilterListProcedure_DataField"]);
			FilterListProcedure_CustomerNumberRequired = ConvertTools.ToBoolean(dr["FilterListProcedure_CustomerNumberRequired"]);
			FilterLike = ConvertTools.ToBoolean(dr["FilterLike"]);
			FilterLikeParameter = ConvertTools.ToString(dr["FilterLikeParameter"]);
			FilterRange = ConvertTools.ToBoolean(dr["FilterRange"]);
			FilterRangeParameter = ConvertTools.ToString(dr["FilterRangeParameter"]);
            FilterDefaultValue = ConvertTools.ToString(dr["FilterDefaultValue"]);
            DefaultColumn = ConvertTools.ToBoolean(dr["DefaultColumn"]);
			FilterGreaterThan = ConvertTools.ToBoolean(dr["FilterGreaterThan"]);
			FilterGreaterThanParameter = ConvertTools.ToString(dr["FilterGreaterThanParameter"]);

            FindControlType();
		}

        public void FindControlType()
        {
            if (DataType == "int" && FilterEqual == true && FilterList == false && FilterLike == false)
            {
                //Show DropDown since there is no list
                if (FilterListProcedure != "")
                    Control = ControlType.fDropDown;
                else
                    Control = ControlType.fInt;
            }
            else if (DataType == "int" && FilterEqual == true && FilterList == true && FilterLike == false)
            {
                //Show Listbox because you can select multiple values
                Control = ControlType.fListBox;
            }
            else if (DataType == "int" && FilterRange == true)
            {
                Control = ControlType.fIntRange;
            }
            if (DataType == "numeric" && FilterEqual == true && FilterList == false && FilterLike == false)
            {
                //Show DropDown since there is no list
                if (FilterListProcedure != "")
                    Control = ControlType.fDropDown;
                else
                    Control = ControlType.fStringEqual;
            }
            else if (DataType == "numeric" && FilterEqual == true && FilterList == true && FilterLike == false)
            {
                //Show Listbox because you can select multiple values
                Control = ControlType.fListBox;
            }
            else if (DataType == "numeric" && FilterRange == true)
            {
                Control = ControlType.fIntRange;
            }
            else if (DataType == "money" && FilterRange == true)
            {
                Control = ControlType.fIntRange;
            }
            else if (DataType == "varchar" && FilterList == false && FilterEqual == false && FilterLike == true)
            {
                //Show Textbox since it is a like
                Control = ControlType.fString;
            }
            else if (DataType == "varchar" && FilterEqual == true && FilterList == false && FilterLike == false)
            {
                //Show DropDown since there is no list
                if (FilterListProcedure != "")
                    Control = ControlType.fDropDown;
                else
                    Control = ControlType.fStringEqual;
            }
            else if (DataType == "varchar" && FilterEqual == true && FilterList == true && FilterLike == false)
            {
                //Show Listbox because you can select multiple values
                if (FilterListProcedure != "")
                    Control = ControlType.fListBox;
                else
                    Control = ControlType.fStringEqual;
            }
            else if (DataType == "char" && FilterList == false && FilterEqual == false && FilterLike == true)
            {
                //Show Textbox since it is a like
                Control = ControlType.fString;
            }
            else if (DataType == "char" && FilterEqual == true && FilterList == false && FilterLike == false)
            {
                //Show DropDown since there is no list
                if (FilterListProcedure != "")
                    Control = ControlType.fDropDown;
                else
                    Control = ControlType.fStringEqual;
            }
            else if (DataType == "char" && FilterEqual == true && FilterList == true && FilterLike == false)
            {
                //Show Listbox because you can select multiple values
                Control = ControlType.fListBox;
            }
            else if (DataType == "datetime" && FilterRange == true)
            {
                Control = ControlType.fDateTimeRange;
            }
            else if (DataType == "datetime" && FilterRange == false && FilterEqual == true)
            {
                Control = ControlType.fDateTime;
            }
            else if (DataType == "datetime" && FilterRange == false && FilterLike == true)
            {
                Control = ControlType.fDateTime;
            }
            else if (DataType == "bit" && FilterEqual == true)
            {
                Control = ControlType.fBoolean;
            }
			else if (DataType == "money" && FilterGreaterThan == true)
			{
				Control = ControlType.fMoney;
			}

        }
	}
}
