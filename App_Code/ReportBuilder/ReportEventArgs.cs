using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Reports
{
	/// <summary>
	/// Summary description for ReportEventArgs
	/// </summary>
	public class ReportEventArgs : EventArgs
	{
		public int ReportId = 0;
        public int UserReportID = 0;
		public ReportEventArgs(int reportId, int userReportID)
		{
			ReportId = reportId;
            UserReportID = userReportID;
		}
	}

    public class ReportMenuArgs : EventArgs
    {
        public string MenuCommand = "";
        public ReportMenuArgs(string menuCommand)
        {
            MenuCommand = menuCommand;
        }
    }
}