using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SunbeltExtensions;

namespace Reports
{
	/// <summary>
	/// Summary description for ReportReader
	/// </summary>
	public class ReportReader
	{
        
		public ReportReader()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static List<ReportFilter> LoadFilters(int reportID, int AccountNumber, bool IsCorpLink)
		{
            DataTable dt = null;

            List<ReportFilter> list = new List<ReportFilter>();
            dt = Reports.ReportData.GetReportDetailsByReportId(reportID);
			foreach (DataRow dr in dt.Rows)
			{
				ReportFilter rf = new ReportFilter(dr, AccountNumber, IsCorpLink);
				list.Add(rf);
			}

			return list;
		}

        public static List<ReportColumns> LoadColumns(int reportID)
        {
            DataTable dt = Reports.ReportData.GetEnabledColumns(reportID);
            List<ReportColumns> list = new List<ReportColumns>();

            foreach (DataRow dr in dt.Rows)
            {
                ReportColumns rc = new ReportColumns(dr);
                list.Add(rc);
            }

            return list;
        }

		public static List<FilterControlBase> BuidFilterControls(TemplateControl parent, List<ReportFilter> list)
		{
			List<FilterControlBase> controlList = new List<FilterControlBase>();
			FilterControlBase c;
			int count = 1;
            string strFiltersUrl = ConfigurationManager.AppSettings["Reporting_FiltersUrl"].ToString();
			foreach (ReportFilter rf in list)
			{
				c = null;
                if (rf.Control == "DropDown")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "/FilterDropDown.ascx");
                else if (rf.Control == "StringEqual")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "/FilterStringEqual.ascx");
                else if (rf.Control == "ListBox")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterListBox.ascx");
                else if (rf.Control == "IntRange")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterIntRange.ascx");
                else if (rf.Control == "String")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterString.ascx");
                else if (rf.Control == "DateTimeRange")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterDateTimeRange.ascx");
                else if (rf.Control == "DateTime")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterDateTime.ascx");
                else if (rf.Control == "Boolean")
                    c = (FilterControlBase)parent.LoadControl(strFiltersUrl + "FilterBoolean.ascx");

                if (c != null)
                {
                    //c.ID = "ReportReader" + count.ToString();
                    //Initialize and add to the control list.
                    ((FilterControlBase)c).Initialize(rf);
                    controlList.Add(c);
                    count++;
                }
			}
			return controlList;
		}
	}
}