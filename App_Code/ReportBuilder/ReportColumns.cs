using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;
using System.Runtime.Serialization;

/// <summary>
/// Summary description for ReportColumns
/// </summary>
namespace Reports
{
    [Serializable()]
    public class ReportColumns
    {
        public int ReportColumnID;
        public string ColumnName;
        public string ColumnDisplayName;
        public bool EnabledGrouping;
        public bool DefaultColumns;
        public string HyperlinkType;


        public ReportColumns(DataRow dr)
        {
            InitControl(dr);
        }

        public void InitControl(DataRow dr)
        {
            ReportColumnID = ConvertTools.ToInt32(dr["ReportColumnID"]);
            ColumnName = ConvertTools.ToString(dr["ColumnName"]);
            ColumnDisplayName = ConvertTools.ToString(dr["Header"]);
            EnabledGrouping = ConvertTools.ToBoolean(dr["EnableGrouping"]);
            DefaultColumns = ConvertTools.ToBoolean(dr["DefaultColumn"]);
            HyperlinkType = string.IsNullOrEmpty(Convert.ToString(dr["HyperlinkType"])) ? null : ConvertTools.ToString(dr["HyperlinkType"], null);
        }
    }
}
