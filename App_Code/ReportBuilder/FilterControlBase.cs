using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

namespace Reports
{
	/// <summary>
	/// Summary description for FilterControlBase
	/// </summary>
	//public abstract class FilterControlBase : System.Web.UI.UserControl
	public abstract class FilterControlBase : Sunbelt.Web.UserControl
	{
		//public ReportFilter reportFilter = null;
		public ReportFilter reportFilter
		{
			get { return (ReportFilter)ViewState["reportFilter"]; }
			set { ViewState["reportFilter"] = value; }
		}

		public ReportData reportData = null;

		public virtual void Initialize(ReportFilter rf)
		{
			reportFilter = rf;
		}

		public virtual void LoadData(ReportData rd)
		{
			reportData = rd;

			if (reportFilter == null)
				throw new InvalidOperationException("FilterControlBase.Initialize(...) must be called first.");
		}

		public abstract List<SqlParameter> GetSqlParameters();
        public abstract List<int> GetReportColumnID();
	}
}