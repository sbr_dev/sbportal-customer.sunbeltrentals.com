﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for PDFViewerReply
/// </summary>
[WebService(Namespace = "http://customer.sunbeltrentals.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class PDFViewerReply : System.Web.Services.WebService {

    public PDFViewerReply () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

	public const string ONSTART = "ON_START";
	public const string ONEND = "ON_END";
	public const string ONERROR = "ON_ERROR";
	public const string WAITING = "WAITING";
	public const string UNDEFINED = "undefined";

	[WebMethod]
	public void OnPdfStart(string docNumber)
	{
		Sunbelt.Tools.WebTools.SetCache(Context, docNumber, ONSTART, DateTime.Now.AddMinutes(5));
	}

	[WebMethod]
	public void OnPdfEnd(string docNumber)
	{
		Sunbelt.Tools.WebTools.SetCache(Context, docNumber, ONEND, DateTime.Now.AddMinutes(5));
	}

	[WebMethod]
	public void OnPdfError(string docNumber, string errorMsg)
	{
		Sunbelt.Tools.WebTools.SetCache(Context, docNumber, ONERROR + errorMsg, DateTime.Now.AddMinutes(5));
	}

	public void InitPdf(HttpContext context, string docNumber)
	{
		Sunbelt.Tools.WebTools.SetCache(context, docNumber, WAITING, DateTime.Now.AddMinutes(5));
	}

	[WebMethod]
	public string GetPdfStatus(string docNumber)
	{
		return Sunbelt.Tools.WebTools.GetCache(Context, docNumber, UNDEFINED) as String;
	}

	public bool IsPdfEnd(HttpContext context, string docNumber)
	{
		string value = Sunbelt.Tools.WebTools.GetCache(context, docNumber, "") as String;
		return (value == ONEND);
	}

	public string IsPdfError(HttpContext context, string docNumber)
	{
		string value = Sunbelt.Tools.WebTools.GetCache(context, docNumber, "") as String;
		if (value.StartsWith(ONERROR))
			return value.Substring(ONERROR.Length);
		return null;
	}

	public void FinishPdf(HttpContext context, string docNumber)
	{
		Sunbelt.Tools.WebTools.RemoveCache(context, docNumber);
	}

	[WebMethod]
	public string HelloWorld()
	{
		return "Hello World from PdfViewerReply";
	}
}
