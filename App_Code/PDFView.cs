﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Common.DAL;
using System.Data.SqlClient;

/// <summary>
/// Summary description for PDFView
/// </summary>
public class PDFView
{
	private int invoiceContractNumber = 0;
    private int sequence = 0;
    private int accountNumber = 0;
    private bool isCorpLink;
    private bool _isQuote = false;

    public int InvoiceContractNumber
    {
        set { invoiceContractNumber = value; }
        get { return invoiceContractNumber; }
    }
    public int Sequence
    {
        set { sequence = value; }
        get { return sequence; }
    }
    public int AccountNumber
    {
        set { accountNumber = value; }
        get { return accountNumber; }
    }
    public bool IsCorpLink
    {
        set { isCorpLink = value; }
        get { return isCorpLink; }
    }
    public bool IsQuote
    {
        set { _isQuote = value; }
        get { return _isQuote; }
    }

    public static bool DoesUserHaveAccessToInvoiceOrContract(int AccountNumber, bool IsCorpLink, int InvoiceNumberContract, bool IsQuote)
    {
		if (SBNet.SiteUser.IsCashCustomer)
			return true;
		
		DataTable dt = LookupCustomerInvoices(AccountNumber, IsCorpLink, InvoiceNumberContract.ToString(), IsQuote);
        if (dt.Rows.Count > 0)
            return true;
        else
            return false;
    }

    public static DataTable LookupCustomerInvoices(int AccountNumber, bool IsCorpLink, string text, bool IsQuote )
    {
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
		using (SPData sp = new SPData("BusinessObjects.sb_LookupCustomerInvoices", conn))
        {
            sp.AddParameterWithValue("@AccountNumber", SqlDbType.Int, 6, ParameterDirection.Input, AccountNumber);
            sp.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, IsCorpLink);
            sp.AddParameterWithValue("@InvoiceNumberPattern", SqlDbType.VarChar, 30, ParameterDirection.Input, text);
            sp.AddParameterWithValue("@ShowQuotes", SqlDbType.Bit, 1, ParameterDirection.Input, IsQuote);
            return sp.ExecuteDataTable();
        }
    }
}
