﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Sunbelt.Common.DAL;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for DataManager
/// </summary>
namespace Sunbelt.EmailCCManager
{
    public class DataManager
    {
        public DataManager() : this(ConfigurationManager.ConnectionStrings["BaseData"].ConnectionString) { }
        public DataManager(string connectionString)
        {
            this.connectionString = connectionString;
        }
        private readonly string connectionString;
        public ExtraCCs GetExtraCCsForPc(int pc, int? companyID)
        {
            ExtraCCs exccs = new ExtraCCs();
            exccs.ProfitCenter = pc;
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "config.sb_GetEmailsExtraCCs";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("PC", pc);
                    cmd.Parameters.AddWithValue("CompanyID", companyID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var reservation = reader["ReservationExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(reservation))
                                exccs.ReservationExtraCCs = reservation.Split(';').ToList();
                            var pickup = reader["PickupExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(pickup))
                                exccs.PickupExtraCCs = pickup.Split(';').ToList();
                            var service = reader["ServiceExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(service))
                                exccs.ServiceExtraCCs = service.Split(';').ToList();

                            var reservationTo = reader["ReservationTOs"] as string;
                            if (!string.IsNullOrEmpty(reservationTo))
                                exccs.ReservationTOs = reservationTo.Split(';').ToList();
                            var pickupTo = reader["PickupTOs"] as string;
                            if (!string.IsNullOrEmpty(pickupTo))
                                exccs.PickupTOs = pickupTo.Split(';').ToList();
                            var serviceTOs = reader["ServiceTOs"] as string;
                            if (!string.IsNullOrEmpty(serviceTOs))
                                exccs.ServiceTOs = serviceTOs.Split(';').ToList();
                        }
                    }
                }
            }
            return exccs;
        }

        public ExtraCCs GetExtraCCsForCustomer(int custid, int? companyID)
        {
            ExtraCCs exccs = new ExtraCCs();
            exccs.CustomerNumber = custid;
            using (var connection = new SqlConnection(this.connectionString))
            {
                connection.Open();
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "config.sb_GetEmailsExtraCCs";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("CustomerNumber", custid);
                    cmd.Parameters.AddWithValue("CompanyID", companyID);
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var reservation = reader["ReservationExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(reservation))
                                exccs.ReservationExtraCCs = reservation.Split(';').ToList();
                            var pickup = reader["PickupExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(pickup))
                                exccs.PickupExtraCCs = pickup.Split(';').ToList();
                            var service = reader["ServiceExtraCCs"] as string;
                            if (!string.IsNullOrEmpty(service))
                                exccs.ServiceExtraCCs = service.Split(';').ToList();

                            var reservationTo = reader["ReservationTOs"] as string;
                            if (!string.IsNullOrEmpty(reservationTo))
                                exccs.ReservationTOs = reservationTo.Split(';').ToList();
                            var pickupTo = reader["PickupTOs"] as string;
                            if (!string.IsNullOrEmpty(pickupTo))
                                exccs.PickupTOs = pickupTo.Split(';').ToList();
                            var serviceTo = reader["ServiceTOs"] as string;
                            if (!string.IsNullOrEmpty(serviceTo))
                                exccs.ServiceTOs = serviceTo.Split(';').ToList();
                        }
                    }
                }
            }
            return exccs;
        }
        public IList<ProfitCenter> GetListofProfitCenters( int? CompanyID )
        {
            IList<ProfitCenter> profitcenters = new List<ProfitCenter>();
            SqlConnection conn = new SqlConnection(this.connectionString);
            //string commandstring = "SELECT [PC], [PCDisplayName], PCName FROM [CORE].[dbo].[ProfitCenters] Where HoursWeek <> 'M-F CLOSED'";
            string commandstring = "config.GetActivePCList";

            SqlCommand command = new SqlCommand();
            command.CommandText = commandstring;
            command.CommandType = CommandType.StoredProcedure;
            //command.CommandType = System.Data.CommandType.StoredProcedure;
            //command.Parameters.Add(new SqlParameter("PCTypeID", 1));
            if(CompanyID != null)
                command.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));

            command.Connection = conn;
            SqlDataReader reader = null;
            try
            {
                conn.Open();
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    profitcenters.Add(new ProfitCenter()
                    {
                        PC = (int)reader["PC"],
                        PCDisplayName = (string)reader["PCDisplayName"],
                        Company = Convert.ToInt16(reader["CompanyID"])
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (conn != null)
                    conn.Close();
            }
            Sunbelt.Log.LogActivity("EmailCCManager", "GetListofProfitCenters: CompanyID" + CompanyID.ToString() + ", Profit Centers found: " + profitcenters.Count);

            return profitcenters;
        }
        public IList<KeyValuePair<string, string>> LookupCustomers(string searchPattern)
        {
            var returnval = new List<KeyValuePair<string, string>>();
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("BusinessObjects.sb_LookupCustomerAccounts", conn))
            {
                if (Regex.IsMatch(searchPattern, "\\D"))
                {
                    sp.AddParameterWithValue("@CustomerNamePattern", SqlDbType.VarChar, 30, ParameterDirection.Input, searchPattern.ToUpper());
                    sp.AddParameterWithValue("@RowCount", SqlDbType.Int, 4, ParameterDirection.Input, 20);
                }
                else
                {
                    int accountNumber = Int32.Parse(searchPattern);
                    if (accountNumber > 0)
                    {
                        sp.AddParameterWithValue("@AccountNumberPattern", SqlDbType.Int, 4, ParameterDirection.Input, Int32.Parse(searchPattern));
                        sp.AddParameterWithValue("@RowCount", SqlDbType.Int, 4, ParameterDirection.Input, 20);
                    }
                }
                var reader = sp.ExecuteReader();
                while (reader.Read())
                {
                    var item = new KeyValuePair<string, string>(((int)reader["AccountNumber"]).ToString(), (string)reader["CustomerName"]);
                    returnval.Add(item);
                }
            }
            return returnval;
        }
        public void SaveExtraCCs(ExtraCCs ccs)
        {
            try
            {
                using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
                {
                    using (SPData sp = new SPData("config.sb_SetEmailsExtraCCs", conn))
                    {
                        if (!ccs.CustomerNumber.HasValue && !ccs.ProfitCenter.HasValue && !ccs.CompanyID.HasValue)
                            throw new ArgumentNullException();
                        if (ccs.ProfitCenter.HasValue)
                            sp.Parameters.AddWithValue("@PC", ccs.ProfitCenter.Value);
                        else
                            sp.Parameters.AddWithValue("@CustomerNumber", ccs.CustomerNumber.Value);

                        sp.Parameters.AddWithValue("@CompanyID", ccs.CompanyID.Value);

                        sp.Parameters.AddWithValue("@ReservationExtraCCs", ParseEmailList(string.Join(";", ccs.ReservationExtraCCs.ToArray())));
                        sp.Parameters.AddWithValue("@PickupExtraCCs", ParseEmailList(string.Join(";", ccs.PickupExtraCCs.ToArray())));
                        sp.Parameters.AddWithValue("@ServiceExtraCCs", ParseEmailList(string.Join(";", ccs.ServiceExtraCCs.ToArray())));
                        sp.ExecuteNonQuery();
                    }
                    if (ccs.ProfitCenter.HasValue)
                    {
                        using (SPData sp = new SPData("config.sb_SetEmailsTOs", conn))
                        {
                            if (!ccs.CustomerNumber.HasValue && !ccs.ProfitCenter.HasValue && !ccs.CompanyID.HasValue)
                                throw new ArgumentNullException();
                            if (ccs.ProfitCenter.HasValue)
                                sp.Parameters.AddWithValue("@PC", ccs.ProfitCenter.Value);
                            else
                                sp.Parameters.AddWithValue("@CustomerNumber", ccs.CustomerNumber.Value);

                            sp.Parameters.AddWithValue("@CompanyID", ccs.CompanyID.Value);

                            sp.Parameters.AddWithValue("@ReservationTOs", ParseEmailList(string.Join(";", ccs.ReservationTOs.ToArray())));
                            sp.Parameters.AddWithValue("@PickupTOs", ParseEmailList(string.Join(";", ccs.PickupTOs.ToArray())));
                            sp.Parameters.AddWithValue("@ServiceTOs", ParseEmailList(string.Join(";", ccs.ServiceTOs.ToArray())));
                            sp.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch( Exception ex)
            {
                throw ex;
            }

        }
        private static object ParseEmailList(string emails)
        {
            if (string.IsNullOrEmpty(emails)) //|| string.IsNullOrWhiteSpace(emails)) Dev/Prod is running older framework version where this method is unavailable
                return DBNull.Value;
            return emails;
        }
    }
}