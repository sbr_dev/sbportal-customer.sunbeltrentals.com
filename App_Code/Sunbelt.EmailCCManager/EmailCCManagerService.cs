﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EmailCCManagerService
/// </summary>
namespace Sunbelt.EmailCCManager
{
    public class EmailCCManagerService
    {
        private const string logSection = "EmailCCManager";
        private DataManager data;
        public EmailCCManagerService(DataManager data)
        {
            this.data = data;
        }
        public ExtraCCs GetExtraCCsForPc(int pc, int? companyID)
        {
            var ccs = this.data.GetExtraCCsForPc(pc, companyID);
            Sunbelt.Log.LogActivity(logSection, "Read from DB: " + ccs.ToString());
            return ccs;
        }
        public ExtraCCs GetExtraCCsForCustomer(int custid, int? companyID)
        {
            var ccs = this.data.GetExtraCCsForCustomer(custid, companyID);
            Sunbelt.Log.LogActivity(logSection, "Read from DB: " + ccs.ToString());
            return ccs;
        }
    }
}