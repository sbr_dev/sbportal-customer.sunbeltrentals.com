﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Summary description for ExtraCCs
/// </summary>
namespace Sunbelt.EmailCCManager
{
    public class ExtraCCs
    {
        public ExtraCCs()
        {
            this.initialize();
        }
        private void initialize()
        {
            this.ReservationExtraCCs = new List<string>();
            this.PickupExtraCCs = new List<string>();
            this.ServiceExtraCCs = new List<string>();

            this.ReservationTOs = new List<string>();
            this.PickupTOs = new List<string>();
            this.ServiceTOs = new List<string>();
            this.CompanyID = 1;
        }
        public int? ProfitCenter { get; set; }
        public int? CustomerNumber { get; set; }
        public int? CompanyID { get; set; }
        public List<string> ReservationExtraCCs { get; set; }
        public List<string> PickupExtraCCs { get; set; }
        public List<string> ServiceExtraCCs { get; set; }
        public List<string> ReservationTOs { get; set; }
        public List<string> PickupTOs { get; set; }
        public List<string> ServiceTOs { get; set; }
        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.ProfitCenter.HasValue)
                sb.Append("ProfitCenter: " + this.ProfitCenter.Value + " ");
            else if (this.CustomerNumber.HasValue)
                sb.Append("Customer: " + this.CustomerNumber.Value + " ");
            else
                return "Invalid Data";
            sb.Append("ReservationCCs: " + string.Join(";", this.ReservationExtraCCs.ToArray()) + " ");
            sb.Append("PickupCCs: " + string.Join(";", this.PickupExtraCCs.ToArray()) + " ");
            sb.Append("ServiceCCs: " + string.Join(";", this.ServiceExtraCCs.ToArray()));

            sb.Append("ReservationTOs: " + string.Join(";", this.ReservationTOs.ToArray()) + " ");
            sb.Append("PickupTOs: " + string.Join(";", this.PickupTOs.ToArray()) + " ");
            sb.Append("ServiceTOs: " + string.Join(";", this.ServiceTOs.ToArray()));
            return sb.ToString();
        }
    }
}