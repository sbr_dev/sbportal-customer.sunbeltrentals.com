﻿using System;
using System.Data;
using System.Collections.Generic;

/// <summary>
/// Summary description for ProfitCenters
/// </summary>
namespace Sunbelt.EmailCCManager
{
    [Serializable()]
    public class ProfitCenters : List<ProfitCenter>
    {
        public ProfitCenters() { }
    }
    public class ProfitCenter
    {
        public ProfitCenter() { }


        public string PCDisplayName { set; get; }
        public int Hierarchy { set; get; }
        public int Company { set; get; }
        public int Territory { set; get; }
        public int Region { set; get; }
        public int Division { set; get; }
		public int PC { set; get; }
        public int AnalysisRegion { set; get; }


		public string LocationKey { set; get; }
		public string PCType { set; get; }
	}
}