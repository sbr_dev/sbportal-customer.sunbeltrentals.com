﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for JsonLabelValue
/// </summary>
namespace Sunbelt.EmailCCManager
{
    public class JsonLabelValue
    {
        public string id { get { return value; } }
        public string label { get; set; }
        public string value { get; set; }

    }
}