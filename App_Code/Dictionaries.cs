﻿using System.Collections.Generic;

public class Dictionaries
{
    public static readonly Dictionary<string, string> CanadaDictionary = new Dictionary<string, string>()
    {
        {"AB","Alberta"},
        {"BC","British Columbia"},
        {"MB","Manitoba"},
        {"NB","New Brunswick"},
        {"NL","Newfoundland and Labrador"},
        {"NT","Northwest Territories"},
        {"NS","Nova Scotia"},
        {"NU","Nunavut"},
        {"ON","Ontario"},
        {"PE","Prince Edward Island"},
        {"QC","Quebec"},
        {"SK","Saskatchewan"},
        {"YT","Yukon"}
    };
    public Dictionaries() { }
}