﻿using SBNet;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace UserProfileExtensions
{
    /// <summary>
    /// Summary description for UserProfileExtensions
    /// </summary>
    public static class UserProfileExtensions
    {
        /// <summary>
        /// This method check if the user is from Canada based on his address. There is a dictionary indicating the provice names and abbreviations and for each province this method try to find 
        /// it in the address property by using Regex Match.
        /// </summary>
        /// <param name="up"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public static bool IsCanadaUser(this UserProfile up, string address)
        {
            var isCanadaUser = false;

            foreach (KeyValuePair<string, string> entry in Dictionaries.CanadaDictionary)
            {
                var matchValue = @"\b" + entry.Value + @"\b";
                var matchKey = @"\b" + entry.Key + @"\b";

                if (Regex.Match(address, @matchValue, RegexOptions.Singleline | RegexOptions.IgnoreCase).Success || Regex.Match(address, @matchKey, RegexOptions.Singleline | RegexOptions.IgnoreCase).Success)
                {
                    isCanadaUser = true;
                    break;
                }
            }
            return isCanadaUser;
        }
    }
}