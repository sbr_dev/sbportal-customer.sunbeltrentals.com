using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using SunbeltWebServices;
using Sunbelt.Tools;
using Sunbelt.Security;
using System.Text.RegularExpressions;
//using SunbeltWebServices;

namespace Sunbelt
{
	public class WebUserInfo : SunbeltWebServices.UserInfo
	{
		public WebUserInfo(SunbeltWebServices.UserInfo ui)
		{
			FullName = ui.FullName;
			DomainUserName = ui.DomainUserName;
			UserEmail = ui.UserEmail;
			ClockNumber = ui.ClockNumber;
			TerminationDate = ui.TerminationDate;
			IsCustomerService = ui.IsCustomerService;
		}
	}
	
	/// <summary>
	/// Summary description for WebUser
	/// </summary>
	public sealed class WebUser
	{
		public static WebUserInfo CurrentUser
		{
			set { WebTools.SetSession("CurrentUser", value); }
			get { return (WebUserInfo)WebTools.GetSession("CurrentUser", null); }
		}

		public WebUser()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static void VerifySBPortalLogin()
		{
			IsSBPortalLogin(true);
		}

		public static void VerifySBPortalLogin(string returnUrl)
		{
			IsSBPortalLogin(true, returnUrl);
		}

		public static bool IsSBPortalLogin(bool autoRedirect)
		{
			return IsSBPortalLogin(autoRedirect, null);
		}

		public static bool IsSBPortalLogin(bool autoRedirect, string returnUrl)
		{
			bool rc = (CurrentUser != null);
			string url = WebTools.ToWebAddress("~/SBPortal/SBLogin.aspx");
#if !DEBUG
			// Production check for https required and only force the 's' if it's required.
			// Making an exception for the test production evironment SUNWEBVM01
			if (ConfigurationSettings.AppSettings["httpsRequired"] == "1")
				url = Regex.Replace(url, "(http:)|(https:)", "https:", RegexOptions.IgnoreCase);
			//throw new Exception("Production redirect to https:// when httpsRequired is 0");
#endif
			if (!string.IsNullOrEmpty(returnUrl))
			{
				SecureQueryString sqs = new SecureQueryString();
				sqs["ReturnUrl"] = returnUrl;
				url += "?wcan=" + sqs.ToString();
			}
			if (!rc && autoRedirect)
				HttpContext.Current.Response.Redirect(url, true);
			return rc;
		}

		public static void VerifyMobileLogin()
		{
			IsMobileLogin(true);
		}

		public static bool IsMobileLogin(bool autoRedirect)
		{
			bool rc = HttpContext.Current.User.Identity.IsAuthenticated;
			if (!rc && autoRedirect)
				HttpContext.Current.Response.Redirect("~/Mobile/Login.aspx", true);
			return rc;
		}
	}
}