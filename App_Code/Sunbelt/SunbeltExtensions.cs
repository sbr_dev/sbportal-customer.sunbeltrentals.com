﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for SunbeltExtensions
/// </summary>
namespace SunbeltExtensions
{
    public static class Extensions
    {
        public static string LogString(this StringBuilder sb, string LogType)
        {
#if DEBUG
            DateTime dt = DateTime.Now;
            string DateTimeStamp = dt.Day + "_" + dt.Month + "_" + dt.Year + "_" + dt.Hour + dt.Minute + dt.Second;
            string tempDir = HttpContext.Current.Server.MapPath("~/Temp/"); 
            FileInfo file = new FileInfo(tempDir + LogType + DateTimeStamp + ".txt");
            if (!file.Exists)
            {
                using (StreamWriter sw = file.CreateText())
                {
                    sw.Write(sb.ToString());
                }
            }

            string LoggedTo = file.FullName;
            return LoggedTo;
#else
            return "LogString is only turned on for debugging";
#endif
        }

        public static string LogString(this string sb, string LogType)
        {
#if DEBUG
            DateTime dt = DateTime.Now;
            string DateTimeStamp = dt.Day + "_" + dt.Month + "_" + dt.Year + "_" + dt.Hour + dt.Minute + dt.Second + dt.Millisecond;
            string tempDir = HttpContext.Current.Server.MapPath("~/Temp/"); 
            FileInfo file = new FileInfo(tempDir + LogType + DateTimeStamp + ".txt");
            if (!file.Exists)
            {
                using (StreamWriter sw = file.CreateText())
                {
                    sw.Write(sb.ToString());
                }
            }

            string LoggedTo = file.FullName;
            return LoggedTo;
#else
            return "LogString is only turned on for debugging";
#endif
        }
        public static string LogInDatabase(this StringBuilder sb)
        {
            return "Logged to ...";
        }
        public static string LogInDatabase(this string sb)
        {
            return "Logged to ...";
        }

        public static bool IsValidEmailAddress(this string emailAddress)
        {
            string ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            Match match = Regex.Match(emailAddress, ValidationExpression);
            return match.Success;
        }
    }
}
