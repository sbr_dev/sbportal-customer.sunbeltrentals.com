using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;

namespace Sunbelt
{
	/// <summary>
	/// LoginEventArgs
	/// </summary>
	public class LoginEventArgs : EventArgs
	{
		public LoginEventArgs(WebUserInfo user)
		{
			User = user;
		}

		public WebUserInfo User;
	}
}


