﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Sunbelt.Compression;

/// <summary>
/// Summary description for SBNetWeb
/// </summary>
namespace SBNet.Web
{
    /// <summary>
    /// Extend the Sunbelt.Web.Webpage class to have view state compression
    /// </summary>
    public partial class WebPage : Sunbelt.Web.WebPage
    {
        public bool EnableViewstateCompression = false;

        protected override void SavePageStateToPersistenceMedium(object state)
        {
            if (EnableViewstateCompression)
            {
                LosFormatter f = new LosFormatter();
                StringWriter sw = new StringWriter();

                f.Serialize(sw, state);
                byte[] b = Convert.FromBase64String(sw.ToString());
                b = GZip.Compress(b);
                if (ScriptManager.GetCurrent(Page) != null)
                    ScriptManager.RegisterHiddenField(this, "__ZIPSTATE", Convert.ToBase64String(b));
                else
                    ClientScript.RegisterHiddenField("__ZIPSTATE", Convert.ToBase64String(b));
            }
            else
                base.SavePageStateToPersistenceMedium(state);
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            if (EnableViewstateCompression)
            {
                string vstate = this.Request.Form["__ZIPSTATE"];
                byte[] b = Convert.FromBase64String(vstate);
                b = GZip.Decompress(b);
                vstate = Convert.ToBase64String(b);

                LosFormatter f = new LosFormatter();
                return f.Deserialize(vstate);
            }
            else
                return base.LoadPageStateFromPersistenceMedium();
        }
	}

	public sealed class Browser
	{
		public static bool IsIE()
		{
			return (HttpContext.Current.Request.Browser.Browser == "IE");
		}

		public static bool IsIE6()
		{
			return (HttpContext.Current.Request.Browser.Type == "IE6");
		}

		public static bool IsOpera()
		{
			return (HttpContext.Current.Request.Browser.Browser == "Opera");
		}
    }
}
