﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;

namespace SBNet.Extensions
{
	//Extension methods must be defined in a static class
	/// <summary>
	/// SBNet StringExtension class
	/// </summary>
	public static class StringExtension
	{
		/// <summary>
		/// Truncates 'text' to 'length' characters if needed.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="length">The length.</param>
		/// <returns></returns>
		public static string Truncate(this string text, int length)
		{
			if (length < 1)
				throw new ArgumentException("Value for 'length' must be greater than zero.", "length");

			if (!string.IsNullOrEmpty(text))
			{
				Regex RegexObj = new Regex(".{0," + length + "}", RegexOptions.Singleline);
				Match MatchResults = RegexObj.Match(text);

				if (MatchResults.Success)
					return MatchResults.Value;
			}
			return text;
		}

		/// <summary>
		/// Truncates 'text' to 'truncateLength' characters if 'text' contains no wrapping characters " -!%{}()".
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="truncateLength">Length of the truncate.</param>
		/// <returns></returns>
		public static string TruncateNonWrappingText(this string text, int truncateLength)
		{
			//Ingore if the string has wrapping characters.
			if (!string.IsNullOrEmpty(text))
			{
				#region RegexBuddy
				// [ \-!%{}\(\)]
				// 
				// Match a single character present in the list below «[ \-!%{}\(\)]»
				//    The character " " « »
				//    A - character «\-»
				//    One of the characters "!%{}" «!%{}»
				//    A ( character «\(»
				//    A ) character «\)»
				#endregion
				if (!System.Text.RegularExpressions.Regex.IsMatch(text, "[ \\-!%{}\\(\\)]"))
				{
					if (text.Length > truncateLength)
						return text.Substring(0, truncateLength) + "...";
				}
			}
			return text;
		}

		/// <summary>
		/// Splits the specified text into equal size pieces. The last piece will be smaller if less than 'pieceSize'.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="pieceSize">Size of the piece.</param>
		/// <returns></returns>
		public static string[] SplitText(this string text, int pieceSize)
		{
			if (pieceSize < 1)
				throw new ArgumentException("Value for 'pieceSize' must be greater than zero.", "pieceSize");

			List<string> list = new List<string>();
			if (!string.IsNullOrEmpty(text))
			{
				Regex RegexObj = new Regex(".{0," + pieceSize + "}", RegexOptions.Singleline);
				Match MatchResults = RegexObj.Match(text);

				while (MatchResults.Success)
				{
					if(MatchResults.Value.Length > 0)
						list.Add(MatchResults.Value);
					MatchResults = MatchResults.NextMatch();
				}
			}
			return list.ToArray();
		}

		/// <summary>
		/// Shortens 'text' to 'length' characters and concatenates "..." to it. 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static string ToTrailingText(this string text, int length)
		{
			if (text.Length > length)
				return text.Substring(0, length) + "...";
			else
				return text;
		}

		/// <summary>
		/// Returns '&amp;nbsp;' if the string is empty or null.
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static string ToTableCellText(this string text)
		{
			if (string.IsNullOrEmpty(text))
				return "&nbsp;";
			return text;
		}
	}

	public static class BooleanExtension
	{
		/// <summary>
		/// Returns "Yes" or "No".
		/// </summary>
		/// <param name="value">if set to <c>true</c> [value].</param>
		/// <returns></returns>
		public static string ToYesNo(this bool value)
		{
			return (value) ? "Yes" : "No";
		}
	}

	public static class DebugExtension
	{
		private class CollectionDump
		{
			public CollectionDump(object value) { collection = value; }
			public object collection { set; get; }
		};

		public static string DumpCollection(this object value, bool expandProperties)
		{
			return DumpCollection(value, expandProperties, 10, false);
		}

		public static string DumpCollection(this object value, bool expandProperties, int collectionCount)
		{
			return DumpCollection(value, expandProperties, collectionCount, false);
		}

		/// <summary>
		/// Dumps the collection to a HTML table.  Optionally expands object properties.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="expandProperties">if set to <c>true</c> [expand properties].</param>
		/// <returns></returns>
		public static string DumpCollection(this object value, bool expandProperties, int collectionCount, bool listOnly)
		{
			if (value != null && (value is IEnumerable))
			{
				CollectionDump dc = new CollectionDump(value);
				return dc.DumpToHtml(expandProperties, collectionCount, listOnly);
			}
			else
				return value.DumpToHtml(expandProperties, collectionCount, listOnly);
		}

		public static string DumpToHtml(this object value, bool expandProperties)
		{
			return DumpToHtml(value, expandProperties, 10, false);
		}

		public static string DumpToHtml(this object value, bool expandProperties, int collectionCount)
		{
			return DumpToHtml(value, expandProperties, collectionCount, false);
		}

		/// <summary>
		/// Dumps the object to a HTML table.  Optionally expands object properties.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="expandProperties">if set to <c>true</c> [expand properties].</param>
		/// <returns></returns>
		/// <summary>
		/// Dumps the object to a HTML table.  Optionally expands object properties.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="expandProperties">if set to <c>true</c> [expand properties].</param>
		/// <returns></returns>
		public static string DumpToHtml(this object value, bool expandProperties, int collectionCount, bool listOnly)
		{
			StringBuilder sb = new StringBuilder();

			string tableStart = "<table cellpadding='0' cellspacing='0' style='background-color: #FFFFFF; border-color: #000000; border-style: solid; border-width: 1px; border-collapse: collapse;'>";
			string tableEnd = "</table>";
			//string tdStart1 = "<td style='text-align: right; vertical-align: top; font-size: 7.5pt; padding: 0ex .5ex 0ex 1ex'>";
			string tdStart1 = "<td style='text-align: right; vertical-align: top; padding: 0ex .5ex 0ex 1ex'>";
			//string tdStart2 = "<td style='text-align: left; vertical-align: top; font-size: 7.5pt; padding: 0ex 1ex 0ex .5ex'>";
			string tdStart2 = "<td style='text-align: left; vertical-align: top; padding: 0ex 1ex 0ex .5ex'>";
			string tdEnd = "</td>";

			if (value == null)
			{
				sb.Append(tableStart);
				sb.Append("<tr>");
				//sb.Append("<td style='text-align: left; background-color: #E0E0E0; font-size: 8pt;'><b>object.DumpToHtml(...) object is null</b></td>");
				sb.Append("<td style='text-align: left; background-color: #E0E0E0'><b>object.DumpToHtml(...) object is null</b></td>");
				sb.Append("</tr>");
				sb.Append(tableEnd);
			}
			else
			{
				Type type = value.GetType();
				sb.Append(tableStart);
				sb.Append("<tr>");
				//sb.Append("<td colspan='2' style='text-align: left; background-color: #E0E0E0; font-size: 8pt;'>");
				sb.Append("<td colspan='2' style='text-align: left; background-color: #E0E0E0'>");
				if (value is string)
					//sb.Append("<b>\"" + HttpUtility.HtmlEncode(value.ToString()).Replace(" ", "&nbsp;") + "\"</b>&nbsp;&nbsp;(" + value.GetType().Name + " Length=" + value.ToString().Length.ToString() + ")");
					sb.Append("<b>\"" + System.Web.HttpUtility.HtmlEncode(value.ToString()) + "\"</b>&nbsp;&nbsp;(" + value.GetType().Name + " Length=" + value.ToString().Length.ToString() + ")");
				else
					sb.Append("<b>" + value.ToString() + "</b> (" + type.Name + ")");
				sb.Append("</td></tr>");

				//Don't expand the properties for strings.
				if (type.FullName != "System.String")
				{
					//PropertyInfo[] piList = type.GetProperties();
					PropertyInfo[] piList = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
					if (piList.Length > 0)
					{
						for (int i = 0; i < piList.Length; i++)
						{
							sb.Append("<tr>");
							object obj = null;
							try { obj = piList[i].GetValue(value, null); }
							catch { }

							if (listOnly)
							{
								sb.Append(tdStart1 + ">>" + piList[i].Name + "<<" + tdEnd + "</tr>");
								continue;
							}

							if (obj != null)
							{
								sb.Append(tdStart1 + piList[i].Name + ":" + tdEnd);
								sb.Append(tdStart2);

								int count = 0;
								if (expandProperties && (obj is IEnumerable) && !(obj is string))
								{
									int length = 0;
									IEnumerator e = ((IEnumerable)obj).GetEnumerator();
									while (e.MoveNext())
										length++;

									sb.Append("<b>length=" + length.ToString() + "</b> (" + ParseObjectType(obj.GetType().FullName) + ")");

									if (length > 0)
									{
										sb.Append("<br />");
										e = ((IEnumerable)obj).GetEnumerator();
										while (e.MoveNext())
										{
											if (count < collectionCount)
												sb.Append(DumpToHtml(e.Current, true));
											count++;
										}
										if (length > collectionCount)
											sb.Append("&nbsp;<i>... more than " + collectionCount.ToString() + " items in this collection.</i>");
									}
								}
								else
								{
									Type oType = obj.GetType();
									//Don't expand System.* objects and don't recurse.
									if (expandProperties &&
										!oType.FullName.StartsWith("System.") && type != oType)
										sb.Append(DumpToHtml(obj, true));
									else
									{
										if (obj is string)
											//sb.Append("<b>\"" + HttpUtility.HtmlEncode(obj.ToString()).Replace(" ", "&nbsp;") + "\"</b>&nbsp;&nbsp;(" + obj.GetType().Name + " Length=" + obj.ToString().Length.ToString() + ")");
											sb.Append("<b>\"" + System.Web.HttpUtility.HtmlEncode(obj.ToString()) + "\"</b>&nbsp;&nbsp;(" + obj.GetType().Name + " Length=" + obj.ToString().Length.ToString() + ")");
										else
										{
											Type t = Nullable.GetUnderlyingType(oType);
											//if(t != null)
											//	sb.Append("<b>" + obj.ToString() + "</b> (Nullable " + oType.Name + ")");
											//else
											sb.Append("<b>" + obj.ToString() + "</b> (" + oType.Name + ")");
										}
									}
								}
								sb.Append(tdEnd);
							}
							else
							{
								sb.Append(tdStart1 + piList[i].Name + ":" + tdEnd);

								string propType = string.Empty;
								Type t = Nullable.GetUnderlyingType(piList[i].PropertyType);
								if (t != null)
									propType = "Nullable " + t.Name;
								else
									propType = piList[i].PropertyType.Name;
								sb.Append(tdStart2 + "<b><i>null</i></b> (" + propType + ")" + tdEnd);
							}
							sb.Append("</tr>");
						}
					}
				}
				sb.Append(tableEnd);
			}
			return sb.ToString();
		}

		private static string ParseObjectType(string fullName)
		{
			string typeName = string.Empty;

			//Try to get the object type from the FullName.
			#region RegexBuddy
			// (?<object_type>[\w*.+]{1,})
			// 
			// Match the regular expression below and capture its match into backreference with name "object_type" «(?<object_type>[\w*.+]{1,})»
			//    Match a single character present in the list below «[\w*.+]{1,}»
			//       Between one and unlimited times, as many times as possible, giving back as needed (greedy) «{1,}»
			//       Match a single character that is a "word character" (letters, digits, etc.) «\w»
			//       One of the characters "*.+" «*.+»
			#endregion
			Match rc = Regex.Match(fullName, "(?<object_type>[\\w*.+]{1,})", RegexOptions.IgnoreCase);
			if (rc.Success && !string.IsNullOrEmpty(rc.Groups["object_type"].ToString()))
			{
				typeName += rc.Groups["object_type"].ToString();

				//Get the object type.
				#region RegexBuddy
				// \[(?<object_type>[\w*.+]{1,})
				// 
				// Match the character "[" literally «\[»
				// Match the regular expression below and capture its match into backreference with name "object_type" «(?<object_type>[\w*.+]{1,})»
				//    Match a single character present in the list below «[\w*.+]{1,}»
				//       Between one and unlimited times, as many times as possible, giving back as needed (greedy) «{1,}»
				//       Match a single character that is a "word character" (letters, digits, etc.) «\w»
				//       One of the characters "*.+" «*.+»
				#endregion
				rc = Regex.Match(fullName, "\\[(?<object_name>[\\w*.+]{1,})", RegexOptions.IgnoreCase);
				if (rc.Success)
					typeName += "&lt;" + rc.Groups["object_name"].ToString() + "&gt;";
			}
			return typeName;
		}
		/// <summary>
		/// Uppercase the first letter of each word. Text is lowercased before conversion.
		/// </summary>
		/// <param name="unCappedText">Word or string to convert.</param>
		/// <returns></returns>
		public static string ToLeadingCaps(this string unCappedText)
		{
			string cappedText = string.Empty;
			string pattern = @"\w+|\W+";

			foreach (Match m in Regex.Matches(unCappedText.ToLower(), pattern))
			{
				string x = m.ToString();  // get the matched string
				if (char.IsLower(x[0]))   // if the first char is lower case
					x = char.ToUpper(x[0]) + x.Substring(1, x.Length - 1);  // capitalize it
				cappedText += x;			  // collect all text
			}
			return cappedText;
		}
        public static string ToNullSafeString(this object obj)
        {
            return obj != null ? obj.ToString() : String.Empty;
        }
	}
}

