using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Common.DAL;
using System.Text;
using System.Diagnostics;

namespace SBNet
{
    /// <summary>
    /// Summary description for log
    /// </summary>
    public class Log
    {
        public enum Sections { PDF, Login, OpenInvoices, InvoicePayment, Reporting, Alerts, Email, ReturnEquipment, ServiceRequest, Reservations, AS400Call, About, Careers, CustomerService, Equipment, Locations, Search, Services, SBPortal }

        public Log()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static int LogCustomerFeedBack(string Comments, AccountEventArgs myAccountEventArgs)
        {
            MembershipUser mu = null;
            try { mu = Membership.GetUser(); }
            catch { }

            Guid UserId = Guid.Empty;
            if (mu != null)
                UserId = new Guid(mu.ProviderUserKey.ToString());

            try
            {
				using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
				using (SPData sp = new SPData("AppLog.sb_InsFeedback", conn))
                {
                    sp.AddParameterWithValue("Host", SqlDbType.VarChar, 50, ParameterDirection.Input, HttpContext.Current.Request.UserHostAddress.ToString());
                    sp.AddParameterWithValue("PageURL", SqlDbType.VarChar, 1000, ParameterDirection.Input, HttpContext.Current.Request.Url.ToString());
                    sp.AddParameterWithValue("UserID", SqlDbType.UniqueIdentifier, 1000, ParameterDirection.Input, UserId);
                    sp.AddParameterWithValue("AccountNumber", SqlDbType.Int, 4, ParameterDirection.Input, myAccountEventArgs.AccountNumber);
                    sp.AddParameterWithValue("@IsCorpLinkAccount", SqlDbType.Bit, 1, ParameterDirection.Input, myAccountEventArgs.IsCorpLink);
                    sp.AddParameterWithValue("Comments", SqlDbType.VarChar, 1000, ParameterDirection.Input, Comments);
                    sp.AddParameterWithValue("UserName", SqlDbType.VarChar, 50, ParameterDirection.Input, (mu != null) ? mu.UserName : "");

                    SqlParameter rcParam = sp.AddParameter("rc", SqlDbType.Int, 5, ParameterDirection.ReturnValue);
                    sp.ExecuteNonQuery();
                    return (Convert.ToInt32(rcParam.Value));
                }
            }
            catch (Exception ex2)
            {
                //If there's an error in the errorlogger, let it go! Let it go!
                Debug.WriteLine(ex2.Message);
            }
            return 0;
        }

        public static DataTable GetAllApplicationActivity()
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("AppLog.sb_SelActivityAll", conn))
            {
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetApplicationActivityById(int LogId)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("AppLog.sb_SelActivityByID", conn))
            {
                sp.AddParameterWithValue("@LogId", SqlDbType.Int, 5, ParameterDirection.Input, LogId);
                return sp.ExecuteDataTable();
            }
        }

        public static DataTable GetApplicationActivityBySection(string appSection)
        {
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("AppLog.sb_SelActivityLogBySection", conn))
            {
                sp.AddParameterWithValue("@Section", SqlDbType.VarChar, 255, ParameterDirection.Input, appSection);
                return sp.ExecuteDataTable();
            }
        }
    }
}