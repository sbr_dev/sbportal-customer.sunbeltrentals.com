﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;

namespace SBNet
{
	/// <summary>
	/// Summary description for SBNetMasterPage
	/// </summary>
	public abstract class SBNetMasterPage : System.Web.UI.MasterPage, ICallbackEventHandler
	{
		public abstract string MainContentWidth
		{
			get; set;
		}

		public abstract void ShowLeftImage(string imageUrl);

		public AccountEventArgs AccountArgs
		{
			get { return (AccountEventArgs)ViewState["Account"]; }
			set { ViewState["Account"] = value; }
		}

		protected string GooleAnalytics = String.Empty;
		
		/// <summary>
		/// This event is raised with the Acount has changed.
		/// </summary>
		public EventHandler<AccountEventArgs> AccountChanged;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			//Response.Write(Request.Url.Host + "<br />");

			//Add this to just www.sunbeltrentals.com
			/*if (Request.Url.Host.StartsWith("customer.sunbelt", StringComparison.CurrentCultureIgnoreCase))
			{
				System.Text.StringBuilder sb = new System.Text.StringBuilder();

                sb.Append("<script type=\"text/javascript\">");
                sb.Append("var _gaq = _gaq || [];");
                sb.Append("_gaq.push(['_setAccount', 'UA-3906297-2']);");
                sb.Append("_gaq.push(['_setDomainName', '.sunbeltrentals.com']);");
                sb.Append("_gaq.push(['_trackPageview']);");
                sb.Append("(function() {");
                sb.Append("var ga = document.createElement('script');");
                sb.Append("ga.type = 'text/javascript';");
                sb.Append("ga.async = true;");
                sb.Append("ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';");
                sb.Append("var s = document.getElementsByTagName('script')[0];");
                sb.Append("s.parentNode.insertBefore(ga, s);");
                sb.Append(" })();");
                sb.Append("</script>");

				GooleAnalytics = sb.ToString();
			}*/
		}

		public void RaiseChangeAccountEvent(AccountEventArgs e)
		{
			EventHandler<AccountEventArgs> eh = AccountChanged;
			if (eh != null)
				AccountChanged(this, e);
		}

	
		protected override void OnPreRender(EventArgs e)
		{
			if (!Page.ClientScript.IsClientScriptIncludeRegistered("MasterPage.js"))
				Page.ClientScript.RegisterClientScriptInclude("MasterPage.js", Page.ResolveUrl("~/MasterPages/MasterPage.js"));

			HyperLink hlCustomerFeedback = (HyperLink)FindControl("hlCustomerFeedback");

			if (hlCustomerFeedback != null)
				hlCustomerFeedback.Visible = false;

			if (AccountArgs != null)
			{
				if (hlCustomerFeedback != null)
					hlCustomerFeedback.Visible = Page.User.Identity.IsAuthenticated;

				//Register common scripts.
				SBNet.Tools.IncludeSBNetClientScript(Page);

				//Register the startup script.
				if (!Page.ClientScript.IsStartupScriptRegistered("masterpages_main"))
				{
					StringBuilder sbCode = new StringBuilder();

					//Set the global account info.
					sbCode.Append("SBNet.Account.Number = " + AccountArgs.AccountNumber + ";\r\n");
					sbCode.Append("SBNet.Account.IsCorpLink = " + AccountArgs.IsCorpLink.ToString().ToLower() + ";\r\n");

					sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
					sbCode.Append("  Sys.Application.add_load(_MasterPage_WindowOnLoad)\r\n");
					sbCode.Append("else\r\n");
					sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', _MasterPage_WindowOnLoad);\r\n");

					sbCode.Append("masterPage.scriptTimeout = " + (Session.Timeout * 60).ToString() + ";\r\n");

					sbCode.Append("masterPage.enabled = " + Page.User.Identity.IsAuthenticated.ToString().ToLower() + ";\r\n");

					Page.ClientScript.RegisterStartupScript(this.GetType(), "masterpages_main", sbCode.ToString(), true);
				}

				//Register client code the handle client script callbacks. See MSDN article
				// 'Implementing Client Callbacks Without Postbacks in ASP.NET Web Pages' for
				// more information.
				String cbReference =
					Page.ClientScript.GetCallbackEventReference(this,
					"arg", "_MasterPage_ClientCallBack", "context", "_MasterPage_ClientErrorCallBack", false);
				String callbackScript;
				callbackScript = "function _MasterPage_ServerCall(arg, context)" +
					"{ " + cbReference + "} ;";
				Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
					"_MasterPage_ServerCall", callbackScript, true);

			}
			base.OnPreRender(e);
		}

		#region ICallbackEventHandler Members

		private string EventArgument;

		public string GetCallbackResult()
		{
			if (EventArgument == "continue")
			{
				//Do nothing.
				return EventArgument;
			}

			string feedBack = EventArgument.Trim();
			feedBack = feedBack.Substring(0, Math.Min(1000, feedBack.Length));
			if (feedBack.StartsWith("[FB]") && (feedBack.Length > 4))
			{
				//Remove the "[FB]".
				feedBack = feedBack.Substring(4);
				//log it.
				SBNet.Log.LogCustomerFeedBack(feedBack, AccountArgs);
				//Email it.
				Sunbelt.Email.SendEmail(ConfigurationManager.AppSettings["EService_Email"].ToString(),
					Page.User.Identity.Name, "", "", "Web Site Feed Back", feedBack, false);

				return "FEEDBACK";
			}

			return EventArgument;
		}

		public void RaiseCallbackEvent(string eventArgument)
		{
			EventArgument = eventArgument;
		}

		#endregion
	}
}
