﻿using System;

public class MigrationStatus
{
    public string UserID;
    public string OktaID;
    public string OktaCreatedDate;
    public string OktaMigrationStatus;
    public string MagentoStatus;
    public string MagentoCreatedDate;
}