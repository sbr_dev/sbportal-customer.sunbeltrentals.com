using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;

public partial class logout_default : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			if (User.Identity.IsAuthenticated)
			{
				FormsAuthentication.SignOut();
				Response.Redirect(WebTools.ToWebAddress("~/Logout.aspx"));
			}
		}
        bool httpsRequired = ConfigurationManager.AppSettings["httpsRequired"] == "1";
        hlLogin.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(hlLogin.NavigateUrl, httpsRequired);
		hlHome.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
    }
}
