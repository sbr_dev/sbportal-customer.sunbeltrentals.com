<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" Theme="Main" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="logout_default"  %>


<asp:Content ID="LeftSectionTitleContent" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
  Create Error
</asp:Content>

<asp:Content ID="RightSectionTitleContent" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>

<asp:Content ID="MainBodyContent" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><asp:Image ImageUrl="~/App_Themes/Main/images/clearpixel.gif" width="1" height="16" runat="server" /></td>
  </tr>
  <tr>
    <td>
        Click the button to throw an error.<br />
        <br />
        <asp:Button ID="btnError01" runat="server" OnClick="btnError01_Click" Text="Divide by Zero" /><br />
        <br />
    </td>
  </tr>
  <tr>
    <td><asp:Image ImageUrl="~/App_Themes/Main/images/clearpixel.gif" width="1" height="16" runat="server" /></td>
  </tr>
</table>

</asp:Content>

