using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using Sunbelt.Tools;

public partial class masterpages_main : SBNet.SBNetMasterPage
{
	public bool TopSearchVisible
	{
		set { Header.TopSearchVisible = value; }
		get { return Header.TopSearchVisible; }
	}

	public bool LeftNavVisible
	{
		get { return UserLeftNav.Visible; }
		set { UserLeftNav.Visible = value; LeftNav.Visible = value; }
	}

	public bool BottomLeftNavVisible
	{
		get { return LeftNav.Visible; }
		set { LeftNav.Visible = value; }
	}

	public override string MainContentWidth
	{
		set { tdMainContent.Style["width"] = value; }
		get { return tdMainContent.Style["width"]; }
	}

    public int GetSessionTimeout
    {
        //Time in seconds
        get { return Server.ScriptTimeout; } 
    }

    public string SessionTimeoutUrl
    {
        get { return ResolveUrl("~/SessionEnded.htm"); }
    }

	//*** This is inherented from base class.
	//public AccountEventArgs AccountArgs
	//{
	//    get { return (AccountEventArgs)ViewState["Account"]; }
	//    set { ViewState["Account"] = value; }
	//}

	public override void ShowLeftImage(string imageUrl)
	{
		LeftImage.Visible = true;
		LeftImage.ImageUrl = imageUrl;
		LeftNavVisible = false;
		BottomLeftNavVisible = false;
	}


	protected void Page_Load(object sender, EventArgs e)
    {
		Response.AddHeader("Content-Type", "text/html; charset=utf-8");
		Response.Cache.SetCacheability(HttpCacheability.NoCache);

		UserLeftNav.ItemSelected += UserLeftNav_ItemSelected;
        
		if (!IsPostBack)
		{
			UpdateUserNavControls();
			hlCustomerFeedback.NavigateUrl = Request.Url.AbsolutePath + "#";
		}
#if false
		if (!Page.ClientScript.IsClientScriptIncludeRegistered("Cookie.js"))
			Page.ClientScript.RegisterClientScriptInclude("Cookie.js", Page.ResolveUrl("~/ClientScript/Cookie.js"));
#endif
	}

#if false
	protected override void OnPreRender(EventArgs e)
	{
		hlCustomerFeedback.Visible = false;
		
		if (AccountArgs != null)
		{
			hlCustomerFeedback.Visible = true;
			
			//Register common scripts.
			SBNet.Tools.IncludeSBNetClientScript(Page);

			//Register the startup script.
			if (!Page.ClientScript.IsStartupScriptRegistered("masterpages_main"))
			{
				StringBuilder sbCode = new StringBuilder();

				//Set the global account info.
				sbCode.Append("SBNet.Account.Number = " + AccountArgs.AccountNumber + ";\r\n");
				sbCode.Append("SBNet.Account.IsCorpLink = " + AccountArgs.IsCorpLink.ToString().ToLower() + ";\r\n");

				sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
				sbCode.Append("  Sys.Application.add_load(_MasterPage_WindowOnLoad)\r\n");
				sbCode.Append("else\r\n");
				sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', _MasterPage_WindowOnLoad);\r\n");

				sbCode.Append("masterPage.scriptTimeout = " + (Session.Timeout * 60).ToString() + ";\r\n");

				sbCode.Append("masterPage.enabled = " + Page.User.Identity.IsAuthenticated.ToString().ToLower() + ";\r\n");

				Page.ClientScript.RegisterStartupScript(this.GetType(), "masterpages_main", sbCode.ToString(), true);
			}

			//Register client code the handle client script callbacks. See MSDN article
			// 'Implementing Client Callbacks Without Postbacks in ASP.NET Web Pages' for
			// more information.
			String cbReference =
				Page.ClientScript.GetCallbackEventReference(this,
				"arg", "_MasterPage_ClientCallBack", "context", "_MasterPage_ClientErrorCallBack", false);
			String callbackScript;
			callbackScript = "function _MasterPage_ServerCall(arg, context)" +
				"{ " + cbReference + "} ;";
			Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
				"_MasterPage_ServerCall", callbackScript, true);

		}
		base.OnPreRender(e);
	}
#endif

	public void ChangeAccount(AccountEventArgs e)
	{
		AccountArgs = e;

		((SBNet.IWebControl)LeftNav).UpdateData(e);
	
		RaiseChangeAccountEvent(e);
	}
	
	
	public void UpdateUserNavControls()
	{
		//If no user loged in, hide the UserLeftNav control.
		bool b = Page.User.Identity.IsAuthenticated && LeftNavVisible;
		UserLeftNav.Visible = b;
		LeftNav.Visible = b;

		Header.UpdateDisplay();
	}

	public void UpdateAccountList()
	{
		UserLeftNav.RefreshCompanyAccounts();
	}
	
	private void UserLeftNav_ItemSelected(object sender, AccountEventArgs e)
    {
        ChangeAccount(e);
    }
}
