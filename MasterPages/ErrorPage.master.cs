using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using Sunbelt.Tools;

//public class MasterPageEventArgs : EventArgs
//{
//    public MasterPageEventArgs(int accountNumber)
//    {
//        AccountNumber = accountNumber;
//    }
//    public int AccountNumber;
//}


public partial class masterpages_errorpage : System.Web.UI.MasterPage
{
	public string MainContentWidth
	{
		set { tdMainContent.Style["width"] = value; }
		get { return tdMainContent.Style["width"]; }
	}

    public int GetSessionTimeout
    {
        //Time in seconds
        get { return Server.ScriptTimeout; } 
    }

    public string SessionTimeoutUrl
    {
        get { return ResolveUrl("~/SessionEnded.htm"); }
    }

	public AccountEventArgs AccountArgs
	{
		get { return (AccountEventArgs)ViewState["Account"]; }
		set { ViewState["Account"] = value; }
	}
	
	public EventHandler<AccountEventArgs> AccountChanged;
	
	protected void Page_Load(object sender, EventArgs e)
    {
		Response.AddHeader("Content-Type", "text/html; charset=utf-8");

		if (!IsPostBack)
		{
			UpdateUserNavControls();
		}
		
	}

	protected override void OnPreRender(EventArgs e)
	{
		if (AccountArgs != null)
		{
			//Register common scripts.
			SBNet.Tools.IncludeSBNetClientScript(Page);

			//Register the startup script.
			if (!Page.ClientScript.IsStartupScriptRegistered("masterpages_main"))
			{
				StringBuilder sbCode = new StringBuilder();

				//Set the global account info.
				sbCode.Append("SBNet.Account.Number = " + AccountArgs.AccountNumber + ";\r\n");
				sbCode.Append("SBNet.Account.IsCorpLink = " + AccountArgs.IsCorpLink.ToString().ToLower() + ";\r\n");

				sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
				sbCode.Append("  Sys.Application.add_load(_MasterPage_WindowOnLoad)\r\n");
				sbCode.Append("else\r\n");
				sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', _MasterPage_WindowOnLoad);\r\n");

				sbCode.Append("masterPage.scriptTimeout = " + (Session.Timeout * 60).ToString() + ";\r\n");

				sbCode.Append("masterPage.enabled = " + Page.User.Identity.IsAuthenticated.ToString().ToLower() + ";\r\n");
				
				Page.ClientScript.RegisterStartupScript(this.GetType(), "masterpages_main", sbCode.ToString(), true);
			}
		}
		base.OnPreRender(e);
	}	
	
	public void UpdateUserNavControls()
	{
		Header.UpdateDisplay();
	}
}
