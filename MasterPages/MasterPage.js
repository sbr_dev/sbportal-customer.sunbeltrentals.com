﻿
var masterPage = {

	scriptTimeout:60 * 20,
	enabled:true,
	secs:0,
	timerID:0,
	divCustomerFeedback:null,
	
	InitializeTimer:function()
	{
		//alert("masterPage.InitializeTimer()");
		
		// Set the length of the timer, in seconds
		this.secs = this.scriptTimeout;
		if(this.timerID > 0)
			clearInterval(this.timerID)
        this.timerID = 0;
        if(this.enabled)
			this.timerID = setInterval("masterPage.OnTimer()", 5000);
	},

	RestartTimer:function()
	{
		this.InitializeTimer();
	},

	OnTimer:function()
	{
		var mp = masterPage;

		mp.secs -= 5;
		//alert("mp.secs = " + mp.secs);

		if(mp.secs > 115 && mp.secs <= 120)
		{
			SBNet.Util.ConfirmBox("Session Timeout", "Your sunbelt session is about to be timed out due to inactivity.<br /><br />Would you like to continue your session or logout?",
				masterPage.OnContinueSession,
				masterPage.OnEndSession,
				"yesno",
				SBNet.Util.webAddress + "App_Themes/Main/Buttons/continue_80x20_darkred.gif",
				SBNet.Util.webAddress + "App_Themes/Main/Buttons/log_out_80x20_darkgray.gif");
		}
		else
		if(mp.secs <= 1)
		{
			clearInterval(mp.timerID)
			window.location = SBNet.Util.webAddress + "SessionTimeout.aspx";
		}
	},
	
	OnContinueSession:function()
	{
		//Make call back.
		_MasterPage_ServerCall("continue", "SESSION");
		masterPage.RestartTimer();
	},
	
	OnEndSession:function()
	{
		//Log off.
		//_MasterPage_ServerCall("end", "SESSION");
		window.location = SBNet.Util.webAddress + "Logout.aspx";
	}
}

function _MasterPage_WindowOnLoad()
{
	//alert("_MasterPage_WindowOnLoad");

	masterPage.InitializeTimer();
	
	BI_WatchAnchors(document.getElementById("tdMasterHeader"));
	BI_WatchAnchors(document.getElementById("tdMasterLeftNav"));
	
	var hl = SBNet.Util.FindASPXControl("A", "hlCustomerFeedback", document.getElementById("tdRightSectionTitle"));
	if(hl != null)
	{
		SBNet.Util.AttachEvent(hl, "onclick", _MasterPage_OnCustomerFeedback);
	
		var div = document.getElementById("divCustomerFeedback");
		var btnSubmit = SBNet.Util.FindASPXControl("INPUT", "btnSubmit", div);
		var btnCancel = SBNet.Util.FindASPXControl("INPUT", "btnCancel", div);
		
		btnSubmit.onclick = _MasterPage_OnSubmit;
		btnCancel.onclick = _MasterPage_OnCancel;

		masterPage.divCustomerFeedback = div;
	}
}

function _MasterPage_OnCustomerFeedback()
{
	if(masterPage.divCustomerFeedback != null)
	{
		masterPage.divCustomerFeedback.style.display = "block";		
		SBNet.Util.CenterControlOnScreen(masterPage.divCustomerFeedback);
		masterPage.divCustomerFeedback.style.zIndex = 5000;
		SBNet.Util.DisableUI(false, masterPage.divCustomerFeedback.style.zIndex);

		var txtFeedBack = SBNet.Util.FindASPXControl("TEXTAREA",
		 "tbComment", masterPage.divCustomerFeedback);
		txtFeedBack.focus();
	}
	return false;
}

function _MasterPage_OnSubmit()
{
	//alert("_MasterPage_OnSubmit");
	if(masterPage.divCustomerFeedback != null)
	{
		masterPage.divCustomerFeedback.style.display = "none";
		SBNet.Util.EnableUI();
	
		var txtFeedBack = SBNet.Util.FindASPXControl("TEXTAREA",
		 "tbComment", masterPage.divCustomerFeedback);
		var text = txtFeedBack.value;

		text = SBNet.EC.entryTrim(text);
		
		var regExp = /[^\w\s'".,~`!@#$%\&\*\(\)\-\+=\{\}\[\]:;\?\\\/|]*/mg;
		text = "[FB]" + text.replace(regExp, "");

		//Send to server...
		_MasterPage_ServerCall(text, "FEEDBACK");

		//Clear feedback box.
		txtFeedBack.value = "";
		
		//Show thank you...
		SBNet.Util.ConfirmBox("Thank You", "Thank you for your feedback.", null, null, "okonly"); 
	}
	return false;
}

function _MasterPage_OnCancel()
{
	//alert("_MasterPage_OnCancel");

	masterPage.divCustomerFeedback.style.display = "none";
	SBNet.Util.EnableUI();

	return false;
}

function _MasterPage_ClientCallBack(rvalue, context)
{
	if(context == "SESSION")
	{
		//Nothing for now.
	}
	else
	if(context == "FEEDBACK")
	{
		//Nothing for now.
	}
	else
		alert("Unknown context: " + context + " - " + rvalue);
}

function _MasterPage_ClientErrorCallBack(message, context)
{
	alert(message);
}
