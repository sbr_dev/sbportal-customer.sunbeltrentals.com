using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.Tools;
using SBNet;

public partial class controls_leftnav : Sunbelt.Web.UserControl, SBNet.IWebControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
	}

	protected void Page_PreRender(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			//turn off https:
			HyperLink1.NavigateUrl = WebTools.ResolveServerUrl(HyperLink1.NavigateUrl, false);
			ReservationID.NavigateUrl = WebTools.ResolveServerUrl(ReservationID.NavigateUrl, false);
			HyperLink19.NavigateUrl = WebTools.ResolveServerUrl(HyperLink19.NavigateUrl, false);
			MakePaymentID.NavigateUrl = WebTools.ResolveServerUrl(MakePaymentID.NavigateUrl, false) + "?tab=Open+Invoices";
			HyperLink21.NavigateUrl = WebTools.ResolveServerUrl(HyperLink21.NavigateUrl, false);
			HyperLink22.NavigateUrl = WebTools.ResolveServerUrl(HyperLink22.NavigateUrl, false);
			hlCreateView.NavigateUrl = WebTools.ResolveServerUrl(hlCreateView.NavigateUrl, false);
			HyperLink11.NavigateUrl = WebTools.ResolveServerUrl(HyperLink11.NavigateUrl, false);
		}
	}

	#region IWebControl Members

	public void UpdateData(SBNet.AccountEventArgs e)
	{
		DocumentQuickLookup1.accountEventArgs = e;
		if (SiteUser.UserAccess.ToOnlinePayments)
		{
			if( AcceptingOnlinePayment() == false )
				MakePaymentID.Attributes["onclick"] = "javascript:SBNet.Util.ConfirmBox('Online Payment', 'Online payments is currently unavailable.  Check back shortly for availability.', null, null, 'okonly');return false;";
		}
		else
		{
			MakePaymentID.Attributes["onclick"] = "javascript:SBNet.Util.ConfirmBox('Online Payment', 'You currently are not setup to access Online Payment.  Please contact your site administrator.', null, null, 'okonly');return false;";
			MakePaymentID.NavigateUrl = Request.Url.AbsolutePath + "#";
		}

		if (SiteUser.UserAccess.ToRentalReservations)
		{
			ReservationID.NavigateUrl = "~/accountActivity/reservations.aspx";
		}
		else
		{
			ReservationID.Attributes["onclick"] = "javascript:SBNet.Util.ConfirmBox('Reservations', 'You currently are not setup to access Reservations.  Please contact your site administrator.', null, null, 'okonly');return false;";
			ReservationID.NavigateUrl = Request.Url.AbsolutePath + "#";
		}
	}

	#endregion

	private bool AcceptingOnlinePayment()
	{
		string PaymentsDisabled = ConfigParameters.GetConfigParameters("OnLinePaymentDisabled");
		if (PaymentsDisabled != "0")
			return false;

		return true;
	}
}
