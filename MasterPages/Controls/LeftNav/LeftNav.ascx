<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftNav.ascx.cs" Inherits="controls_leftnav" %>
<%@ Register src="~/SiteControls/DocumentQuickView/DocumentQuickLookup.ascx" tagname="DocumentQuickLookup" tagprefix="uc1" %>
<table id="LeftNav" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="SubHeader"><asp:Image ID="Image19" ImageUrl="~/MasterPages/Controls/LeftNav/Images/leftnav_accounttools.gif" width="197" height="46" runat="server" /></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText"><asp:HyperLink ID="HyperLink1" NavigateUrl="~/AccountTools/default.aspx" runat="server"><strong>ACCOUNT DASHBOARD</strong></asp:HyperLink></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText"><asp:HyperLink id="ReservationID" NavigateUrl="~/accountActivity/reservations.aspx" runat="server"><strong>RESERVATIONS</strong></asp:HyperLink></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText"><asp:HyperLink ID="HyperLink2" NavigateUrl="~/accountActivity/ExtendRental.aspx" runat="server"><strong>EXTEND RENTAL</strong></asp:HyperLink></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText">
		    <asp:HyperLink ID="HyperLink19" NavigateUrl="~/accountActivity/returnequipment.aspx" runat="server">
		        <strong>RETURN EQUIPMENT</strong>
		    </asp:HyperLink>
        </td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText">
    		<asp:HyperLink id="MakePaymentID" NavigateUrl="~/AccountTools/default.aspx"  runat="server"><strong>MAKE PAYMENT</strong></asp:HyperLink>
		</td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText"><asp:HyperLink ID="HyperLink21" NavigateUrl="~/accountActivity/requestservicecall.aspx" runat="server"><strong>REQUEST SERVICE CALL</strong></asp:HyperLink></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText"><asp:HyperLink ID="HyperLink22" NavigateUrl="~/AccountTools/MyJobsites.aspx" runat="server"><strong>MANAGE JOB SITES</strong></asp:HyperLink></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText">
		    &nbsp;<%--<asp:HyperLink ID="HyperLink22" NavigateUrl="~/accountActivity/setupalerts.aspx" runat="server"><strong>SETUP ALERTS</strong></asp:HyperLink>--%>
        </td>
	</tr>
	<tr>
		<td class="SubHeader"><asp:Image ID="imgReports" runat="server" ImageUrl="~/MasterPages/Controls/LeftNav/Images/leftnav_reports.gif"></asp:Image></td>
	</tr>
	<tr>
		<td class="SmallDarkGreyText">
		   <asp:HyperLink ID="hlCreateView" NavigateUrl="~/AccountReports/ReportBuilder/DynamicReporting.aspx" runat="server" Font-Bold="true">
		        CREATE/VIEW
		   </asp:HyperLink>
        </td>
	</tr>
	<%--<tr>
		<td class="SmallDarkGreyText">
		   &nbsp;<asp:HyperLink ID="hlScheduleReports" NavigateUrl="~/AccountReports/ReportBuilder/DynamicReporting.aspx" runat="server" Font-Bold="true">
		        SCHEDULED REPORTS
		   </asp:HyperLink>
        </td>
	</tr>--%>
	<tr>
		<td class="SmallDarkGreyText">
		    <asp:HyperLink ID="HyperLink11" NavigateUrl="~/accountReports/AccountSummary.aspx" runat="server" Font-Bold="true">
		        ACCOUNT SUMMARY
		   </asp:HyperLink>
        </td>
	</tr>
	<tr>
	    <td style="font-size: 1px; padding-top: 10px; border-bottom: solid 2px #ebebeb;">&nbsp;</td>
	</tr>
    <tr>
	    <td class="SmallDarkGreyText">
	         <strong>DOCUMENT QUICK VIEW</strong><br />
	         <uc1:DocumentQuickLookup ID="DocumentQuickLookup1" runat="server" />
	    </td> 
	</tr>
	<tr>
		<td style="height: 2em;">&nbsp;</td>
	</tr>
	<tr>
		<td class="SubHeader" style="padding-top: 125px;text-align: center;"><asp:Image ID="Image20" ImageUrl="~/MasterPages/Controls/LeftNav/Images/leftnav_sblogo.gif" width="162" height="67" runat="server" /></td>
	</tr>
</table>               