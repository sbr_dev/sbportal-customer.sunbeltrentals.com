<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserTopNav.ascx.cs" Inherits="_00_global_controls_userheadersubnav" %>

	         <asp:LoginView ID="UserTopNavView" runat="server">
               <LoggedInTemplate>
                    <table width="268" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left" class="MedWhiteText"><strong><asp:Label ID="lblFullName" runat="server" /></strong>&nbsp;(<asp:HyperLink ID="EditProfileLink" CssClass="SmallWhiteText" runat="server" NavigateUrl="~/UserProfile/EditProfile.aspx"><span class="SmallWhiteText">Edit My Profile</span></asp:HyperLink>)
                          <%--<br><asp:Label ID="lblCompany" runat="server" />--%>
                        </td>
                      </tr>
                      <tr>
                        <td align="left"><asp:Image ImageUrl="~/App_Themes/Main/images/clearpixel.gif" width="1" height="9" runat="server" /></td>
                      </tr>
                      <tr>
                        <td align="left">
                          <table width="50%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="75"><asp:HyperLink ID="LogoutLink" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_logout.gif" width="75" height="21" border="0" runat="server" /></td>
                              <td width="2"><asp:Image ID="imgSpacer01" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_spacer.gif" width="2" height="21" border="0" runat="server" /></td>
                              <td width="106"><asp:HyperLink ID="MyAccount" NavigateUrl="/" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_myaccount.gif" width="106" height="21" border="0" runat="server" /></td>
                              <td width="2"><asp:Image ID="imgSpacer02" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_spacer.gif" width="2" height="21" border="0" runat="server" /></td>
                              <td width="83"><asp:HyperLink ID="CheckoutLink" NavigateUrl="/equipment/cart/viewcart.aspx" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_checkout.gif" width="83" height="21" border="0" runat="server" /></td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
               </LoggedInTemplate>

               <AnonymousTemplate>
                  <table width="268" border="0" cellspacing="0" cellpadding="0">                   
                    <tr>
                      <td width="75"><asp:HyperLink ID="LoginLink" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_login.gif" width="75" height="21" border="0" runat="server" /></td>
                      <td width="2"><asp:Image ID="imgSpacer03" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_spacer.gif" width="2" height="21" border="0" runat="server" /></td>                    
                      <td width="106"><asp:HyperLink ID="AnonMyAccount" NavigateUrl="/" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_myaccount.gif" width="106" height="21" border="0" runat="server" /></td>
                      <td width="2"><asp:Image ID="imgSpacer04" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_spacer.gif" width="2" height="21" border="0" runat="server" /></td>
                      <td width="83"><asp:HyperLink ID="AnonCheckoutLink" NavigateUrl="/equipment/cart/viewcart.aspx" ImageUrl="~/MasterPages/Controls/UserTopNav/Images/usertopnav_checkout.gif" width="83" height="21" border="0" runat="server" /></td>
                    </tr>
                  </table>
               </AnonymousTemplate>
             </asp:LoginView>



