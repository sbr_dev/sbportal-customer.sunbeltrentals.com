using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;

public partial class _00_global_controls_userheadersubnav : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            HyperLink ToolsLink = (HyperLink)UserTopNavView.FindControl("MyAccount");
            HyperLink AnonToolsLink = (HyperLink)UserTopNavView.FindControl("AnonMyAccount");
			HyperLink LoginLink = (HyperLink)UserTopNavView.FindControl("LoginLink");
			HyperLink LogoutLink = (HyperLink)UserTopNavView.FindControl("LogoutLink");
            HyperLink EditProfileLink = (HyperLink)UserTopNavView.FindControl("EditProfileLink");           

            if (ToolsLink != null)
            { ToolsLink.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(ConfigurationManager.AppSettings["CustomerExtraNet"]); }

            if (AnonToolsLink != null)
            { AnonToolsLink.NavigateUrl = ConfigurationManager.AppSettings["CustomerExtraNet"]; }

            if (LoginLink != null)
            { LoginLink.NavigateUrl = ConfigurationManager.AppSettings["LoginPage"]; }

			if (LogoutLink != null)
			{ LogoutLink.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(ConfigurationManager.AppSettings["LogoutPage"],false); }
            
			HyperLink CheckoutLink = (HyperLink)UserTopNavView.FindControl("CheckoutLink");
			if (CheckoutLink != null)
			{ CheckoutLink.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + "/equipment/cart/viewcart.aspx"; }
			
			HyperLink AnonCheckoutLink = (HyperLink)UserTopNavView.FindControl("AnonCheckoutLink");
			if (AnonCheckoutLink != null)
			{ AnonCheckoutLink.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + "/equipment/cart/viewcart.aspx"; }

            if (EditProfileLink != null)
            {
                bool httpsRequired = Convert.ToInt16(ConfigurationManager.AppSettings["httpsRequired"]) == 1 ? true :false ;
                EditProfileLink.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(EditProfileLink.NavigateUrl, httpsRequired); 
            }

#if false
			if (EditProfileLink != null)
            {
                if (FullName == String.Empty)
                {
                    EditProfileLink.Visible = false;
                }
                else
                {
                    EditProfileLink.NavigateUrl = "EditProfileLinkGoesHere.aspx";
                    EditProfileLink.Visible = true;
                }
            }
#endif
        }
		// Load the controls with the user's information
		// -----------------------------------------------
		//FullName = SBNet.SiteUser.Name;
		//if (HttpContext.Current.Request.Cookies["SiteUser"] != null)
		//{
		//    HttpCookie cookie = HttpContext.Current.Request.Cookies["SiteUser"];

		//    FullName = cookie["Name"].ToString();
		//    //Company = cookie["CompanyName"].ToString();
		//}
		// -----------------------------------------------

	}

	protected override void OnPreRender(EventArgs e)
	{
		FullName = SiteUser.FirstName + " " + SiteUser.LastName;
		
		base.OnPreRender(e);
	}
	
	public string FullName
    {
        set
        {
            Label lblFullName = (Label)UserTopNavView.FindControl("lblFullName");
            if (lblFullName != null)
            { lblFullName.Text = value; }
        }
#if false 
		get
        {
            Label lblFullName = (Label)UserTopNavView.FindControl("lblFullName");
            if (lblFullName != null)
            { return lblFullName.Text; }
            else
            { return String.Empty; }
        }
#endif
    }

#if false
	public string Company
    {
        set
        {
            Label lblCompany = (Label)UserTopNavView.FindControl("lblCompany");
            if (lblCompany != null)
            { lblCompany.Text = value; }
        }
    }
#endif

	public void UpdateDisplay()
	{
		//tdLogin.Visible = !Page.User.Identity.IsAuthenticated;
		//tdLogout.Visible = Page.User.Identity.IsAuthenticated;
	}
}
