<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Footer.ascx.cs" Inherits="_00_global_controls_footer" %>

<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center" valign="top"><asp:Image ID="Image1" runat="server" ImageUrl="~/APP_Themes/main/images/clearpixel.gif" Width="1" Height="30" /></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#CCCCCC"><asp:Image ID="Image2" runat="server" ImageUrl="~/APP_Themes/main/images/clearpixel.gif" Width="1" Height="1" /></td>
  </tr>
  <tr>
    <td align="center" valign="top"><asp:Image ID="Image3" runat="server" ImageUrl="~/APP_Themes/main/images/clearpixel.gif" Width="1" Height="8" /></td>
  </tr>
  <tr>
	<td align="center" valign="top" class="TinyLightGreyText" style="padding-bottom: 4px">
	<asp:HyperLink ID="hlHome" runat="server">Home</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlEquipment"
	 runat="server">Equipment</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlServices"
	 runat="server">Services</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlLocations"
	 runat="server">Locations</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlCustomerService"
	 runat="server">Customer Service</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlAboutSunbelt"
	 runat="server">About Sunbelt</asp:HyperLink>&nbsp;|&nbsp;<asp:HyperLink ID="hlNews"
	 runat="server">News</asp:HyperLink>
	 </td>
  </tr>
  <tr>
    <td align="center" valign="top" class="SmallDarkGreyText" style="padding-bottom: 12px">� <asp:Label ID="copyyear" runat="server" /> Sunbelt Rentals. All rights reserved. Privacy Policy. Sunbelt Locations Toll-Free 800-667-9328</td>
  </tr>  
</table>