using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _00_global_controls_footer : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
	    if (!Page.IsPostBack)
	    {
		    copyyear.Text = DateTime.Now.Year.ToString();

			string webRoot = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
			hlHome.NavigateUrl = webRoot + "/";
			hlEquipment.NavigateUrl = webRoot + "/equipment/";
			hlServices.NavigateUrl = webRoot + "/services/";
			hlLocations.NavigateUrl = webRoot + "/locations/";
			hlCustomerService.NavigateUrl = webRoot + "/customerservice/";
			hlAboutSunbelt.NavigateUrl = webRoot + "/about/";
			hlNews.NavigateUrl = webRoot + "/about/newsroom/";
	    }
    }
}
