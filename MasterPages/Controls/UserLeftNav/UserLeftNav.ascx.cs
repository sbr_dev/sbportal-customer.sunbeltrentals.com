using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt.BusinessObjects;
using Sunbelt.Tools;
using SBNet;
using SBNet.Extensions;


public partial class controls_navaccountsummary : System.Web.UI.UserControl
{
    #region Members

    public EventHandler<SBNet.AccountEventArgs> ItemSelected;

	public TimeSpan LoadTime;

	//NOTE: all the SessionCache properties below a need "ULN_" prefix. SBNetSqlMembershipProvider
	// logout will clear it.
	
	private int _accountNumber
	{
		set { WebTools.SetSessionCache("ULN_acc", value, new TimeSpan(0, Session.Timeout, 0)); }
		get { return (int)WebTools.GetSessionCache("ULN_acc", 0); }
	}
	private string _userName
	{
		set { WebTools.SetSessionCache("ULN_user", value, new TimeSpan(0, Session.Timeout, 0)); }
		get { return (string)WebTools.GetSessionCache("ULN_user", ""); }
	}
	private UserCustomerAccounts _uca
	{
		set { WebTools.SetSessionCache("ULN_uca", value, new TimeSpan(0, Session.Timeout, 0)); }
		get { return (UserCustomerAccounts)WebTools.GetSessionCache("ULN_uca", null); }
	}
	private Customer _cust
	{
		set { WebTools.SetSessionCache("ULN_cust", value, new TimeSpan(0, Session.Timeout, 0)); }
		get { return (Customer)WebTools.GetSessionCache("ULN_cust", null); }
	}

	#endregion

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        UserAdministration.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(UserAdministration.NavigateUrl, false);  //force http:
		if (!(IsPostBack))
		{
			if ((Visible == true) && Page.User.Identity.IsAuthenticated)
			{
				//Check for new user.
				if (_userName != Page.User.Identity.Name)
				{
					ClearCacheData();
					_userName = Page.User.Identity.Name;

				}
				LoadCompanyAccounts(_userName);

				SelectedItem(Convert.ToInt32(_accountNumber), _cust.CorpLinkNumber);
			}
			else
			{
				ClearCacheData();
			}
		}


	
		qcAccount.GoClick += qcAccount_OnGoClick;
		qcAccount.ValidateTextBoxValue = CheckAccountNumber;

		SBNet.Tools.IncludeSBNetClientScript(Page);
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
	}
	
	private void ClearCacheData()
	{
		//Clear all saved data.
#if false
		_accountNumber = 0;
		_userName = "";
		//Can't set cache value to null, so call RemoveCache for _uca.
		WebTools.RemoveSessionCache("_uca");
		//Same for _cust.
		WebTools.RemoveSessionCache("_cust");
#else
		WebTools.RemoveSessionCacheByPrefix("ULN_");
#endif
	}

	#endregion

    #region Data Methods

	public void RefreshCompanyAccounts()
	{
		_accountNumber = 0;
		LoadCompanyAccounts(_userName);
	}

	private void LoadCompanyAccounts(string userName)
    {
		DateTime start = DateTime.Now;

		SBNet.UserProfile up = SBNet.SBNetSqlMembershipProvider.CurrentUser;
		SiteUser.FirstName = up.firstName;
		SiteUser.LastName = up.lastName;

		//If anything missing, get all the data.
		if (_uca == null || _accountNumber == 0 || _cust == null)
		{
			ClearCacheData();
			_userName = userName;

			_uca = CustomerData.GetUserCustomerAccounts(userName, 1);
			if (_uca.Count > 0)
			{
				_accountNumber = _uca[0].AccountKey;

				if (up.ebClock == 0) //Customer
				{
					//Try to set the account to the one in the cookie. Must be in the list.
					string cookieAccount = WebTools.GetCookie("Settings", "Account", _accountNumber.ToString());
					foreach (Sunbelt.BusinessObjects.UserCustomerAccount u in _uca)
					{
						if (u.AccountNumber.ToString() == cookieAccount)
						{
							_accountNumber = u.AccountNumber;
							break;
						}

					}
				}
			}
			_cust = CustomerData.GetCustomer(Math.Abs(_accountNumber), 1);

		}
		LoadCompanyDetail(_cust);

		//Adjust dislay, employee vs. customer
		divCustomerDDL.Visible = false;
		divEmployeeDDL.Visible = false;
		if (up.ebClock > 0)	//Employee
		{
			divEmployeeDDL.Visible = true;
			ddlAccount.Items.Clear();

			qcAccount.Text = Math.Abs(_accountNumber).ToString();
		}
		else //Customer
		{
			divCustomerDDL.Visible = true;
			qcAccount.Text = "";

			ddlAccount.DataSource = _uca;
			ddlAccount.DataTextField = "AccountNumber";
			ddlAccount.DataValueField = "AccountKey";
			ddlAccount.DataBind();

			ddlAccount.SelectedValue = _accountNumber.ToString();
		}
		LoadTime = DateTime.Now - start;
	}

	private void BindAccountDropDown()
	{
		ddlAccount.DataSource = _uca;
		ddlAccount.DataTextField = "AccountNumber";
		ddlAccount.DataValueField = "AccountKey";
		ddlAccount.DataBind();

		try
		{
			ddlAccount.SelectedValue = WebTools.GetCookie("Settings", "Account", _accountNumber.ToString());
			_accountNumber = Convert.ToInt32(ddlAccount.SelectedValue);
		}
		catch
		{
			ddlAccount.SelectedValue = _accountNumber.ToString();
		}
	}

	private void ClearCompanyDetail()
	{
		lblCompanyName.Text = string.Empty;
		lblContactName.Text = string.Empty;
		lblContactName.ToolTip = string.Empty;
		lblAddress1.Text = string.Empty;
		lblAddress2.Text = string.Empty;
		lblPhone.Text = string.Empty;
	}

	private void LoadCompanyDetail(Customer CustomerDetail)
    {
		ClearCompanyDetail();
        if ((CustomerDetail != null) && (CustomerDetail.AccountNumber > 0))
        {
            lblCompanyName.Text = CustomerDetail.CompanyName.Trim();
			
			string contactName = CustomerDetail.ContactName.Trim();
			lblContactName.Text = contactName.TruncateNonWrappingText(20);
			if (lblContactName.Text.Length != contactName.Length)
				lblContactName.ToolTip = contactName;

			lblAddress1.Text = CustomerDetail.Address.Address1.Trim();
			lblAddress2.Text = CustomerDetail.Address.City.Trim() + ",&nbsp;" + CustomerDetail.Address.State.Trim() + "  " + CustomerDetail.Address.Zip.Trim();
			lblPhone.Text = CustomerDetail.PrimaryPhone.Trim().Replace(")", ") "); ;
			lblFax.Text = CustomerDetail.FaxNumber.Trim().Replace(")", ") ");
			if (SiteUser.AccountID != CustomerDetail.AccountNumber)
			{
				SiteUser.AccountID = CustomerDetail.AccountNumber;
				//If the account changes, update user's SecurityAccess.
				// It will reload itself next time it is accessed.
				SiteUser.UserAccess = null;
				//Set the can rent cookie.
				SiteUser.CanRent = SiteUser.UserAccess.ToRentalReservations;
			}
			if (SiteUser.CorpLinkID != CustomerDetail.CorpLinkNumber)
				SiteUser.CorpLinkID = CustomerDetail.CorpLinkNumber;
			SiteUser.Company = CustomerDetail.CompanyName.Trim();
			SBNet.UserProfile up = SBNet.SBNetSqlMembershipProvider.CurrentUser;
			lnkEditBillingInfo.Visible = CustomerDetail.canEditBilling || up.ebClock != 0;
        }
    }

    #endregion

	#region Control Events

	protected void OnSelected_Change(object sender, EventArgs e)
	{
		//Remember the selected account.
		WebTools.SetCookie("/", "Settings", "Account", ddlAccount.SelectedValue, 30, 0);
	
		//New account, new customer.
		string account = ddlAccount.SelectedItem.Value;
		_accountNumber = Convert.ToInt32(account);
		_cust = CustomerData.GetCustomer(Math.Abs(_accountNumber), 1);
		LoadCompanyDetail(_cust);

		SelectedItem(_accountNumber, _cust.CorpLinkNumber);
	}

	public void SelectedItem(int accountNumber, int corpLink)
	{
		EventHandler<SBNet.AccountEventArgs> eh = ItemSelected;
		if (eh != null)
			ItemSelected(this, new SBNet.AccountEventArgs(accountNumber, corpLink, 1));
	}

	protected string CheckAccountNumber(string accountValue)
	{
		SBNet.QuickAccountInfo qai = null;
		int account = Math.Abs(ConvertTools.ToInt32(accountValue));

		if (account == Math.Abs(_accountNumber))
			return "SAME";
		if(account > 0)
			qai = SBNet.Portal.GetCustomerAccount(account);
		if (qai == null)
			return "Account '" + accountValue + "' does not exist. Please enter a valid account.";
		return "";
	}
	
	protected void qcAccount_OnGoClick(object sender, QueryAccountControlEventArgs e)
	{
		SBNet.QuickAccountInfo qai = null;
		int account = Math.Abs(ConvertTools.ToInt32(e.TextboxValue));

		//Verify account.
		if (account > 0)
			qai = SBNet.Portal.GetCustomerAccount(account, 1);
		if (qai != null)
		{
			if (qai.IsCorpLink)
				account *= -1;

			if (account != _accountNumber)
			{
				//Change their default account.
				string userName = Page.User.Identity.Name;

				//Delete current account(s).
				SBNet.UserSecurity.DeleteSecurityAccess(userName, null, 1);
				//Delete cash customer account(s).
				SBNet.UserSecurity.DeleteCashCustomerAccount(userName, null, null);

				//Set this as the user's account.
				SBNet.UserCustomerAccount uca = new SBNet.UserCustomerAccount(qai);
				SBNet.SecurityAccess sa = new SBNet.SecurityAccess();
				sa.AccountNumber = Math.Abs(account);
				sa.SelectAll();
				sa.ToAccountAdmin = false;

				UserProfile up = SBNetSqlMembershipProvider.CurrentUser;
				try
				{
					Sunbelt.Security.SecureNameValueCollection snvc = new Sunbelt.Security.SecureNameValueCollection();
					snvc["ClockNumber"] = up.ebClock.ToString();

					SunbeltWebServices.Authentication auth = new SunbeltWebServices.Authentication();
					SunbeltWebServices.UserInfo ui = auth.GetUserByClockID(snvc.ToString());

					sa.ToAccountAdmin = ui.IsCustomerService;
				}
				catch (Exception ex)
				{
					//For now ignore the "(SAMAccountName=) search filter is invalid" error.
					if (ex.Message.IndexOf("(SAMAccountName=) search filter is invalid") == -1)
					{
						//Log it.
						Sunbelt.Log.LogError(ex, "SunbeltWebServices.Authentication.GetUserByClockID failed for " + up.ebClock.ToString());
					}
				}

				//Insert new record
				SBNet.UserSecurity.InsertSecurityAccess(userName, uca, sa);


				/******************************************/
				/* Do same thing as OnSelected_Change(..) */
				/******************************************/

				//New account, new customer.
				_accountNumber = account;
				_cust = CustomerData.GetCustomer(Math.Abs(account), 1);
				LoadCompanyDetail(_cust);

				SelectedItem(_accountNumber, _cust.CorpLinkNumber);

				/******************************************/
				/* Done */
				/******************************************/
				
				//Reload.
				//ClearCacheData();
				//LoadCompanyAccounts(userName);
			}
			//Fire event.
			//SelectedItem(Convert.ToInt32(account));
		}
	}

	#endregion
}
