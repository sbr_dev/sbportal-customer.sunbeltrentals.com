using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Sunbelt.Common.DAL;
using Sunbelt.Tools;

public partial class UserLeftNav_QueryAccount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		StringBuilder sb = new StringBuilder();    
		
		Response.Clear();

		sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?><SearchResult>");

		string text = HttpUtility.UrlDecode(Request.QueryString["search"]);
        string company = HttpUtility.UrlDecode(Request.QueryString["c"]) ?? "1";
        int companyID = Convert.ToInt32(company);

        if (!string.IsNullOrEmpty(text))
		{
			DataTable dt = new DataTable();

			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("BusinessObjects.sb_LookupCustomerAccounts", conn)) 
			{
				if (Regex.IsMatch(text, "\\D"))
				{
					sp.AddParameterWithValue("@CustomerNamePattern", SqlDbType.VarChar, 30, ParameterDirection.Input, text.ToUpper());
					sp.AddParameterWithValue("@RowCount", SqlDbType.Int, 4, ParameterDirection.Input, 20);
                    sp.AddParameterWithValue("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, companyID);
                    dt = sp.ExecuteDataTable();
				}
				else
				{
					int accountNumber = ConvertTools.ToInt32(text);
					if (accountNumber > 0)
					{
						sp.AddParameterWithValue("@AccountNumberPattern", SqlDbType.Int, 4, ParameterDirection.Input, ConvertTools.ToInt32(text));
						sp.AddParameterWithValue("@RowCount", SqlDbType.Int, 4, ParameterDirection.Input, 20);
                        sp.AddParameterWithValue("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, companyID);
                        dt = sp.ExecuteDataTable();
					}
				}
			}
			foreach (DataRow row in dt.Rows)
			{
				sb.Append(string.Format("<item><value>{0}</value><number>{0}</number><corplink>{1}</corplink><customer>{2}</customer><iscl>{3}</iscl><status>{4}</status><companyID>{5}</companyID></item>",
					row["AccountNumber"].ToString().Trim(),
					row["CorpLinkNumber"].ToString().Trim(),
					WebTools.XmlEncode(row["CustomerName"].ToString().Trim()),
					row["IsCorpLinkAccount"].ToString().Trim().ToLower(),
					row["Status"].ToString().Trim(), companyID)  );
			}
		}

		sb.Append("</SearchResult>");
		Response.Write(sb.ToString());
		Response.End();
	}
}
