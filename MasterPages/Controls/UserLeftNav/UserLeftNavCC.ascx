﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserLeftNavCC.ascx.cs" Inherits="MasterPages_Controls_UserLeftNav_UserLeftNavCC" %>

<table id="UserLeftNav" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="Spacer" style="width: 161px;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 161px"><asp:Label ID="lblUserName" runat="server" font-bold="true" Text=""></asp:Label></td>
    </tr>
    <tr>
        <td class="Spacer" style="width: 161px;">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 161px">
            <span style="font-weight: bold;">
                Billing Address:
            </span>
        </td>
    </tr>
    <tr>
        <td style="width: 161px"><table class="Address" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblAddress1" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblAddress2" runat="server" Text=""></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAddress3" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
				<tr>
					<td  style="width: 161px;line-height: 1px;">&nbsp;</td>
					</tr>
						<tr>
							<td style="width: 161px">
								<table  class="Phone" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td align="right"><strong>Phone:</strong></td>
										<td align="left"><asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></td>
									</tr>
									<tr>
										<td align="right"><strong>Fax:</strong></td>
										<td align="left"><asp:Label ID="lblFax" runat="server" Text=""></asp:Label></td>
									</tr>
								</table>
							</td>
						</tr>
            </table></td>
    </tr>
    <tr>
        <td class="Bottom" style="width: 161px">&nbsp;</td>
    </tr>
</table>
