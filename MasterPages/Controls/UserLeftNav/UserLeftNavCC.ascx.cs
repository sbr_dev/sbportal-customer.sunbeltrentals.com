﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SBNet;

public partial class MasterPages_Controls_UserLeftNav_UserLeftNavCC : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			SetUserInfo();
		}
    }

	private void SetUserInfo()
	{
		UserProfile up = SBNetSqlMembershipProvider.CurrentUser;

		if (up != null)
		{
			lblUserName.Text = up.firstName + " " + up.lastName;
			lblAddress1.Text = up.address1;
			lblAddress2.Text = up.address2;
			if (!string.IsNullOrEmpty(up.address3))
				lblAddress2.Text += "<br />" + up.address3;

            // Add City, State, Zip
            lblAddress3.Text = up.city.Trim() + ", " + up.state.Trim() + " " + up.zip.Trim();

			lblPhone.Text = PhoneFormatter(up.phone);
			lblFax.Text = PhoneFormatter(up.fax);
		}
	}

    /// <summary>
    /// Formats a valid 7 or 10 digit numeric string as a phone number in (999)999-9999 format.
    /// </summary>
    /// <param name="PhoneNumber">
    /// The string to format
    /// </param>
    /// <returns>
    /// System.String
    /// </returns>
    private string PhoneFormatter(string PhoneNumber)
    {
        // First check to see if the string is a valid phone number
        if (!(PhoneNumber.Length == 10 || PhoneNumber.Length == 7))
        {
            // If the string is not a valid PhoneNumber, take no action
            // just return the original string.
            return PhoneNumber;
        }

        StringBuilder sb = new StringBuilder(PhoneNumber);

        if (sb.Length == 10)
        {
            sb.Insert(0, "(");
            sb.Insert(4, ") ");
            sb.Insert(9, "-");
        }

        if (sb.Length == 7)
        {
            sb.Insert(3, "-");
        }

        return sb.ToString();
    }
}
