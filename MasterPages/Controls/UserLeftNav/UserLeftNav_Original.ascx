<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserLeftNav.ascx.cs" Inherits="controls_navaccountsummary" %>

<%@ Register src="~/SiteControls/QueryAccountControl/QueryAccountControl.ascx" tagname="QueryAccountControl" tagprefix="uc2" %>

<%--<asp:UpdatePanel ID='upd1' UpdateMode='Conditional' runat='server'>
    <ContentTemplate>--%>
        <table id="UserLeftNav" cellpadding="0" cellspacing="0" border="0">
<%--            <tr>
                <td style="width: 161px"><br />
                   <asp:Label ID='lblContactName' runat='server' Font-Bold="true" Text=''></asp:Label>
                    </td>
            </tr>--%>
            <tr>
                <td style="width: 161px"><br />
                    <asp:Label ID="lblCompanyName" runat="server" Font-Bold="true" Text=""></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 161px; white-space: nowrap;">
                    <div id="divCustomerDDL" runat="server">Account: <asp:DropDownList ID="ddlAccount" OnSelectedIndexChanged='OnSelected_Change' runat="server" Font-Bold="true"
                        Width="80px" CssClass="SmallBlackText" AutoPostBack="True"></asp:DropDownList></div>
                    <div id="divEmployeeDDL" runat="server" style="white-space: nowrap;">Account: <uc2:QueryAccountControl ID="qcAccount" runat="server"
						QueryUrl="~/MasterPages/Controls/UserLeftNav/QueryAccount.aspx" ClientOnRowBound="_ULN.AccountOnRowBound" Width="6em" FontSize="8pt"
						ValueCssClass="SuggestValueText" DoShowMore="False" SearchText="" SearchPadding="1px 10px"
						ClientCallServer="_ULN_CheckAccountNumber" ClientCallBack="_ULN.ReceiveAccountData"
						ClientErrorCallBack="_ULN.OnServerError" ClientOnGoClick="_ULN.OnGoClick" /></div>
                    </td>
            </tr>
            <tr>
                <td class="Link2" style="width: 161px; height:20px">
                    <strong>Billing Address:</strong><span runat="server" visible="false">&nbsp;(<asp:HyperLink ID="lnkEditBillingInfo" class="Link" NavigateUrl="~/AccountTools/BillingInfo.aspx" runat="server"><strong>Editx</strong></asp:HyperLink>)</span> </td>
            </tr>
            <tr>
                <td style="width: 161px;">
                    <table class="Address" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAddress1" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblAddress2" runat="server" Text=""></asp:Label></td>
                        </tr>
						<tr>
							<td  style="width: 161px;line-height: 1px;">&nbsp;</td>
						</tr>
						<tr>
							<td style="width: 161px">
								<table  class="Phone" cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td align="right"><strong>Phone:</strong></td>
										<td align="left"><asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></td>
									</tr>
									<tr>
										<td align="right"><strong>Fax:</strong></td>
										<td align="left"><asp:Label ID="lblFax" runat="server" Text=""></asp:Label></td>
									</tr>
								</table>
							</td>
						</tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="Link" style="width: 161px;height:5px"></td>
            </tr>
            <tr>
                <td class="Line" style="width: 161px">&nbsp;</td>
            </tr>
            <tr>
                <td class="Link" style="width: 161px">
                    <asp:HyperLink ID="UserAdministration" NavigateUrl="~/UserAdmin/Default.aspx" runat="server"><strong>User Administration</strong></asp:HyperLink></td>
            </tr>
            <tr>
                <td class="Bottom" style="width: 161px">
                    &nbsp;</td>
            </tr>
        </table>
<%--    </ContentTemplate>
</asp:UpdatePanel>
--%>

<script type="text/javascript" language="javascript">
//<![CDATA[

var _ULN = {

	OnGoClick:function()
	{
		var textBox = document.getElementById("<% =qcAccount.TextBoxControl.ClientID %>");
		if(textBox.value.length > 0)
		{
			_ULN_CheckAccountNumber(textBox.value, "CHECK");
		}
		return false;
	},

	ReceiveAccountData:function(rvalue, context)
	{
		if(context == "CHECK")
		{
			if(rvalue.length > 0)
			{
				if(rvalue != "SAME")
					SBNet.Util.ConfirmBox("Account Lookup", rvalue, null, null, "okonly");
			}
			else
				<% =Page.ClientScript.GetPostBackEventReference(qcAccount.GoButtonControl, "") %>;
		}
		else
			alert("Unknown context: " + context + " - " + rvalue);
	},

	OnServerError:function(message, context)
	{
		alert(message);
	},


	//// Event handlers for the query control ////
	AccountOnRowBound:function(div)
	{
		var spcl = _QueryControl_getSpanById("iscl", div);
		var bold = (spcl.innerHTML == "true");
		
		var spv = _QueryControl_getSpanById("value", div);
		var stat = _QueryControl_getSpanById("status", div);
		if(bold)
			spv.innerHTML = "<b>" + spv.innerHTML + "</b>";
		
		var spc = _QueryControl_getSpanById("customer", div);
		spc.style.display = "";
		if(bold)
			spc.innerHTML =  "&nbsp;<b>(" + stat.innerHTML + ")</b>" + "&nbsp;-&nbsp;" + spc.innerHTML;
		else	
			spc.innerHTML =  "&nbsp;(" + stat.innerHTML + ")" + "&nbsp;-&nbsp;" + spc.innerHTML;
		if(bold)
			spc.innerHTML = "<b>" + spc.innerHTML + "</b>";
	}
}

//]]>
</script>