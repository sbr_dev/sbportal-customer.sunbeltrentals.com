<%@ Control Language="C#" AutoEventWireup="true" CodeFile="topNav.ascx.cs" Inherits="controls_topNav" %>

<%@ Register TagPrefix="Sunbelt" TagName="TopNavSearch" Src="~/MasterPages/Controls/TopNavSearch/TopNavSearch.ascx" %>

<table id="TopNav" border="0" cellspacing="0" cellpadding="0" style="Width: 100%;">
	<tr>
		<td><asp:Image ID="GlobalSiteAnchor" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="52px" Height="37px"  /></td>

		<td><asp:HyperLink ID="Nav_Equipment" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_equipment.gif" NavigateUrl="/equipment/default.aspx" Width="95px" Height="37px" /></td>

		<td><asp:Image ID="imgNavBG01" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="28" Height="37" /></td>

		<td><asp:HyperLink ID="Nav_Services" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_services.gif" NavigateUrl="/services/" Width="85" Height="37" /></td>

		<td><asp:Image ID="imgNavBG02" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="28" Height="37" /></td>

		<td><asp:HyperLink ID="Nav_Locations" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_locations.gif" NavigateUrl="/locations/" Width="92" Height="37" /></td>

		<td><asp:Image ID="imgNavBG03" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="28" Height="37" /></td>

		<td><asp:HyperLink ID="Nav_CustomerService" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_customerservices.gif" NavigateUrl="/customerservice/" Width="149" Height="37" /></td>

		<td><asp:Image ID="imgNavBG04" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="28" Height="37" /></td>

		<td><asp:HyperLink ID="Nav_AboutSunbelt" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_aboutsunbelt.gif" NavigateUrl="/about/" Width="121" Height="37" /></td>

		<td><asp:Image ID="imgNavBG05" runat="server" ImageUrl="~/MasterPages/Controls/TopNav/Images/nav_bg.gif" Width="66" Height="37" /></td>

		<td><Sunbelt:TopNavSearch runat="server" ID="TopNavSearch" /></td>

		<%--<td style="Width: 100%;">&nbsp;</td>--%>
	</tr>
</table>