using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

public partial class controls_topNav : System.Web.UI.UserControl
{
	public bool TopSearchVisible
	{
		set { TopNavSearch.Visible = value; }
		get { return TopNavSearch.Visible; }
	}
	
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!Page.IsPostBack)
		{
			// Setup the cross site links
			// -------------------------------------------
			Nav_Equipment.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + Nav_Equipment.NavigateUrl;
			Nav_Services.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + Nav_Services.NavigateUrl;
			Nav_Locations.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + Nav_Locations.NavigateUrl;
			Nav_CustomerService.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + Nav_CustomerService.NavigateUrl;
			Nav_AboutSunbelt.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"] + Nav_AboutSunbelt.NavigateUrl;
			// -------------------------------------------
			SetupNav();
		}
	}

	private string _ActiveNavSection = String.Empty;
	public string ActiveNavSection
	{
		get { return _ActiveNavSection; }
		set { _ActiveNavSection = value; }
	}

	private void SetupNav()
	{
		string strActiveNavSection = GetActiveNavSection();

		HyperLink imgActive = (HyperLink)this.FindControl(strActiveNavSection);

		if (imgActive != null)
		{
			switch (GetActiveNavSection())
			{
				case "Nav_Equipment":
					imgActive.ImageUrl = "~/MasterPages/Controls/TopNav/Images/nav_equipment_on.gif";
					break;

				case "Nav_Services":
					imgActive.ImageUrl = "~/MasterPages/Controls/TopNav/Images/nav_services_on.gif";
					break;

				case "Nav_Locations":
					imgActive.ImageUrl = "~/MasterPages/Controls/TopNav/Images/nav_locations_on.gif";
					break;

				case "Nav_CustomerService":
					imgActive.ImageUrl = "~/MasterPages/Controls/TopNav/Images/nav_customerservices_on.gif";
					break;

				case "Nav_AboutSunbelt":
					imgActive.ImageUrl = "~/MasterPages/Controls/TopNav/Images/nav_aboutsunbelt_on.gif";
					break;
			}
		}
	}

	private string GetActiveNavSection()
	{
		string strReturnVal = String.Empty;

		if (_ActiveNavSection == String.Empty)
		{
			string strRootFolder = String.Empty;
			strRootFolder = Path.GetDirectoryName(HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"].ToString());

			switch (strRootFolder)
			{
				case "\\equipment":
					strReturnVal = "Nav_Equipment";
					break;

				case "\\services":
					strReturnVal = "Nav_Services";
					break;

				case "\\locations":
					strReturnVal = "Nav_Locations";
					break;

				case "\\customerservice":
					strReturnVal = "Nav_CustomerService";
					break;

				case "\\about":
					strReturnVal = "Nav_AboutSunbelt";
					break;
			}
		}
		else
		{
			strReturnVal = _ActiveNavSection;
		}

		return strReturnVal;
	}
}
