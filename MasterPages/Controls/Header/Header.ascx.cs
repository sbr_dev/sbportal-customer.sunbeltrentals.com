using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class controls_header : System.Web.UI.UserControl
{
	public bool TopSearchVisible
	{
		set { TopNav.TopSearchVisible = value; }
		get { return TopNav.TopSearchVisible; }
	}

	public bool TopNavVisible
    {
        get { return TopNav.Visible; }
        set { TopNav.Visible = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
	}

	public void UpdateDisplay()
	{
		UserTopNav.UpdateDisplay();
	}
}
