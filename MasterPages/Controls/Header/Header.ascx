<%@ Control Language="C#" AutoEventWireup="true" CodeFile="header.ascx.cs" Inherits="controls_header" %>
<%@ Register TagPrefix="Sunbelt" TagName="TopNav" Src="~/MasterPages/Controls/TopNav/topNav.ascx" %>
<%@ Register TagPrefix="Sunbelt" TagName="UserTopNav" Src="~/MasterPages/Controls/UserTopNav/UserTopNav.ascx" %>

      <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; border-bottom-style: solid; border-bottom-width: 8px; border-bottom-color: #cccccc">
		<tr>
			<td><Sunbelt:TopNav ID="TopNav" runat="server" /></td>
		</tr>
		<tr>
			<td style="border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #FFFFFF">
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr valign="top">
				  <td><asp:Image ID="Image1" ImageUrl="~/MasterPages/controls/header/images/mast_main.gif" width="216" height="95" border="0" usemap="#SBLogoMap" runat="server" /></td>
				  <td class="HeaderBGImage" width="100%" align="right" valign="bottom">
					<%--<Sunbelt:UserTopNav ID="UserTopNav" runat="server" />--%>
				  <table>
<!--						  <tr><td valign="top" height="39" >
						  <asp:HyperLink ImageUrl="~/Images/TryNewSiteButton.png"  NavigateUrl="http://beta.sunbeltrentals.com" runat="server" /></td></tr>
-->						  
						  <tr><td><Sunbelt:UserTopNav ID="UserTopNav" runat="server" /></td></tr>
				  </table>
					
				  </td>
				</tr>
			  </table>	  
			</td>
		</tr>
      </table>
	  <map name="SBLogoMap">
        <area shape="rect" coords="64,1,187,95" href="<%=ConfigurationManager.AppSettings["SUNBELT_ROOT"]%>/">
	  </map>
