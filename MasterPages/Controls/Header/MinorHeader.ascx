<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MinorHeader.ascx.cs" Inherits="controls_header" %>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top">
          <td width="10%"><asp:Image ID="Image1" ImageUrl="~/MasterPages/controls/header/images/mast_main.gif" width="216" height="95" border="0" usemap="#SBLogoMap" runat="server" /></td>
          <td class="HeaderBGImage" width="90%" align="right" valign="bottom">
             <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr></tr>
             </table>          
          </td>
		</tr>
	  </table>	  
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><asp:Image ID="Image2" ImageUrl="~/App_Themes/Main/images/clearpixel.gif" width="1" height="1" runat="server" /></td>
		</tr>
        <tr>
          <td bgcolor="#cccccc"><asp:Image ID="Image3" ImageUrl="~/App_Themes/Main/images/clearpixel.gif" width="1" height="8" runat="server" /></td>
		</tr>
	  </table>
	  <map name="SBLogoMap">
        <area shape="rect" coords="64,1,187,95" href="<%=ConfigurationManager.AppSettings["SUNBELT_ROOT"]%>/">
	  </map>
