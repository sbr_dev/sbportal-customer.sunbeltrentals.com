using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Sunbelt.BusinessObjects.Web;
using Sunbelt.Utilities;
using System.Text;

public partial class _00_global_controls_topnavsearch : System.Web.UI.UserControl
{
	public string WebRoot
	{
		get
		{
			if (ConfigurationManager.AppSettings["SUNBELT_ROOT"] != null)
			{ return Convert.ToString(ConfigurationManager.AppSettings["SUNBELT_ROOT"]).TrimEnd('/'); }
			else
			{ return String.Empty; }
		}
	}
	
	protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Context.Request.Cookies["LastSearch"] != null)
            {
                this.SearchBox.Text = Context.Request.Cookies["LastSearch"].Value;
            }

            if (Request.QueryString["s"] != null)
            {
                this.SearchBox.Text = Request.QueryString["s"];
            }

            SearchBox.Attributes["onKeyUp"] = "javascript:QuickSearch_SearchKeyUp('" + SearchBox.ClientID + "', event);";
			tdSearchText.Attributes["background"] = ResolveUrl("~/MasterPages/Controls/TopNavSearch/Images/topnavsearch_bg.gif");
		}
	}

	protected override void OnPreRender(EventArgs e)
	{
		ReqisterClentScript();
		base.OnPreRender(e);
	}

	private void ReqisterClentScript()
	{
		//Register common scripts.
		SBNet.Tools.IncludeEntryControlsClientScript(Page);

		//Register the EOR startup script.
		if (!Page.ClientScript.IsStartupScriptRegistered("_00_gctns"))
		{
			StringBuilder sbCode = new StringBuilder();

			sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
			sbCode.Append("  Sys.Application.add_load(QuickSearch_WindowOnLoad)\r\n");
			sbCode.Append("else\r\n");
			sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', QuickSearch_WindowOnLoad);\r\n");
			Page.ClientScript.RegisterStartupScript(this.GetType(), "_00_gctns", sbCode.ToString(), true);
		}
	}
	
	protected void GoSearch_Click(object sender, ImageClickEventArgs e)
    {
        string searchtext = SearchBox.Text.Trim();

        if ((searchtext != "Search") & (searchtext != String.Empty))
        {
            Response.Redirect(WebRoot + "/Search/default.aspx?s=" + Server.UrlEncode(searchtext));
        }

    }

    /*
    protected void GoSearch_Click(object sender, ImageClickEventArgs e)
    {
        string searchtext = SearchBox.Text.Trim();

        if (searchtext != string.Empty)
        {
            HttpCookie thisCookie = new HttpCookie("LastSearch", searchtext);
            Response.Cookies.Add(thisCookie);

            if (Regex.IsMatch(searchtext, @"\A\d\d\d-\d\d\d\d\Z"))
            {
                string[] catclass = searchtext.Split('-');
                QuickSearchResults.SearchEquipment item = QuickSearchResults.GetEquipmentByCatClass(catclass[0], catclass[1]);

                if (item.RentalItemID != String.Empty && item.CategoryID != String.Empty)
                {
                    Response.Redirect(WebRoot + "/equipment/equipment.aspx?itemid=" + item.RentalItemID + "&catid=" + item.CategoryID);
                }
            }

            string pcsearch = searchtext.ToUpper();
            if (Regex.IsMatch(pcsearch, @"\APC\d\d\d\Z"))
            {
                string pcNum = pcsearch.Replace("PC", String.Empty);
                if (Common.PCExists(pcNum))
                {
                    Response.Redirect(WebRoot + "/locations/locationdetail.aspx?id=" + pcNum);
                }
            }

            if (Regex.IsMatch(searchtext, @"\A\d\d\d-\Z"))
            {
                string cat = searchtext.Replace("-", String.Empty);
                string searchid = QuickSearchResults.SearchByCat(cat);
                Response.Redirect("/equipment/equipmentfinderresults.aspx?searchid=" + searchid);
            }

            Response.Redirect(WebRoot + "/Search/default.aspx?s=" + Server.UrlEncode(searchtext));
        }
    }
    */

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (SearchBox.Text != String.Empty)
        {/*
            QuickSearchResults searchresults = new QuickSearchResults(Search.Text);

            GL.DataSource = searchresults.Results;
            GL.DataBind();

            bool hasresults = GL.Items.Count > 0;
            RFD.Visible = hasresults;

            if (searchresults.HasMoreResults)
            {
                MR.Text = "View More \"" + Search.Text + "\" Results...";
                MRL.Visible = true;
            }
            else
            {
                MRL.Visible = false;
            }
        */}
    }
}
