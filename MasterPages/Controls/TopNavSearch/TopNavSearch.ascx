<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TopNavSearch.ascx.cs" Inherits="_00_global_controls_topnavsearch" %>

<script type="text/javascript">
    //<![CDATA[
    function QuickSearch_WindowOnLoad()
    {
		//SBNet.EC.SafeTextEntry($get('<%= SearchBox.ClientID %>'), true);
    }

    function QuickSearch_OpenResults(result)
    {
        $get('<%=FoundResults.ClientID%>').innerHTML = result; 
        
        var results = $get('<%= QuickResults.ClientID %>');  
        var searchBox = $get('<%= SearchBox.ClientID %>');
        
        //Set Postion
        MoveResults();
        
        //Turn on/off
        if (result == '')
        {
            results.style.visibility = 'hidden';
            results.style.display = 'none';
        }
        else
        {
            results.style.visibility = 'visible';
            results.style.display = 'block';
        }
    }

    function QuickSearch_SearchKeyUp(searchBox, ev)
    {
        var key = ev.keyCode || ev.which;
        
        if (key == 13)
        {
            var btn = $get('<%= GoSearch.ClientID %>');
            btn.click();
        }
        else
        {
            setTimeout('UpdateSearchBox()', 1);
        }
    }

    function UpdateSearchBox() 
    {
        QuickSearch.SearchBox.GetHtml($get('<%=SearchBox.ClientID%>').value, OnSucceeded);
    }

    function OnSucceeded(result, userContext, methodName)
    {
        QuickSearch_OpenResults(result); 
    }

    function OnFailed(error, userContext, methodName) 
    {
        QuickSearch_OpenResults("An error occured.");
    }

    //Load Item
    function LI(classid, itemid)
    {
        window.location = '<%= this.WebRoot %>/equipment/equipment.aspx?itemid=' + itemid + '&catid=' + classid;
    }

    //Load Category
    function LC(classid)
    {
        window.location = '<%= this.WebRoot %>/equipment/category.aspx?id=' + classid;
    }

    //Search Equipment
    function SE()
    {        
        window.location = '<%= this.WebRoot %>/search/default.aspx?s=' + $get('<%=SearchBox.ClientID%>').value + '&type=eq';
    }

    function MoveResults()
    {
        var results = $get('<%= QuickResults.ClientID %>');       
        var anchor = Sys.UI.DomElement.getLocation($get('<%= GoSearch.ClientID %>'));
        Sys.UI.DomElement.setLocation(results, anchor.x - 255, anchor.y + 35);
    }
    
    if (window.attachEvent)
    {
        window.attachEvent('onresize', function <%= this.ClientID %>_Resize() { MoveResults(); });
    }
    else if (window.addEventListener)
    {
        window.addEventListener('resize', function <%= this.ClientID %>_Resize() { MoveResults(); }, false);
    }
    //]]>
</script>

<style type="text/css">
    #<%= QuickResults.ClientID %>
    {
        width: 300px;   /* this width value is also effected by the padding we will later set on the links. */
        border:solid 1px #000000;
    }

    #<%= QuickResults.ClientID %> ul 
    {
        list-style-type: none;
        font-family: verdana, arial, sanf-serif;
        font-size: 10px;
        line-height: 14px;
        margin: 0; 
        padding: 0;
    }         

    #<%= QuickResults.ClientID %> a 
    {
        display: block;
        width: 288px;
        font-size: 10px;
        padding: 5px 2px 5px 10px;
        background: #dcdcdc;
        text-decoration: none; /*lets remove the link underlines*/
    }

    #<%= QuickResults.ClientID %> a:link, #<%= QuickResults.ClientID %> a:active, #<%= QuickResults.ClientID %> a:visited 
    {
        color: #000000;                
    }

    #<%= QuickResults.ClientID %> a:hover 
    {
        background: #333333;
        color: #ffffff;
    }

    #<%= QuickResults.ClientID %> IMG
    
        display:inline;
        vertical-align:middle;
        border-width: 0px;
    } 

    #<%= QuickResults.ClientID %> LI.CAT
    {
        display:inline;
        vertical-align:middle;
    }       
                            
    #<%= QuickResults.ClientID %> a.CAT
    {
	    display: block;
        width: 288px;
        font-size: 12px;
        font-weight: bold; 
        white-space: normal;
        padding: 5px 2px 5px 10px;
        background: #000000;
        text-decoration: none; /*lets remove the link underlines*/
    }           

    #<%= QuickResults.ClientID %> a.CAT:link, #<%= QuickResults.ClientID %> a.CAT:active, #<%= QuickResults.ClientID %> a.CAT:visited 
    {
        color: #FFFFFF;
    }

    #<%= QuickResults.ClientID %> a.CAT:hover 
    {
        background: #555555;
        color: #000000;
    }


    #<%= QuickResults.ClientID %> LI.MORE
    {
        display:inline;
        vertical-align:middle;
    }       
                            
    #<%= QuickResults.ClientID %> a.MORE
    {
	    text-align: center;
	    display: block;
        width: 288px;
        font-size: 12px;
        font-weight: bold; 
        white-space: normal;
        padding: 5px 2px 5px 10px;
        background: #777777;
        text-decoration: none; /*lets remove the link underlines*/
    }           

    #<%= QuickResults.ClientID %> a.MORE:link, #<%= QuickResults.ClientID %> a.MORE:active, #<%= QuickResults.ClientID %> a.MORE:visited 
    {
        color: #FFFFFF;
    }

    #<%= QuickResults.ClientID %> a.MORE:hover 
    {
        background: #555555;
        color: #000000;
    }            
</style>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Services>
        <asp:ServiceReference path="~/MasterPages/Controls/TopNavSearch/SearchBox.asmx" />
    </Services>
</asp:ScriptManagerProxy>

<asp:Panel ID="SearchPanel" runat="server" DefaultButton="GoSearch">
	<table width="190" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td width="23"><asp:Image ID="imgMagnify" ImageUrl="~/MasterPages/Controls/TopNavSearch/Images/topnavsearch_magnify.gif" width="23" height="37" runat="Server" /></td>
		<td width="120" id="tdSearchText" runat="server"><asp:TextBox ID="SearchBox" Width="118" Height="16" BorderWidth="0" runat="server" autocomplete="off" /></td>
		<td width="47"><asp:ImageButton ID="GoSearch" OnClick="GoSearch_Click" ImageUrl="~/MasterPages/Controls/TopNavSearch/Images/topnavsearch_go.gif" width="47" height="37" runat="Server" /></td>
	  </tr>
	</table>
</asp:Panel>
	
	
<div id="QuickResults" runat="server" style="position:absolute; visibility:hidden; display:block; width:300px; z-index:150; text-align:left;" >
    <asp:Label runat="server" ID="FoundResults" EnableViewState="false" />
</div>        

<ajaxToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
            TargetControlID="SearchBox" WatermarkCssClass="WatermarkExtender"
            WatermarkText="Search" />