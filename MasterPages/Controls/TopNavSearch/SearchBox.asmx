﻿<%@ WebService Language="C#" Class="QuickSearch.SearchBox" %>

using System;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Text;
using Sunbelt.Utilities;
using System.Data;

namespace QuickSearch
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class SearchBox : System.Web.Services.WebService
    {
        [WebMethod]
        public string GetHtml(string request)
        {
            string results = String.Empty;
            
            if (request != String.Empty)
            {
                QuickSearchResults searchresults = new QuickSearchResults(request);

                if (searchresults.Results.Count > 0)
                {
                    StringBuilder sb = new StringBuilder("<ul>");
                    foreach (QuickSearchResults.SearchCategory cat in searchresults.Results)
                    {
                        //Header Row
                        sb.Append("<li class='CAT' onclick=\"LC('" + cat.CategoryID + "')\";>");
                        sb.Append("<a class='CAT' href='#'>");
                        sb.Append("<img src='" + cat.ImageUrl + "'/>&nbsp;");
                        sb.Append(cat.Title);
                        sb.Append("</a></li>");

                        foreach (QuickSearchResults.SearchEquipment item in cat.SearchItems)
                        {
                            //Item Row
                            sb.Append("<li onclick=\"LI('" + item.CategoryID + "','" + item.RentalItemID + "')\";>");
                            sb.Append("<a href='#'>");
                            sb.Append(item.Title);
                            sb.Append("</a></li>");
                        }
                    }

                    if (searchresults.HasMoreResults)
                    {
                        //More Results Row
                        sb.Append("<li class='MORE' onclick=\"SE()\";>");
                        sb.Append("<a class='MORE' href='#'>More \"");
                        sb.Append(request);
                        sb.Append("\" Results</a></li>");
                    }
                    
                    //Close List
                    sb.Append("</ul>");

                    results = sb.ToString();
                }
            }

            return results;
        }
    }
}

