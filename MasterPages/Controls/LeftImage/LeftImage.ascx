﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LeftImage.ascx.cs" Inherits="MasterPages_Controls_LeftImage_LeftImage" %>
<table id="tblLeftImage" cellpadding="0" cellspacing="0" border="0" style="height:100%">
	<tr>
		<td style="padding-top:40px"><asp:Image ID="imgImage" runat="server" AlternateText="Image Here" /></td>
	</tr>
	<tr>
		<td style="padding-top:200px; text-align:center;"><asp:Image ID="Image20" ImageUrl="~/MasterPages/Controls/LeftNav/Images/leftnav_sblogo.gif" width="162" height="67" runat="server" AlternateText="Sunbelt Logo" /></td>
	</tr>
</table>               