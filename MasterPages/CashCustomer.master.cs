﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SBNet;

public partial class MasterPages_CashCustomer : SBNet.SBNetMasterPage
{
	public bool TopSearchVisible
	{
		set { Header.TopSearchVisible = value; }
		get { return Header.TopSearchVisible; }
	}

	public bool LeftNavVisible
	{
		get { return UserLeftNav.Visible; }
		set { UserLeftNav.Visible = value; LeftNav.Visible = value; }
	}

	public bool BottomLeftNavVisible
	{
		get { return LeftNav.Visible; }
		set { LeftNav.Visible = value; }
	}

	public override string MainContentWidth
	{
		set { tdMainContent.Style["width"] = value; }
		get { return tdMainContent.Style["width"]; }
	}

	public override void ShowLeftImage(string imageUrl)
	{
		LeftImage.Visible = true;
		LeftImage.ImageUrl = imageUrl;
		LeftNavVisible = false;
		BottomLeftNavVisible = false;
	}

	protected void Page_Load(object sender, EventArgs e)
    {
		Response.AddHeader("Content-Type", "text/html; charset=utf-8");
		Response.Cache.SetCacheability(HttpCacheability.NoCache);

		if (!IsPostBack)
		{
			UpdateUserNavControls();
			hlCustomerFeedback.NavigateUrl = Request.Url.AbsolutePath + "#";

			AccountArgs = new AccountEventArgs(SiteUser.DLState, SiteUser.DLNumber);
			RaiseChangeAccountEvent(AccountArgs);
		}

#if false
		if (!Page.ClientScript.IsClientScriptIncludeRegistered("Cookie.js"))
			Page.ClientScript.RegisterClientScriptInclude("Cookie.js", Page.ResolveUrl("~/ClientScript/Cookie.js"));
#endif

	}

	public void UpdateUserNavControls()
	{
		//If no user loged in, hide the UserLeftNav control.
		bool b = Page.User.Identity.IsAuthenticated && LeftNavVisible;
		UserLeftNav.Visible = b;
		LeftNav.Visible = b;

		Header.UpdateDisplay();
	}
}
