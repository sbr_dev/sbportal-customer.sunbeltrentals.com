using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;

public partial class masterpages_main : System.Web.UI.MasterPage
{
	protected void Page_Load(object sender, EventArgs e)
    {
		Response.AddHeader("Content-Type", "text/html; charset=utf-8");
		Response.Cache.SetCacheability(HttpCacheability.NoCache);
	}
}
