<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Main.master" Theme="Main" AutoEventWireup="true" CodeFile="Config.aspx.cs" Inherits="SBPortal_Config"  %>

<%@ Register src="~/SBPortal/ConfigParameters/ConfigParameters.ascx" tagname="ConfigParameters" tagprefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">
<uc1:ConfigParameters ID="ConfigParameters" runat="server" />
</asp:Content>

