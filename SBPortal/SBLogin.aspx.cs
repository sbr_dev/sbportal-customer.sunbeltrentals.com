using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SunbeltWebServices;
using Sunbelt;
using Sunbelt.Security;
using Sunbelt.Tools;
using System.Net;

public partial class SBPortal_SBLogin : System.Web.UI.Page
{
	private string ReturnUrl
	{
		set { ViewState["ReturnUrl"] = value; }
		get { return (string)ViewState["ReturnUrl"]; }
	}

	protected void Page_Load(object sender, EventArgs e)
    {
		// Added as QA SunbeltWebServices is SSL enabled
		ServicePointManager.Expect100Continue = true;
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

		if (!IsPostBack)
		{
			if (ConfigurationSettings.AppSettings["httpsRequired"] == "1")
			{
				//This code will force the login page in to https mode.
				if (Request.Url.Scheme == "http")
				{
					string url = Request.Url.AbsoluteUri.Replace("http:", "https:");
					Response.Redirect(url, true);
					return;
				}
			}
		}
		if (Request.QueryString["wcan"] != null)
		{
			try
			{
				SecureQueryString sqs = new SecureQueryString(Request.QueryString["wcan"]);
				ReturnUrl = sqs["ReturnUrl"];
			}
			catch { /* Ignore any errors. */ }
		}

		SunbeltLogin1.UserValidated += new EventHandler<Sunbelt.LoginEventArgs>(SunbeltLogin1_UserValidated);
    }

	protected override void OnPreRender(EventArgs e)
	{
		//This will set the form action to match the pages url
		//(ie. "action=https://.../sbportal/sblogin.aspx" not just "action=sblogin.aspx").
		string s = Page.Request.Url.ToString();

		Page.Form.Action = s;

		base.OnPreRender(e);
	}


	void SunbeltLogin1_UserValidated(object sender, Sunbelt.LoginEventArgs e)
	{
		WebUser.CurrentUser = e.User;
		if(string.IsNullOrEmpty(ReturnUrl))
			Response.Redirect("~/SBPortal/Default.aspx", true);
		else
			Response.Redirect(ReturnUrl, true);
	}
}
