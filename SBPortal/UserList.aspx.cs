﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Security;
using SBNet;

public partial class SBPortal_UserList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//Check if logged in.
		WebUser.VerifySBPortalLogin(Request.Url.PathAndQuery);

		if (!IsPostBack)
		{
			sdsUsers.SelectParameters["ApplicationName"].DefaultValue = Membership.ApplicationName;
		}
	}

	protected string ConvertTime(DateTime dt)
	{
		dt = TimeZoneInfo.ConvertTime(dt,
			TimeZoneInfo.Utc,
			TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
		return Sunbelt.Tools.DateTimeTools.ToShortestDateTimeString(dt);
	}
}
