﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SBPortal_Config : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
		//Check if logged in.
		Sunbelt.WebUser.VerifySBPortalLogin();

		Master.LeftNavVisible = false;

    }
}
