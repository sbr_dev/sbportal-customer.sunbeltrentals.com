﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ConfigParameters.ascx.cs" Inherits="UserControls_Meter" %>
<%@ Register Assembly="SBNetWebControls" TagPrefix="SBNet" Namespace="SBNetWebControls" %>
<%@ Import Namespace="SBNet.Extensions" %>

<asp:GridView ID="gvMeter" runat="server" 
	BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="0" ForeColor="Black" 
	    AutoGenerateColumns="False"  ShowFooter="false" 
        onrowdatabound="gvMeter_RowDataBound" 
        onrowcreated="gvMeter_RowCreated"
	    onrowdeleting="gvMeter_RowDeleting" 
	    onrowcancelingedit="gvMeter_RowCancelingEdit" 
        onrowediting="gvMeter_RowEditing" style="text-align: center" 
	onrowupdating="gvMeter_RowUpdating" SkinID="Gray"  >
	<EditRowStyle VerticalAlign="Top" />

    <EmptyDataRowStyle VerticalAlign="Middle" HorizontalAlign="Center" />
        <EmptyDataTemplate>
		    <div style="width: 30em; height: 4em; vertical-align:middle; text-align:center">
			    <br /><asp:ImageButton ID="btnNewMeter" runat="server" OnClick="btnAdd_Click" ImageUrl="../Images/addMeter.png" AlternateText="Add New Meter" />
		    </div>
        </EmptyDataTemplate>
    <Columns>
	    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
		    <ItemTemplate>
               <asp:Label ID="lbParameterID" Text='<%# Eval("ParameterID", "{0}") %>' runat="server" />
		    </ItemTemplate>
		    <ItemStyle  CssClass="cell" HorizontalAlign="Left" VerticalAlign="Top" />
		    <EditItemTemplate> 
               <asp:Label ID="lbParameterID" Text='<%# Eval("ParameterID", "{0}") %>' runat="server" />
		    </EditItemTemplate>
	    </asp:TemplateField>
	    <asp:TemplateField HeaderText="Name">
		    <ItemTemplate>
               <asp:Label ID="lblMeterType" Text='<%# Eval("Name", "{0}") %>' runat="server" />
		    </ItemTemplate>
		    <ItemStyle  CssClass="cell"  HorizontalAlign="Left" />
		    <EditItemTemplate>
               <asp:Label ID="lblMeterType" Text='<%# Eval("Name", "{0}") %>' runat="server" />
		    </EditItemTemplate>
	    </asp:TemplateField>
	    <asp:TemplateField HeaderText="Value">
			    <ItemTemplate>
					<asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value", "{0}") %>' ToolTip='<%# Eval("Description", "{0}") %>'></asp:Label>
			    </ItemTemplate>
		        <ItemStyle  CssClass="cell"/>
		        <EditItemTemplate>
					<asp:Textbox ID="tbValue" Text='<%# Eval("Value", "{0}") %>' runat="server" Width="98%" />
    		    </EditItemTemplate>
	    </asp:TemplateField>
        <asp:CommandField ButtonType="Link" ShowDeleteButton="true" DeleteText="Remove" 
            ShowCancelButton="true" CancelText="Cancel" UpdateText="Save"
            ShowEditButton="True" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="cell">
        </asp:CommandField>
    </Columns>
</asp:GridView>