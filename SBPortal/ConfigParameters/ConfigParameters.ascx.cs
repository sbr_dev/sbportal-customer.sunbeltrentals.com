﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sunbelt;
using Sunbelt.Tools;
using System.Data;
using SBNetWebControls;
using System.Text;
using System.Collections;
using System.IO;

using Sunbelt.Common.DAL;
using System.Data.SqlClient;

public partial class UserControls_Meter : System.Web.UI.UserControl
{
	//private enum eMeter { Name=0, Type, Active, DeRegulated, DeRegulated_ID,DeRegulated_Contract,DeRegulated_Expiration }
	//private const string DEREGULATED_EXPIRATION_UNINITIALIZE = "1/1/0001 12:00:00 AM";

    DataTable ConfigParameters
    {
		set { WebTools.SetSession("ConfigParameters", value); }
		get { return (DataTable)WebTools.GetSession("ConfigParameters", null); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			LoadGrid();
		}
    }

	public void LoadGrid()
	{
		ConfigParameters = GetData();
		gvMeter.DataSource = ConfigParameters;
		gvMeter.DataBind();
	}

	private DataTable GetData()
	{
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
		using (SPData sp = new SPData("config.sb_GetSiteParameters", conn))
		{
			return sp.ExecuteDataTable();
		}
	}


    protected override void OnPreRender(EventArgs e)
    {
        SBNet.Tools.IncludeEntryControlsClientScript(Page);

		//if (!Page.ClientScript.IsClientScriptIncludeRegistered("Meter.js"))
		//    Page.ClientScript.RegisterClientScriptInclude("Meter.js",
		//        ResolveUrl(AppRelativeTemplateSourceDirectory + "Meter.js"));

		////Register the startup script.
		//if (!Page.ClientScript.IsStartupScriptRegistered("MeterStartup"))
		//{
		//    StringBuilder sbCode = new StringBuilder();

		//    sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
		//    sbCode.Append("  Sys.Application.add_load(_Meter_OnLoad)\r\n");
		//    sbCode.Append("else\r\n");
		//    sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', _Meter_OnLoad);\r\n");
		//    Page.ClientScript.RegisterStartupScript(this.GetType(), "MeterStartup", sbCode.ToString(), true);
		//}

        base.OnPreRender(e);
    }

	protected void gvMeter_RowUpdating(object sender, GridViewRowEventArgs e)
	{
		if( e.Row.RowIndex < ConfigParameters.Rows.Count )
		{
			//string ParameterID = ((Label)gvMeter.Rows[e.Row.RowIndex].Cells[0].FindControl("ParameterID")).Text;
			//string tbValue = ((TextBox)gvMeter.Rows[e.Row.RowIndex].Cells[0].FindControl("tbValue")).Text;
			//string ddlMeterTypeID = ((DropDownList)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("ddlMeterTypes")).SelectedValue;
			//bool DeregulatedMarket = ((CheckBox)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("chkDeregulated_selectable")).Checked;
			//string txtDeRegID = ((TextBox)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("txtDeregulatedID")).Text;
			//string txtDeRegContract = ((TextBox)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("txtDeregulatedContract")).Text;
			//DateTime txtDeRegExpiration = ((PopupCalendar)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("popDeregulatedContractExpiration")).Date;
			//bool Active = ((CheckBox)gvMeter.Rows[e.RowIndex].Cells[0].FindControl("chkActive")).Checked;

			//location.Meters[e.RowIndex].MeterLabel = txtMeterLabel;
			//location.Meters[e.RowIndex].MeterTypeID = Convert.ToInt32(ddlMeterTypeID);
			//DataRow[] dr = MeterTypes.Select("MeterTypeID =" + ddlMeterTypeID);
			//location.Meters[e.RowIndex].MeterTypeCode = Convert.ToChar(dr[0]["MeterTypeCode"]);
			//location.Meters[e.RowIndex].MeterTypeName = dr[0]["MeterTypeName"].ToString();

			//location.Meters[e.RowIndex].DeregulatedMarket = DeregulatedMarket;
			//if (DeregulatedMarket == false)
			//{
			//    txtDeRegID = "";
			//    txtDeRegContract = "";
			//    txtDeRegExpiration = new DateTime();
			//}
			//location.Meters[e.RowIndex].DeregulatedID = txtDeRegID;
			//location.Meters[e.RowIndex].DeregulatedContract = txtDeRegContract;
			//location.Meters[e.RowIndex].DeregulatedContractExpiration = txtDeRegExpiration;
			//location.Meters[e.RowIndex].Active = Active;


			//using (SPData sp = new SPData("config.sb_UpdSiteParameter", ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData)))
			//{
			//    sp.AddParameterWithValue("@ParameterID", SqlDbType.Int, 4, ParameterDirection.Input, Convert.ToInt32(ParameterID));
			//    sp.AddParameterWithValue("@Value", SqlDbType.VarChar, 255, ParameterDirection.Input, tbValue);

			//     ConfigParameters  =  sp.ExecuteDataTable();
			//}


			//gvMeter.EditIndex = -1;
			//gvMeter.DataSource = ConfigParameters;
			//gvMeter.DataBind();

			//if (location.Meters[e.RowIndex].ID == 0)
			//    AccountDAL.CreateMeter(location.Meters[e.RowIndex]);
			//else
			//{
			//    int newMeter = AccountDAL.UpdateMeter(location.Meters[e.RowIndex]);
			//    if( newMeter > 0 )  // new meter created to preserve meters tied to existing invoice.  No invoice to meter the meter is just updated
			//        location.Meters[e.RowIndex].ID = newMeter;
			//}
		}

	}

	protected void gvMeter_RowDataBound(object sender, GridViewRowEventArgs e)
    {
		//if (e.Row.RowType == DataControlRowType.DataRow)
		//{
		//    foreach (Control var in e.Row.Cells[7].Controls)
		//    {
		//        if (var.GetType().ToString() == "System.Web.UI.WebControls.DataControlLinkButton")
		//        {
		//            LinkButton lnk = (LinkButton)var;
		//            if (lnk.CommandName == "Delete")
		//                lnk.OnClientClick = "return confirm('Are you sure you want to remove this Meter?')";
		//        }
		//    }

		//    if (e.Row.RowIndex == gvMeter.EditIndex)
		//    {
		//        if (MeterTypes == null)
		//            MeterTypes = AccountDAL.GetMeterTypes();

		//        DropDownList ddlMeterTypes = (DropDownList)e.Row.FindControl("ddlMeterTypes");
		//        if (ddlMeterTypes != null)
		//        {
		//            ddlMeterTypes.DataTextField = "MeterTypeName";
		//            ddlMeterTypes.DataValueField = "MeterTypeID";
		//            ddlMeterTypes.DataSource = MeterTypes;
		//            ddlMeterTypes.DataBind();
		//            if (location.Meters[e.Row.RowIndex].ID == 0)  //New meters do not have id's
		//                ddlMeterTypes.Items.Insert(0, new ListItem("--Select--", "0"));
		//            else
		//                ddlMeterTypes.SelectedValue = ((Sunbelt.PCUtility.Lib.Meter)(e.Row.DataItem)).MeterTypeID.ToString();
		//        }
		//        e.Row.Cells[(int)eMeter.Name].Style.Add("vertical-align", "top");
		//        e.Row.Cells[(int)eMeter.Type].Style.Add("vertical-align", "top");
		//        e.Row.Cells[(int)eMeter.Active].Style.Add("vertical-align", "top");
 
		//    }
		//}
		//LinkButton btn = (LinkButton)e.Row.FindControl("");
    }
    
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
		//Fire_EditClicked(this);

		////Add a blank item and set it to edit mode.
		//location.AddMeter(new Meter());

		//gvMeter.EditIndex = location.Meters.Count - 1;
		//gvMeter.DataSource = location.Meters;
		//gvMeter.DataBind();
    }

    protected void gvMeter_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
		//if (gvMeter.EditIndex > -1)
		//{		// To catch a page refresh after a add meter was cancelled, which reissues a cancel which blows up.   Check it e.RowIndex < location.Meters.Count is to catch a cancel
		//    if( e.RowIndex < location.Meters.Count && location.Meters[e.RowIndex].ID == 0)  //New meters do not have id's
		//        location.Meters.RemoveAt(e.RowIndex);

		gvMeter.EditIndex = -1;
		gvMeter.DataSource = ConfigParameters;
		gvMeter.DataBind();

		//    Fire_CancelClicked(sender);
		//}
    }

    protected void gvMeter_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (gvMeter.EditIndex == -1)
        {
			////Add add link to the footer.
			//if (e.Row.RowType == DataControlRowType.Footer)
			//{
			//    TableCell tc = e.Row.Cells[e.Row.Cells.Count - 1];

			//    //LinkButton btn = new LinkButton();
			//    //btn.Text = "Add Meter";
			//    //btn.Click += new EventHandler(btnAdd_Click);

			//    ImageButton btn = new ImageButton();
			//    btn.ImageUrl = "../Images/addMeter.png";              
			//    btn.CommandName = "AddMeter";
			//    btn.AlternateText = "Add Meter";
			//    btn.Click +=new ImageClickEventHandler(btnAdd_Click);

			//    tc.Controls.Add(btn);
			//}
			//if (e.Row.RowType == DataControlRowType.Header )
			//    e.Row.SetRenderMethodDelegate(RenderHeader);
        }
    }
	////method for rendering the columns headers	
	//private void RenderHeader(HtmlTextWriter output, Control container)
	//{
	//    for (int i = 0; i < container.Controls.Count; i++)
	//    {
	//        TableCell cell = (TableCell)container.Controls[i];
	//        //stretch non merged columns for two rows
	//        if (!info.MergedColumns.Contains(i))
	//        {
	//            cell.Attributes["rowspan"] = "2";
	//            cell.RenderControl(output);
	//        }
	//        else //render merged columns common title
	//            if (info.StartColumns.Contains(i))
	//            {
	//                output.Write(string.Format("<th align='center' scope='col' colspan='{0}'>{1}</th>",
	//                         info.StartColumns[i], info.Titles[i]));
	//            }
	//    }

	//    //close the first row	
	//    output.Write("</tr>");

	//    //set attributes for the second row
	//    gvMeter.HeaderStyle.AddAttributesToRender(output);
	//    //start the second row
	//    output.RenderBeginTag("tr");

	//    //render the second row (only the merged columns)
	//    for (int i = 0; i < info.MergedColumns.Count; i++)
	//    {
	//        TableCell cell = (TableCell)container.Controls[info.MergedColumns[i]];
	//        cell.RenderControl(output);
	//    }
		
	//    //Close the row.
	//    output.RenderEndTag();
	//}

    protected void gvMeter_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
		//if (e.RowIndex > -1)
		//{
		//    bool wasDeleted = AccountDAL.DeleteMeter(location.Meters[e.RowIndex].ID);
		//    if( wasDeleted == true )
		//        location.Meters.RemoveAt(e.RowIndex);
		//    else
		//        location.Meters[e.RowIndex].Active=false;

		//    gvMeter.EditIndex = -1;
		//    gvMeter.DataSource = location.Meters;
		//    gvMeter.DataBind();
		//    Fire_SaveClicked(this);
		//}
    }

    protected void gvMeter_RowEditing(object sender, GridViewEditEventArgs e)
    {
		gvMeter.EditIndex = e.NewEditIndex;
		gvMeter.DataSource = ConfigParameters;
		gvMeter.DataBind();
		//Fire_EditClicked(this);
		//ShowDeregColumns(false);
    }

    private void ShowDeregColumns(bool display )
    {
        gvMeter.Columns[4].Visible = display;
        gvMeter.Columns[5].Visible = display;
        gvMeter.Columns[6].Visible = display;
    }
	protected void gvMeter_RowUpdating(object sender, GridViewUpdateEventArgs e)
	{
		if (e.RowIndex < ConfigParameters.Rows.Count)
		{
			GridViewRow gvr = gvMeter.Rows[e.RowIndex];
			string ParameterID = ((Label)gvr.FindControl("lbParameterID")).Text;
			string tbValue = ((TextBox)gvr.FindControl("tbValue")).Text;

			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
			using (SPData sp = new SPData("config.sb_UpdSiteParameter", conn))
			{
				sp.AddParameterWithValue("@ParameterID", SqlDbType.Int, 4, ParameterDirection.Input, Convert.ToInt32(ParameterID));
				sp.AddParameterWithValue("@Value", SqlDbType.VarChar, 255, ParameterDirection.Input, tbValue);

				ConfigParameters = sp.ExecuteDataTable();
			}


			gvMeter.EditIndex = -1;
			LoadGrid();
			gvMeter.DataSource = ConfigParameters;
			gvMeter.DataBind();
		}
	}
}
