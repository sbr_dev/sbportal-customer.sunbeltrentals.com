using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Sunbelt.Common.DAL;
using Sunbelt.Tools;

public partial class SBPortal_QueryEmail : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		//Check if logged in.
		Sunbelt.WebUser.VerifySBPortalLogin();

		StringBuilder sb = new StringBuilder();

		Response.Clear();

		sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?><SearchResult>");

		string text = HttpUtility.UrlDecode(Request.QueryString["search"]);
		bool showMore = ConvertTools.ToBoolean(Request.QueryString["more"]);
		if (!string.IsNullOrEmpty(text))
		{
			DataTable dt = new DataTable();
			using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.SecurityData))
			using (SPData sp = new SPData("dbo.sb_User_LookupUserName", conn))
			{
				sp.AddParameterWithValue("@ApplicationName", SqlDbType.VarChar, 30, ParameterDirection.Input, "/SecurityTest01");
				sp.AddParameterWithValue("@UserNamePattern", SqlDbType.VarChar, 30, ParameterDirection.Input, text);
				sp.AddParameterWithValue("@RowCount", SqlDbType.Int, 4, ParameterDirection.Input, (showMore) ? 25 : 11);
				dt = sp.ExecuteDataTable();
			}
			foreach (DataRow row in dt.Rows)
			{
				sb.Append(string.Format("<item><key>{0}</key><value>{1}</value></item>",
					row["UserID"].ToString().Trim(), row["UserName"].ToString().Trim()));
			}
		}

		sb.Append("</SearchResult>");
		Response.Write(sb.ToString());
		Response.End();
	}
}
