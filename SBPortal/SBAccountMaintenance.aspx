<%@ Page Language="C#" MasterPageFile="~/MasterPages/Minor.master" AutoEventWireup="true" CodeFile="SBAccountMaintenance.aspx.cs" Inherits="SBPortal_SBAccountMaintenance" Title="SB Account Maintenance" %>

<%@ Register Src="Controls/InformationSummary.ascx" TagName="InformationSummary" TagPrefix="uc4" %>
<%@ Register Src="../SiteControls/UserProfile/UPValidationSummary.ascx" TagName="UPValidationSummary" TagPrefix="uc2" %>
<%@ Register Src="Controls/UserProfile/ViewProfile.ascx" TagName="ViewProfile" TagPrefix="uc1" %>
<%@ Register Src="../SiteControls/QueryControl/QueryControl.ascx" TagName="QueryControl" TagPrefix="uc1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>

<%@ Register src="../SiteControls/QueryAccountControl/QueryAccountControl.ascx" tagname="QueryAccountControl" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
    Account Maintenance
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainBodyContent" Runat="Server">
	<uc2:UPValidationSummary ID="UPValidationSummary1" runat="server" />
	<uc4:InformationSummary ID="InformationSummary1" runat="server" />
	<cc1:RoundedCornerPanel ID="RoundedCornerPanel1" runat="server" SkinID="rcpUserProfile">
		<div id="divUserProfile">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="h2">Manage Accounts</td>
				</tr>
			</table>
			<uc1:ViewProfile ID="ViewProfile1" runat="server" HideStatistics="true" />
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td class="h3">Account List</td>
				</tr>
				<tr>
					<td>
						<asp:GridView ID="gvAccessAreas" runat="server" AutoGenerateColumns="False" OnRowCancelingEdit="gvAccessAreas_RowCancelingEdit"
						OnRowEditing="gvAccessAreas_RowEditing" OnRowUpdating="gvAccessAreas_RowUpdating" OnRowDataBound="gvAccessAreas_RowDataBound"
						OnRowDeleting="gvAccessAreas_RowDeleting" SkinID="GrayNoHover" DataKeyNames="AccountNumber">
							<EmptyDataTemplate>No accounts are associated with this user.</EmptyDataTemplate>
							<RowStyle HorizontalAlign="Center" />
							<HeaderStyle HorizontalAlign="Center" />
						<Columns>
							<asp:TemplateField HeaderText="Account" ItemStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<asp:Label ID="lbAccountNumber" runat="server" Text='<%#Eval("AccountNumber")%>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Account Admin">
								<ItemTemplate>
									<asp:Image ID="imgAccountAdmin" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkAccountAdmin" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToAccountAdmin"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Rental<br />Reservations">
								<ItemTemplate>
									<asp:Image ID="imgRentalReservations" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkRentalReservations" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToRentalReservations"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Rental<br />Returns">
								<ItemTemplate>
									<asp:Image ID="imgRentalReturns" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkRentalReturns" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToRentalReturns"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Online<br />Payments">
								<ItemTemplate>
									<asp:Image ID="imgOnlinePayments" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkOnlinePayments" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToOnlinePayments"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Reporting">
								<ItemTemplate>
									<asp:Image ID="imgReporting" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkReporting" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToReporting"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Alerts">
								<ItemTemplate>
									<asp:Image ID="imgAlerts" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkAlerts" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToAlerts"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Location<br />Maintenance">
								<ItemTemplate>
									<asp:Image ID="imgLocationMaintenance" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkLocationMaintenance" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToLocationMaintenance"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="E-Subscriptions">
								<ItemTemplate>
									<asp:Image ID="imgESubscriptions" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkESubscriptions" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToESubscriptions"))%> '/>
								</EditItemTemplate>
							</asp:TemplateField>
							<%--<asp:TemplateField HeaderText="Company Info">
								<ItemTemplate>
									<asp:Image ID="imgCompanyInfo" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkCompanyInfo" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToCompanyInfo"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Rebate Status">
								<ItemTemplate>
									<asp:Image ID="imgRebateStatus" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkRebateStatus" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToRebateStatus"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Rate Guide">
								<ItemTemplate>
									<asp:Image ID="imgRateGuide" runat="server" ImageUrl="~/SBPortal/Images/clearpixel.gif" Width="11px" Height="14px" /> 
								</ItemTemplate>
								<EditItemTemplate>
									<asp:CheckBox ID="chkRateGuide" runat="server" Checked='<%#Convert.ToBoolean(Eval("ToRateGuide"))%>' />
								</EditItemTemplate>
							</asp:TemplateField>--%>
							<asp:CommandField HeaderText="Change Access" ShowEditButton="True" ShowDeleteButton="true" DeleteText="Remove" EditImageUrl="~/SBPortal/Images/btnEdit.gif" ButtonType="Image" CancelImageUrl="~/SBPortal/Images/btnCancel.gif" DeleteImageUrl="~/SBPortal/Images/btnDelete.gif" UpdateImageUrl="~/SBPortal/Images/btnSave.gif" UpdateText="Save" ShowCancelButton="true">
								  <ItemStyle CssClass="NoWrap" HorizontalAlign="Center" VerticalAlign="Middle" />
							</asp:CommandField>
						</Columns>              
					</asp:GridView>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td style="text-align: right;">
						<asp:ImageButton ID="btnAddAccount" runat="server" ImageUrl="~/App_Themes/Main/Buttons/add_account_100x20_darkred.gif" OnClick="btnAddAccount_Click" />&nbsp;<asp:ImageButton ID="btnDone" runat="server" ImageUrl="~/App_Themes/Main/Buttons/done_80x20_darkred.gif" OnClick="btnDone_Click" />
					</td>
				</tr>
			</table>
		</div>
	</cc1:RoundedCornerPanel>			
	<%--Verify user box--%>	
	<cc1:RoundedCornerPanel ID="rcpAddAccount" runat="server" SkinID="rcpBlackWithTitle" TopText="Add Account">
		<table>
			<tr>
				<td colspan="3">Enter account number</td>
			</tr>
			<tr>
				<td>Account:</td>
				<td><uc3:QueryAccountControl ID="qcAccount" runat="server"
						QueryUrl="~/MasterPages/Controls/UserLeftNav/QueryAccount.aspx"
						ClientOnRowBound="SBAM.AccountOnRowBound" Width="12em" FontSize="8pt"
						ValueCssClass="SuggestValueText" DoShowMore="True" SearchText="" SearchPadding="1px 10px"
						ClientCallServer="SBAM_CheckAccountNumber" ClientCallBack="SBAM.ReceiveAccountData"
						ClientErrorCallBack="SBAM.OnServerError" ClientOnGoClick="SBAM.OnGoClick" /></td>
				<td>&nbsp;<asp:ImageButton ID="btnCancel" runat="server" ImageUrl="~/App_Themes/Main/Buttons/cancel_60x20_darkgray.gif" /></td>
			</tr>
		</table>
	</cc1:RoundedCornerPanel>
    
<script type="text/javascript" language="javascript">
//<![CDATA[

var SBAM = {

//properties
deleteControlID:null,
divAddAccount:null,
tbAccount:null,
    
Init:function()
{
	//alert("SBAM.Init");
	SBAM.divAddAccount = document.getElementById("<% =rcpAddAccount.ClientID %>");
	SBAM.tbAccount = document.getElementById("<% =qcAccount.TextBoxControl.ClientID %>");
},

OnAdminCBClick:function(control)
{
	if(control.checked)
	{
		//Find the containing TR tag.
		while(control && (control.tagName != "TR"))
		{
			control = control.parentNode;
		}
		if(control)
		{
			//Find all INPUT objects.
			var list = control.getElementsByTagName("INPUT");
			for(var i = 0; i < list.length; i++)
			{
				//If a checkbox, check it.
				if(list[i].type == "checkbox")
					list[i].checked = true;
			}
		}
	}
},

OnAddAccountClick:function()
{
	SBAM.tbAccount.value = "";
	
	SBNet.Util.DisableUI(false, SBAM.divAddAccount.style.zIndex);
	SBAM.divAddAccount.style.display = "block";		
	SBNet.Util.CenterControlOnScreen(SBAM.divAddAccount);

	SBNet.EC.ResetInvalidFields();
	SBAM.tbAccount.focus();

	return false;
},

OnCancelClick:function()
{
	SBAM.divAddAccount.style.display = "none";		
	SBNet.Util.EnableUI();
	return false;
},

OnGoClick:function()
{
	var textBox = document.getElementById("<% =qcAccount.TextBoxControl.ClientID %>");
	if(textBox.value.length > 0)
	{
		SBAM_CheckAccountNumber(textBox.value, "CHECK");
	}
	return false;
},

ReceiveAccountData:function(rvalue, context)
{
	if(context == "CHECK")
	{
		if(rvalue.length > 0)
		{
			if(rvalue != "GO")
				SBNet.Util.ConfirmBox("Account Lookup", rvalue, SBAM.OnOk, null, "okonly");
			else
				<% =Page.ClientScript.GetPostBackEventReference(qcAccount.GoButtonControl, "") %>;
		}
	}
	else
		alert("Unknown context: " + context + " - " + rvalue);
},

OnOk:function()
{
	SBNet.Util.DisableUI(false, SBAM.divAddAccount.style.zIndex);
},

OnServerError:function(message, context)
{
	alert(message);
},

AccountOnRowBound:function(div)
{
	var spcl = _QueryControl_getSpanById("iscl", div);
	var bold = (spcl.innerHTML == "true");
	
	var spv = _QueryControl_getSpanById("value", div);
	var stat = _QueryControl_getSpanById("status", div);
	var c = _QueryControl_getSpanById("companyID", div);
	if(bold)
		spv.innerHTML = "<b>" + spv.innerHTML + "</b>";
	
	var spc = _QueryControl_getSpanById("customer", div);
	spc.style.display = "";
	if(bold)
		spc.innerHTML =  "&nbsp;<b>(" + stat.innerHTML + ")</b>" + "&nbsp;-&nbsp;" + spc.innerHTML;
	else	
		spc.innerHTML =  "&nbsp;(" + stat.innerHTML + ")" + "&nbsp;-&nbsp;" + spc.innerHTML;
	if(bold)
		spc.innerHTML = "<b>" + spc.innerHTML + "</b>";
	spc.innerHTML = spc.innerHTML + " (" + ((c.innerHTML == "1") ? "US)" : "CA)" );
},

OnDeleteClick:function(controlID, account, user)
{
	SBAM.deleteControlID = controlID;
	
	SBNet.Util.ConfirmBox("Remove User", "Are you sure you wish to remove account <b>" + account + "</b><br />from user <b>" + user + "</b>?",
		SBAM.OnDeleteConfirm,
		null,
		"yesno");

	return false;
},

OnDeleteConfirm:function()
{
	__doPostBack('<% =gvAccessAreas.UniqueID %>', SBAM.deleteControlID);
}

}

//]]>
</script>
</asp:Content>

