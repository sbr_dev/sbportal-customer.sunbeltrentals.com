﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Sunbelt.EmailCCManager;

public partial class _Default : System.Web.UI.Page
{
    public string InitialReservationEmails = "\"\"";
    public string InitialPickupEmails = "\"\"";
    public string InitialServiceEmails = "\"\"";
    public string InitialReservationToEmails = "\"\"";
    public string InitialPickupToEmails = "\"\"";
    public string InitialServiceToEmails = "\"\"";


    private DataManager data;
    private EmailCCManagerService service;
    private JavaScriptSerializer serializer;

    public int? getddlCompanyID
    {
        get { return string.IsNullOrEmpty(hfCompanyID.Value) ? (int?)null : (int?)Convert.ToInt32(ddlCompany.SelectedValue); }
    }
    public int MyProperty { get; set; }

protected void Page_Load(object sender, EventArgs e)
    {
		//Check if logged in.
		Sunbelt.WebUser.VerifySBPortalLogin("/SBPortal/EmailCCManager/");

        this.data = new DataManager();
        this.service = new EmailCCManagerService(this.data);
        this.serializer = new JavaScriptSerializer();
        if (IsPostBack)
        {
            //spanErrorMessage.InnerText = string.Empty;
            //var extraccs = new ExtraCCs();
            //extraccs.ReservationExtraCCs = Request.Form["ReservationEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //extraccs.PickupExtraCCs = Request.Form["PickupEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //extraccs.ServiceExtraCCs = Request.Form["ServiceEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //extraccs.ReservationTOs = Request.Form["ReservationToEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //extraccs.PickupTOs = Request.Form["PickupToEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //extraccs.ServiceTOs = Request.Form["ServiceToEmails"].Split(',').Where(x => x != string.Empty).ToList();
            //try
            //{
            //    if (ddLookupType.SelectedValue == "Profit Center")
            //        extraccs.ProfitCenter = Int32.Parse(tbPcSearch.Text);
            //    else
            //        extraccs.CustomerNumber = Int32.Parse(tbCustomerSearch.Text);

            //    extraccs.CompanyID = Int32.Parse(ddlCompany.SelectedValue);
            //}
            //catch
            //{
            //    spanErrorMessage.InnerText = "Please enter a valid Profit Center Id or Customer Number.";
            //    return;
            //}
            // write to db
            // this.data.SaveExtraCCs(extraccs);
            // update page
            //PopulateEmails();
            // this is to show the "saved" message on the page.
            ltExtraHeaderCode.Text = "<script type=\"text/javascript\">$(document).ready(function(){$('#saveMessage').show().delay(5000).fadeOut(\"slow\");});</script>";
        }
        else
            ddlCompany.SelectedValue = "1";
        # region Ajax Methods
        if (Request.QueryString["json"] != null)
        {
            var jsonMethod = Request.QueryString["json"];
            var companyid = Request.QueryString["companyid"];
            switch (jsonMethod)
            {
                case "pc":
                    if (Request.QueryString["term"] != null)
                    {
                        var pclist = this.data.GetListofProfitCenters(Int32.Parse(companyid) ).Where(x => x.PCDisplayName.ToLower().Contains(Request.QueryString["term"].ToLower())).Select(x => new JsonLabelValue() { label = x.PCDisplayName, value = x.PC.ToString() }).ToList();
                        Response.Clear();
                        Response.ContentType = "application/json";
                        Response.CacheControl = "no-cache";
                        Response.AddHeader("Pragma", "no-cache");
                        Response.Write(this.serializer.Serialize(pclist));
                        Response.End();
                        return;
                    }
                    break;
                case "ccs":
                    if (Request.QueryString["pcid"] != null)
                    {
                        int pcid;
                        ExtraCCs ccs;
                        if (Int32.TryParse(Request.QueryString["pcid"], out pcid))
                            ccs = this.service.GetExtraCCsForPc(pcid, getddlCompanyID);
                        else
                            ccs = new ExtraCCs();
                        Response.Clear();
                        Response.ContentType = "application/json";
                        Response.CacheControl = "no-cache";
                        Response.AddHeader("Pragma", "no-cache");
                        Response.Write(this.serializer.Serialize(ccs));
                        Response.End();
                        return;
                    }
                    if (Request.QueryString["cid"] != null)
                    {
                        int cid;
                        ExtraCCs ccs;
                        if (Int32.TryParse(Request.QueryString["cid"], out cid))
                            ccs = this.service.GetExtraCCsForCustomer(cid, getddlCompanyID);
                        else
                            ccs = new ExtraCCs();
                        Response.Clear();
                        Response.ContentType = "application/json";
                        Response.CacheControl = "no-cache";
                        Response.AddHeader("Pragma", "no-cache");
                        Response.Write(this.serializer.Serialize(ccs));
                        Response.End();
                        return;
                    }
                    break;
                case "customer":
                    if (Request.QueryString["term"] != null)
                    {
                        var clist = this.data.LookupCustomers(Request.QueryString["term"]).Select(x => new JsonLabelValue() { label = x.Key + " - " + x.Value, value = x.Key }).ToList();
                        Response.Clear();
                        Response.ContentType = "application/json";
                        Response.CacheControl = "no-cache";
                        Response.AddHeader("Pragma", "no-cache");
                        Response.Write(this.serializer.Serialize(clist));
                        Response.End();
                        return;
                    }
                    break;
            }
        #endregion
        }
    }
    protected void SaveItAll(object sender, EventArgs e)
    {
        spanErrorMessage.InnerText = string.Empty;
        var extraccs = new ExtraCCs();
        extraccs.ReservationExtraCCs = Request.Form["ReservationEmails"].Split(',').Where(x => x != string.Empty).ToList();
        extraccs.PickupExtraCCs = Request.Form["PickupEmails"].Split(',').Where(x => x != string.Empty).ToList();
        extraccs.ServiceExtraCCs = Request.Form["ServiceEmails"].Split(',').Where(x => x != string.Empty).ToList();
        extraccs.ReservationTOs = Request.Form["ReservationToEmails"].Split(',').Where(x => x != string.Empty).ToList();
        extraccs.PickupTOs = Request.Form["PickupToEmails"].Split(',').Where(x => x != string.Empty).ToList();
        extraccs.ServiceTOs = Request.Form["ServiceToEmails"].Split(',').Where(x => x != string.Empty).ToList();
        try
        {
            if (ddLookupType.SelectedValue == "Profit Center")
                extraccs.ProfitCenter = Int32.Parse(tbPcSearch.Text);
            else
                extraccs.CustomerNumber = Int32.Parse(tbCustomerSearch.Text);

            extraccs.CompanyID = Int32.Parse(ddlCompany.SelectedValue);
        }
        catch
        {
            spanErrorMessage.InnerText = "Please enter a valid Profit Center Id or Customer Number.";
            return;
        }
        this.data.SaveExtraCCs(extraccs);
    }

    private void PopulateEmails()
    {
        // retrieve data
        ExtraCCs extraccs;

        if (ddLookupType.SelectedValue == "Profit Center")
        {
            extraccs = this.service.GetExtraCCsForPc(Int32.Parse(tbPcSearch.Text), getddlCompanyID);
            litSelectedTitle.InnerText = "" +  ddlCompany.Text + "  Profit Center: " + tbPcSearch.Text; 
        }
        else
        {
            extraccs = this.service.GetExtraCCsForCustomer(Int32.Parse(tbCustomerSearch.Text), getddlCompanyID);
            litSelectedTitle.InnerText = "Customer Number: " + tbCustomerSearch.Text;
        }
        InitialReservationEmails = this.serializer.Serialize(extraccs.ReservationExtraCCs);
        InitialPickupEmails = this.serializer.Serialize(extraccs.PickupExtraCCs);
        InitialServiceEmails = this.serializer.Serialize(extraccs.ServiceExtraCCs);

        InitialReservationToEmails = this.serializer.Serialize(extraccs.ReservationTOs);
        InitialPickupToEmails = this.serializer.Serialize(extraccs.PickupTOs);
        InitialServiceToEmails = this.serializer.Serialize(extraccs.ServiceTOs);
        // load controls
        foreach (var email in extraccs.ReservationExtraCCs)
        {
            var control = (ASP.EmailCC)LoadControl("controls/EmailCC.ascx");
            control.Email = email;
            phReservations.Controls.Add(control);
        }
        foreach (var email in extraccs.PickupExtraCCs)
        {
            var control = (ASP.EmailCC)LoadControl("controls/EmailCC.ascx");
            control.Email = email;
            phPickupRequests.Controls.Add(control);
        }
        foreach (var email in extraccs.ServiceExtraCCs)
        {
            var control = (ASP.EmailCC)LoadControl("controls/EmailCC.ascx");
            control.Email = email;
            phServiceRequests.Controls.Add(control);
        }
    }
     protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddl = sender as DropDownList;
        hfCompanyID.Value = ddl.SelectedValue;
        tbPcSearch.Text = string.Empty;
        tbCustomerSearch.Text = string.Empty;
    }

}