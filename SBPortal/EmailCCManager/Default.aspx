﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Minor.master" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Reference Control="Controls/EmailCC.ascx" %>
<%@ Register Src="Controls/EmailCC.ascx" TagName="EmailCC" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
    <style>
        .cc-column
        {
            display: inline-block;
            padding-right: 20px;
            vertical-align: top;
        }
        #results
        {
            padding-bottom: 30px;
        }
        #saveMessage
        {
            background-color: #fdf2af;
            color: #000000;
            font-size: larger;
            font-weight: bolder;
            width: 40%;
            border: 1px solid #a9a4a4;
            text-align: center;
        }
    </style>
    <link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.20/themes/base/jquery-ui.css"
        rel="Stylesheet" />
	</asp:Content>
	<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.20/jquery-ui.min.js" type="text/javascript"></script>
    <script src="scripts/default.js" type="text/javascript"></script>
    <asp:Literal ID="ltExtraHeaderCode" runat="server"></asp:Literal>
	</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
    Sunbelt Email CC Manager
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">
    <script type="text/javascript">
    var ReservationEmails = <%=InitialReservationEmails %>;
    var PickupEmails = <%=InitialPickupEmails %>;
    var ServiceEmails = <%=InitialServiceEmails %>;
    var ReservationToEmails = <%=InitialReservationToEmails %>;
    var PickupToEmails = <%=InitialPickupToEmails %>;
    var ServiceToEmails = <%=InitialServiceToEmails %>;
    </script>
    <div>
        <div style="margin-bottom:8px;">Company&nbsp;&nbsp;:&nbsp;<asp:DropDownList ID="ddlCompany" data-id="ddlCompany"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
            <asp:ListItem Value="1" Text="United States" Selected="True">United States</asp:ListItem>
            <asp:ListItem Value="2" Text="Canada">Canada</asp:ListItem>
        </asp:DropDownList>

        </div>
 
        <input type="hidden" id="hfCompanyID" name="CompanyID"  runat="server" />
        <div style="margin-bottom:8px;">
        <label style="width:30px;" for="DropDownList1">
            Lookup By:</label>
        <asp:DropDownList ID="ddLookupType" runat="server" data-id="ddLookupType" onkeydown = "return (event.keyCode!=13);">
            <asp:ListItem>Profit Center</asp:ListItem>
            <asp:ListItem>Customer</asp:ListItem>
        </asp:DropDownList>
            </div>
        <label for="TextBox2">
            Search&nbsp;&nbsp;&nbsp;&nbsp;:</label>
        <asp:TextBox ID="tbPcSearch" runat="server" data-id="pctextbox" Width="264px" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
        <asp:TextBox ID="tbCustomerSearch" runat="server" data-id="customertextbox" Width="264px"
            Style="display: none;" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
        <h3><span id="litSelectedTitle" runat="server" data-id="SelectedTitle"></span></h3>
        <div id="results">
            <div id="ReservationColumn" class="cc-column">
                <h2>
                    Reservations CC</h2>
                <input type="text" id="tbNewReservationCC" />
                <input type="button" value="+" id="btnReservationEmailAdd" /><br />
                <div id="phReservations" runat="server" data-id="divReservations">
                </div>
            </div>
            <div id="PickupsColumn" class="cc-column">
                <h2>
                    Pickup Requests CC</h2>
                <input type="text" id="tbNewPickupCC" />
                <input type="button" value="+" id="btnPickupEmailAdd" /><br />
                <div id="phPickupRequests" runat="server" data-id="divPickups" />
            </div>
            <div id="ServiceColumn" class="cc-column">
                <h2>
                    Service Requests CC</h2>
                <input type="text" id="tbNewServiceCC" />
                <input type="button" value="+" id="btnServiceEmailAdd" /><br />
                <div id="phServiceRequests" runat="server" data-id="divService" />
            </div>
            <div id="TheTosFields">
                <div id="ReservationToColumn" class="cc-column">
                    <h2>
                        Reservation To</h2>
                    <input type="text" id="tbNewReservationTO" />
                    <input type="button" value="+" id="btnReservationToEmailAdd" /><br />
                    <div id="phReservationsTo" runat="server" data-id="divReservationsTo" />
                </div>

                <div id="SetPickupRequestToColumn" class="cc-column">
                    <h2>
                        Pickup Request To</h2>
                    <input type="text" id="tbNewPickupTO" />
                    <input type="button" value="+" id="btnPickupToEmailAdd" /><br />
                    <div id="phPickupsTo" runat="server" data-id="divPickupsTo" />
                </div>
                <div id="ServiceToColumn" class="cc-column">
                    <h2>
                        Service To</h2>
                    <input type="text" id="tbNewServiceTO" />
                    <input type="button" value="+" id="btnServiceToEmailAdd" /><br />
                    <div id="phServiceTo" runat="server" data-id="divServiceTo" />
                </div>
            </div>
        </div>
        <input type="hidden" id="hfReservationEmails" name="ReservationEmails" />
        <input type="hidden" id="hfPickupEmails" name="PickupEmails" />
        <input type="hidden" id="hfServiceEmails" name="ServiceEmails" />
        <input type="hidden" id="hfReservationToEmails" name="ReservationToEmails" />
        <input type="hidden" id="hfPickupToEmails" name="PickupToEmails" />
        <input type="hidden" id="hfServiceToEmails" name="ServiceToEmails" />
        <div id="saveCautionMessage" ><h4>Caution:  Adding To's will override the default emails(PCxxx, PCMxxxx, DSPxxxx, etc...) </h4></div>
        <asp:Button ID="btnSave" runat="server" Text="Save" data-id="btnSave" OnClick="SaveItAll"/>
        <div id="saveMessage" style="display: none;">
            Email CCs Successfully Saved.</div>
        <span id="spanErrorMessage" runat="server" class="errortxt"></span>
    </div>
    <div id="extraEmail" style="display: none;">
        <uc1:EmailCC ID="EmailCC1" runat="server" />
    </div>
</asp:Content>