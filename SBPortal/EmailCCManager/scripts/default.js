﻿if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 0) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}
$(document).ready(function () {
    var basepath = "./";
    var pctextbox = $('*[data-id="pctextbox"]');
    var customertextbox = $('*[data-id="customertextbox"]');
    UpdateHiddenFields();
    UpdateSearchTextbox();
    $('*[data-id="ddLookupType"]').change(function () {
        UpdateSearchTextbox();
    });
    $('*[data-id="ddlCompany"]').change(function () {
        UpdateCompanyTextbox();
    });
    pctextbox.bind("autocompleteselect", function (event, ui) {
        //console.log(ui.item.id);
        JsonPageUpdate({ pcid: ui.item.id, companyName: $('*[data-id="ddlCompany"] option:selected').text() });
    });
    customertextbox.bind("autocompleteselect", function (event, ui) {
    //    console.log(ui.item.id);
        JsonPageUpdate({ cid: ui.item.id, companyName: $('*[data-id="ddlCompany"] option:selected').text() });
    });
    pctextbox.autocomplete({
        source: basepath + "Default.aspx?json=pc&companyID=" + $('*[data-id="ddlCompany"]').val() + "&companyName=" + $('*[data-id="ddlCompany"] option:selected').text(),
        minLength: 1
    });
    customertextbox.autocomplete({
        source: basepath + "Default.aspx?json=customer&companyID=" + $('*[data-id="ddlCompany"]').val() + "&companyName=" + $('*[data-id="ddlCompany"] option:selected').text(),
        minLength: 1
    });
    function JsonPageUpdate(requestdata) {
        ToggleSubmitable(false);
        ToggleVisibleTos(true);

        $('*[data-id="SelectedTitle"]').text("");
        if (requestdata.pcid) {
            $('*[data-id="SelectedTitle"]').text("Profit Center: " + requestdata.pcid + " (" + requestdata.companyName + ")");
            ToggleSubmitable(true);
            ToggleVisibleTos(true);
        } else if (requestdata.cid) {
            $('*[data-id="SelectedTitle"]').text("Customer Number: " + requestdata.cid + " (" + requestdata.companyName + ")");
            ToggleSubmitable(true);
            ToggleVisibleTos(false);

        }
        $.getJSON(basepath + "Default.aspx?json=ccs", requestdata, function (json) {
            //console.log(json);
            LoadExtraCCVariables(json);
            UpdateCCColumns();
            UpdateHiddenFields();
        });
    }
    $("#tbNewReservationCC").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewReservationToEmail($(this).val());
            return false;
        }
    });
    $("#btnReservationEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewReservationEmail(newval);
    });
    function AddNewReservationEmail(newval) {
        if (newval != "" && ReservationEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divReservations");
            ReservationEmails.push(newval);
            UpdateHiddenFields();
        }
    }
    $("#tbNewPickupCC").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewPickupEmail($(this).val());
            return false;
        }
    });
    $("#btnPickupEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewPickupEmail(newval);
    });
    function AddNewPickupEmail(newval) {
        if (newval != "" && PickupEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divPickups");
            PickupEmails.push(newval);
            UpdateHiddenFields();
        }
    }
    $("#tbNewServiceCC").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewServiceEmail($(this).val());
            return false;
        }
    });
    $("#btnServiceEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewServiceEmail(newval);
    });

    function AddNewServiceEmail(newval) {
        if (newval != "" && ServiceEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divService");
            ServiceEmails.push(newval);
            UpdateHiddenFields();
        }
    }
/***************************/


    $("#btnReservationToEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewReservationToEmail(newval);
    });

    $("#tbNewReservationTO").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewReservationToEmail($(this).val());
            return false;
        }
    });
    $("#btnReservationEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewReservationEmail(newval);
    });

    function AddNewReservationToEmail(newval) {
        if (newval != "" && ReservationToEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divReservationsTo");
            ReservationToEmails.push(newval);
            UpdateHiddenFields();
        }
    }
/**/
    $("#tbNewPickupTO").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewPickupToEmail($(this).val());
            return false;
        }
    });
    $("#btnPickupToEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewPickupToEmail(newval);
    });
    function AddNewPickupToEmail(newval) {
        if (newval != "" && PickupToEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divPickupsTo");
            PickupToEmails.push(newval);
            UpdateHiddenFields();
        }
    }
/**/
    $("#tbNewServiceTO").keydown(function (e) {
        if (e.keyCode == 13) {
            AddNewServiceToEmail($(this).val());
            return false;
        }
    });
    $("#btnServiceToEmailAdd").click(function () {
        var newval = $(this).prev("input").val();
        AddNewServiceToEmail(newval);
    });

    function AddNewServiceToEmail(newval) {
        if (newval != "" && ServiceToEmails.indexOf(newval) == -1) {
            CloneEmailElement(newval, "divServiceTo");
            ServiceToEmails.push(newval);
            UpdateHiddenFields();
        }
    }
/**/



    $('.emailelement > input[name="remove"]').click(function () {
        var emailelement = $(this).parent();
        if (emailelement.parent().data("id") == "divReservations") {
            //console.log("removing array element from reservations");
            var index = ReservationEmails.indexOf(emailelement.children("span").html());
            ReservationEmails.splice(index, 1);
        }
        if (emailelement.parent().data("id") == "divPickups") {
            //console.log("removing array element from pickups");
            var index = PickupEmails.indexOf(emailelement.children("span").html());
            PickupEmails.splice(index, 1);
        }
        if (emailelement.parent().data("id") == "divService") {
            //console.log("removing array element from service");
            var index = ServiceEmails.indexOf(emailelement.children("span").html());
            ServiceEmails.splice(index, 1);
        }

        if (emailelement.parent().data("id") == "divReservationsTo") {
            //console.log("removing array element from reservations");
            var index = ReservationToEmails.indexOf(emailelement.children("span").html());
            ReservationToEmails.splice(index, 1);
        }
        if (emailelement.parent().data("id") == "divPickupsTo") {
            //console.log("removing array element from pickups");
            var index = PickupToEmails.indexOf(emailelement.children("span").html());
            PickupToEmails.splice(index, 1);
        }
        if (emailelement.parent().data("id") == "divServiceTo") {
            //console.log("removing array element from service");
            var index = ServiceToEmails.indexOf(emailelement.children("span").html());
            ServiceToEmails.splice(index, 1);
        }

        emailelement.remove();
        UpdateHiddenFields();
    });
    function UpdateSearchTextbox() {
        var selected = $('*[data-id="ddLookupType"] option:selected').val();
        if (selected == "Customer") {
            pctextbox.hide();
            customertextbox.show();
            JsonPageUpdate({ cid: customertextbox.val(), companyName: $('*[data-id="ddlCompany"] option:selected').text() });
        } else {
            pctextbox.show();
            customertextbox.hide();
            JsonPageUpdate({ pcid: pctextbox.val(), companyName: $('*[data-id="ddlCompany"] option:selected').text() });
        }
    }
    function UpdateCompanyTextbox() {
        var selected = $('*[data-id="ddlCompany"] option:selected').val();
        if (selected == "Customer") {
            pctextbox.hide();
            customertextbox.show();
            JsonPageUpdate({ cid: customertextbox.val(), companyName: $('*[data-id="ddlCompany"] option:selected').text() });
        } else {
            pctextbox.show();
            customertextbox.hide();
            JsonPageUpdate({ pcid: pctextbox.val(), companyName: $('*[data-id="ddlCompany"] option:selected').text() });
        }
    }
    function UpdateHiddenFields() {
        $('#hfReservationEmails').val(ReservationEmails);
        $('#hfPickupEmails').val(PickupEmails);
        $('#hfServiceEmails').val(ServiceEmails);

        $('#hfReservationToEmails').val(ReservationToEmails);
        $('#hfPickupToEmails').val(PickupToEmails);
        $('#hfServiceToEmails').val(ServiceToEmails);
       
    }
    function LoadExtraCCVariables(extraccs) {
        ReservationEmails = extraccs.ReservationExtraCCs;
        PickupEmails = extraccs.PickupExtraCCs;
        ServiceEmails = extraccs.ServiceExtraCCs;

        ReservationToEmails = extraccs.ReservationTOs;
        PickupToEmails = extraccs.PickupTOs;
        ServiceToEmails = extraccs.ServiceTOs;
    }
    function UpdateCCColumns() {
        $('div[data-id="divReservations"]').empty();
        $('div[data-id="divPickups"]').empty();
        $('div[data-id="divService"]').empty();
        $('div[data-id="divReservationsTo"]').empty();
        $('div[data-id="divPickupsTo"]').empty();
        $('div[data-id="divServiceTo"]').empty();

        for (var i = 0; i < ReservationEmails.length; i++) {
            CloneEmailElement(ReservationEmails[i], "divReservations");
        }
        for (var i = 0; i < PickupEmails.length; i++) {
            CloneEmailElement(PickupEmails[i], "divPickups");
        }
        for (var i = 0; i < ServiceEmails.length; i++) {
            CloneEmailElement(ServiceEmails[i], "divService");
        }

        for (var i = 0; i < ReservationToEmails.length; i++) {
            CloneEmailElement(ReservationToEmails[i], "divReservationsTo");
        }
        for (var i = 0; i < PickupToEmails.length; i++) {
            CloneEmailElement(PickupToEmails[i], "divPickupsTo");
        }
        for (var i = 0; i < ServiceToEmails.length; i++) {
            CloneEmailElement(ServiceToEmails[i], "divServiceTo");
        }
    }
    function CloneEmailElement(emailaddy, appendtoelementdataid) {
        $('#extraEmail > .emailelement').clone(true)
            .appendTo('div[data-id="' + appendtoelementdataid + '"]').children("span").html(emailaddy);
    }
    function ToggleSubmitable(enabled) {
        $("#tbNewReservationCC").prop('disabled', !enabled);
        $("#tbNewPickupCC").prop('disabled', !enabled);
        $("#tbNewServiceCC").prop('disabled', !enabled);
        $('#btnReservationEmailAdd').prop('disabled', !enabled);
        $('#btnPickupEmailAdd').prop('disabled', !enabled);
        $('#btnServiceEmailAdd').prop('disabled', !enabled);

        $("#tbNewReservationTO").prop('disabled', !enabled);
        $("#tbNewPickupTO").prop('disabled', !enabled);
        $("#tbNewServiceTO").prop('disabled', !enabled);
        $('#btnReservationToEmailAdd').prop('disabled', !enabled);
        $('#btnPickupToEmailAdd').prop('disabled', !enabled);
        $('#btnServiceToEmailAdd').prop('disabled', !enabled);

        $('*[data-id="btnSave"]').prop('disabled', !enabled);
    }


    function ToggleVisibleTos( isSearchByPC)  {
        if (isSearchByPC) {
            $("#tbNewReservationTO").show();
            $("#tbNewPickupTO").show();
            $("#tbNewServiceTO").show();
            $('#btnReservationToEmailAdd').show();
            $('#btnPickupToEmailAdd').show();
            $('#btnServiceToEmailAdd').show();
            $("#TheTosFields").show();
            $("#saveCautionMessage").show();
        }
        else
        {
            $("#tbNewReservationTO").hide();
            $("#tbNewPickupTO").hide();
            $("#tbNewServiceTO").hide();
            $('#btnReservationToEmailAdd').hide();
            $('#btnPickupToEmailAdd').hide();
            $('#btnServiceToEmailAdd').hide();
            $("#TheTosFields").hide();
            $("#saveCautionMessage").hide();
        }
        
    }
});