using DeleteAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SBNet;
using Sunbelt;
using Sunbelt.Common.DAL;
using Sunbelt.Security;
using Sunbelt.Tools;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using UserProfileExtensions;

public partial class SBPortal_Default : Sunbelt.Web.WebPage
{
    private enum PageState { Error, CustomerService, Employee, Redirect }
    private PageState State
    {
        get { return (PageState)GetViewState("State", PageState.Error); }
        set { SetViewState("State", value); }
    }
    private string CurCSUser
    {
        get { return (string)GetViewState("CurCSUser", ""); }
        set { SetViewState("CurCSUser", value); }
    }
    private int DefaultAccount
    {
        get { return (int)GetViewState("defAcc", 0); }
        set { SetViewState("defAcc", value); }
    }
    private bool ShowUserList
    {
        get { return (bool)GetViewState("UserList", false); }
        set { SetViewState("UserList", value); }
    }
    private bool HideEmployees
    {
        get { return ConvertTools.ToBoolean(WebTools.GetCookie("SBPortal", "HideEmp", "false")); }
        set { WebTools.SetCookie("SBPortal", "HideEmp", value.ToString().ToLower()); }
    }
    private string DomainUserName
    {
        get { return Sunbelt.WebUser.CurrentUser.DomainUserName.ToString(); }
        set {; }
    }

    protected string RedirectUrl;
    public string isOKTAMigrated { get; private set; }
    public string isOKTACreated { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Check if logged in.
        WebUser.VerifySBPortalLogin();

        //log out of the web site.
        SBNet.SBNetSqlMembershipProvider.LogoutUser();

        //Do stuff...
        if (!IsPostBack)
        {
            lbSwitch.Visible = false;
            tbEmail.Focus();

            //Hide this...
            rcpChangePassword.Style["display"] = "none";
            rcpChangePassword.Style["position"] = "absolute";
            rcpChangePassword.Attributes["onmousedown"] = "javascript:event.cancelBubble = true";
            rcpChangePassword.Style["z-index"] = "10001";

            btnSavePassword.OnClientClick = "javascript:return OnSavePasswordClick();";
            btnCancelPassword.OnClientClick = "javascript:return OnCancelPasswordClick();";

            WebUserInfo ui = WebUser.CurrentUser;

            //User must have a clock id.
            if (ui.ClockNumber == 0)
            {
                lbError.Text = "Your Sunbelt domain account has no associated Clock Number.";
                return;
            }

            UserProfile up = CheckForWebAccount(ui);
            if (up == null)
                return;

            tbEmail.Text = up.email;

            cbHideEmployees.Checked = HideEmployees;

            //Check if the user has an account. If not put them in employee mode
            // so they can pick one.
            List<SecurityAccess> accountList = UserSecurity.GetAccessByUser(up.email);

            if (ui.IsCustomerService && (accountList.Count > 0))
            {
                //If returning from 'SBAccountMaintenance.aspx', the user name is passed
                // on the query string so we can reload the user's profile.
                if (Request.QueryString["wcan"] != null)
                {
                    SecureQueryString sqs = new SecureQueryString(Request.QueryString["wcan"]);

                    qcImpersonate.Text = sqs["UserName"];
                    btnGetUser_Click(this, (ImageClickEventArgs)null);
                }

                State = PageState.CustomerService;
            }
            else //Employee mode
            {
                //Get user account.
                if (accountList.Count > 0)
                {
                    DefaultAccount = accountList[0].AccountNumber;
                    qcAccount.Text = accountList[0].AccountNumber.ToString();
                }
                else
                {
#if DEBUG
					qcAccount.Text = "5";
#else
                    qcAccount.Text = "1";
#endif
                }

                State = PageState.Employee;
            }
            sdsUsers.SelectParameters["ApplicationName"].DefaultValue = Membership.ApplicationName;
        }
        UpdateDisplay();

        ViewProfile1.UserProfileUpdated += ViewProfile1_UserProfileUpdated;
        ViewProfile1.CashCustomerConverted += ViewProfile1_CashCustomerConverted;
    }

    protected override void OnPreRender(EventArgs e)
    {
        SBNet.Tools.IncludeEntryControlsClientScript(this);

        base.OnPreRender(e);
    }

    protected string ConvertTime(DateTime dt)
    {
        dt = TimeZoneInfo.ConvertTime(dt,
            TimeZoneInfo.Utc,
            TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
        return Sunbelt.Tools.DateTimeTools.ToShortestDateTimeString(dt);
    }

    protected DateTime ToEST(DateTime dt)
    {
        return TimeZoneInfo.ConvertTime(dt,
            TimeZoneInfo.Utc,
            TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
    }

    private UserProfile CheckForWebAccount(WebUserInfo ui)
    {
        //Check if employee has a web account.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        UserProfile up = p.GetUserProfile(ui.ClockNumber);

        if (up == null)
        {
            up = p.GetUserProfile(ui.UserEmail);
            if (up == null)
            {
                string lastTryEmail = ui.DomainUserName + "@sunbeltrentals.com";
                up = p.GetUserProfile(lastTryEmail);
                if (up == null)
                {
                    //Redirect to 'AddUser.aspx'.
                    Sunbelt.Security.SecureQueryString sqs = new SecureQueryString();
                    sqs["EbClock"] = ui.ClockNumber.ToString();
                    RedirectUrl = ResolveUrl("~/AddUser.aspx") + "?wcan=" + sqs.ToString();
                    hlAddUser.NavigateUrl = RedirectUrl;

                    State = PageState.Redirect;
                    UpdateDisplay();

                    return null;
                }
            }
        }
        return up;
    }

    private void UpdateDisplay()
    {
        WebUserInfo ui = WebUser.CurrentUser;
        divCustomerService.Visible = false;
        divEmpolyee.Visible = false;
        divRedirect.Visible = false;
        divUserList.Visible = false;
        divError.Visible = false;
        switch (State)
        {
            case PageState.CustomerService:
                divCustomerService.Visible = true;
                btnDeleteUser.OnClientClick = "javascript:return OnDeleteUser();";

                divUserList.Visible = ShowUserList;
                btnUserList.ImageUrl = (ShowUserList) ? ResolveUrl("~/App_Themes/Main/Buttons/user_list_up_100x20_darkgray.gif") : ResolveUrl("~/App_Themes/Main/Buttons/user_list_down_100x20_darkgray.gif");

                break;
            case PageState.Employee:
                divEmpolyee.Visible = true;
                break;
            case PageState.Redirect:
                divRedirect.Visible = true;
                break;
            default: //Error
                divError.Visible = true;
                break;
        }
    }

    private void ShowUserInfo(string userName)
    {
        divUserInformation.Visible = true;
        MembershipUser mu;
        SBNet.CashCustomer cc;
        ViewProfile1.ViewProfile(userName, out mu, out cc);
        btnUserManageAccounts.Visible = (cc == null);
    }

    private void HideUserInfo()
    {
        divUserInformation.Visible = false;
    }

    private bool VerifyUser(string userName, bool bFullCheck, out MembershipUser mu)
    {
        //get user profile.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        mu = p.GetUser(userName, false);

        if (mu != null)
        {
            if (!bFullCheck)
                return true;

            if (!mu.IsApproved)
            {
                UPValidationSummary1.DisplayError("User account is has not been approved.  Click 'Unlock Account' to Approve this account.");
            }
            else
            if (mu.IsLockedOut)
            {
                UPValidationSummary1.DisplayError("User account is locked out.  Click 'Unlock Account' to unlock this account.");
            }
            else
            {
                //Account is okay.
                return true;
            }
        }
        else
        {
            UPValidationSummary1.DisplayError("There is no account for '" + userName + "' in the user database.");
        }
        return false;
    }

    private bool CheckCSUser()
    {
        string userName = qcImpersonate.Text.Trim();
        if (CurCSUser != userName)
        {
            UPValidationSummary1.DisplayError("The User Email value '" + userName + "' does not match the displayed user profile information. Click the \"Get User\" button to update the user profile.");
            return false;
        }
        return true;
    }

    protected void btnLogin_Click(object sender, ImageClickEventArgs e)
    {
        if (!IsValid)
            return;

        //Verify user against aspnetdb.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        MembershipUser mu = p.GetUser(tbEmail.Text, false);
        if (mu == null)
        {
            UPValidationSummary1.DisplayError("There is no account for '" + tbEmail.Text + "' in the user database.");
            return;
        }

        //Verify account.
        int account = ConvertTools.ToInt32(qcAccount.Text);
        QuickAccountInfo qai = Portal.GetCustomerAccount(account);
        if (qai == null)
        {
            UPValidationSummary1.DisplayError("Account " + qcAccount.Text + " does not exist. Please enter a valid account.");
            return;
        }

        //Unlock user account if needed.
        if (!mu.IsApproved && !p.ApproveUser(mu.UserName))
        {
            UPValidationSummary1.DisplayError("Unable to approve user account.");
            return;
        }
        if (mu.IsLockedOut && !p.UnlockUser(mu.UserName))
        {
            UPValidationSummary1.DisplayError("Unable to unlock user account.");
            return;
        }

        //Update the EbClock if needed.
        UserProfile up = p.GetUserProfile(tbEmail.Text);
        WebUserInfo ui = WebUser.CurrentUser;
        if (up.ebClock != ui.ClockNumber)
            p.UpdateEmployeeClockNumber(mu.UserName, ui.ClockNumber);

        //Change their default account if needed.
        if (account != DefaultAccount)
        {
            //Delete current account.
            UserSecurity.DeleteSecurityAccess(mu.UserName, null, null);

            //Set this as the user's account.
            SBNet.UserCustomerAccount uca = new SBNet.UserCustomerAccount(qai);
            SecurityAccess usa = new SecurityAccess();
            usa.AccountNumber = account;
            usa.SelectAll();

            //Insert new record
            UserSecurity.InsertSecurityAccess(mu.UserName, uca, usa);
        }
        //Save this cookie to use else where. Set the path to the root so
        // the cookie is availble to all pages.
        WebTools.SetCookie("/", "User", "Email", tbEmail.Text, 0, 0);

        //Log them in...
        SBNet.SBNetSqlMembershipProvider.LoginUser(tbEmail.Text);
    }

    protected void btnGetUser_Click(object sender, ImageClickEventArgs e)
    {
        string userName = qcImpersonate.Text.Trim();

        HideUserInfo();
        MembershipUser mu;
        CurCSUser = "";
        if (VerifyUser(userName, false, out mu))
        {
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " is getting info of user: " + userName, userName: userName);
            CurCSUser = userName;
            ShowUserInfo(userName);
        }

        btnSetPassword.Visible = true;
        btnSetPassword.ToolTip = "Users can update their own password via Mobile App or Command Center";
    }

    protected void btnUserManageAccounts_Click(object sender, ImageClickEventArgs e)
    {
        if (!CheckCSUser())
            return;

        SecureQueryString sqs = new SecureQueryString();
        sqs["UserName"] = CurCSUser;

        //Redirect to account maintenance and pass the username in the querystring.
        Response.Redirect("~/SBPortal/SBAccountMaintenance.aspx?wcan=" + sqs.ToString());
    }

    protected void btnUnlockAccount_Click(object sender, ImageClickEventArgs e)
    {
        if (!CheckCSUser())
            return;

        string userName = qcImpersonate.Text;

        //Verify user against aspnetdb.
        MembershipUser mu;
        if (!VerifyUser(userName, false, out mu))
            return;

        Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " is attempting to unlock user account: " + userName, userName: userName);

        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        mu = p.GetUser(userName, true);

        if (!mu.IsApproved && !p.ApproveUser(userName))
        {
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " was unable to approve user account: " + userName, userName: userName);
            UPValidationSummary1.DisplayError("Unable to approve user account.");
        }
        if (mu.IsLockedOut && !p.UnlockUser(userName))
        {
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " was unable to unlock user account: " + userName, userName: userName);
            UPValidationSummary1.DisplayError("Unable to unlock user account.");
        }
        InformationSummary1.AddInfo("User account has been unlocked.");
        Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " has unlocked user account: " + userName, userName: userName);
        ShowUserInfo(userName);
    }

    protected void btnSetPassword_Click(object sender, ImageClickEventArgs e)
    {
        if (!CheckCSUser())
            return;

        string userName = qcImpersonate.Text;

        //Verify user against aspnetdb.
        MembershipUser mu;
        if (!VerifyUser(userName, false, out mu))
            return;

        //We need the old password to change it.  Since the user's password is encrypted,
        // reset it. This returns a new system generated password. We can use it to change
        // the user's password to the new value they entered.
        string oldPassword = mu.ResetPassword();
        string newPassword = tbNewPassword.Text;
        string errorMessage = "";
        //get user profile.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        UserProfile up = p.GetUserProfile(userName);

        //CCC-1040 PS - 997 Password validation
        if (!ValidatePassword(up.firstName, up.lastName, newPassword, out errorMessage))
        {
            UPValidationSummary1.DisplayError(errorMessage);
        }
        else
        {
            //Now change it.
            if (!mu.ChangePassword(oldPassword, newPassword))
            {
                Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " was unable to set password for: " + userName, userName: userName);
                UPValidationSummary1.DisplayError("Unable to change user's password to \"" + newPassword + "\".");
            }
            else
            {
                Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " set password for: " + userName, userName: userName);
                InformationSummary1.AddInfo("User's password has been changed to \"" + newPassword + "\".");
            }
        }
    }

    protected void btnDeleteUser_Click(object sender, ImageClickEventArgs e)
    {
        if (!CheckCSUser())
            return;

        string userName = qcImpersonate.Text;

        //Verify user against aspnetdb.
        MembershipUser mu;
        if (!VerifyUser(userName, false, out mu))
            return;

        Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " is attempting to delete user account: " + userName, userName: userName);

        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        UserProfile up = p.GetUserProfile(userName);

        string country = up.country.Trim().ToUpper();
        string address = string.Join(" ", up.address1, up.address2, up.address3, up.state);
        bool isCanada = country.Equals("CANADA") || up.IsCanadaUser(address);

        if (Convert.ToBoolean(ConfigurationManager.AppSettings["UseMuleDeleteAPI"]))
        {
            var ms = GetMigrationStatusByUserId(up.userGuid.ToString());
            string oktaId = "";
            if (ms != null)
                oktaId = ms.OktaID;
            DeleteUserRequest deleteUserRequest = new DeleteUserRequest
            {
                Email = mu.Email,
                OktaUserId = oktaId,
                CompanyId = isCanada ? "02" : "01",
                UserGuid = up.userGuid.ToString()
            };

            string response = null;
            response = DeleteUserProfile(deleteUserRequest);

            if (response == "")
            {
                CurCSUser = "";
                InformationSummary1.AddInfo("User \"" + userName + "\" has been deleted from the Web User database.");
                HideUserInfo();
                qcImpersonate.Text = "";
                BuildFilterExpression();
                Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " deleted user account: " + userName, userName: userName);
            }
            else
            {
                UPValidationSummary1.DisplayError("Unable to delete the user account. " + response);
            }
        }
        else
        {
            //DELETE HERE....
            if (p.DeleteUser(CurCSUser, true))
            {
                CurCSUser = "";
                InformationSummary1.AddInfo("User \"" + userName + "\" has been deleted from the Web User database.");
                HideUserInfo();
                qcImpersonate.Text = "";
                BuildFilterExpression();
                Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, DomainUserName + " deleted user: " + userName + " in SWS", userName: userName);
            }
            else
                UPValidationSummary1.DisplayError("Unable to delete the user account.");
        }
    }

    protected void lbSwitch_Click(object sender, EventArgs e)
    {
        if (State == PageState.CustomerService)
            State = PageState.Employee;
        else
            State = PageState.CustomerService;

        UpdateDisplay();
    }
    protected void btnUserList_Click(object sender, ImageClickEventArgs e)
    {
        ShowUserList = !ShowUserList;
        UpdateDisplay();
    }

    protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "LoadUser")
        {
            string userEmail = ((LinkButton)e.CommandSource).Text;
            qcImpersonate.Text = userEmail;
            btnGetUser_Click(sender, new ImageClickEventArgs(0, 0));
            ShowUserList = false;
            UpdateDisplay();
        }
    }

    protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void gvUsers_Sorting(object sender, GridViewSortEventArgs e)
    {
        BuildFilterExpression();
    }

    protected void btnUserSearch_Click(object sender, ImageClickEventArgs e)
    {
        BuildFilterExpression();
    }

    protected void cbHideEmployees_CheckedChanged(object sender, EventArgs e)
    {
        HideEmployees = cbHideEmployees.Checked;
        BuildFilterExpression();
    }

    private void BuildFilterExpression()
    {
        StringBuilder sbFilter = new StringBuilder();

        string search = tbSearchText.Text.Trim();
        if (!string.IsNullOrEmpty(search))
        {
            sbFilter.Append("(Email like '%" + search + "%'");
            sbFilter.Append(" OR FirstName like '%" + search + "%'");
            sbFilter.Append(" OR LastName like '%" + search + "%')");
        }

        //if (cbHideEmployees.Checked)
        if (HideEmployees)
        {
            if (sbFilter.Length > 0)
                sbFilter.Append(" AND ");
            sbFilter.Append("(EbClock = 0 OR EbClock is null)");
        }

        sdsUsers.FilterParameters.Clear();
        sdsUsers.FilterExpression = sbFilter.ToString();
    }

    protected void ViewProfile1_UserProfileUpdated(object sender, EventArgs e)
    {
        InformationSummary1.AddInfo("User profile has been updated.");
    }
    protected void ViewProfile1_CashCustomerConverted(object sender, EventArgs e)
    {
        InformationSummary1.AddInfo("User converted from cash customer to credit customer.");
        ShowUserInfo(CurCSUser);
    }

    //PS - 997 CCC-1138 Strong Password Validation Rules
    private bool ValidatePassword(string firstName, string lastName, string password, out string ErrorMessage)
    {
        var input = password;
        ErrorMessage = string.Empty;

        var hasNumber = new Regex(@"[0-9]+");
        var hasUpperChar = new Regex(@"[A-Z]+");
        var hasMiniMaxChars = new Regex(@".{8,128}");
        var hasLowerChar = new Regex(@"[a-z]+");
        var hasSymbols = new Regex(@"[!@#$%^&*()_+=\[{\]};:<>|./?,-]");
        var hasIdentical = new Regex(@"((.)\2{1})\2+");

        if (input.Contains(firstName.ToLower()) || input.Contains(lastName.ToLower())
            || input.Contains(firstName.ToUpper()) || input.Contains(lastName.ToUpper())
            || input.Contains(firstName) || input.Contains(lastName))
        {
            ErrorMessage = "Password should not contain username, first name or last name.";
            return false;
        }
        else if (!hasMiniMaxChars.IsMatch(input))
        {
            ErrorMessage = "Password should not be lesser than 8 or greater than 128 characters.";
            return false;
        }
        else if (input.IndexOf(' ') != -1)
        {
            ErrorMessage = "Password should not contain space.";
            return false;
        }
        else if (hasIdentical.IsMatch(input))
        {
            ErrorMessage = "Password should not contain more than 2 identical characters in a row.";
            return false;
        }
        else if (hasUpperChar.IsMatch(input) && hasLowerChar.IsMatch(input) && hasNumber.IsMatch(input))
        {
            return true;
        }
        else if (hasUpperChar.IsMatch(input) && hasLowerChar.IsMatch(input) && hasSymbols.IsMatch(input))
        {
            return true;
        }

        else if (hasUpperChar.IsMatch(input) && hasNumber.IsMatch(input) && hasSymbols.IsMatch(input))
        {
            //ErrorMessage = "Password should contain at least one special character.";
            return true;
        }
        else if (hasNumber.IsMatch(input) && hasLowerChar.IsMatch(input) && hasSymbols.IsMatch(input))
        {
            return true;
        }
        else if (!hasLowerChar.IsMatch(input))
        {
            ErrorMessage = "Password should contain at least one lower case letter.";
            return false;
        }
        else if (!hasUpperChar.IsMatch(input))
        {
            ErrorMessage = "Password should contain at least one upper case letter.";
            return false;
        }
        else if (!hasNumber.IsMatch(input))
        {
            ErrorMessage = "Password should contain at least one numeric value.";
            return false;
        }
        else
        {
            return true;
        }
    }

    // https://sunbelt.atlassian.net/wiki/spaces/IS/pages/3270508581/Delete+Account#SB-Portal-Delete-User-Experience-API
    /*
        Dev URL: https://devint-api.sunbeltrentals.com

        curl --location --request DELETE 'https://qa-api.sunbeltrentals.com/adminportal/api/v1/customers' \
        --header 'client_id: 0oa2p3us3gEE3mz7K1d7' \
        --header 'client_secret: DMnb4W-tR_N1yYCbbGio8C8R2Py_Omaq3v9s0cSh' \
        --header 'companyId: 1' \
        --header 'x-correlation-id: admin-portal-xapi-44573' \
        --header 'email: test@domain.com' \
        --header 'oktaUserId: 00u3tltqbhur8hpw11d7'
    */
    public string DeleteUserProfile(DeleteUserRequest deleteUserRequest)
    {
        string responseStr = String.Empty;
        string userName = deleteUserRequest != null ? deleteUserRequest.Email : string.Empty;

        try
        {
            string requestURI = ConfigurationManager.AppSettings["MuleDeleteApiEndpoint"];
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(requestURI);
            webRequest = this.PopulateDeleteRequest(webRequest, deleteUserRequest);

            string requestStr = String.Format("{0}{1}{2}", this.GetRequestString(webRequest), Environment.NewLine, JsonConvert.SerializeObject(deleteUserRequest, Formatting.Indented));
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, requestStr, userName: userName);

            // Get the response
            HttpStatusCode status;
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream responseStream = webResponse.GetResponseStream())
                using (StreamReader reader = new StreamReader(responseStream))
                {
                    status = ((HttpWebResponse)webResponse).StatusCode;
                    responseStr = reader.ReadToEnd();

                    // Deserialize
                    var responseDeserialized = JsonConvert.DeserializeObject(responseStr);
                    Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, String.Format("Request: {0}, StatusCode: {1}, Response: {2}", requestStr, status.ToString(), responseStr), userName: userName);
                }
            }
        }
        catch (WebException wex)
        {
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, wex.Message, userName: userName);

            string errorContent = string.Empty;
            try
            {
                if (wex.Response != null)
                {
                    var resp = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();

                    // This is based on the confluence page json: https://sunbelt.atlassian.net/wiki/spaces/IS/pages/3270508581/Delete+Account
                    dynamic obj = JsonConvert.DeserializeObject(resp);
                    errorContent = obj.error.message;
                }
            }
            catch
            {
                // Do Nothing 
            }

            string exceptionMsg = wex.Message;
            if (!string.IsNullOrEmpty(errorContent))
                exceptionMsg = wex.Message + " - " + errorContent;
            Sunbelt.Log.LogError(wex, MethodBase.GetCurrentMethod().Name, String.Format("Error Deleting User {0}: {1}", userName, exceptionMsg), "", "", "", HttpContext.Current, null, null, null, null);

            return exceptionMsg;
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogActivity(MethodBase.GetCurrentMethod().Name, ex.Message, userName: userName);

            Sunbelt.Log.LogError(ex, MethodBase.GetCurrentMethod().Name, String.Format("General Error Deleting User {0}: {1}", userName, ex.Message), "", "", "", HttpContext.Current, null, null, null, null);
            return ex.Message;
        }

        return responseStr;
    }

    private HttpWebRequest PopulateDeleteRequest(HttpWebRequest webRequest, DeleteUserRequest deleteUserRequest)
    {
        webRequest.Method = "DELETE";
        webRequest.UserAgent = "SBPortal";
        webRequest.ContentType = "application/json";
        webRequest.Accept = "application/json";
        webRequest.Timeout = System.Threading.Timeout.Infinite;
        webRequest.Headers.Add("client_id", ConfigurationManager.AppSettings["MuleClientKey"]);
        webRequest.Headers.Add("client_secret", ConfigurationManager.AppSettings["MuleClientSecret"]);
        webRequest.Headers.Add("companyId", deleteUserRequest.CompanyId);
        webRequest.Headers.Add("email", deleteUserRequest.Email);
        webRequest.Headers.Add("x-correlation-id", "SBPortal-Delete-" + Guid.NewGuid().ToString());
        if (deleteUserRequest.OktaUserId != "")
            webRequest.Headers.Add("oktaUserId", deleteUserRequest.OktaUserId);
        return webRequest;
    }

    private string GetRequestString(HttpWebRequest request)
    {
        string requestStr = string.Empty;
        try
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Request: {0} {1}{2}", request.Method, request.RequestUri, Environment.NewLine);

            foreach (string headerKey in request.Headers.AllKeys)
            {
                string headerValue = request.Headers[headerKey];
                if (headerKey.ToLower().Contains("password") || headerKey.ToLower().Contains("secret"))
                {
                    // Mask/Hide password or secret
                    headerValue = "********";
                }
                builder.AppendFormat("{0}: {1}{2}", headerKey, headerValue, Environment.NewLine);
            }
            requestStr = builder.ToString();
        }
        catch
        {
            // do nothing
        }
        return requestStr;
    }

    public MigrationStatus GetMigrationStatusByUserId(string userGuid)
    {
        MigrationStatus migrationStatus = null;
        DataTable dt = new DataTable();

        using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.SecurityData))
        using (SPData sp = new SPData("dbo.GetMigrationStatusByUserId", conn))
        {

            sp.AddParameterWithValue("@userGuid", SqlDbType.VarChar, 36, ParameterDirection.Input, userGuid);
            dt = sp.ExecuteDataTable();
        }

        foreach (DataRow dr in dt.Rows)
        {
            migrationStatus = new MigrationStatus
            {
                MagentoCreatedDate = string.IsNullOrEmpty(dr["MagentoCreatedDate"].ToString()) ? "---" : FormatDate(ConvertTools.ToDateTime(dr["MagentoCreatedDate"])),
                OktaCreatedDate = string.IsNullOrEmpty(dr["OktaCreatedDate"].ToString()) ? "---" : FormatDate(ConvertTools.ToDateTime(dr["OktaCreatedDate"])),
                UserID = dr["UserID"].ToString(),
                OktaID = dr["OKTAUserId"].ToString(),
                OktaMigrationStatus = dr["OktaMigrationStatus"].ToString(),
                MagentoStatus = dr["magentoStatus"].ToString(),
            };

        }
        return migrationStatus;
    }

    private string FormatDate(DateTime dt)
    {
        return DateTimeTools.ToShortestDateTimeString(dt);

    }
}
