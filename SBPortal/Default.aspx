<%@ Page Language="C#" MasterPageFile="~/MasterPages/Minor.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="SBPortal_Default" Title="SB Portal" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="Controls/InformationSummary.ascx" TagName="InformationSummary" TagPrefix="uc4" %>
<%@ Register Src="../SiteControls/UserProfile/UPValidationSummary.ascx" TagName="UPValidationSummary" TagPrefix="uc3" %>
<%@ Register Src="Controls/UserProfile/ViewProfile.ascx" TagName="ViewProfile" TagPrefix="uc2" %>
<%@ Register Src="../SiteControls/QueryControl/QueryControl.ascx" TagName="QueryControl" TagPrefix="uc1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="SBNet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftSectionTitleContent" runat="Server">
    Sunbelt Employee Portal
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainBodyContent" runat="Server">
    <uc3:UPValidationSummary ID="UPValidationSummary1" runat="server" />
    <uc4:InformationSummary ID="InformationSummary1" runat="server" />
    <cc1:RoundedCornerPanel ID="RoundedCornerPanel1" runat="server" SkinID="rcpUserProfile">
        <div id="divUserProfile">
            <div id="divRedirect" runat="server">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="h2">Online Account</td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 480px">You are being automatically redirected to the 
						<asp:HyperLink ID="hlAddUser" runat="server" NavigateUrl="#">Add User page</asp:HyperLink>
                            to create your online user account.
                        </td>
                    </tr>
                </table>
                <script type="text/javascript" language="javascript">
                    //<![CDATA[

                    SBNet.Util.AttachEvent(window, "onload", OnWindowLoad);

                    function OnWindowLoad() {
                        setTimeout("DoRedirect()", 5000);
                    }

                    function DoRedirect() {
                        window.location = "<% =RedirectUrl %>";
                    }

//]]>
                </script>
            </div>
            <div id="divEmpolyee" runat="server">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="h2">Select Account</td>
                        <td style="text-align: right;">
                            <SBNet:HelpLink ID="helpEmployee" runat="server"
                                EnableJavaScript="True" Target="_blank" WindowHeight="500" WindowWidth="500"
                                HelpTagName="TOP" HelpUrl="~/SBPortal/EmployeeHelp.aspx"
                                ImageUrl="~/App_Themes/Main/Images/help.gif"
                                NavigateUrl="~/SBPortal/EmployeeHelp.aspx">Need Help?</SBNet:HelpLink></td>
                    </tr>
                    <tr>
                        <td class="left">User Email:</td>
                        <td class="right">
                            <asp:TextBox ID="tbEmail" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="20em"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="left">Account Number:</td>
                        <td class="right">
                            <uc1:QueryControl ID="qcAccount" runat="server" DoShowMore="True" SearchText=" Searching..."
                                QueryUrl="~/SBPortal/QueryAccount.aspx" ClientOnQuery="AccountOnQuery"
                                ClientOnRowBound="AccountOnRowBound" ErrorMessage="Account Number is required" IsRequired="True"
                                ValueCssClass="SuggestValueText" />
                            <%--&nbsp;<asp:ImageButton ID="btnAddAccount" runat="server" AlternateText="Add Account" ImageAlign="AbsMiddle" ImageUrl="../App_Themes/Main/Buttons/add_account_100x20_darkred.gif" OnClick="btnAddAccount_Click" />&nbsp;<asp:ImageButton ID="btnEmployeeManageAccounts" runat="server" AlternateText="Manage Accounts" ImageAlign="AbsMiddle" ImageUrl="../App_Themes/Main/Buttons/manage_accounts_120x20_darkred.gif" OnClick="btnEmployeeManageAccounts_Click" />--%>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: right; width: 100%;">
                            <br />
                            <asp:ImageButton ID="btnLogin" runat="server" AlternateText="Login" ImageUrl="../App_Themes/Main/Buttons/login_80x20_darkred.gif" OnClick="btnLogin_Click" /></td>
                    </tr>
                </table>
                <script type="text/javascript" language="javascript">
                    //<![CDATA[

                    function AccountOnQuery(sQuery, bShowMore) {
                        if (bShowMore)
                            return "&more=true";
                        else
                            return "";
                    }

                    function AccountOnRowBound(div) {
                        //alert("AccountOnRowBound(" + div + ")");

                        var spcl = _QueryControl_getSpanById("iscl", div);
                        var bold = (spcl.innerHTML == "true");

                        var spv = _QueryControl_getSpanById("value", div);
                        var stat = _QueryControl_getSpanById("status", div);
                        if (bold)
                            spv.innerHTML = "<b>" + spv.innerHTML + "</b>";

                        var spc = _QueryControl_getSpanById("customer", div);
                        spc.style.display = "";
                        if (bold)
                            spc.innerHTML = "&nbsp;<b>(" + stat.innerHTML + ")</b>" + "&nbsp;-&nbsp;" + spc.innerHTML;
                        else
                            spc.innerHTML = "&nbsp;(" + stat.innerHTML + ")" + "&nbsp;-&nbsp;" + spc.innerHTML;
                        if (bold)
                            spc.innerHTML = "<b>" + spc.innerHTML + "</b>";
                    }

//]]>
                </script>
            </div>
            <%--divEmpolyee--%>
            <div id="divCustomerService" runat="server">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="h2" colspan="3">Manage User</td>
                    </tr>
                    <tr>
                        <td class="left">User Email:</td>
                        <td class="right">
                            <uc1:QueryControl ID="qcImpersonate" runat="server" QueryUrl="~/SBPortal/QueryEmail.aspx" Width="20em"
                                DoShowMore="True" SearchText=" Searching..." ClientOnQuery="EmailOnQuery" ErrorMessage="Email is required" IsRequired="True"
                                ValueCssClass="SuggestValueText" MaxLength="100" />
                        </td>
                        <td style="padding-left: 8px;">
                            <asp:ImageButton ID="btnGetUser" runat="server" AlternateText="Get User Profile" ImageAlign="AbsMiddle" ImageUrl="../App_Themes/Main/Buttons/get_user_60x20_darkred.gif" OnClick="btnGetUser_Click" /></td>
                    </tr>
                </table>
                <div id="divUserInformation" runat="server" visible="false">
                    <uc2:ViewProfile ID="ViewProfile1" runat="server" ShowEditButton="true" />
                    <table cellpadding="2" cellspacing="0" style="margin-top: 6px;">
                        <tr>
                            <td>
                                <asp:ImageButton ID="btnSetPassword" runat="server"
                                    AlternateText="Set Password"
                                    ImageUrl="../App_Themes/Main/Buttons/set_password_100x20_darkred.gif"
                                    OnClick="btnSetPassword_Click" />
                                <td>
                                    <asp:ImageButton ID="btnUnlockAccount" runat="server" AlternateText="Unlock Account" ToolTip="Unlock Account is now managed in Okta system. User can self-service via the �Unlock Account� function on Mobile App or Command Center" ImageUrl="../App_Themes/Main/Buttons/unlock_account_100x20_darkred.gif" OnClick="btnUnlockAccount_Click" /></td>
                                <td>
                                    <asp:ImageButton ID="btnUserManageAccounts" runat="server" AlternateText="Manage Accounts" ImageUrl="../App_Themes/Main/Buttons/manage_accounts_120x20_darkred.gif" OnClick="btnUserManageAccounts_Click" /></td>
                                <td>
                                    <asp:ImageButton ID="btnDeleteUser" runat="server" AlternateText="Delete User" ImageUrl="../App_Themes/Main/Buttons/delete_user_100x20_darkred.gif" OnClick="btnDeleteUser_Click" /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

                    <script type="text/javascript" language="javascript">
                        SBNet.Util.AttachEvent(window, "onload", OnWindowLoad);

                        function OnWindowLoad() {
                            document.getElementById("<% =btnSetPassword.ClientID %>").addEventListener("click", onClientClickSetPassword);
                            document.getElementById("<% =btnUnlockAccount.ClientID %>").addEventListener("click", onClickOverrideDefault);
                        }

                        function onClickOverrideDefault(event) {
                            if (event.ctrlKey) {
                                event.preventDefault();
                                document.getElementById(this.id).removeEventListener("click", onClickOverrideDefault);
                                document.getElementById(this.id).click();
                            }
                            else {
                                event.preventDefault();
                            }
                        }

                        function onClientClickSetPassword(event) {
                            if (event.ctrlKey) {
                                event.preventDefault();
                                document.getElementById(this.id).removeEventListener("click", onClientClickSetPassword);
                                return OnSetPassword();
                            }
                            else {
                                event.preventDefault();
                            }
                        }
                    </script>
                </div>
                <table style="width: 100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="text-align: left; width: 50%;">
                            <br />
                            <asp:ImageButton ID="btnUserList"
                                runat="server" AlternateText="Show/Hide User List"
                                ImageUrl="~/App_Themes/Main/Buttons/user_list_down_100x20_darkgray.gif"
                                OnClick="btnUserList_Click" CausesValidation="False" /></td>
                    </tr>
                </table>
                <cc1:RoundedCornerPanel ID="rcpChangePassword" runat="server" TopText="Set User Password" SkinID="rcpBlackWithTitle">
                    <table>
                        <tr>
                            <td colspan="3">Enter new password</td>
                        </tr>
                        <tr>
                            <td>Password:</td>
                            <td>
                                <asp:TextBox ID="tbNewPassword" runat="server" TextMode="Password" MaxLength="128" ToolTip="Spaces will be trimmed at begin and end of Password"></asp:TextBox></td>
                            <td>
                                <asp:ImageButton ID="btnCancelPassword" runat="server" ImageUrl="~/App_Themes/Main/Buttons/cancel_60x20_darkgray.gif" ImageAlign="AbsMiddle" />&nbsp;<asp:ImageButton ID="btnSavePassword" runat="server" ImageUrl="~/App_Themes/Main/Buttons/ok_60x20_darkred.gif" ImageAlign="AbsMiddle" /></td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td colspan="3">
                                <ul>
                                    <li>Password must meet at least 3 out of the following 4 complexity rules</li>
                                    <li>at least 1 uppercase character (A-Z)</li>
                                    <li>at least 1 lowercase character (a-z)</li>
                                    <li>at least 1 digit (0-9)</li>
                                    <li>at least 1 special character except ('&#' combination, <, >) no spaces at starting, ending or in betweend</li>
                                </ul>
                                <ul>
                                    <li>Password does not contain username, first name or last name</li>
                                    <li>At least 8 characters</li>
                                    <li>At most 128 characters</li>
                                    <li>Not more than 2 identical characters in a row (e.g., 111 not allowed)</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </cc1:RoundedCornerPanel>
                <script type="text/javascript" language="javascript">
                    //<![CDATA[

                    SBNet.Util.AttachEvent(window, "onload", OnWindowLoad);
                    var rcpChangePassword = null;
                    var tbNewPassword = null;

                    function OnWindowLoad() {
                        rcpChangePassword = document.getElementById("<% =rcpChangePassword.ClientID %>");
                        tbNewPassword = document.getElementById("<% =tbNewPassword.ClientID %>");
                        SBNet.EC.SafeTextEntry(tbNewPassword, true);
                    }

                    function OnDeleteUser() {
                        SBNet.Util.ConfirmBox("Delete User", "Are you sure you wish to <b>PERMANENTLY</b> delete this user<br />from the Web User database?",
                            OnDeleteConfirm, null, "yesno");
                        return false;
                    }

                    function OnDeleteConfirm() {
	<% =ClientScript.GetPostBackEventReference(btnDeleteUser, "") %>;
                    }

                    function OnSetPassword() {
                        SBNet.Util.ConfirmBox("Set User Password", "Are you sure you wish to change the password for this user?",
                            OnSetPasswordConfirm, null, "yesno");
                        return false;
                    }

                    function OnSetPasswordConfirm() {
                        //Show set password div.
                        SBNet.Util.DisableUI(false, rcpChangePassword.style.zIndex);
                        rcpChangePassword.style.display = "block";
                        SBNet.Util.CenterControlOnScreen(rcpChangePassword);
                        SBNet.EC.SetMinLength(tbNewPassword, 4);
                        SBNet.EC.ResetInvalidFields();
                        tbNewPassword.value = "";
                        tbNewPassword.focus();
                        return false;
                    }

                    function OnSavePasswordClick() {
                        if (SBNet.EC.EntryFormatsAreValid()) {
		<% =ClientScript.GetPostBackEventReference(btnSetPassword, "") %>;
                        }
                        return false;
                    }

                    function OnCancelPasswordClick() {
                        SBNet.EC.SetMinLength(tbNewPassword, 0);
                        rcpChangePassword.style.display = "none";
                        SBNet.Util.EnableUI();
                        return false;
                    }

                    function EmailOnQuery(sQuery, bShowMore) {
                        if (bShowMore)
                            return "&more=true";
                        else
                            return "";
                    }

//]]>
                </script>
            </div>
            <%--divCustomerService--%>
            <div id="divUserList" runat="server" style="padding-top: 10px">
                <asp:Panel ID="panSearch" runat="server" DefaultButton="btnUserSearch">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="tbSearchText" runat="server" CausesValidation="True"></asp:TextBox></td>
                            <td>
                                <asp:ImageButton ID="btnUserSearch" runat="server" ImageAlign="AbsMiddle"
                                    ImageUrl="~/App_Themes/Main/Buttons/search_60x20_darkred.gif"
                                    CausesValidation="False" OnClick="btnUserSearch_Click" /></td>
                            <td>&nbsp;<asp:CheckBox ID="cbHideEmployees" runat="server"
                                Text="Hide Sunbelt Employees" AutoPostBack="True"
                                OnCheckedChanged="cbHideEmployees_CheckedChanged" /></td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:GridView ID="gvUsers" runat="server" DataSourceID="sdsUsers"
                    SkinID="Gray" AllowPaging="True" PageSize="50" AllowSorting="true"
                    AutoGenerateColumns="false" OnRowCommand="gvUsers_RowCommand"
                    OnSorting="gvUsers_Sorting" OnRowDataBound="gvUsers_RowDataBound">
                    <PagerSettings Position="TopAndBottom" />
                    <EmptyDataTemplate>
                        No users match the search criteria.
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Email" SortExpression="Email">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbUserEmail" runat="server" Text='<%# Eval("Email") %>' CausesValidation="False" CommandName="LoadUser"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="First" DataField="FirstName" SortExpression="FirstName" />
                        <asp:BoundField HeaderText="Last" DataField="LastName" SortExpression="LastName" />
                        <asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone" />
                        <asp:BoundField HeaderText="Fax" DataField="Fax" SortExpression="Fax" />
                        <%--<asp:TemplateField HeaderText="Address" SortExpression="Address1">
						<ItemTemplate>
							<asp:Label ID="lbAddress" runat="server" Text="Address Here"></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>--%>
                        <%--<asp:BoundField HeaderText="Address1" DataField="Address1" SortExpression="Address1" />
					<asp:BoundField HeaderText="Address2" DataField="Address2" SortExpression="Address2" />
					<asp:BoundField HeaderText="City" DataField="City" SortExpression="City" />
					<asp:BoundField HeaderText="State" DataField="State" SortExpression="State" />
					<asp:BoundField HeaderText="Postal Code" DataField="PostalCode" SortExpression="PostalCode" />--%>
                        <asp:BoundField HeaderText="EbClock" DataField="EbClock" SortExpression="EbClock" />
                        <asp:TemplateField HeaderText="Created" SortExpression="CreateDate">
                            <ItemTemplate><%# Sunbelt.Tools.WebTools.HtmlEncode(Sunbelt.Tools.DateTimeTools.ToAbbreviatedDateTimeString(ToEST(Convert.ToDateTime(Eval("CreateDate"))))) %></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:SqlDataSource ID="sdsUsers" runat="server"
                    ConnectionString="<%$ ConnectionStrings:SecurityData %>"
                    SelectCommand="sb_Membership_GetAllUsers" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="ApplicationName" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <script type="text/javascript" language="javascript">
                    SBNet.EC.SafeTextEntry(document.getElementById("<% =tbSearchText.ClientID %>"), true);

//]]>
                </script>
            </div>
            <%--divUserList--%>
            <div id="divError" runat="server">
                <asp:Label ID="lbError" runat="server"></asp:Label>
                <p>Please contact Customer Service.</p>
            </div>
            <br />
        </div>
    </cc1:RoundedCornerPanel>
    <table style="text-align: left;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <br />
                <br />
                <asp:LinkButton ID="lbSwitch" runat="server" OnClick="lbSwitch_Click" CausesValidation="False">Switch</asp:LinkButton></td>
        </tr>
    </table>
</asp:Content>

