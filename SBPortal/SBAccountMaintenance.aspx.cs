using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Security;

public partial class SBPortal_SBAccountMaintenance : Sunbelt.Web.WebPage
{
	private string userName
    {
        get { return (string)GetViewState("userName", ""); }
        set { SetViewState("userName", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Check if logged in.
        WebUser.VerifySBPortalLogin();

        if (!IsPostBack)
        {
			//Hide this...
			rcpAddAccount.Style["display"] = "none";
			rcpAddAccount.Style["position"] = "absolute";
			rcpAddAccount.Attributes["onmousedown"] = "javascript:event.cancelBubble = true";
			rcpAddAccount.Style["z-index"] = "10001";
			qcAccount.ZIndex = 10002;

			if (Request.QueryString["wcan"] != null)
			{
				SecureQueryString sqs = new SecureQueryString(Request.QueryString["wcan"]);
				userName = sqs["UserName"];
            }

			GetUserAccessData();
            GetUserInformation();
        }

		ReqisterClentScript();
	}

	private void ReqisterClentScript()
	{
		//Register common scripts.
		SBNet.Tools.IncludeEntryControlsClientScript(this);

		qcAccount.GoClick += qcAccount_OnGoClick;
		qcAccount.ValidateTextBoxValue = CheckAccountNumber;

		btnAddAccount.OnClientClick = "javascript:return SBAM.OnAddAccountClick();";
		btnCancel.OnClientClick = "javascript:return SBAM.OnCancelClick();";

		if (!ClientScript.IsStartupScriptRegistered("SBAM_Script"))
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("SBNet.Util.AttachEvent(window, \"onload\", SBAM.Init);\r\n");
			ClientScript.RegisterStartupScript(GetType(), "SBAM_Script", sb.ToString(), true);
		}
	}
	
	protected void GetUserInformation()
    {
		MembershipUser mu;
		SBNet.CashCustomer cc;
		ViewProfile1.ViewProfile(userName, out mu, out cc);
    }

	private void GetUserAccessData()
	{
		List<SecurityAccess> list = UserSecurity.GetAccessByUser(userName);
		gvAccessAreas.DataSource = list;
		gvAccessAreas.DataBind();
	}

	protected string CheckAccountNumber(string accountValue)
	{
		int account = Math.Abs(ConvertTools.ToInt32(accountValue));
		if (account > 0)
		{
			//Make sure account is valid.
			SBNet.QuickAccountInfo qai = SBNet.Portal.GetCustomerAccount(account);
			if (qai == null)
				return "Account '" + accountValue + "' does not exist. Please enter a valid account.";

			//See if the user alreay has this account.
			SecurityAccess sa = UserSecurity.GetAccessForUserAccount(userName, account, qai.CorpLinkNumber);
			if(sa != null)
				return "User already has access to account '<b>" + accountValue + "</b>'. Please enter a different account.";

			//Looks good...
			return "GO";
		}
		return "Account '" + accountValue + "' does not exist. Please enter a valid account.";
	}

	protected void qcAccount_OnGoClick(object sender, QueryAccountControlEventArgs e)
	{
		SBNet.QuickAccountInfo qai = null;
		int account = Math.Abs(ConvertTools.ToInt32(e.TextboxValue));

		//Verify account.
		if (account > 0)
			qai = SBNet.Portal.GetCustomerAccount(account);
		if (qai != null)
		{
			SecurityAccess sa = new SecurityAccess();
			sa.UserName = userName;
			sa.AccountNumber = account;
			UserSecurity.InsertSecurityAccess(userName, new UserCustomerAccount(qai), sa);
			GetUserAccessData();
		}
	}

	protected void gvAccessAreas_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
		gvAccessAreas.EditIndex = -1;
		gvAccessAreas.SelectedIndex = -1;
		btnAddAccount.Visible = true;
		GetUserAccessData();
	}
    protected void gvAccessAreas_RowEditing(object sender, GridViewEditEventArgs e)
    {
		gvAccessAreas.EditIndex = e.NewEditIndex;
		gvAccessAreas.SelectedIndex = e.NewEditIndex;
		btnAddAccount.Visible = false;
		GetUserAccessData();
	}
    protected void gvAccessAreas_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
		GridViewRow gvr = gvAccessAreas.Rows[e.RowIndex];
		SecurityAccess u = new SecurityAccess();

		int account = Convert.ToInt32(gvAccessAreas.DataKeys[e.RowIndex].Value);

		u.UserName = userName;
		u.AccountNumber = account;
		// TODO: Need to get companyId from selected account
		u.CompanyID = 1;

		u.ToAccountAdmin = ((CheckBox)gvr.FindControl("chkAccountAdmin")).Checked;
		u.ToRentalReservations = ((CheckBox)gvr.FindControl("chkRentalReservations")).Checked;
		u.ToRentalReturns = ((CheckBox)gvr.FindControl("chkRentalReturns")).Checked;
		u.ToOnlinePayments = ((CheckBox)gvr.FindControl("chkOnlinePayments")).Checked;
		u.ToReporting = ((CheckBox)gvr.FindControl("chkReporting")).Checked;
		u.ToAlerts = ((CheckBox)gvr.FindControl("chkAlerts")).Checked;
		u.ToLocationMaintenance = ((CheckBox)gvr.FindControl("chkLocationMaintenance")).Checked;
		u.ToESubscriptions = ((CheckBox)gvr.FindControl("chkESubscriptions")).Checked;
		//u.ToCompanyInfo = ((CheckBox)gvr.FindControl("chkCompanyInfo")).Checked;
		//u.ToRebateStatus = ((CheckBox)gvr.FindControl("chkRebateStatus")).Checked;
		//u.ToRateGuide = ((CheckBox)gvr.FindControl("chkRateGuide")).Checked;

		UserSecurity.UpdateSecurityAccess(userName, u);

		//Turn off the editing
		gvAccessAreas.EditIndex = -1;
		gvAccessAreas.SelectedIndex = -1;
		btnAddAccount.Visible = true;
		GetUserAccessData();
	}
    protected void gvAccessAreas_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
		GridViewRow gvr = gvAccessAreas.Rows[e.RowIndex];

		int account = Convert.ToInt32(gvAccessAreas.DataKeys[e.RowIndex].Value);
		UserSecurity.DeleteSecurityAccess(userName, account, null);

		gvAccessAreas.SelectedIndex = -1;
		GetUserAccessData();
	}
	protected void gvAccessAreas_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			SecurityAccess sa = (SecurityAccess)e.Row.DataItem;

			if (e.Row.RowIndex != gvAccessAreas.EditIndex)
			{
				//Get the delete button.
				ImageButton btn = (ImageButton)e.Row.Cells[e.Row.Cells.Count - 1].Controls[2];
				btn.OnClientClick = "javascript:return SBAM.OnDeleteClick('Delete$" + e.Row.RowIndex.ToString() + "', " + sa.AccountNumber.ToString() +
					", '" + sa.UserName + "');";

				string checkUrl = ResolveUrl("~/SBPortal/Images/Check2.gif");

				if (sa.ToAccountAdmin)
					((Image)e.Row.FindControl("imgAccountAdmin")).ImageUrl = checkUrl;
				if (sa.ToRentalReservations)
					((Image)e.Row.FindControl("imgRentalReservations")).ImageUrl = checkUrl;
				if (sa.ToRentalReturns)
					((Image)e.Row.FindControl("imgRentalReturns")).ImageUrl = checkUrl;
				if (sa.ToOnlinePayments)
					((Image)e.Row.FindControl("imgOnlinePayments")).ImageUrl = checkUrl;
				if (sa.ToReporting)
					((Image)e.Row.FindControl("imgReporting")).ImageUrl = checkUrl;
				if (sa.ToAlerts)
					((Image)e.Row.FindControl("imgAlerts")).ImageUrl = checkUrl;
				if (sa.ToLocationMaintenance)
					((Image)e.Row.FindControl("imgLocationMaintenance")).ImageUrl = checkUrl;
				if (sa.ToESubscriptions)
					((Image)e.Row.FindControl("imgESubscriptions")).ImageUrl = checkUrl;
				//if (sa.ToCompanyInfo)
				//	((Image)e.Row.FindControl("imgCompanyInfo")).ImageUrl = checkUrl;
				//if (sa.ToRebateStatus)
				//    ((Image)e.Row.FindControl("imgRebateStatus")).ImageUrl = checkUrl;
				//if (sa.ToRateGuide)
				//	((Image)e.Row.FindControl("imgRateGuide")).ImageUrl = checkUrl;
			}
			else
			{
				CheckBox cb = (CheckBox)e.Row.FindControl("chkAccountAdmin");
				cb.Attributes["onclick"] = "javascript:SBAM.OnAdminCBClick(this);";
			}
		}
	}
	protected void btnAddAccount_Click(object sender, ImageClickEventArgs e)
	{
	}
	protected void btnDone_Click(object sender, ImageClickEventArgs e)
	{
		SecureQueryString sqs = new SecureQueryString();

		sqs["UserName"] = userName;
		Response.Redirect("~/SBPortal/Default.aspx?wcan=" + sqs.ToString());
	}
}
