<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InformationSummary.ascx.cs" Inherits="SBPortal_Controls_InformationSummary" %>
<div id="divInfoSummary" runat="server" style="color:Blue;">
	<ul>
		<asp:Repeater ID="repInfo" runat="server" OnItemDataBound="repInfo_ItemDataBound">
			<ItemTemplate>
				<li><asp:Label ID="lbInfo" runat="server" Text="[lbInfo]"></asp:Label></li>
			</ItemTemplate>
		</asp:Repeater>
	</ul>
</div>