<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewProfile.ascx.cs" Inherits="SBPortal_Controls_UserProfile_ViewProfile" %>
<br />
<table id="tblViewUser" runat="server" cellpadding="0" cellspacing="0">
    <tr>
        <td class="h3" colspan="2"><b>User Profile</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left">Employee Clock:</td>
                    <td class="right">
                        <asp:TextBox ID="tbEbClock" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="6em"></asp:TextBox></td>
                    <td style="width: 99%; text-align: right">&nbsp;<asp:ImageButton ID="btnEdit" runat="server" ImageAlign="AbsMiddle" ImageUrl="../../Images/btnEdit.gif" OnClick="btnEdit_Click" /></td>
                </tr>
            </table>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="left">Name:</td>
        <td class="right">
            <asp:TextBox ID="tbName" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="18em"></asp:TextBox></td>
        <td class="left">&nbsp;&nbsp;&nbsp;
		    <asp:Label ID="lbCustomerType" runat="server" Text="lbCustomerType"
                Font-Size="Larger"></asp:Label>&nbsp;&nbsp;<asp:LinkButton ID="lbConvert"
                    runat="server" OnClick="lbConvert_Click" ToolTip="Convert to Cash Customer">(Convert)</asp:LinkButton>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table cellpadding="0" cellspacing="0">
                <td class="left">DL State:</td>
                <td class="right">
                    <asp:TextBox ID="tbLicenseState" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="4em"></asp:TextBox></td>
                <td class="left">&nbsp;&nbsp;Number:</td>
                <td class="right">
                    <asp:TextBox ID="tbLicenseNumber" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="8em"></asp:TextBox></td>
            </table>
        </td>
    </tr>
    <tr>
        <td class="left">DOB:</td>
        <td class="right" colspan="2">
            <asp:TextBox ID="tbDateOfBirth" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="8em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Phone:</td>
        <td class="right" colspan="2">
            <asp:TextBox ID="tbPhone" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="18em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Fax:</td>
        <td class="right" colspan="2">
            <asp:TextBox ID="tbFax" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="18em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left" style="vertical-align: top;">Address:</td>
        <td class="right" colspan="2">
            <asp:TextBox ID="tbAddress" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="18em" Rows="3" TextMode="MultiLine"></asp:TextBox></td>
    </tr>
</table>
<table id="tblEditUser" runat="server" cellpadding="0" cellspacing="0">
    <tr>
        <td class="h3" colspan="2"><b>User Profile</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left">Employee Clock:</td>
                    <td class="right">
                        <asp:TextBox ID="tbNewEbClock" runat="server" Width="6em" MaxLength="7"></asp:TextBox></td>
                    <td style="width: 99%; text-align: right">&nbsp;<asp:ImageButton ID="btnSave" runat="server" ImageAlign="AbsMiddle" ImageUrl="../../Images/btnSave.gif" OnClick="btnSave_Click" />&nbsp;<asp:ImageButton ID="btnCancel" runat="server"
                        ImageAlign="AbsMiddle" ImageUrl="../../Images/btnCancel.gif" CausesValidation="False" OnClick="btnCancel_Click" /></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left">
                        <asp:Label ID="lbNewEbClock" runat="server" Text="lbNewEbClock" Font-Size="Small" Visible="false" ForeColor="#ff0000"></asp:Label></td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td class="left">First Name*:</td>
        <td class="right">
            <asp:TextBox ID="tbFirstName" runat="server" MaxLength="40"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="tbFirstName"
                ErrorMessage="Frist Name is required" Display="Dynamic">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Frist Name is required" />
            </asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="left">Last Name*:</td>
        <td class="right">
            <asp:TextBox ID="tbLastName" runat="server" MaxLength="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="tbLastName"
                ErrorMessage="Last Name is required" Display="Dynamic">
                <asp:Image ID="Image10" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Last Name is required" />
            </asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="left">Phone:</td>
        <td class="right">
            <asp:TextBox ID="tbNewPhone" runat="server" MaxLength="20"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Fax:</td>
        <td class="right">
            <asp:TextBox ID="tbNewFax" runat="server" MaxLength="20"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Address 1:</td>
        <td class="right">
            <asp:TextBox ID="tbAddress1" runat="server" MaxLength="40" Columns="40"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Address 2:</td>
        <td class="right">
            <asp:TextBox ID="tbAddress2" runat="server" MaxLength="40" Columns="40"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Address 3:</td>
        <td class="right">
            <asp:TextBox ID="tbAddress3" runat="server" MaxLength="40" Columns="40"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">City:</td>
        <td class="right">
            <asp:TextBox ID="tbCity" runat="server" MaxLength="20" Columns="30"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">State/Province:</td>
        <td class="right">
            <asp:TextBox ID="tbState" runat="server" MaxLength="20" Columns="10"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Postal Code:</td>
        <td class="right">
            <asp:TextBox ID="tbPostalCode" runat="server" MaxLength="20" Columns="10"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Country:</td>
        <td class="right">
            <asp:TextBox ID="tbCountry" runat="server" MaxLength="30" Columns="30"></asp:TextBox></td>
    </tr>
</table>
<br />
<table id="tblStats" runat="server" cellpadding="0" cellspacing="0">
    <tr>
        <td class="h3" colspan="4"><b>Profile Statistics</b></td>
    </tr>
    <tr>
        <td colspan="4">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td class="left">Approved:</td>
                    <td class="right">
                        <asp:TextBox ID="tbApproved" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="3em"></asp:TextBox></td>
                    <td class="left">&nbsp;Locked Out:</td>
                    <td class="right">
                        <asp:TextBox ID="tbLockedOut" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="3em"></asp:TextBox></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="left">Creation Date:</td>
        <td class="right">
            <asp:TextBox ID="tbCreationDate" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td class="left">&nbsp;Last Activity:</td>
        <td class="right">
            <asp:TextBox ID="tbLastActivity" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Last Lockout:</td>
        <td class="right">
            <asp:TextBox ID="tbLastLockout" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td class="left">&nbsp;Last Login:</td>
        <td class="right">
            <asp:TextBox ID="tbLastLogin" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">PW Changed:</td>
        <td class="right">
            <asp:TextBox ID="tbPasswordChanged" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
<br />
<table id="tblProfileStatus" runat="server" cellpadding="0" cellspacing="0">
    <tr>
        <td class="h3" colspan="4"><b>Profile Status</b></td>
    </tr>
    <tr>
        <td class="left">User Id:</td>
        <td class="right"><asp:TextBox ID="tbUserID" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td class="left">&nbsp;Okta User Id:</td>
        <td class="right"><asp:TextBox ID="tbOKTAID" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Okta Creation Date:</td>
        <td class="right"><asp:TextBox ID="tbOKTACreationDate" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td class="left">&nbsp;Okta Migration Status:</td>
        <td class="right"><asp:TextBox ID="tbOKTAMigrationStatus" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
    </tr>
    <tr>
        <td class="left">Magento Date:</td>
        <td class="right"><asp:TextBox ID="tbMagentoDate" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
        <td class="left">&nbsp;Magento Status:</td>
        <td class="right"><asp:TextBox ID="tbMagentoMigrationStatus" runat="server" ReadOnly="true" CssClass="ReadOnly" Width="12em"></asp:TextBox></td>
    </tr>
</table>
<br />
<script type="text/javascript" language="javascript">
    //<![CDATA[
    var _SBP_VP =
    {
        WindowOnLoad: function () {
            document.getElementById("<% =btnEdit.ClientID %>").addEventListener("click", onClickOverride);
            if (document.getElementById("<% =lbConvert.ClientID %>") != null)
                document.getElementById("<% =lbConvert.ClientID %>").addEventListener("click", onClientClickLbConvert);

            function onClickOverride(event) {
                if (event.ctrlKey) {
                    event.preventDefault();
                    document.getElementById(this.id).removeEventListener("click", onClickOverride);
                    document.getElementById(this.id).click();
                }
                else {
                    event.preventDefault();
                }
            }

            function onClientClickLbConvert(event) {
                if (event.ctrlKey) {
                    event.preventDefault();
                    document.getElementById(this.id).removeEventListener("click", onClientClickLbConvert);
                    return _SBP_VP.OnConvertToCredit();
                }
                else {
                    event.preventDefault();
                }
            }

            if (document.getElementById("<% =tbFirstName.ClientID %>") != null) {
                SBNet.EC.IntegerEntry(document.getElementById("<% =tbNewEbClock.ClientID %>"), false, true, 1, 100000);
                SBNet.EC.AllowNoValue(document.getElementById("<% =tbNewEbClock.ClientID %>"));

                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbFirstName.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbLastName.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbNewPhone.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbNewFax.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbAddress1.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbAddress2.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbAddress3.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbCity.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbState.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbPostalCode.ClientID %>"), true);
                SBNet.EC.SafeTextEntry(document.getElementById("<% =tbCountry.ClientID %>"), true);
            }
        },

        OnSaveProfile: function () {
            if (!SBNet.EC.EntryFormatsAreValid()) {
                SBNet.Util.ConfirmBox("Invalid Fields", "Some profile fields have invalid values.<br /><br />Please correct all fields with a yellow background.", null, null, "okonly");
                return false;
            }
            return true;
        },

        OnConvertToCredit: function () {
            SBNet.Util.ConfirmBox("Convert To Credit Customer",
                "Are you sure you wish to convert this <b>cash</b> customer to a <b>credit</b> customer?<br />" +
                "This will delete all Driver's License information and <b><i>cannot be undone</i></b>.",
                _SBP_VP.OnConvertToCreditConfirm, null, "yesno");
            return false;
        },

        OnConvertToCreditConfirm: function () {
    <% =Page.ClientScript.GetPostBackEventReference(lbConvert, "") %>;
        }
    }

    SBNet.Util.AttachEvent(window, "onload", _SBP_VP.WindowOnLoad);

    //]]>
</script>
