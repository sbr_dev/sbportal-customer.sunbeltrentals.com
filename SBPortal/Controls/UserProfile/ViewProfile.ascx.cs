using SBNet;
using Sunbelt.Common.DAL;
using Sunbelt.Tools;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Security;
using System.Web.UI;
using UserProfileExtensions;

public partial class SBPortal_Controls_UserProfile_ViewProfile : Sunbelt.Web.UserControl
{
    public EventHandler UserProfileUpdated;
    public EventHandler CashCustomerConverted;
    public string OKTAMigratedDate { get; private set; }
    public string OKTACreatedDate { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        Tools.IncludeEntryControlsClientScript(Page);
        btnSave.OnClientClick = "javascript:return _SBP_VP.OnSaveProfile();";
    }

    public bool HideStatistics
    {
        set { tblStats.Visible = !value; }
        get { return !tblStats.Visible; }
    }
    public bool ShowEditButton
    {
        set { SetViewState("ShowEdit", value); }
        get { return (bool)GetViewState("ShowEdit", false); }
    }
    private string UserName
    {
        set { SetViewState("user", value); }
        get { return (string)GetViewState("user", ""); }
    }

    public void EditProfile(string userName)
    {
        UserName = userName;
        tblViewUser.Visible = false;
        tblEditUser.Visible = true;
        FillEditForm();
    }

    public void ViewProfile(string userName, out MembershipUser mu, out SBNet.CashCustomer cc)
    {
        UserName = userName;
        tblViewUser.Visible = true;
        tblEditUser.Visible = false;
        FillUserForm(out mu, out cc);
    }

    private bool UserExistsInOkta(string userName)
    {
        try
        {
            using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.SecurityData))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    string query = string.Format("SELECT [OKTAMigratedDate],[OKTACreatedDate] FROM [sbExtranetSecurity].[dbo].[aspnet_Membership] where Email = '{0}'", userName);
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            OKTACreatedDate = reader["OKTACreatedDate"].ToString();
                            OKTAMigratedDate = reader["OKTAMigratedDate"].ToString();
                        }
                    }
                }
            }

            return !string.IsNullOrEmpty(OKTACreatedDate);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public MigrationStatus GetMigrationStatusByUserId(string userGuid)
    {
        MigrationStatus migrationStatus = null;
        DataTable dt = new DataTable();

        using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.SecurityData))
        using (SPData sp = new SPData("dbo.GetMigrationStatusByUserId", conn))
        {

            sp.AddParameterWithValue("@userGuid", SqlDbType.VarChar, 36, ParameterDirection.Input, userGuid);
            dt = sp.ExecuteDataTable();
        }

        foreach (DataRow dr in dt.Rows)
        {
            migrationStatus = new MigrationStatus
            {
                MagentoCreatedDate = string.IsNullOrEmpty(dr["MagentoCreatedDate"].ToString()) ? "---" : FormatDate(ConvertTools.ToDateTime(dr["MagentoCreatedDate"])),
                OktaCreatedDate = string.IsNullOrEmpty(dr["OktaCreatedDate"].ToString()) ? "---" : FormatDate(ConvertTools.ToDateTime(dr["OktaCreatedDate"])),
                UserID = dr["UserID"].ToString(),
                OktaID = dr["OKTAUserId"].ToString(),
                OktaMigrationStatus = dr["OktaMigrationStatus"].ToString(),
                MagentoStatus = dr["magentoStatus"].ToString(),
            };

        }
        return migrationStatus;
    }

    public CashCustomer GetCashCustomer(string userName, int companyID)
    {
        CashCustomer cashCustomer = null;
        DataTable dt = new DataTable();

        using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.SecurityData))
        using (SPData sp = new SPData("dbo.sb_CashCustomerByUserName", conn))
        {

            sp.AddParameterWithValue("@ApplicationName", SqlDbType.VarChar, 256, ParameterDirection.Input, "/SecurityTest01");
            sp.AddParameterWithValue("@UserName", SqlDbType.VarChar, 256, ParameterDirection.Input, userName);
            sp.AddParameterWithValue("@CompanyID", SqlDbType.Int, 4, ParameterDirection.Input, companyID);
            dt = sp.ExecuteDataTable();
        }

        foreach (DataRow dr in dt.Rows)
        {
            cashCustomer = new CashCustomer
            {
                DateOfBirth = ConvertTools.ToDateTime(dr["DateOfBirth"]),
                DriverLicenseNumber = dr["DriverLicenseNumber"].ToString(),
                DriverLicenseState = dr["DriverLicenseState"].ToString()
            };

        }
        return cashCustomer;
    }

    private void FillUserForm(out MembershipUser mu, out SBNet.CashCustomer cc)
    {
        ClearUserForm();

        btnEdit.Visible = ShowEditButton;
        lbCustomerType.Text = "UNKNOWN";

        //get user profile.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;

        cc = null;
        mu = p.GetUser(UserName, false);

        if (mu != null)
        {
            UserProfile up = p.GetUserProfile(UserName);
            var country = up.country.Trim().ToUpper();
            var address = string.Join(" ", up.address1, up.address2, up.address3, up.state);
            var isCanada = country.Equals("CANADA") || up.IsCanadaUser(address);
            var companyId = isCanada ? 2 : 1;

            string userId = up.userGuid.HasValue ? up.userGuid.Value.ToString() : string.Empty;
            tbUserID.Text = userId;

            var ms = GetMigrationStatusByUserId(userId);
            if (ms != null)
            {
                tbOKTACreationDate.Text = ms.OktaCreatedDate;
                tbMagentoDate.Text = ms.MagentoCreatedDate;
                tbUserID.Text = ms.UserID;
                tbOKTAID.Text = ms.OktaID;
                tbOKTAMigrationStatus.Text = ms.OktaMigrationStatus;
                tbMagentoMigrationStatus.Text = ms.MagentoStatus;
            }

            if (UserExistsInOkta(UserName))
            {
                tbOKTAMigrationStatus.Text = "CREATED";
                tbOKTACreationDate.Text = OKTACreatedDate;
            }

            if (up.ebClock > 0)
                tbEbClock.Text = up.ebClock.ToString();
            else
                tbEbClock.Text = "";

            tbName.Text = up.firstName + " " + up.lastName;
            tbPhone.Text = up.phone;
            tbFax.Text = up.fax;

            tbAddress.Text = up.address1 + "\r\n";
            if (up.address2.Length > 0)
                tbAddress.Text += up.address2 + "\r\n";
            if (up.address3.Length > 0)
                tbAddress.Text += up.address3 + "\r\n";
            tbAddress.Text += up.city + ", " + up.state + " " + up.zip;
            if (up.country.Length > 0)
                tbAddress.Text += "\r\n" + up.country;

            tbCreationDate.Text = FormatDate(mu.CreationDate);
            tbApproved.Text = (mu.IsApproved) ? "YES" : "NO";
            tbLockedOut.Text = (mu.IsLockedOut) ? "YES" : "NO";
            tbLastActivity.Text = FormatDate(mu.LastActivityDate);
            tbLastLockout.Text = FormatDate(mu.LastLockoutDate);
            tbLastLogin.Text = FormatDate(mu.LastLoginDate);
            tbPasswordChanged.Text = FormatDate(mu.LastPasswordChangedDate);

            // Get cash customer details, if any. If there are cash customer details, the user is a Cash Customer.
            cc = GetCashCustomer(UserName, companyId);

            if (cc != null)
            {
                tbLicenseState.Text = cc.DriverLicenseState;
                tbLicenseNumber.Text = cc.DriverLicenseNumber;
                tbDateOfBirth.Text = cc.DateOfBirth.ToShortDateString();

                lbCustomerType.Text = "Cash Customer";
                lbConvert.Visible = true;
                lbConvert.ToolTip = "Credit users can only be created with the Online CCA Process through mobile app or Command Center";
                btnEdit.ToolTip = "Users can update their own profile via Mobile App or Command Center";
            }
            else
            {
                lbCustomerType.Text = (up.ebClock > 0) ? "Employee" : "Credit Customer";
                lbConvert.Visible = false;
                btnEdit.ToolTip = "Employee users can only be created with create account/employee";
            }
        }
    }

    private void ClearUserForm()
    {
        tbEbClock.Text = "";
        tbName.Text = "";
        tbPhone.Text = "";
        tbFax.Text = "";
        tbAddress.Text = "";

        tbCreationDate.Text = "";
        tbApproved.Text = "";
        tbLockedOut.Text = "";
        tbLastActivity.Text = "";
        tbLastLockout.Text = "";
        tbLastLogin.Text = "";
        tbPasswordChanged.Text = "";

        tbLicenseState.Text = "";
        tbLicenseNumber.Text = "";
        tbDateOfBirth.Text = "";

        tbOKTACreationDate.Text = "";
        tbMagentoDate.Text = "";
        tbUserID.Text = "";
        tbOKTAID.Text = "";
        tbOKTAMigrationStatus.Text = "";
        tbMagentoMigrationStatus.Text = "";
    }

    private void FillEditForm()
    {
        ClearEditForm();

        //get user profile.
        SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
        MembershipUser mu = p.GetUser(UserName, false);
        if (mu != null)
        {
            UserProfile up = p.GetUserProfile(UserName);
            //PS - 998 ConfigurationManager.AppSettings["COOKIE_DOMAIN"]
            if (!UserName.Contains("@sunbeltrentals.com"))
            {
                tbNewEbClock.Enabled = false;
                lbNewEbClock.Visible = true;
                lbNewEbClock.Text = "Only sunbeltrentals.com emails can be converted to employee user.";
            }
            else
            {
                tbNewEbClock.Enabled = true;
                lbNewEbClock.Visible = false;
            }

            if (up.ebClock > 0)
                tbNewEbClock.Text = up.ebClock.ToString();
            else
                tbNewEbClock.Text = "";
            tbFirstName.Text = up.firstName;
            tbLastName.Text = up.lastName;
            tbNewPhone.Text = up.phone;
            tbNewFax.Text = up.fax;
            tbAddress1.Text = up.address1;
            tbAddress2.Text = up.address2;
            tbAddress3.Text = up.address3;
            tbCity.Text = up.city;
            tbState.Text = up.state;
            tbPostalCode.Text = up.zip;
            tbCountry.Text = up.country;
        }
        else
        {
            SBNet.CashCustomer cc;
            ViewProfile(UserName, out mu, out cc);
        }
    }

    private void ClearEditForm()
    {
        tbNewEbClock.Text = "";
        tbFirstName.Text = "";
        tbLastName.Text = "";
        tbNewPhone.Text = "";
        tbNewFax.Text = "";
        tbAddress1.Text = "";
        tbAddress2.Text = "";
        tbAddress3.Text = "";
        tbCity.Text = "";
        tbState.Text = "";
        tbPostalCode.Text = "";
        tbCountry.Text = "";
    }

    private void SaveEditForm()
    {
        if (Page.IsValid)
        {
            //get user profile.
            SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
            MembershipUser mu = p.GetUser(UserName, true);
            if (mu != null)
            {
                UserProfile up = p.GetUserProfile(UserName);

                up.ebClock = ConvertTools.ToInt32(tbNewEbClock.Text.Trim());
                up.firstName = tbFirstName.Text.Trim();
                up.lastName = tbLastName.Text.Trim();
                up.phone = tbNewPhone.Text.Trim();
                up.fax = tbNewFax.Text.Trim();
                up.address1 = tbAddress1.Text.Trim();
                up.address2 = tbAddress2.Text.Trim();
                up.address3 = tbAddress3.Text.Trim();
                up.city = tbCity.Text.Trim();
                up.state = tbState.Text.Trim();
                up.zip = tbPostalCode.Text.Trim();
                up.country = tbCountry.Text.Trim();

                p.UpdateUserProfile(UserName, up);
            }
            else
            {
                SBNet.CashCustomer cc;
                ViewProfile(UserName, out mu, out cc);
            }
        }
    }

    private string FormatDate(DateTime dt)
    {
        return DateTimeTools.ToShortestDateTimeString(dt);
    }
    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        EditProfile(UserName);

    }
    protected void btnSave_Click(object sender, ImageClickEventArgs e)
    {
        SaveEditForm();
        if (Page.IsValid)
        {
            MembershipUser mu;
            SBNet.CashCustomer cc;
            ViewProfile(UserName, out mu, out cc);

            EventHandler eh = UserProfileUpdated;
            if (eh != null)
                eh(this, EventArgs.Empty);
        }
    }
    protected void btnCancel_Click(object sender, ImageClickEventArgs e)
    {
        MembershipUser mu;
        SBNet.CashCustomer cc;
        ViewProfile(UserName, out mu, out cc);
    }
    protected void lbConvert_Click(object sender, EventArgs e)
    {
        SBNetSqlMembershipProvider sqlProvider = (SBNetSqlMembershipProvider)Membership.Provider;
        sqlProvider.RemoveCashCustomer(UserName);

        MembershipUser mu;
        CashCustomer cc;
        ViewProfile(UserName, out mu, out cc);

        EventHandler eh = CashCustomerConverted;
        if (eh != null)
            eh(this, EventArgs.Empty);
    }
}