<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SunbeltLogin.ascx.cs"
	Inherits="UserControls_SunbeltLogin" %>

<script type="text/javascript">
//<![CDATA[

if(typeof(window.attachEvent) != "undefined")
{
	window.attachEvent("onload", SBL_OnControlLoad);
}
else
{
	//If non Microsoft
	window.addEventListener("load", SBL_OnControlLoad, false);
}

var IsImpersonateMode = <% =IsImpersonateMode.ToString().ToLower() %>;

function SBL_OnControlLoad()
{
	var hidH = document.getElementById("<% =hfHelpVisible.ClientID %>");
	SBL_ShowHelp(hidH.value == "block");

	var tbU = document.getElementById("<% =tbUserName.ClientID %>");
	if(tbU.value == "")
		SBL_SetFocus(tbU);
	else
		SBL_SetFocus(document.getElementById("<% =tbPassword.ClientID %>"));

	if(IsImpersonateMode)
		SBL_ShowBox();
}

function SBL_ShowBox()
{
	if(window.showModalDialog)
	{
		var sFeatures = "dialogHeight:122px; dialogWidth:296px; center:yes; resizable:no; scroll:no; unadorned:yes; status:no; help:no";
		var vArguments = "";

		var vReturnValue = window.showModalDialog("<% =ResolveUrl("~/UserControls/SunbeltLogin/LoginHost.aspx") %>", vArguments, sFeatures);
		if(vReturnValue != null)
		{
			document.getElementById("<% =tbPassword.ClientID %>").value = "_no_pass_";
			SBL_OnSignIn(true);
			__doPostBack('Impersonate', vReturnValue);
		}
	}
}

function SBL_SetFocus(control)
{
	control.focus();
	control.select();
}

function SBL_ToggleHelp()
{
	var divH = document.getElementById("divHelp");

	if(divH.style.display == "none")
		SBL_ShowHelp(true);
	else
		SBL_ShowHelp(false);
}

function SBL_ShowHelp(bShow)
{
	var divH = document.getElementById("divHelp");
	var hidH = document.getElementById("<% =hfHelpVisible.ClientID %>");

	if(bShow)
	{
		divH.style.display = "block";
		hidH.value = "block"
	}
	else
	{
		divH.style.display = "none";
		hidH.value = "none";
	}
}

function SBL_OnSignIn(bNoPostBack)
{
	if(Page_IsValid)
	{
		//disable button
		document.getElementById("<% =btnSignIn.ClientID %>").disabled = true;
		//show message
		document.getElementById("trProcessing").style.display = "block";
		//Do not postback if requested.
		if(typeof(bNoPostBack) == "boolean" && bNoPostBack == true)
			return true;
		//post back...
		<% =Page.ClientScript.GetPostBackEventReference(btnSignIn, "") %>;
		return true;
	}
	return false;
}

//]]>
</script>

<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="vertical-align: top;">
			<div id="divControls" style="width: 320px; font-size: 9pt; font-family: Verdana, Arial, Sans-Serif;">
				<table cellpadding="3" cellspacing="0" border="0" style="width: 100%; font-size: 9pt;">
					<tr>
						<td style="height: 22px; text-align: left; background-color: #4B843D; color: #ffffff;">
							&nbsp;<strong>Sign in to Sunbelt</strong></td>
						<td style="font-size: 8pt; text-align: right; height: 22px; width: 34px; color: #ffffff; background-color: #4B843D;">
							<asp:HyperLink ID="linkHelp" runat="server" NavigateUrl="javascript:SBL_ToggleHelp();" ForeColor="#ffffff" TabIndex="5">help</asp:HyperLink>&nbsp;</td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" border="0" style="font-size: 9pt; width: 100%; border-right: #4B843D 2px solid; border-top: #4B843D 2px solid; border-left: #4B843D 2px solid; background-color:#E6F2E3;">
					<tr>
						<td style="text-align:right; white-space:nowrap; padding-left:6px;">
							<asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="tbUserName"
								Display="Dynamic" ErrorMessage="User name or Clock ID required." SetFocusOnError="True">*</asp:RequiredFieldValidator>User Name <i>or</i> Clock ID:</td>
						<td style="padding-right:5px;">
							<asp:TextBox ID="tbUserName" runat="server" MaxLength="100" TabIndex="1" 
								Columns="16"></asp:TextBox>
							<asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="tbUserName"
								Display="None" EnableClientScript="False" ErrorMessage="User name or Clock ID is invalid."
								ValidationExpression="[-\w.]*" TabIndex="1"></asp:RegularExpressionValidator></td>
					</tr>
					<tr>
						<td style="text-align:right; white-space:nowrap; padding-left:6px;">
							<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="tbPassword"
								Display="Dynamic" ErrorMessage="Password required." SetFocusOnError="True">*</asp:RequiredFieldValidator>Password:</td>
						<td style="padding-right:5px;">
							<asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="20" 
								TabIndex="2" Columns="16"></asp:TextBox>
							<asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="tbPassword"
								Display="None" EnableClientScript="False" ErrorMessage="Password is invalid."
								ValidationExpression="[\w\W]*" TabIndex="2"></asp:RegularExpressionValidator></td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" border="0" style="font-size: 9pt; width: 100%; border-right: #4B843D 2px solid; border-left: #4B843D 2px solid; border-bottom: #4B843D 2px solid; background-color:#E6F2E3;">
					<tr>
						<td style="height: 36px">
							&nbsp;<asp:CheckBox ID="cbRememberUserName" runat="server" Text="Remember my sign in" TabIndex="3" /></td>
						<td style="text-align: right; height: 36px;">
							<asp:Button ID="btnSignIn" runat="server" Text="Sign In" OnClick="btnSignIn_Click" TabIndex="4" /></td>
					</tr>
					<tr id="trProcessing" style="display: none;">
						<td colspan="2" style="text-align: right; font-size: 7pt; font-style: italic;">Processing your request...</td>
					</tr>
				</table>
				<table cellpadding="3" cellspacing="0" border="0" style="width: 100%;">
					<tr>
						<td style="font-size: 7pt; height: 18px; text-align: left; background-color: #4B843D;">
							&nbsp;�2008 Sunbelt Rentals</td>
					</tr>
				</table>
				<asp:ValidationSummary ID="ValidationSummary1" runat="server" />
			</div>
		</td>
		<td style="vertical-align: top;">
			<div id="divHelp" style="width: 310px; font-size: 9pt; font-family: Verdana, Arial, Sans-Serif;
				display: none;">
				<div style="width: 100%; background-color: #4B843D; color: #ffffff; padding-bottom: 3px; padding-top: 3px; text-align: center;"><strong>Login Help</strong></div>
				<div style="padding-right: 3px; padding-left: 10px; padding-bottom: 3px; padding-top: 3px; text-align: left;">
					The user name and password required to login is the exact same one you
					use to login in to your terminal server session or onto your computer. It is also
					the same one you use for email purposes.<br />
					<br />
					For example, if someone is the Manager of PC 999, their username and password will
					be<br />
					<br />
					<strong>UserName: "PCM999"</strong><br />
					<br />
					<strong>Password: *******</strong> <span style="font-size: 6.5pt">(Terminal Services
						Password)</span><br />
					<br />
					If you should have any problems with logging in, please feel free to contact us
					via <a href="mailto:productionsupport@sunbeltrentals.com?subject=Intranet Login">email</a>
					by clicking on the link below.<br />
					<br />
					<center>
						<a href="mailto:productionsupport@sunbeltrentals.com?subject=Intranet Login">Help Desk
							Request</a></center>
					<br />
					<center id="linkIntranet" runat="server">
						<a href="http://Intranet/">Go to the Intranet site</a></center>
				</div>
			</div>
		</td>
	</tr>
</table>
<asp:HiddenField ID="hfHelpVisible" runat="server" Value="none" />
<script type="text/javascript">
//<![CDATA[

if(typeof(window.attachEvent) != "undefined")
	document.getElementById("<% =btnSignIn.ClientID %>").attachEvent("onclick", SBL_OnSignIn);
else
	document.getElementById("<% =btnSignIn.ClientID %>").addEventListener("click", SBL_OnSignIn, false);

//]]>
</script>