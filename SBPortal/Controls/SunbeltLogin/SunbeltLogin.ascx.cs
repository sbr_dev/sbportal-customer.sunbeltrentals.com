using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt;
using Sunbelt.Tools;
using Sunbelt.Security;
using SunbeltWebServices;

public partial class UserControls_SunbeltLogin : System.Web.UI.UserControl
{
	public event EventHandler<LoginEventArgs> UserValidated;

	public bool HideHelp
	{
		set { linkHelp.Visible = !value; }
		get { return linkHelp.Visible; }
	}
	public bool HideRememberUserName
	{
		set { cbRememberUserName.Visible = !value; }
		get { return cbRememberUserName.Visible; }
	}
	public bool HideIntranetLink
	{
		set { linkIntranet.Visible = !value; }
		get { return linkIntranet.Visible; }
	}

	protected bool IsImpersonateMode = false;

	protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			if (Convert.ToBoolean(GetAppSetting("SunbeltLoginHideInternalLinks", "false")))
			{
				HideRememberUserName = true;
				HideIntranetLink = true;
			}

			if (cbRememberUserName.Visible)
			{
				cbRememberUserName.Checked = Convert.ToBoolean(WebTools.GetCookie("SunbeltLogin", "RememberMe", "false"));
				if (cbRememberUserName.Checked)
				{
					tbUserName.Text = WebTools.GetCookie("SunbeltLogin", "UserName", "");
				}
			}

			tbUserName.Focus();
		}
	}

	public void DisplayError(string errorMessage)
	{
		//Create a custom validator
		CustomValidator cv = new CustomValidator();
		cv.ErrorMessage = errorMessage;
		cv.Text = string.Empty;
		cv.Enabled = true;
		cv.Display = ValidatorDisplay.None;
		cv.Attributes.Add("IsVSControl", "true");
		cv.ClientValidationFunction = "";
		cv.EnableClientScript = false;
		cv.IsValid = false;
		ValidationSummary1.Controls.Add(cv);
	}

	private string GetAppSetting(string key, string defaultValue)
	{
		if (ConfigurationManager.AppSettings[key] != null)
			return ConfigurationManager.AppSettings[key];
		return defaultValue;
	}

	private UserInfo AuthenicateUser(string userName, string password)
	{
		SunbeltWebServices.Authentication auth = new SunbeltWebServices.Authentication();

		try
		{
			//Try user name and password first.
			SecureNameValueCollection snvc = new SecureNameValueCollection();
			snvc["UserName"] = userName;
			snvc["Password"] = password;
			return auth.GetUserByUserName(snvc.ToString());
		}
		catch(Exception ex1)
		{
			try
			{
				//Try clock id next.
				SecureNameValueCollection snvc = new SecureNameValueCollection();
				snvc["ClockNumber"] = userName;
				snvc["Last4SSN"] = password;
				UserInfo ui = auth.GetUserByClockNumber(snvc.ToString());
				if(ui.TerminationDate <= DateTime.Now)
					throw new Exception(ex1.Message + " : User is not a Sunbelt employee. Termination date is " +
						DateTimeTools.ToShortDateString(ui.TerminationDate));

				return ui;

			}
			catch (Exception ex2)
			{
				throw new Exception(ex1.Message + " : " + ex2.Message);
			}
		}
	}

	private void RaiseUserValidated(WebUserInfo user)
	{
		//Save user name if requested.
		if (cbRememberUserName.Visible)
		{
			WebTools.SetCookie("SunbeltLogin", "RememberMe", cbRememberUserName.Checked.ToString(), 10);
			WebTools.SetCookie("SunbeltLogin", "UserName", (cbRememberUserName.Checked) ? tbUserName.Text : "", 10);
		}

		//Raise the UserValidated event.
		EventHandler<LoginEventArgs> handler = UserValidated;
		if (handler != null)
			handler(this, new LoginEventArgs(user));
	}
	
	protected void btnSignIn_Click(object sender, EventArgs e)
	{
		if (Page.IsValid)
		{
			try
			{
				string userName = tbUserName.Text;
				string password = tbPassword.Text;

				//Double check user name and password.
				if (!Regex.IsMatch(userName, "\\A([-\\w.]*)\\z", RegexOptions.Singleline))
					throw new Exception("User name is invalid.");
				if (!Regex.IsMatch(password, "\\A([\\w\\W]*)\\z", RegexOptions.Singleline))
					throw new Exception("Password is invalid.");

				SunbeltWebServices.UserInfo ui = AuthenicateUser(userName, password);
				RaiseUserValidated(new WebUserInfo(ui));
			}
			catch (Exception ex)
			{
				string msg = ex.Message;
				DisplayError("Login Unsuccessful. Please try again. (" + ex.Message + ")");
			}
		}
	}
}
