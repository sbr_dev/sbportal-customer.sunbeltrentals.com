﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Minor.master" AutoEventWireup="true" CodeFile="UserList.aspx.cs" Inherits="SBPortal_UserList" Title="User List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">
	<asp:GridView ID="gvUsers" runat="server" DataSourceID="sdsUsers" 
		SkinID="Gray" AllowPaging="True" PageSize="50" AllowSorting="true"
		AutoGenerateColumns="false">
		<PagerSettings Position="TopAndBottom" />
		<Columns>
			<asp:BoundField HeaderText="Email" DataField="Email" SortExpression="Email" />
			<asp:BoundField HeaderText="First" DataField="FirstName" SortExpression="FirstName" />
			<asp:BoundField HeaderText="Last" DataField="LastName" SortExpression="LastName" />
			<asp:BoundField HeaderText="Phone" DataField="Phone" SortExpression="Phone" />
			<asp:BoundField HeaderText="Fax" DataField="Fax" SortExpression="Fax" />
			<asp:BoundField HeaderText="Address1" DataField="Address1" SortExpression="Address1" />
			<asp:BoundField HeaderText="Address2" DataField="Address2" SortExpression="Address2" />
			<asp:BoundField HeaderText="City" DataField="City" SortExpression="City" />
			<asp:BoundField HeaderText="State" DataField="State" SortExpression="State" />
			<asp:BoundField HeaderText="Postal Code" DataField="PostalCode" SortExpression="PostalCode" />
			<asp:BoundField HeaderText="Country" DataField="Country" SortExpression="Country" />
			<asp:BoundField HeaderText="EbClock" DataField="EbClock" SortExpression="EbClock" />
			<asp:BoundField HeaderText="Approved" DataField="IsApproved" SortExpression="IsApproved" />
			<asp:BoundField HeaderText="Locked Out" DataField="IsLockedOut" SortExpression="IsLockedOut" />
			<asp:TemplateField HeaderText="Created" SortExpression="CreateDate">
				<ItemTemplate>
					<%# ConvertTime(Convert.ToDateTime(Eval("CreateDate"))) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Last Login" SortExpression="LastLoginDate">
				<ItemTemplate>
					<%# ConvertTime(Convert.ToDateTime(Eval("LastLoginDate")))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="Last Activity" SortExpression="LastActivityDate">
				<ItemTemplate>
					<%# ConvertTime(Convert.ToDateTime(Eval("LastActivityDate")))%>
				</ItemTemplate>
			</asp:TemplateField>
			<%--<asp:BoundField HeaderText="UserId" DataField="UserId" SortExpression="UserId" />
			<asp:BoundField HeaderText="UserName" DataField="UserName" SortExpression="UserName" />--%>
		</Columns>
	</asp:GridView>
	<asp:SqlDataSource ID="sdsUsers" runat="server" 
		ConnectionString="<%$ ConnectionStrings:SecurityData %>" 
		SelectCommand="sb_Membership_GetAllUsers" SelectCommandType="StoredProcedure">
		<SelectParameters>
			<asp:Parameter Name="ApplicationName" Type="String" />
		</SelectParameters>
	</asp:SqlDataSource>
</asp:Content>

