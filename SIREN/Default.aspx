﻿<%@ Page Title="SIREN" Language="C#" MasterPageFile="~/MasterPages/SIREN.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Hurricane_Default" MaintainScrollPositionOnPostback="true" %>

<%@ Register src="~/SiteControls/QueryControl/QueryControl.ascx" tagname="QueryControl" tagprefix="uc1" %>
<%@ Register src="USStates.ascx" tagname="USStates" tagprefix="uc2" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>

<%@ Register src="USStates2.ascx" tagname="USStates2" tagprefix="uc3" %>

<%@ Register src="ValidationSummary.ascx" tagname="ValidationSummary" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
	<style type="text/css">
		.SectionHeader { font-size: smaller; color: #707070; padding-top:1.5ex;  border-width: 1px; border-color: #C0C0C0; border-bottom-style: solid; }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">

	<div style="text-align:right">
		<asp:LinkButton ID="btnShowAdd" runat="server" CausesValidation="False" onclick="btnShowAdd_Click">Add New Request</asp:LinkButton>
	</div>
	<div ID="panAddData" runat="server" visible="false"	
		style="border: 1px solid #C0C0C0; margin-top: 8px; margin-bottom: 8px; padding-right: 8px; padding-left: 8px; background-color: #F0F0F0;">
		<h5>New Request</h5>
		<uc4:ValidationSummary ID="ValidationSummary2" runat="server" />
		<table style="text-align:left">
			<tr>
				<td colspan="2" class="SectionHeader">Customer Contact</td>
			</tr>
			<tr>
				<td colspan="2">Account:<uc1:querycontrol ID="qcAccount" runat="server" 
						IsRequired="False" QueryUrl="~/SIREN/QueryAccount.aspx"
					ClientOnRowBound="_ULN.AccountOnRowBound" ClientOnChange="_ULN.AccountChanged"
					 Width="5em" />&nbsp;Customer&nbsp;Name*:<asp:TextBox ID="tbCustomerName" runat="server" Width="20em" MaxLength="64"></asp:TextBox>
					 <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="tbCustomerName" ErrorMessage="Customer name is required"><asp:Image ID="Image3" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td colspan="2">Contact&nbsp;Name*:<asp:TextBox ID="tbContactName" runat="server" Width="10em" MaxLength="256"></asp:TextBox>
				<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ControlToValidate="tbContactName" ErrorMessage="Contact name is required"><asp:Image ID="Image7" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator>&nbsp;Contact&nbsp;Phone*:<asp:TextBox
					ID="tbContactPhone" runat="server" MaxLength="16"></asp:TextBox>
					<asp:RegularExpressionValidator ID="revContactNumber" runat="server" 
						Display="Dynamic" ControlToValidate="tbContactPhone" ErrorMessage="Invalid contact phone number" 
						ValidationExpression="\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}" ><asp:Image ID="Image1" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is invalid" /></asp:RegularExpressionValidator>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ControlToValidate="tbContactPhone" ErrorMessage="Contact phone is required"><asp:Image ID="Image6" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td colspan="2" class="SectionHeader">Delivery Information</td>
			</tr>
			<tr>
				<td colspan="2">City*:<asp:TextBox ID="tbCity" runat="server" Width="12em" MaxLength="64"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"  ControlToValidate="tbCity" ErrorMessage="City is required"><asp:Image ID="Image5" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator>&nbsp;State*:<uc2:USStates ID="USStates1" runat="server" />
				&nbsp;Date&nbsp;Needed*:<cc1:PopupCalendar ID="pcDateNeeded" runat="server" TextBoxWidth="6em"
					ShowTitle="false" ShowYear="false" ReadOnly="false"></cc1:PopupCalendar>
					&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"  ControlToValidate="pcDateNeeded" ErrorMessage="Date is required"><asp:Image ID="Image9" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td colspan="2" class="SectionHeader">Equipment</td>
			</tr>
			<tr>
				<td colspan="2">Cat*:<asp:DropDownList ID="ddlCat" runat="server" 
                        onselectedindexchanged="ddlCat_SelectedIndexChanged" AutoPostBack="True">
					</asp:DropDownList>
                    <asp:CompareValidator Operator="NotEqual" Type="String" ID="CompareValidator1" runat="server" 
                    ControlToValidate="ddlCat"  ValueToCompare="0" Display="Dynamic" ErrorMessage="Category is required"><asp:Image ID="imgWarn" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:CompareValidator></td>
			<tr>
				<td colspan="2">Class*:<asp:DropDownList ID="ddlClass" runat="server">
					</asp:DropDownList>
                    <asp:CompareValidator Operator="NotEqual" Type="String" ID="CompareValidator3" runat="server" 
                    ControlToValidate="ddlClass" ValueToCompare="0" Display="Dynamic" ErrorMessage="Class is required"><asp:Image ID="Image2" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:CompareValidator></td>
			</tr>
			<tr>
				<td colspan="2">Request&nbsp;Type*:<asp:DropDownList ID="ddlRequestType" runat="server">
					</asp:DropDownList>
					<asp:CompareValidator Operator="NotEqual" Type="String" ID="cvRequestType" runat="server" 
                    ControlToValidate="ddlRequestType" ValueToCompare="0" Display="Dynamic" ErrorMessage="Request Type is required"><asp:Image ID="Image8" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:CompareValidator></td>
			</tr>
			<tr>
				<td colspan="2">Quantity*:<asp:TextBox ID="tbQuantity" runat="server" Width="4em">1</asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"  ControlToValidate="tbQuantity"   ErrorMessage="Quantity is required"><asp:Image ID="Image4" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator></td>

			</tr>
			<tr id="trContract" runat="server" visible="false">
				<td colspan="2">Contract&nbsp;Number:<asp:TextBox ID="tbContractNumber" runat="server"></asp:TextBox>&nbsp;Status:<asp:DropDownList ID="ddlStatus" runat="server">
					</asp:DropDownList></td>
			</tr>
			<%--<tr>
				<td colspan="2">Assigned&nbsp;PC:<asp:DropDownList ID="ddlPC" runat="server">
					</asp:DropDownList>
                    <asp:CompareValidator Operator="NotEqual" Type="String" ID="CompareValidator4" runat="server" 
                    ControlToValidate="ddlPC"  ValueToCompare="0" Display="Dynamic" ErrorMessage="Assigned PC is a required"><asp:Image ID="Image8" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:CompareValidator>
					
				</td>
			</tr>--%>
			<tr>
				<td colspan="2" class="SectionHeader">Sunbelt Contact</td>
			</tr>
			<tr>
				<td colspan="2">(Name, Phone, PC)*:<asp:TextBox ID="tbLeadFrom" runat="server" Width="30em" MaxLength="256">
				</asp:TextBox><asp:RequiredFieldValidator runat="server" ID="rfvLeadFrom" ControlToValidate="tbLeadFrom" ErrorMessage="Sunbelt Contact Lead From is required"><asp:Image ID="Image200" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td style="vertical-align:top">Notes:</td>
				<td><asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine" Columns="60" Rows="3" MaxLength="1024"></asp:TextBox></td>
			</tr>
			<tr>
				<td colspan="2">* Denotes a required field.</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;">
					<asp:Button ID="btnAddMore" runat="server"
					Text="Add Equipment Lines" onclick="btnAddMore_Click" Width="160px" />&nbsp;<asp:Button ID="btnAddNew"
					runat="server" Text="Add Single Request" onclick="btnAddNew_Click" Width="160px" /></td>
			</tr>
		</table>
		<br />
	</div>
	<br />
	<center>
	<table style="text-align:left">
		<tr>
			<td><asp:LinkButton ID="btnShowFilter" runat="server" CausesValidation="False" onclick="btnShowFilter_Click">Filter</asp:LinkButton></td>
		</tr>
		<tr ID="trFilterData" runat="server" visible="false">
			<td>
				<div ID="panFilterData" runat="server" 
					style="border: 1px solid #C0C0C0; margin-top: 8px; margin-bottom: 8px; padding-right: 8px; padding-left: 8px; background-color: #F0F0F0;">
					<h5>Filter</h5>
					<table>
						<tr>
							<td>Account:<asp:TextBox runat="server" ID="tbfAccount"></asp:TextBox>&nbsp;Customer:<asp:TextBox runat="server" ID="tbfCustomerName"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Category:<asp:TextBox runat="server" ID="dbfCategory"></asp:TextBox>&nbsp;Class:<asp:TextBox runat="server" ID="dbfClass"></asp:TextBox></td>
						</tr>
						<tr>
							<td>City:<asp:TextBox runat="server" ID="dbfCity"></asp:TextBox>&nbsp;State:<asp:TextBox runat="server" ID="dbfState"></asp:TextBox></td>
						</tr>
						<tr>
							<td>Status ID:<asp:DropDownList ID="ddlfStatus" runat="server"></asp:DropDownList></td>
						</tr>
						<tr>
							<td >Notes:<asp:TextBox runat="server" ID="dbfNotes"></asp:TextBox></td>
						</tr>
						<tr>
							<td align="right"><asp:Button ID="btnFilterReset" Visible="true" runat="server" Text="Clear Filter" 
									OnClientClick="javascript:return FilterReset()" onclick="btnFilterReset_Click" />&nbsp;<asp:Button ID="btnFilter" runat="server" Text="Apply Filter"  onclick="btnFilter_Click" /></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<asp:GridView ID="GridView1" runat="server" AllowPaging="True" PageSize="100" onrowdatabound="GridView1_RowDataBound" 
					SkinID="Gray" onpageindexchanged="GridView1_PageIndexChanged" 
					onpageindexchanging="GridView1_PageIndexChanging" onsorting="GridView1_Sorting" 
						AutoGenerateColumns="False" onrowediting="GridView1_RowEditing" 
						DataKeyNames="OrderID" onrowcommand="GridView1_RowCommand" 
						onrowcancelingedit="GridView1_RowCancelingEdit" AllowSorting="True" 
						onrowupdating="GridView1_RowUpdating">
					<PagerSettings Position="TopAndBottom" />
					<PagerStyle HorizontalAlign="Left" />
					<RowStyle HorizontalAlign="Center" />
					<EditRowStyle VerticalAlign="Top" />
					<EmptyDataTemplate>
						<div style="width: 500px"><br /><br />No data rows in table<br /><br /><br /></div>
					</EmptyDataTemplate>
					<Columns>
						<asp:BoundField DataField="OrderID" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="OrderID" Visible="true" />
						<asp:TemplateField HeaderText="Requested" SortExpression="DateCreated">
							<ItemTemplate>
								<%# Sunbelt.Tools.DateTimeTools.ToShortestDateString(Convert.ToDateTime(Eval("DateCreated").ToString())) + " " + Sunbelt.Tools.DateTimeTools.ToMilitaryTimeString(Convert.ToDateTime(Eval("DateCreated").ToString()))%>
							</ItemTemplate>
						</asp:TemplateField>	
						<asp:BoundField DataField="CustomerNumber" HeaderText="Acct." SortExpression="CustomerNumber" ReadOnly="true" />
						<asp:TemplateField HeaderText="Customer" SortExpression="CustomerName">
							<ItemStyle HorizontalAlign="Left" />
							<ItemTemplate>
								<%# Eval("CustomerName")%>
							</ItemTemplate>
							<EditItemTemplate>
								A:<uc1:querycontrol ID="qcAccount" runat="server" IsRequired="False" QueryUrl="~/SIREN/QueryAccount.aspx"
									ClientOnRowBound="_ULN.AccountOnRowBound" ClientOnChange="_ULN.AccountChanged"
									Width="7em" Text='<%# Eval("CustomerNumber") %>' />N:<asp:TextBox ID="tbCustomerName" runat="server" Width="7em" MaxLength="64" Text='<%# Eval("CustomerName")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Contact" SortExpression="ContactName">
							<ItemTemplate>
								<%# Eval("ContactName")%><br /><%# Sunbelt.Tools.WebTools.HtmlEncode(Eval("ContactPhone").ToString()) %>
							</ItemTemplate>
							<EditItemTemplate>
								N:<asp:TextBox ID="tbContactName" runat="server" Width="7em" MaxLength="256" Text='<%# Eval("ContactName")%>'></asp:TextBox><br />P:<asp:TextBox
									ID="tbContactPhone" runat="server" MaxLength="16" Width="7em" Text='<%# Eval("ContactPhone")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="City" SortExpression="City">
							<ItemTemplate>
								<%# Eval("City")%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox ID="tbCity" runat="server" Width="8em" MaxLength="64" Text='<%# Eval("City")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="State" SortExpression="State">
							<ItemTemplate>
								<%# Eval("State") %>
							</ItemTemplate>
							<EditItemTemplate>
								<uc3:USStates2 ID="ddlStates" runat="server" />
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Date Needed" SortExpression="DateNeeded">
							<ItemTemplate>
								<%# Sunbelt.Tools.DateTimeTools.ToShortestDateString(Convert.ToDateTime(Eval("DateNeeded").ToString())) %>
							</ItemTemplate>
							<EditItemTemplate>
								<cc1:PopupCalendar ID="pcDateNeeded" runat="server" TextBoxWidth="5.5em"
									ShowTitle="false" ShowYear="false" ReadOnly="false"></cc1:PopupCalendar>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="Category" HeaderText="Cat" SortExpression="Category" ReadOnly="true" />
						<asp:TemplateField HeaderText="Class" SortExpression="Class">
							<ItemTemplate>
								<asp:Label ID="lbClass" runat="server" Text='<%# Eval("Class")%>'></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Type" SortExpression="RequestTypeID">
							<ItemTemplate>
								<asp:Label ID="lbRequestType" runat="server" Text="lbRequestType"></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:DropDownList ID="ddlRequestType" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</asp:TemplateField>	
						<asp:TemplateField HeaderText="Qty." SortExpression="Quantity">
							<ItemTemplate>
								<%# Eval("Quantity")%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox ID="tbQuantity" runat="server" Width="2em" MaxLength="6" Text='<%# Eval("Quantity")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Contract" SortExpression="ContractNumber">
							<ItemTemplate>
								<%# Eval("ContractNumber")%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox ID="tbContractNumber" runat="server" Width="5em" MaxLength="9" Text='<%# Eval("ContractNumber")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="LeadFrom" HeaderText="Lead From" SortExpression="LeadFrom" ReadOnly="true" />
						<asp:TemplateField HeaderText="StatusID" SortExpression="StatusID">
							<ItemTemplate>
								<asp:Label ID="lbStatus" runat="server" Text="Label"></asp:Label></ItemTemplate><EditItemTemplate>
								<asp:DropDownList ID="ddlStatus" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</asp:TemplateField>
						<%--<asp:TemplateField HeaderText="Assigned PC" SortExpression="AssignedPC">
							<ItemTemplate>
								<%# Eval("AssignedPC")%>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:DropDownList ID="ddlPC" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</asp:TemplateField>--%>
						<asp:TemplateField HeaderText="CCR" SortExpression="CommandCenterRepID">
							<ItemTemplate>
								<asp:Label ID="lbCCR" runat="server" Text="lbCCR"></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:DropDownList ID="ddlCCR" runat="server"></asp:DropDownList>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Notes" SortExpression="Notes">
							<ItemTemplate>
								<asp:Label ID="lbNotes" runat="server" Width="14em" Text='<%# Eval("Notes")%>'></asp:Label>
							</ItemTemplate>
							<EditItemTemplate>
								<asp:TextBox ID="tbNotes" runat="server" TextMode="MultiLine"
									Columns="18" Rows="4" MaxLength="1024" Text='<%# Eval("Notes")%>'></asp:TextBox>
							</EditItemTemplate>
						</asp:TemplateField>
						<asp:CommandField ButtonType="Link" ShowEditButton="true" EditText="Edit"
							ShowDeleteButton="false" DeleteText="Remove" 
							ShowCancelButton="true" CancelText="Cancel"
							UpdateText="Save" ItemStyle-HorizontalAlign="Center">
							<ItemStyle HorizontalAlign="Center" />
						</asp:CommandField>
					</Columns>
				</asp:GridView>
			</td>
		</tr>
	</table>
	
		<%--<asp:SqlDataSource ID="SqlDataHurrican" runat="server" 
			ConnectionString="<%$ ConnectionStrings:CustomerFulfillmentConnectionString %>" 
			SelectCommand="sb_SelOrders" SelectCommandType="StoredProcedure">
			<SelectParameters>
				<asp:Parameter Name="OrderID" Type="Int32" />
			</SelectParameters>
		</asp:SqlDataSource>--%>
	</center>
	
<script type="text/javascript" language="javascript">
//<![CDATA[

	var _ULN = {

		OnWindowLoad: function()
		{
			//alert("OnWindowLoad");
			//var obj = document.getElementById("<% =tbQuantity.ClientID %>");
			var obj = SBNet.Util.FindASPXControl("INPUT", "tbQuantity", null);
			if (obj != null)
				SBNet.EC.IntegerEntry(obj, true, true, 1, 1000);

			//obj = document.getElementById("<% =qcAccount.TextBoxControl.ClientID %>");
			//if (obj != null)
			//{
			//	SBNet.EC.SafeTextEntry(obj, true);
			//	SBNet.EC.IsRequired(obj, true);
			//}

			obj = document.getElementById("<% =tbCustomerName.ClientID %>");
			if (obj != null)
			{
				SBNet.EC.SafeTextEntry(obj, true);
				SBNet.EC.IsRequired(obj, true);
			}

			//obj = document.getElementById("<% =tbCity.ClientID %>");
			obj = SBNet.Util.FindASPXControl("INPUT", "tbCity", null);
			if (obj != null)
			{
				SBNet.EC.SafeTextEntry(obj, true);
				SBNet.EC.IsRequired(obj, true);
			}

			//obj = document.getElementById("<% =tbContactName.ClientID %>");
			obj = SBNet.Util.FindASPXControl("INPUT", "tbContactName", null);
			if (obj != null)
			{
				SBNet.EC.SafeTextEntry(obj, true);
				SBNet.EC.IsRequired(obj, true);
			}

			//obj = document.getElementById("<% =tbContactPhone.ClientID %>");
			obj = SBNet.Util.FindASPXControl("INPUT", "tbContactPhone", null);
			if (obj != null)
			{
				SBNet.EC.USPhoneEntry(obj, true);
				SBNet.EC.IsRequired(obj, true);
			}

			obj = document.getElementById("<% =pcDateNeeded.TextBoxControl.ClientID %>");
			if (obj == null)
				obj = SBNet.Util.FindASPXControl("INPUT", "txtDate", null);
			if (obj != null)
			{
				SBNet.EC.DateEntry(obj, true);
				SBNet.EC.IsRequired(obj, true);
			}

			obj = SBNet.Util.FindASPXControl("INPUT", "tbContractNumber", null);
			if (obj != null)
			{
				SBNet.EC.IntegerEntry(obj, false, true);
				SBNet.EC.AllowNoValue(obj, true);
			}

			obj = document.getElementById("<% =tbNotes.ClientID %>");
			if (obj != null)
				SBNet.EC.SafeTextEntry(obj, true);
		},

		OnServerError: function(message, context)
		{
			alert(message);
		},


		//// Event handlers for the query control ////
		AccountOnRowBound: function(div)
		{
			var spcl = _QueryControl_getSpanById("iscl", div);
			var bold = (spcl.innerHTML == "true");

			var spv = _QueryControl_getSpanById("value", div);
			var stat = _QueryControl_getSpanById("status", div);
			if (bold)
				spv.innerHTML = "<b>" + spv.innerHTML + "</b>";

			var spc = _QueryControl_getSpanById("customer", div);
			spc.style.display = "";
			if (bold)
				spc.innerHTML = "&nbsp;<b>(" + stat.innerHTML + ")</b>" + "&nbsp;-&nbsp;" + spc.innerHTML;
			else
				spc.innerHTML = "&nbsp;(" + stat.innerHTML + ")" + "&nbsp;-&nbsp;" + spc.innerHTML;
			if (bold)
				spc.innerHTML = "<b>" + spc.innerHTML + "</b>";
		},

		AccountChanged: function(div)
		{
			var control = document.getElementById("<% =tbCustomerName.ClientID %>");
			if (control == null)
				control = SBNet.Util.FindASPXControl("INPUT", "tbCustomerName", null);
			control.value = div.children[3].innerText;
		}
	}

	function FilterReset()
	{
		var Control = document.getElementById('<%=tbfAccount.ClientID %>')
		Control.value = "";

		Control = document.getElementById('<%=tbfCustomerName.ClientID %>')
		Control.value = "";

		Control = document.getElementById('<%=dbfCategory.ClientID %>')
		Control.value = ""

		Control = document.getElementById('<%=dbfCity.ClientID %>')
		Control.value = "";

		Control = document.getElementById('<%=dbfClass.ClientID %>')
		Control.value = "";

		Control = document.getElementById('<%=dbfState.ClientID %>')
		Control.value = "";

		Control = document.getElementById('<%=ddlfStatus.ClientID %>')
		Control.value = "0";

		Control = document.getElementById('<%=dbfNotes.ClientID %>')
		Control.value = "";

		return true;
	}

	SBNet.Util.AttachEvent(window, "onload", _ULN.OnWindowLoad);

//]]>
</script>
</asp:Content>

