using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class SIREN_ValidationSummary : System.Web.UI.UserControl
{
	public string CssClass
	{
		set { validationSummary.CssClass = value; }
		get { return validationSummary.CssClass; }
	}
	public Unit Width
	{
		set { validationSummary.Width = value; }
		get { return validationSummary.Width; }
	}
	public ValidationSummaryDisplayMode DisplayMode
	{
		set { validationSummary.DisplayMode = value; }
		get { return validationSummary.DisplayMode; }
	}
	public string HeaderText
	{
		set { validationSummary.HeaderText = value; }
		get { return validationSummary.HeaderText; }
	}
	public Color BackColor
	{
		set { validationSummary.BackColor = value; }
		get { return validationSummary.BackColor; }
	}
	public Color ForeColor
	{
		set { validationSummary.ForeColor = value; }
		get { return validationSummary.ForeColor; }
	}
	public bool FontBold
	{
		set { validationSummary.Font.Bold = value; }
		get { return validationSummary.Font.Bold; }
	}
	public string FontName
	{
		set { validationSummary.Font.Name = value; }
		get { return validationSummary.Font.Name; }
	}
	public FontUnit FontSize
	{
		set { validationSummary.Font.Size = value; }
		get { return validationSummary.Font.Size; }
	}
	/*
	EnableClientScript="true"
    EnableViewState="false"
	*/ 
	
	protected void Page_Load(object sender, EventArgs e)
	{
	}
	
	/// <summary>
	/// Displays the error text in the validation summary.
	/// </summary>
	/// <param name="text"></param>
	public void DisplayError(string errorMessage)
	{
		//Create a custom validator
		CustomValidator cv = new CustomValidator();
		cv.ErrorMessage = errorMessage;
		cv.Text = string.Empty;
		//cv.ServerValidate += new ServerValidateEventHandler(cv_ServerValidate);
		cv.Enabled = true;
		cv.Display = ValidatorDisplay.None;
		cv.Attributes.Add("IsVSControl", "true");
		cv.ClientValidationFunction = "UPVS.DefaultValidate";
		cv.EnableClientScript = true;
		cv.IsValid = false;
		//cv.Validate();
		cv.IsValid = false;
		phValidators.Controls.Add(cv);
	}

	/*
	private void cv_ServerValidate(object source, ServerValidateEventArgs args)
	{
		//Set to invalid to make it show the text.
		args.IsValid = false;
	}
	*/
}
