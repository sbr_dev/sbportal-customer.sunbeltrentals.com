﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sunbelt.Tools;
using System.Data;
using Sunbelt.Common.DAL;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

public partial class Hurricane_Default : SBNet.Web.WebPage
{
	private static string ConnectionString
	{
		get { return ConfigurationManager.ConnectionStrings["HurricaneData"].ConnectionString; }
	}

	private DataTable EquipmentList
	{
		get { return (DataTable)WebTools.GetCache("_HEList", null); }
		set { WebTools.SetCache("_HEList", value, new TimeSpan(0, 30, 0)); }
	}
	
	private string SortExpression
	{
		get { return (string)GetViewState("SortE", "DateCreated"); }
		set { SetViewState("SortE", value); }
	}
	private SortDirection SortDirection
	{
		get { return (SortDirection)GetViewState("SortD", SortDirection.Ascending); }
		set { SetViewState("SortD", value); }
	}
    private bool IsFilterMode
    {
        get { return (bool)GetViewState("Filter", false); }
        set { SetViewState("Filter", value); }
    }

	protected override void OnPreInit(EventArgs e)
	{
		//Turn on ViewState compression.
		//EnableViewstateCompression = true;
		base.OnPreInit(e);
	}

	protected void Page_Load(object sender, EventArgs e)
    {
		//REDIRECT TO NEW SITE!!!
		Response.Redirect("http://siren", true);
		return;
/*		
		if (!IsPostBack)
		{
			//Init Dropdowns here!!!
            LoadCategories();
			LoadClasses();
			//LoadPCs();
			//LoadStatus(ddlStatus, false, false);
			LoadRequestTypes(ddlRequestType, false, true);

            LoadStatus(ddlfStatus, false, true);
            IsFilterMode = false;
#if DEBUG
			LoadGrid(true);
#else
			LoadGrid(true);
#endif
		}

		SBNet.Tools.IncludeEntryControlsClientScript(this);
*/
    }
	
	private void LoadGrid(bool forceLoad)
	{
		if (EquipmentList == null || forceLoad)
		{
			//Get Data here...
			using (SqlConnection conn = new SqlConnection(ConnectionString))
			using (SPData sp = new SPData("dbo.sb_SelOrders", conn))
			{
				EquipmentList = sp.ExecuteDataTable();
			}
		}

        StringBuilder rowFilter = new StringBuilder();
        StringBuilder infoText = new StringBuilder();
		if (IsFilterMode)
		{
			if (tbfAccount.Text != "")
			{
				string value = tbfAccount.Text.Trim();
				rowFilter.Append("CustomerNumber=" + ConvertTools.ToInt32(value) + " AND ");
				infoText.Append("Customer Number=" + value + ", ");
			}
			if (tbfCustomerName.Text != "")
			{
				string value = tbfCustomerName.Text.Trim();
				rowFilter.Append("CustomerName like '%" + value + "%' AND ");
				infoText.Append("Customer Name=" + value + ", ");
			}
			if (dbfCategory.Text != "")
			{
				string value = dbfCategory.Text.Trim();
				rowFilter.Append("Category=" + ConvertTools.ToInt32(value) + " AND ");
				infoText.Append("Category=" + value + ", ");
			}
			if (dbfClass.Text != "")
			{
				string value = dbfClass.Text.Trim();
				rowFilter.Append("Class=" + ConvertTools.ToInt32(value) + " AND ");
				infoText.Append("Class=" + value + ", ");
			}
			if (dbfCity.Text != "")
			{
				string value = dbfCity.Text.Trim();
				rowFilter.Append("City like '%" + value + "%' AND ");
				infoText.Append("City=" + value + ", ");
			}
			if (dbfState.Text != "")
			{
				string value = dbfState.Text.Trim();
				rowFilter.Append("State='" + value + "' AND ");
				infoText.Append("State=" + value + ", ");
			}
			if (ddlfStatus.SelectedValue != "0")
			{
				string value = ddlfStatus.SelectedValue;
				rowFilter.Append("StatusID='" + ConvertTools.ToInt32(value) + "' AND ");
				infoText.Append("StatusID=" + value + ", ");
			}
			if (dbfNotes.Text != "")
			{
				string value = dbfState.Text.Trim();
				rowFilter.Append("Notes like '%" + value + "%' AND ");
				infoText.Append("Notes=" + value + ", ");
			}

			//Remove trailing "AND".
			if (rowFilter.Length > 5)
			{
				rowFilter.Remove(rowFilter.Length - 5, 5);
				infoText.Remove(infoText.Length - 2, 2);
			}

		}
		else
		{
			string value = ddlfStatus.SelectedValue;
            rowFilter.Append("StatusID  <> 4 AND StatusID <> 5 ");  // exclude Filled and Cancelled
			//infoText.Append("StatusID=" + value + ", ");
		}

		bool showAllColumns = (GridView1.EditIndex < 0);
		GridView1.Columns[0].Visible = showAllColumns;   //order
		GridView1.Columns[1].Visible = showAllColumns;	 //requested
		GridView1.Columns[2].Visible = showAllColumns;	 //account
		//GridView1.Columns[3].Visible = showAllColumns;	 //customer
		//GridView1.Columns[8].Visible = showAllColumns;	 //Cat
		//GridView1.Columns[9].Visible = showAllColumns;	 //Class
		GridView1.Columns[13].Visible = showAllColumns;	 //Lead

	
		if(!string.IsNullOrEmpty(SortExpression) || (rowFilter.Length > 0))
		{
			string sort = SortExpression + " ";
			if (SortDirection == SortDirection.Ascending)
				sort += "ASC";
			else
				sort += "DESC";

            DataView dv = new DataView(EquipmentList, rowFilter.ToString(), sort, DataViewRowState.CurrentRows);
			GridView1.DataSource = dv;
			GridView1.DataBind();
		}
		else
		{
			GridView1.DataSource = EquipmentList;
			GridView1.DataBind();
		}
	}
	private void LoadCategories()
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("WynneData.sb_GetEquipmentCats", conn))
		{
			ddlCat.DataSource = sp.ExecuteReader();
			ddlCat.DataTextField = "CatName";
			ddlCat.DataValueField = "CatID";
			ddlCat.DataBind();
		}
	}
	private void LoadClasses()
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("WynneData.sb_GetEquipmentClasses", conn))
		{
			sp.AddParameterWithValue("@CatID", SqlDbType.VarChar, 20, ParameterDirection.Input, Convert.ToInt32(ddlCat.SelectedValue));
			
			ddlClass.DataSource = sp.ExecuteReader();
			ddlClass.DataTextField = "ClassName";
			ddlClass.DataValueField = "ClassID";
			ddlClass.DataBind();
		}
	}
	private void LoadPCs(DropDownList ddl)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("WynneData.sb_GetEquipmentProfitCenters", conn))
	    {
			ddl.DataSource = sp.ExecuteReader();
			ddl.DataTextField = "PCDisplayName";
			ddl.DataValueField = "PC";
			ddl.DataBind();
		}
	}
	private void LoadShortPCs(DropDownList ddl)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("WynneData.sb_GetEquipmentProfitCenters", conn))
		{
			ddl.DataSource = sp.ExecuteReader();
			ddl.DataTextField = "PC";
			ddl.DataValueField = "PC";
			ddl.DataBind();

			ddl.Items.RemoveAt(0);
			ddl.Items.Insert(0, new ListItem("-PC-", "0"));
		}
	}
	private void LoadCCRs(DropDownList ddl)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("dbo.sb_GetCommandCenterReps", conn))
		{
			ddl.DataSource = sp.ExecuteReader();
			ddl.DataTextField = "CommandCenterRep";
			ddl.DataValueField = "CommandCenterRepID";
			ddl.DataBind();

			ddl.Items.RemoveAt(0);
			ddl.Items.Insert(0, new ListItem("-None-", "0"));
		}
	}
	private string GetCCR(int ccr)
	{
		switch (ccr)
		{
			case 0:
				return "";
			case 1:
				//return "In Progress";
				return "LocalPC";
			case 2:
				return "GT1";
			case 3:
				return "GT2";
			case 4:
				return "GT3";
			case 5:
				return "GT4";
			case 6:
				return "GT5";
			case 7:
				return "GT6";
			case 8:
				return "PP1";
			case 9:
				return "PP2";
			case 10:
				return "PP3";
			case 11:
				return "PP4";
			case 12:
				return "PP5";
			case 13:
				return "PP6";
			default:
				return "unknown";
		}
	}
	private void LoadStatus(DropDownList ddl, bool shortText, bool SelectRequired)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("dbo.sb_GetStatusList", conn))
		{
			ddl.DataSource = sp.ExecuteReader();
			ddl.DataTextField = (shortText == true) ? "Short" : "Status";
			ddl.DataValueField = "StatusID";
			ddl.DataBind();

            if (SelectRequired)
                ddl.Items.Insert(0, new ListItem("--Select--", "0"));
        }
	}
	private string GetStatus(int status)
	{
		switch (status)
		{
			case 1:
				return "New";
			case 2:
				//return "In Progress";
				return "Progress";
			case 3:
				//return "Assigned to PC";
				return "Assigned";
			case 4:
				//return "Order Filled";
				return "Filled";
			case 5:
				return "Cancelled";
            case 6:
                return "Contacted";
            default:
				return "unknown";
		}
	}
	private void LoadRequestTypes(DropDownList ddl, bool shortText, bool SelectRequired)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("dbo.sb_GetRequestTypes", conn))
		{
			ddl.DataSource = sp.ExecuteReader();
			ddl.DataTextField = (shortText == true) ? "Short" : "RequestType";
			ddl.DataValueField = "RequestTypeID";
			ddl.DataBind();

			if (!SelectRequired)
				ddl.Items.RemoveAt(0);
		}
	}
	private string GetRequestType(int type)
	{
		switch (type)
		{
			case 1:
				return "General Tool";
			case 2:
				return "Pump & Power";
			default:
				return "unknown";
		}
	}
	private void ClearForm()
	{
		qcAccount.Text = string.Empty;
		tbCustomerName.Text = string.Empty;
		tbLeadFrom.Text = string.Empty;
		ddlCat.SelectedIndex = 0;
		ddlClass.SelectedIndex = 0;
		tbQuantity.Text = "1";
		tbCity.Text = string.Empty;
		USStates1.SelectedValue = "0";
		pcDateNeeded.Date = DateTime.MinValue;
		tbContactName.Text = string.Empty;
		tbContactPhone.Text = string.Empty;
		//tbContractNumber.Text = string.Empty;
		//ddlStatus.SelectedIndex = 0;
		//ddlPC.SelectedIndex = 0;
		tbNotes.Text = string.Empty;
	}

	protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			DataRow dr = ((DataRowView)e.Row.DataItem).Row;
			Label lb;

			if (e.Row.RowIndex == GridView1.EditIndex)
			{
				SBNetWebControls.PopupCalendar pcDate = (SBNetWebControls.PopupCalendar)e.Row.FindControl("pcDateNeeded");
				pcDate.Date = Convert.ToDateTime(dr["DateNeeded"].ToString());

				ASP.siren_usstates2_ascx ddlStates = (ASP.siren_usstates2_ascx)e.Row.FindControl("ddlStates");
				ddlStates.SelectedValue = dr["State"].ToString();

				DropDownList ddl = (DropDownList)e.Row.FindControl("ddlStatus");
				LoadStatus(ddl, true, false);
				ddl.SelectedValue = dr["StatusID"].ToString();

				//ddl = (DropDownList)e.Row.FindControl("ddlPC");
				//LoadShortPCs(ddl);
				//ddl.SelectedValue = dr["AssignedPC"].ToString();

				ddl = (DropDownList)e.Row.FindControl("ddlCCR");
				LoadCCRs(ddl);
				ddl.SelectedValue = dr["CommandCenterRepID"].ToString();

				ddl = (DropDownList)e.Row.FindControl("ddlRequestType");
				LoadRequestTypes(ddl, true, false);
				ddl.SelectedValue = dr["RequestTypeID"].ToString();
			}
			else
			{
				lb = (Label)e.Row.FindControl("lbStatus");
				lb.Text = GetStatus(Convert.ToInt32(dr["StatusID"].ToString()));

				lb = (Label)e.Row.FindControl("lbCCR");
				lb.Text = GetCCR(ConvertTools.ToInt32(dr["CommandCenterRepID"].ToString()));

				lb = (Label)e.Row.FindControl("lbRequestType");
				lb.Text = GetRequestType(Convert.ToInt32(dr["RequestTypeID"].ToString()));
			}

			lb = (Label)e.Row.FindControl("lbClass");
			lb.Text = "(" + dr["Class"].ToString() + ") " + dr["ClassName"].ToString().Trim();
			//lb.ToolTip = dr["ClassName"].ToString().Trim();
			
		}
	}
	protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
	{
		//Don't sort in edit mode.
		if (GridView1.EditIndex == -1)
		{
			if (e.SortExpression == SortExpression)
				SortDirection = (SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
			else
				SortDirection = SortDirection.Ascending;
			SortExpression = e.SortExpression;
			LoadGrid(false);
		}
	}
	
	protected void GridView1_PageIndexChanged(object sender, EventArgs e)
	{
	}
	
	protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
	{
		GridView1.PageIndex = e.NewPageIndex;
		LoadGrid(false);
	}

	protected void btnAddMore_Click(object sender, EventArgs e)
	{
		AddNew();
		ddlCat.SelectedIndex = 0;
		LoadClasses();
		tbQuantity.Text = "1";
		LoadGrid(true);
	}

	protected void btnAddNew_Click(object sender, EventArgs e)
	{
		AddNew();
		ClearForm();
		//panAddData.Visible = false;
		LoadGrid(true);
	}

	protected void AddNew()
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("dbo.sb_InsOrder", conn))
		{
			string s = qcAccount.ValueControl.Value;
			sp.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input,
				(!string.IsNullOrEmpty(s)) ? (object)Convert.ToInt32(s) : (object)DBNull.Value);
			sp.AddParameterWithValue("@CustomerName", SqlDbType.VarChar, 64, ParameterDirection.Input,
				tbCustomerName.Text.Trim());
			sp.AddParameterWithValue("@Category", SqlDbType.Int, 4, ParameterDirection.Input,
				Convert.ToInt32(ddlCat.SelectedValue));
			sp.AddParameterWithValue("@Class", SqlDbType.Int, 4, ParameterDirection.Input,
				Convert.ToInt32(ddlClass.SelectedValue));

			sp.AddParameterWithValue("@RequestTypeID", SqlDbType.Int, 4, ParameterDirection.Input,
				Convert.ToInt32(ddlRequestType.SelectedValue));

			s = tbLeadFrom.Text.Trim();
			sp.AddParameterWithValue("@LeadFrom", SqlDbType.VarChar, 256, ParameterDirection.Input,
				(!string.IsNullOrEmpty(s)) ? (object)s : (object)DBNull.Value);

			sp.AddParameterWithValue("@City", SqlDbType.VarChar, 64, ParameterDirection.Input,
				tbCity.Text);
			sp.AddParameterWithValue("@State", SqlDbType.VarChar, 2, ParameterDirection.Input,
				USStates1.SelectedValue);
			sp.AddParameterWithValue("@DateNeeded", SqlDbType.DateTime, 20, ParameterDirection.Input,
				pcDateNeeded.Date);

			sp.AddParameterWithValue("@ContactName", SqlDbType.VarChar, 256, ParameterDirection.Input,
				tbContactName.Text.Trim());
	
			sp.AddParameterWithValue("@ContactPhone", SqlDbType.VarChar, 16, ParameterDirection.Input,
				tbContactPhone.Text.Trim());

			sp.AddParameterWithValue("@Quantity", SqlDbType.Int, 4, ParameterDirection.Input,
				Convert.ToInt32(tbQuantity.Text.Trim()));
			
			//sp.AddParameterWithValue("@StatusID", SqlDbType.Int, 4, ParameterDirection.Input,
			//	Convert.ToInt32(ddlStatus.SelectedValue));
			sp.AddParameterWithValue("@StatusID", SqlDbType.Int, 4, ParameterDirection.Input, 1);

			//sp.AddParameterWithValue("@AssignedPC", SqlDbType.Int, 4, ParameterDirection.Input,
			//    (ddlPC.SelectedValue != "0") ? (object)Convert.ToInt32(ddlPC.SelectedValue) : (object)DBNull.Value);
			sp.AddParameterWithValue("@AssignedPC", SqlDbType.Int, 4, ParameterDirection.Input, DBNull.Value);

			//s = tbContractNumber.Text.Trim();
			//sp.AddParameterWithValue("@ContractNumber", SqlDbType.Int, 4, ParameterDirection.Input,
			//	(!string.IsNullOrEmpty(s)) ? (object)Convert.ToInt32(s) : (object)DBNull.Value);

			sp.AddParameterWithValue("@CommandCenterRepID", SqlDbType.Int, 4, ParameterDirection.Input, DBNull.Value);
			
			s = tbNotes.Text.Trim();
			sp.AddParameterWithValue("@Notes", SqlDbType.VarChar, 1024, ParameterDirection.Input,
				(!string.IsNullOrEmpty(s)) ? (object)s : (object)DBNull.Value);
            try
            {
                sp.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if( ex.Message.ToLower().IndexOf("duplicate record" ) > 0 )
                {
                    ValidationSummary2.DisplayError("Add new Request failed, duplicate Request record found.");
                    return;   //ignore error
                }
            }
		}
	}
	
	protected void UpdateRow(int orderID, int account, string customerName,	int requestType,
		int catNo, int classNo, string city, string state, DateTime dateNeeded, string contactName,
		string contactPhone, int quantity, string leadFrom, int status,	int assignedPC,
		int ccr, int contractNumber, string notes)
	{
		using (SqlConnection conn = new SqlConnection(ConnectionString))
		using (SPData sp = new SPData("dbo.sb_UpdOrder", conn))
		{
			sp.AddParameterWithValue("@OrderID", SqlDbType.Int, 4, ParameterDirection.Input,
				(orderID > 0) ? (object)orderID : (object)DBNull.Value);
			
			sp.AddParameterWithValue("@CustomerNumber", SqlDbType.Int, 4, ParameterDirection.Input,
				(account > 0) ? (object)account : (object)DBNull.Value);
			
			sp.AddParameterWithValue("@CustomerName", SqlDbType.VarChar, 64, ParameterDirection.Input, customerName);
			sp.AddParameterWithValue("@RequestTypeID", SqlDbType.Int, 4, ParameterDirection.Input, requestType);
			sp.AddParameterWithValue("@Category", SqlDbType.Int, 4, ParameterDirection.Input, catNo);
			sp.AddParameterWithValue("@Class", SqlDbType.Int, 4, ParameterDirection.Input, classNo);
			sp.AddParameterWithValue("@City", SqlDbType.VarChar, 64, ParameterDirection.Input, city);
			sp.AddParameterWithValue("@State", SqlDbType.VarChar, 2, ParameterDirection.Input, state);
			sp.AddParameterWithValue("@DateNeeded", SqlDbType.DateTime, 20, ParameterDirection.Input, dateNeeded);
			sp.AddParameterWithValue("@ContactName", SqlDbType.VarChar, 256, ParameterDirection.Input, contactName);
			sp.AddParameterWithValue("@ContactPhone", SqlDbType.VarChar, 16, ParameterDirection.Input, contactPhone);
			sp.AddParameterWithValue("@Quantity", SqlDbType.Int, 4, ParameterDirection.Input, quantity);
			sp.AddParameterWithValue("@LeadFrom", SqlDbType.VarChar, 256, ParameterDirection.Input, leadFrom);
			sp.AddParameterWithValue("@StatusID", SqlDbType.Int, 4, ParameterDirection.Input, status);

			sp.AddParameterWithValue("@CommandCenterRepID", SqlDbType.Int, 4,  ParameterDirection.Input,
				(ccr > 0) ? (object)ccr : (object)DBNull.Value);

			sp.AddParameterWithValue("@AssignedPC", SqlDbType.Int, 4, ParameterDirection.Input,
				(assignedPC > 0) ? (object)assignedPC : (object)DBNull.Value);

			sp.AddParameterWithValue("@ContractNumber", SqlDbType.Int, 4, ParameterDirection.Input,
				(contractNumber > 0) ? (object)contractNumber : (object)DBNull.Value);

			sp.AddParameterWithValue("@Notes", SqlDbType.VarChar, 1024, ParameterDirection.Input,
				(!string.IsNullOrEmpty(notes)) ? (object)notes : (object)DBNull.Value);

			sp.ExecuteNonQuery();
		}
	}
	
	protected void ddlCat_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadClasses();
    }
	protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
	{
		panAddData.Visible = false;
		trFilterData.Visible = false;
		GridView1.EditIndex = e.NewEditIndex;
		LoadGrid(false);
	}
	protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
	{
		GridViewRow gvr = GridView1.Rows[e.RowIndex];

		if (EquipmentList == null)
		{
			using (SqlConnection conn = new SqlConnection(ConnectionString))
			using (SPData sp = new SPData("dbo.sb_SelOrders", conn))
			{
				EquipmentList = sp.ExecuteDataTable();
			}
		}

		string order = GridView1.DataKeys[e.RowIndex].Value.ToString();
		DataRow dr = EquipmentList.Select("OrderID = " + order)[0]; 

		int orderID = Convert.ToInt32(order);
		
		//int account = ConvertTools.ToInt32(dr["CustomerNumber"].ToString());
		ASP.sitecontrols_querycontrol_querycontrol_ascx qc = 
			(ASP.sitecontrols_querycontrol_querycontrol_ascx)gvr.FindControl("qcAccount");
		int account = ConvertTools.ToInt32(qc.ValueControl.Value);
		if(account == 0)
			account = ConvertTools.ToInt32(qc.Text);

		//string customerName = dr["CustomerName"].ToString();
		string customerName = ((TextBox)gvr.FindControl("tbCustomerName")).Text.Trim();
	
		int requestType = Convert.ToInt32(((DropDownList)gvr.FindControl("ddlRequestType")).SelectedValue);

		int catNo = Convert.ToInt32(dr["Category"].ToString());
		int classNo = Convert.ToInt32(dr["Class"].ToString());

		int quantity = ConvertTools.ToInt32(((TextBox)gvr.FindControl("tbQuantity")).Text.Trim(), 1);
		string city = ((TextBox)gvr.FindControl("tbCity")).Text.Trim();
		string state = ((ASP.siren_usstates2_ascx)gvr.FindControl("ddlStates")).SelectedValue;
		DateTime dateNeeded = ((SBNetWebControls.PopupCalendar)gvr.FindControl("pcDateNeeded")).Date;
		string contactName = ((TextBox)gvr.FindControl("tbContactName")).Text.Trim();
		string contactPhone = ((TextBox)gvr.FindControl("tbContactPhone")).Text.Trim();

		string leadFrom = dr["LeadFrom"].ToString();

		int status = Convert.ToInt32(((DropDownList)gvr.FindControl("ddlStatus")).SelectedValue);
		//int assignedPC = Convert.ToInt32(((DropDownList)gvr.FindControl("ddlPC")).SelectedValue);
		int assignedPC = 0;

		int ccr = Convert.ToInt32(((DropDownList)gvr.FindControl("ddlCCR")).SelectedValue);
		
		int contractNumber = ConvertTools.ToInt32(((TextBox)gvr.FindControl("tbContractNumber")).Text.Trim());
		string notes = ((TextBox)gvr.FindControl("tbNotes")).Text.Trim();

		UpdateRow(orderID, account, customerName, requestType,
			catNo, classNo, city, state, dateNeeded,
			contactName, contactPhone, quantity, leadFrom, status,
			assignedPC, ccr, contractNumber, notes);

		GridView1.EditIndex = -1;
		LoadGrid(true);
	}
	protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
	{
		GridView1.EditIndex = -1;
		LoadGrid(false);
	}
	protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
	{

	}
	protected void btnShowAdd_Click(object sender, EventArgs e)
	{
		panAddData.Visible = !panAddData.Visible;
		if (panAddData.Visible == true)
		{
			trFilterData.Visible = false;
			GridView1.EditIndex = -1;
			LoadGrid(false);
		}
	}
	protected void btnFilter_Click(object sender, EventArgs e)
	{
	    panAddData.Visible = false;
	    trFilterData.Visible = true;
	    IsFilterMode = true;
		LoadGrid(false);
	}
	protected void btnFilterReset_Click(object sender, EventArgs e)
	{
		panAddData.Visible = false;
		trFilterData.Visible = false;
		IsFilterMode = false;
		LoadGrid(false);
	}
	protected void btnShowFilter_Click(object sender, EventArgs e)
	{
		trFilterData.Visible = !trFilterData.Visible;
		if (trFilterData.Visible == true)
		{
			panAddData.Visible = false;
			GridView1.EditIndex = -1;
			LoadGrid(false);
		}
	}
}
