using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

[ValidationPropertyAttribute("SelectedValue")]
public partial class UserControls_USStates : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (shortStateName)
        {
            // ----------------------------------------------------
            // If the ShortStateName property is set to true
            // use the value as the display text. E.g. NC
            // ----------------------------------------------------
            foreach(ListItem theItem in StateDropDown.Items)
            {
                theItem.Text = theItem.Value;
            }
        }
    }

	public DropDownList stateDropDown
	{
		get { return StateDropDown; }
	}

	public string SelectedValue
	{
		set
		{
			try
			{
				StateDropDown.SelectedValue = value;
			}
			catch
			{
				StateDropDown.SelectedIndex = 0;
			}
		}
		get { return StateDropDown.SelectedValue; }
	}

    public string SelectedItem
    {
        get { return StateDropDown.SelectedItem.Text; }
    }

    private bool shortStateName = false;
    public bool ShortStateName
    {
        get { return shortStateName; }
        set { shortStateName = value; }
    }

    public string CssClass
    {
        get { return StateDropDown.CssClass; }
        set { StateDropDown.CssClass = value; }
    }

    public DropDownList StateList
    {
        get { return StateDropDown; }

    }
}
