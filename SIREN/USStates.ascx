<%@ Control Language="C#" AutoEventWireup="true" CodeFile="USStates.ascx.cs" Inherits="UserControls_USStates" %>

<asp:DropDownList ID="StateDropDown" runat="server">
	<asp:ListItem Text="-- Select --" Value="0" /> 
	<asp:ListItem Text="AL - Alabama" Value="AL" /> 
	<asp:ListItem Text="AR - Arkansas" Value="AR"/> 
	<asp:ListItem Text="AZ - Arizona" Value="AZ"/> 
	<asp:ListItem Text="CA - California" Value="CA"/> 
	<asp:ListItem Text="CO - Colorado" Value="CO"/> 
	<asp:ListItem Text="CT - Connecticut" Value="CT"/> 
	<asp:ListItem Text="DC - District of Columbia" Value="DC"/> 
	<asp:ListItem Text="DE - Delaware" Value="DE"/> 
	<asp:ListItem Text="FL - Florida" Value="FL"/> 
	<asp:ListItem Text="GA - Georgia" Value="GA"/> 
	<asp:ListItem Text="HI - Hawaii" Value="HI"/> 
	<asp:ListItem Text="IA - Iowa" Value="IA"/> 
	<asp:ListItem Text="ID - Idaho" Value="ID"/> 
	<asp:ListItem Text="IL - Illinois" Value="IL"/> 
	<asp:ListItem Text="IN - Indiana" Value="IN"/> 
	<asp:ListItem Text="KS - Kansas" Value="KS"/> 
	<asp:ListItem Text="KY - Kentucky" Value="KY"/> 
	<asp:ListItem Text="LA - Louisiana" Value="LA"/> 
	<asp:ListItem Text="MA - Massachusetts" Value="MA"/> 
	<asp:ListItem Text="MD - Maryland" Value="MD"/> 
	<asp:ListItem Text="ME - Maine" Value="ME"/> 
	<asp:ListItem Text="MI - Michigan" Value="MI"/> 
	<asp:ListItem Text="MN - Minnesota" Value="MN"/> 
	<asp:ListItem Text="MO - Missouri" Value="MO"/> 
	<asp:ListItem Text="MS - Mississippi" Value="MS"/> 
	<asp:ListItem Text="MT - Montana" Value="MT"/> 
	<asp:ListItem Text="NC - North Carolina" Value="NC"/> 
	<asp:ListItem Text="ND - North Dakota" Value="ND"/> 
	<asp:ListItem Text="NE - Nebraska" Value="NE"/> 
	<asp:ListItem Text="NH - New Hampshire" Value="NH"/> 
	<asp:ListItem Text="NJ - New Jersey" Value="NJ"/> 
	<asp:ListItem Text="NM - New Mexico" Value="NM"/> 
	<asp:ListItem Text="NV - Nevada" Value="NV"/> 
	<asp:ListItem Text="NY - New York" Value="NY"/> 
	<asp:ListItem Text="OH - Ohio" Value="OH"/> 
	<asp:ListItem Text="OK - Oklahoma" Value="OK"/> 
	<asp:ListItem Text="OR - Oregon" Value="OR"/> 
	<asp:ListItem Text="PA - Pennsylvania" Value="PA"/> 
	<asp:ListItem Text="RI - Rhode Island" Value="RI"/> 
	<asp:ListItem Text="SC - South Carolina" Value="SC"/> 
	<asp:ListItem Text="SD - South Dakota" Value="SD"/> 
	<asp:ListItem Text="TN - Tennessee" Value="TN"/> 
	<asp:ListItem Text="TX - Texas" Value="TX"/> 
	<asp:ListItem Text="UT - Utah" Value="UT"/> 
	<asp:ListItem Text="VA - Virginia" Value="VA"/> 
	<asp:ListItem Text="VT - Vermont" Value="VT"/> 
	<asp:ListItem Text="WA - Washington" Value="WA"/> 
	<asp:ListItem Text="WI - Wisconsin" Value="WI"/> 
	<asp:ListItem Text="WV - West Virginia" Value="WV"/> 
	<asp:ListItem Text="WY - Wyoming" Value="WY"/> 
</asp:DropDownList>
<asp:CompareValidator ID="CompareValidatorUSStates1" ControlToValidate="StateDropDown" runat="server"  Operator="NotEqual" Type="String" ValueToCompare="0" Display="Dynamic" ErrorMessage="State is required">
<asp:Image ID="Image4" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" />
</asp:CompareValidator>
