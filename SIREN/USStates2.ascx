<%@ Control Language="C#" AutoEventWireup="true" CodeFile="USStates2.ascx.cs" Inherits="UserControls_USStates2" %>

<asp:DropDownList ID="StateDropDown" runat="server">
	<%--<asp:ListItem Text="-- Select --" Value="0" />--%> 
	<asp:ListItem Text="AL" Value="AL" /> 
	<asp:ListItem Text="AR" Value="AR"/> 
	<asp:ListItem Text="AZ" Value="AZ"/> 
	<asp:ListItem Text="CA" Value="CA"/> 
	<asp:ListItem Text="CO" Value="CO"/> 
	<asp:ListItem Text="CT" Value="CT"/> 
	<asp:ListItem Text="DC" Value="DC"/> 
	<asp:ListItem Text="DE" Value="DE"/> 
	<asp:ListItem Text="FL" Value="FL"/> 
	<asp:ListItem Text="GA" Value="GA"/> 
	<asp:ListItem Text="HI" Value="HI"/> 
	<asp:ListItem Text="IA" Value="IA"/> 
	<asp:ListItem Text="ID" Value="ID"/> 
	<asp:ListItem Text="IL" Value="IL"/> 
	<asp:ListItem Text="IN" Value="IN"/> 
	<asp:ListItem Text="KS" Value="KS"/> 
	<asp:ListItem Text="KY" Value="KY"/> 
	<asp:ListItem Text="LA" Value="LA"/> 
	<asp:ListItem Text="MA" Value="MA"/> 
	<asp:ListItem Text="MD" Value="MD"/> 
	<asp:ListItem Text="ME" Value="ME"/> 
	<asp:ListItem Text="MI" Value="MI"/> 
	<asp:ListItem Text="MN" Value="MN"/> 
	<asp:ListItem Text="MO" Value="MO"/> 
	<asp:ListItem Text="MS" Value="MS"/> 
	<asp:ListItem Text="MT" Value="MT"/> 
	<asp:ListItem Text="NC" Value="NC"/> 
	<asp:ListItem Text="ND" Value="ND"/> 
	<asp:ListItem Text="NE" Value="NE"/> 
	<asp:ListItem Text="NH" Value="NH"/> 
	<asp:ListItem Text="NJ" Value="NJ"/> 
	<asp:ListItem Text="NM" Value="NM"/> 
	<asp:ListItem Text="NV" Value="NV"/> 
	<asp:ListItem Text="NY" Value="NY"/> 
	<asp:ListItem Text="OH" Value="OH"/> 
	<asp:ListItem Text="OK" Value="OK"/> 
	<asp:ListItem Text="OR" Value="OR"/> 
	<asp:ListItem Text="PA" Value="PA"/> 
	<asp:ListItem Text="RI" Value="RI"/> 
	<asp:ListItem Text="SC" Value="SC"/> 
	<asp:ListItem Text="SD" Value="SD"/> 
	<asp:ListItem Text="TN" Value="TN"/> 
	<asp:ListItem Text="TX" Value="TX"/> 
	<asp:ListItem Text="UT" Value="UT"/> 
	<asp:ListItem Text="VA" Value="VA"/> 
	<asp:ListItem Text="VT" Value="VT"/> 
	<asp:ListItem Text="WA" Value="WA"/> 
	<asp:ListItem Text="WI" Value="WI"/> 
	<asp:ListItem Text="WV" Value="WV"/> 
	<asp:ListItem Text="WY" Value="WY"/> 
</asp:DropDownList>