<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" Theme="Main" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="logout_default"  %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>

<asp:Content ID="LeftSectionTitleContent" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
  Your Online Session Has Ended
</asp:Content>

<asp:Content ID="RightSectionTitleContent" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>

<asp:Content ID="MainBodyContent" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<div id="divUserProfile">
<cc1:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:460px;">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="h2">Sign off successful.</td>
		</tr>
		<tr>
			<td class="left">&nbsp;</td>
		</tr>
		<tr>
			<td class="left">Thank you for using Sunbelt Rentals.</td>
		</tr>
		<tr>
			<td class="left">&nbsp;</td>
		</tr>
	</table>
</cc1:RoundedCornerPanel>
<br />
<table cellpadding="0" cellspacing="0">	
	<tr>
		<td class="right2"><hr style="width:460px" /></td>
	</tr>
	<tr>
		<td class="right"><asp:HyperLink ID="hlLogin" runat="server" NavigateUrl="~/Login.aspx">Log in</asp:HyperLink>
		</td>
	</tr>
	<tr>
		<td class="right"><asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
	</tr>
</table>
</div>
</asp:Content>

