using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;

public partial class UserProfile_EditProfile : Sunbelt.Web.WebPage
{
	private string GoToUrl
	{
		set { SetViewState("GoToUrl", value); }
		get { return (string)GetViewState("GoToUrl", ""); }
	}

	protected override void OnPreInit(EventArgs e)
	{
		//Change the master page if needed.
		if (SiteUser.CheckForCashCustomer(false))
			MasterPageFile = "~/MasterPages/CashCustomer.master";

		base.OnPreInit(e);
	}
	
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			if (Request.QueryString["gotourl"] != null)
			{
				GoToUrl = Request.QueryString["gotourl"];
			}
			
			if (!string.IsNullOrEmpty(User.Identity.Name))
			{
				UPEditUser1.IsCashCustomer = SiteUser.IsCashCustomer;
				UPEditUser1.EditUserProfile(User.Identity.Name);
			}
			else
			{
				//Record hacker attempt.
				//Transfer to home page.
				Server.Transfer("~/Default.aspx", false);
			}
		}

		UPEditUser1.UserErrorRasied += new EventHandler<UserErrorEvent>(UPEditUser1_UserErrorRasied);
		UPEditUser1.UserUpdated += new EventHandler<UserEvent>(UPEditUser1_UserUpdated);
	}

	void UPEditUser1_UserUpdated(object sender, UserEvent e)
	{
		if (!string.IsNullOrEmpty(GoToUrl))
		{
			Response.Redirect(GoToUrl);
			return;
		}
	}

	void UPEditUser1_UserErrorRasied(object sender, UserErrorEvent e)
	{
		UPValidationSummary1.DisplayError(e.Message);
	}
}
