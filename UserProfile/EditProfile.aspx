<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="EditProfile.aspx.cs" Inherits="UserProfile_EditProfile" Title="Edit My Profile" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ Register Src="~/SiteControls/UserProfile/UPValidationSummary.ascx" TagName="UPValidationSummary" TagPrefix="uc2" %>
<%@ Register Src="~/SiteControls/UserProfile/UPEditUser.ascx" TagName="UPEditUser" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
	Edit My Profile
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<uc2:UPValidationSummary ID="UPValidationSummary1" runat="server" />
<%--<cc1:RoundedCornerPanel ID="RoundedCornerPanel1" runat="server" SkinID="rcpGreyNoTitle">--%>
	<div id="divUserProfile">
	<%----CONTENT START HERE----%>
		<uc1:UPEditUser ID="UPEditUser1" runat="server" />
	<%----CONTENT END HERE----%>
	</div>
<%--</cc1:RoundedCornerPanel>--%>
</asp:Content>

