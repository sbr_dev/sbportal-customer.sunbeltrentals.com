﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Error_ErrorQuickLinks : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lnkGoToScheduledNotifications_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Error/ScheduledNotifications.aspx");
    }
    protected void lnkGoToErrors_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Error/Default.aspx");
    }
    protected void lnkGoToActivity_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Error/AccountActivity.aspx");
    }
}
