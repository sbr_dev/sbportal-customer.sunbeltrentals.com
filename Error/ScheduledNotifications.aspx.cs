﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Common.DAL;
using System.Data.SqlClient;

public partial class Error_ScheduledNotifications : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((SBNet.SBNetSqlMembershipProvider.CurrentUser == null) || (SBNet.SBNetSqlMembershipProvider.CurrentUser.ebClock <= 0))
        {
            ddlNotificationTypes.Visible = false;
            lblNoAccess.Visible = true;
            GroupingControl1.Visible = false;
        }
        else
        {
            if (!IsPostBack)
            {
                GroupingControl1.Visible = true;
                lblNoAccess.Visible = false;
                ddlNotificationTypes.Visible = true;

                GroupingControl1.dtData = GetAlertsScheduled();
                GroupingControl1.BindData();
            }
        }
    }

    protected void ddlNotificationTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlNotificationTypes.SelectedValue == "Alerts")
        {
            GroupingControl1.ClearViewState();
            GroupingControl1.GroupedByColumnName = "";
            GroupingControl1.dtData = GetAlertsScheduled();
            GroupingControl1.BindData();
        }
        else if (ddlNotificationTypes.SelectedValue == "Reports")
        {
            GroupingControl1.ClearViewState();
            GroupingControl1.GroupedByColumnName = "";
            GroupingControl1.dtData = GetReportsScheduled();
            GroupingControl1.BindData();
        }
    }

    protected static DataTable GetAlertsScheduled()
    {
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
		using (SPData sp = new SPData("Alerts.sb_MonitorScheduling", conn))
        {
            return sp.ExecuteDataTable();
        }
    }
    protected static DataTable GetReportsScheduled()
    {
		using (SqlConnection conn = ConfigManager.GetNewSqlConnection(ConfigManager.ConnectionType.BaseData))
		using (SPData sp = new SPData("ReportSpec.sb_MonitorScheduling", conn))
        {
            return sp.ExecuteDataTable();
        }
    }
}
