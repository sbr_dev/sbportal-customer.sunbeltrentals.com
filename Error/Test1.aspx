<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test1.aspx.cs" Inherits="Error_Test1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Error Test1</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<h3>Error Test</h3>
		<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button1" ToolTip="Throw and catch" />
		&nbsp;SqlException - Throw and catch<br />
		<asp:Button ID="Button2" runat="server" Text="Button2" OnClick="Button2_Click" ToolTip="Throw and catch" />
		&nbsp;System.Exception - Throw and catch<br />
		<asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Button3" ToolTip="Throw and don't catch" />
		&nbsp;System.FormatException - Throw and don't catch<br />
		<asp:Button ID="Button4" runat="server" Text="Button4" onclick="Button4_Click" ToolTip="Throw 404 aspx page" />
		&nbsp;404 Redirect<br />
		<h3>Log Test</h3>
		<asp:Button ID="Button5" runat="server" Text="Button5" ToolTip="Throw 404 non aspx page" onclick="Button5_Click" />
		&nbsp;Sunbelt.Log.LogActivity(...)<br />
	</div>
    </form>
</body>
</html>
