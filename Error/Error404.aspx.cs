﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Sunbelt.Tools;
using SBNet;

public partial class Error_Error404 : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		lbExceptionMsg.Text = "";

		try
		{
			Error.ExceptionInfo ei = null;

			try { ei = (Error.ExceptionInfo)Session["_EXCEPTION_"]; }
			catch { }
			if (ei == null)
				ei = (Error.ExceptionInfo)Application["_EXCEPTION_"];
			if (ei == null)
			{
				ei = new SBNet.Error.ExceptionInfo(Server.GetLastError(), 0);
			}

			lbExceptionMsg.Text = "<br />The follow error has occurred.<br /><br />";
			if (ei.ExecptionObject != null)
			{
				Exception ex = ei.ExecptionObject.GetBaseException();

				lbErrorID.Text = ei.ReferenceNumber.ToString();
				if (ex is SqlException)
				{
					lbExceptionMsg.Text += WebTools.HtmlEncode(BuildExceptionText((SqlException)ex), false, true);
				}
				else //All others.
				{
					lbExceptionMsg.Text += WebTools.HtmlEncode(BuildExceptionText(ex), false, true);
				}
			}
			else
			{
				Exception ex = new Exception("'" + HttpUtility.UrlDecode(ClientQueryString) + "' not found.");
//Don't log 404's.
#if false
				int rc = 0;
				try
				{
					rc = SBNet.Error.LogErrToSQL(ex, "");
				}
				catch { }
				lbErrorID.Text = rc.ToString();
#else
				lbErrorID.Text = "NO PAGE";
#endif
				lbExceptionMsg.Text += WebTools.HtmlEncode(BuildExceptionText(ex), false, true);
			}

			//#if DEBUG
			//            divExceptionInfo.Visible = true;
			//#else
			//            divExceptionInfo.Visible = false;
			//#endif
			divExceptionInfo.Visible = ConvertTools.ToBoolean(ConfigurationManager.AppSettings["ShowStackTrace"].ToString());
		}
		catch (Exception /*ex*/)
		{
			//Do what???
		}
		finally
		{
			//Clear session data...
			try { Session["_EXCEPTION_"] = null; }
			catch { }
			Application["_EXCEPTION_"] = null;
		}

		//btnBack.Attributes.Add("onclick", "javascript:window.history.back(0);");
	}

	private string BuildExceptionText(SqlException ex)
	{
		StringBuilder sb = new StringBuilder();

		sb.Append("-------- SQL Exception " + DateTime.Now.ToString() + " --------\r\n");
		sb.Append(GetUserNames());
		if (ex.Errors.Count > 0)
		{
			foreach (SqlError sqle in ex.Errors)
			{
				sb.Append("-- Error --\r\n");
				sb.Append("  Message: " + sqle.Message + "\r\n");
				sb.Append("  Number: " + sqle.Number + "\r\n");
				sb.Append("  Procedure: " + sqle.Procedure + "\r\n");
				sb.Append("  Server: " + sqle.Server + "\r\n");
				sb.Append("  Source: " + sqle.Source + "\r\n");
				sb.Append("  State: " + sqle.State + "\r\n");
				sb.Append("  Severity: " + sqle.Class + "\r\n");
				sb.Append("  LineNumber: " + sqle.LineNumber + "\r\n");
			}
		}
		else
		{
			sb.Append("-- Error --\r\n");
			sb.Append("Message: " + ex.Message + "\r\n");
			sb.Append("Number: " + ex.Number + "\r\n");
		}
		sb.Append("-----------\r\n");
		sb.Append(GetStackTrace(ex) + "\r\n");

		return sb.ToString();
	}

	private string BuildExceptionText(Exception ex)
	{
		StringBuilder sb = new StringBuilder();

		sb.Append("-------- Exception " + DateTime.Now.ToString() + " --------\r\n");
		sb.Append(GetUserNames());
		sb.Append("-- Error --\r\n");
		sb.Append("Type: " + ex.GetType().ToString() + "\r\n");
		sb.Append("Message: " + ex.Message + "\r\n");
		sb.Append("Source: " + ex.Source + "\r\n");

		sb.Append("-----------\r\n");
		sb.Append(GetStackTrace(ex) + "\r\n");

		return sb.ToString();
	}

	private string GetUserNames()
	{
		StringBuilder sb = new StringBuilder();

		try
		{
			string userName = User.Identity.Name;
			sb.Append("Web User: " + userName + "\r\n");
		}
		catch { sb.Append("Web User: NOT VALID\r\n"); }

		sb.Append("HttpContext User: " + User.Identity.Name + "\r\n");
		sb.Append("Windows Identity: " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + "\r\n");
		return sb.ToString();
	}

	private string GetStackTrace(Exception ex)
	{
		StringBuilder sb = new StringBuilder();

		sb.Append("Stack Trace:\r\n");
		if (ex.StackTrace != null)
			sb.Append(ex.StackTrace.ToString());
		else
			sb.Append("  No stack trace available.");
		return sb.ToString();
	}
}
