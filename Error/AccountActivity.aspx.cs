﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Sunbelt.Tools;

public partial class Error_AccountActivity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (string s in Enum.GetNames(typeof(SBNet.Log.Sections)))
            {
                dlSections.Items.Add(new ListItem(s, s));
            }
            dlSections.Items.Insert(0, new ListItem("--Select--"));

            if (SBNet.SBNetSqlMembershipProvider.CurrentUser == null)
            {
                lvErrors.Visible = false;
                pnlSummary.Visible = false;
                pnlShowHide.Visible = false;
                lblNoAccess.Visible = true;
            }
            else if (SBNet.SBNetSqlMembershipProvider.CurrentUser.ebClock <= 0)
            {
                lvErrors.Visible = false;
                pnlSummary.Visible = false;
                pnlShowHide.Visible = false;
                lblNoAccess.Visible = true;
            }
        }
    }

    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
        DataTable dtActivity = SBNet.Log.GetAllApplicationActivity();
        lvErrors.DataSource = dtActivity;
        lvErrors.DataBind();
    }
    protected void btnByLogId_Click(object sender, EventArgs e)
    {
        DataTable dtActivity = SBNet.Log.GetApplicationActivityById(ConvertTools.ToInt32(txtLogId.Text, 0));
        lvErrors.DataSource = dtActivity;
        lvErrors.DataBind();
    }
    protected void dlSections_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtActivity = SBNet.Log.GetApplicationActivityBySection(dlSections.SelectedValue);
        lvErrors.DataSource = dtActivity;
        lvErrors.DataBind();
    }
}
