﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="ScheduledNotifications.aspx.cs" Inherits="Error_ScheduledNotifications" Title="Notifications" Theme="Main" EnableEventValidation="false" %>

<%@ Register src="ErrorQuickLinks.ascx" tagname="ErrorQuickLinks" tagprefix="uc1" %>

<%@ Register src="../SiteControls/GroupingTable/GroupingControl.ascx" tagname="GroupingControl" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainBodyContent" Runat="Server">
    <uc1:ErrorQuickLinks ID="ErrorQuickLinks1" runat="server" />
    <br />
    <asp:DropDownList ID="ddlNotificationTypes" runat="server" AutoPostBack="true" 
        onselectedindexchanged="ddlNotificationTypes_SelectedIndexChanged">
        <asp:ListItem Text="Alerts" Value="Alerts" />
        <asp:ListItem Text="Reports" Value="Reports" />
    </asp:DropDownList>
    <br /><br />
    <uc2:GroupingControl ID="GroupingControl1" runat="server" TableClass="dgMain" HeaderRowClass="dgHeaderGreen" AllowColumnSorting="true"
        HeaderRowClassWithSorting="dgHeaderGreenWithSorting" ImageSortingDown="~/Images/sort_asc_white.gif" ImageSortingUp="~/Images/sort_desc_white.gif"
        ImageSortingNone="~/Images/sort_none.gif" Width="900" />
    <br /><br />
    <asp:Label ID="lblNoAccess" runat="server" Text="You currently do not have access to this page!" Visible="false" Font-Size="Large" Font-Bold="true" />
</asp:Content>

