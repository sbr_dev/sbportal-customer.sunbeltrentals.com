﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sunbelt.Tools;

public partial class Error_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foreach (string s in Enum.GetNames(typeof(SBNet.Error.Sections)))
            {
                dlSections.Items.Add(new ListItem(s, s));
            }
            dlSections.Items.Insert(0, new ListItem("--Select--"));

            if ((SBNet.SBNetSqlMembershipProvider.CurrentUser == null) ||
				(SBNet.SBNetSqlMembershipProvider.CurrentUser.ebClock <= 0))
            {
				divHasAccess.Visible = false;
				lvErrors.Visible = false;
                pnlSummary.Visible = false;
                pnlShowHide.Visible = false;
				
				divNoAccess.Visible = true;
				lblNoAccess.Visible = true;
            }
        }
    }

    protected void lvErrors_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        //string s = e.Item.UniqueID.ToString();
        ListView lvItems = (ListView)e.Item.FindControl("lvItems");
        lvItems.Visible = false;

        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("row");
        tr.Visible = false;
    }
    protected void lvErrors_LayoutCreated(object sender, EventArgs e)
    {
        
    }
    protected void btnByLogId_Click(object sender, EventArgs e)
    {
        DataTable dtErrors = Sunbelt.Log.GetApplicationErrorById(ConvertTools.ToInt32(txtLogId.Text, 0));
        lvErrors.DataSource = dtErrors;
        lvErrors.DataBind();
    }
    protected void btnByUserId_Click(object sender, EventArgs e)
    {
		DataTable dtErrors = Sunbelt.Log.GetApplicationErrorByEmail(txtUserId.Text);
        lvErrors.DataSource = dtErrors;
        lvErrors.DataBind();
    }
    protected void dlSections_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dlSections.SelectedIndex > 0)
        {
			DataTable dtErrors = Sunbelt.Log.GetApplicationErrorBySection(dlSections.SelectedValue);
            lvErrors.DataSource = dtErrors;
            lvErrors.DataBind();
        }
    }
    protected void btnSelectAll_Click(object sender, EventArgs e)
    {
		DataTable dtErrors = Sunbelt.Log.GetAllApplicationErrors();
        lvErrors.DataSource = dtErrors;
        lvErrors.DataBind();
    }
}