﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/ErrorPage.master" AutoEventWireup="true" CodeFile="Error404.aspx.cs" Inherits="Error_Error404" Title="Error" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/MasterPages/ErrorPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
	<style type="text/css">
		.T1 { color: red; font-size: 12pt; font-weight: bold; }
		.T2 { font-size: 10pt; }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
	Page Not Found 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainBodyContent" Runat="Server">
	<p>We're sorry but the page you requested does not exist.</p>
	<p>If you believe this to be a problem we should review, you may call our support line at <b>866-786-2358 (866-SUNBELT)</b>.</p>
	<p>Please refer to the incidence id:&nbsp;<strong class="MedRedText"><asp:Label ID="lbErrorID" runat="server" Text="[lbErrorID]"></asp:Label></strong></p>
	
	<cc1:RoundedCornerPanel ID="divExceptionInfo" runat="server" SkinID="rcpGreyNoTitle">
		<asp:Label ID="lbExceptionMsg" runat="server" CssClass="T2" Text="Error message here."></asp:Label>
	</cc1:RoundedCornerPanel>
</asp:Content>

