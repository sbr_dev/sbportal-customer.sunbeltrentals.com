﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Error_Default" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<%@ Register src="ErrorQuickLinks.ascx" tagname="ErrorQuickLinks" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Styles" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<script type="text/javascript" language="javascript">
//<![CDATA[

function FrequencyChanged(type)
{
    var rbLogId = document.getElementById('<%=rbByLogId.ClientID %>');
    var txtLogId = document.getElementById('<%=txtLogId.ClientID %>');
    var rbUserId = document.getElementById('<%=rbUserId.ClientID %>');
    var txtUserId = document.getElementById('<%=txtUserId.ClientID %>');
    var rbSelectAll = document.getElementById('<%=rbSelectAll.ClientID %>');
    var rbSection = document.getElementById('<%=rbSection.ClientID %>');
    var dlSections = document.getElementById('<%=dlSections.ClientID %>');

    if (type == 'log')
    {
        rbUserId.checked = false;
        txtUserId.value = "";
        txtUserId.disabled = true;
        
        txtLogId.disabled = false;
        
        rbSection.checked = false;
        dlSections.disabled = true;
        dlSections.selectedIndex = 0;
        
        rbSelectAll.checked = false;
    }
    else if (type == 'user')
    {
        rbLogId.checked = false;
        txtLogId.value = "";
        txtLogId.disabled = true;
        
        txtUserId.disabled = false;
        
        rbSection.checked = false;
        dlSections.disabled = true;
        dlSections.selectedIndex = 0;
        
        rbSelectAll.checked = false;
    }
    else if (type == 'section')
    {
        rbUserId.checked = false;
        txtUserId.value = "";
        txtUserId.disabled = true;
        
        rbLogId.checked = false;
        txtLogId.value = "";
        txtLogId.disabled = true;
        
        dlSections.disabled = false;
                
        rbSelectAll.checked = false;
    }
    else
    {
        rbUserId.checked = false;
        txtUserId.value = "";
        txtUserId.disabled = true;
        
        rbLogId.checked = false;
        txtLogId.value = "";
        txtLogId.disabled = true;
        
        rbSection.checked = false;
        dlSections.disabled = true;
    }
}

//]]>
</script>
<div id="divHasAccess" runat="server">
<uc1:ErrorQuickLinks ID="ErrorQuickLinks1" runat="server" />
    <br />
<asp:Panel ID="pnlShowHide" runat="server" BackColor="#ededed">
    <table id="Header" style="width: 100%; cursor: pointer;" cellpadding="0" cellspacing="0">
        <tr>
            <td>&nbsp;<asp:Label ID="lblShowHide" runat="server" Text="Hide"  style="width: 7px;"></asp:Label></td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlSummary" runat="server">
    <table>
        <tr>
            <td colspan="2"><asp:RadioButton ID="rbSelectAll" runat="server" Text="Select All" onclick="javascript:FrequencyChanged('All');" /></td>
            <td><asp:Button ID="btnSelectAll" runat="server" Text="Go" onclick="btnSelectAll_Click" /></td>
        </tr>
        <tr>
            <td><asp:RadioButton ID="rbByLogId" runat="server" Text="Select By LogId" onclick="javascript:FrequencyChanged('log');" /></td>
            <td><asp:TextBox ID="txtLogId" runat="server" /></td>
            <td><asp:Button ID="btnByLogId" runat="server" Text="Go" onclick="btnByLogId_Click" /></td>
        </tr>
        <tr>
            <td><asp:RadioButton ID="rbUserId" runat="server" Text="Select By Email" onclick="javascript:FrequencyChanged('user');" /></td>
            <td><asp:TextBox ID="txtUserId" runat="server" /></td>
            <td><asp:Button ID="btnByUserId" runat="server" Text="Go" onclick="btnByUserId_Click" /></td>
        </tr>
        <tr>
            <td><asp:RadioButton ID="rbSection" runat="server" Text="Select By Section" onclick="javascript:FrequencyChanged('section');" /></td>
            <td colspan="2"><asp:DropDownList ID="dlSections" runat="server" AutoPostBack="True" onselectedindexchanged="dlSections_SelectedIndexChanged" /></td>
        </tr>
    </table>
</asp:Panel>
<Ajax:CollapsiblePanelExtender ID="cpeSummary" runat="Server"
        TargetControlID="pnlSummary"
        ExpandControlID="pnlShowHide"
        CollapseControlID="pnlShowHide" 
        TextLabelID="lblShowHide"
        CollapsedText="Show"
        SuppressPostBack="true" 
        ExpandedText="Hide"
        Collapsed="False">
</Ajax:CollapsiblePanelExtender>
<asp:ListView ID="lvErrors" runat="server" OnItemDataBound="lvErrors_OnItemDataBound">
    <LayoutTemplate>
        <table id="tbErrors" cellpadding="0" cellspacing="0" class="lvErrors" width="800px">
            <tr id="header" runat="server" visible="false">
                <th>&nbsp;</th>
                <th>LogID</th>
                <th>Last Error</th>
                <th>Application</th>
                <th>Section</th>
                <th>Host</th>
                <th class="PageUrl">PageUrl</th>
                <th>UserID</th>
                <th>Account #</th>
                <th>IsCorp</th>
                <th>Error #</th>
                <th>Error Message</th>
                <th>Stack Trace</th>
                <th>Routine Name</th>
                <th>SQL</th>
                <th>Comments</th>
            </tr>
            <tr id="itemPlaceholder" runat="server" />
            <tr id="NoGrouping" runat="server" />
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr id="row" runat="server">
            <th><img src="../Images/plus.png" /></th>
            <th colspan="14">&nbsp;</th>
        </tr>
        <tr id="NoGrouping" runat="server">
            <td>
                <div id="divTop" runat="server" class="divErrors" style="font-weight: 700; color: White; width: 100%;">
                    &nbsp;&nbsp;Log ID:&nbsp;<%# Eval("LogID")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Error Date:&nbsp;<asp:Label ID="lblErrorDate" runat="server" Text='<%# Eval("ErrDate")%>' ToolTip='<%# Eval("Last Logged Error")%>' />
                </div>
                <asp:Panel ID="pnlError" runat="server" style="border: solid 1px gray;" Width="100%">
                    <table>
                        <tr><td colspan="4"></td></tr>
                        <tr>
                            <td colspan="4"><span style="font-weight: 700;">General Error Info:</span><br />
                            App:&nbsp;<%# Eval("App")%>&nbsp;&nbsp;&nbsp;
                            Section:&nbsp;<%# Eval("Section")%>&nbsp;&nbsp;&nbsp;
                            Host:&nbsp;<%# Eval("Host")%>&nbsp;&nbsp;&nbsp;<br />
                            Page Url:&nbsp;<%# Eval("PageUrl")%><br />
                            Referrer Url:&nbsp;<%# Eval("ReferrerURL")%>&nbsp;
                            </td>
                        </tr>
                        <tr><td colspan="4">&nbsp;</td></tr>
                        <tr>
                            <td colspan="4"><span style="font-weight: 700;">Customer Information:</span><br />
                                Customer Name:&nbsp;<%# Eval("Email")%>&nbsp;&nbsp;&nbsp;
                                Account #:&nbsp;<%# Eval("AccountNumber")%>&nbsp;&nbsp;&nbsp;
                                Corp Link:&nbsp;<%# Eval("IsCorpLinkAccount")%>
                            </td>
                        </tr>
                        <tr><td colspan="4">&nbsp;</td></tr>
                        <tr>
                            <td colspan="4"><span style="font-weight: 700;">Error Message:</span><br />
                                <%# Eval("ErrMessage")%><br /><br /><span style="font-weight: 700;">Stack Trace:</span><br /><%# Sunbelt.Tools.WebTools.HtmlEncode(Eval("StackTrace").ToString())%>
                                <br /><br /><span style="font-weight: 700;">Routine Name:&nbsp;</span><%# Eval("RoutineName")%>
                                <br /><br /><span style="font-weight: 700;">SQL:&nbsp;</span><%# Eval("SQL")%>
                                <br /><br /><span style="font-weight: 700;">Comments:&nbsp;</span><%# Eval("Comments")%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <asp:ListView ID="lvItems" runat="server">
            <LayoutTemplate>
                <tr runat="server" id="itemPlaceholder" />
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("LogID")%></td>
                    <td><%# Eval("Last Logged Error")%></td>
                    <td><%# Eval("App")%></td>
                    <td><%# Eval("Section")%></td>
                    <td><%# Eval("Host")%></td>
                    <td class="PageUrl"><%# Eval("PageUrl")%></td>
                    <td><%# Eval("UserID")%></td>
                    <td><%# Eval("AccountNumber")%></td>
                    <td><%# Eval("IsCorpLinkAccount")%></td>
                    <td><%# Eval("ErrNumber")%></td>
                    <td><%# Eval("ErrDate")%></td>
                    <td><%# Eval("ErrMessage")%></td>
                    <td><%# Eval("StackTrace")%></td>
                    <td><%# Eval("RoutineName")%></td>
                    <td><%# Eval("SQL")%></td>
                    <td><%# Eval("Comments")%></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </ItemTemplate>
</asp:ListView>
</div>
<div id="divNoAccess" runat="server" visible="false">
<br /><br /><asp:Label ID="lblNoAccess" runat="server" Text="You currently do not have access to this page!" Visible="false" Font-Size="Large" Font-Bold="true" />
</div>
</asp:Content>

