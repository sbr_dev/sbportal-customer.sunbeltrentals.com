using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sunbelt;

public partial class Error_Test1 : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}
	protected void Button1_Click(object sender, EventArgs e)
	{
		try
		{
			SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ApplicationLog"].ConnectionString);
			using (conn)
			{
				conn.Open();
				SqlCommand cmd = new SqlCommand("SP_NONE", conn);
				cmd.CommandType = CommandType.StoredProcedure;
				int rc = cmd.ExecuteNonQuery();
			}
		}
		catch(Exception ex)
		{
			SBNet.Error.HandleError(ex, true);
		}
	}
	protected void Button2_Click(object sender, EventArgs e)
	{
		try
		{
			throw new Exception("This is a test exception handled by SBNet.Error.HandleError(...).");
		}
		catch (Exception ex)
		{
			SBNet.Error.HandleError(ex, true);
		}
	}
	protected void Button3_Click(object sender, EventArgs e)
	{
		string s = "Not a number.";
		int n = Convert.ToInt32(s);
	}
	protected void Button4_Click(object sender, EventArgs e)
	{
		Response.Redirect("~/NoPageAtAll.aspx");
	}
	protected void Button5_Click(object sender, EventArgs e)
	{
		Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.WebPage.ToString(), "Activity log test.");
	}
}
