<%@ Page Language="C#" MasterPageFile="~/MasterPages/Main.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" Title="Log In: Sunbelt Rentals" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<%@ Register Src="SiteControls/UserProfile/UPResetPassword.ascx" TagName="UPResetPassword" TagPrefix="uc3" %>
<%@ Register Src="SiteControls/UserProfile/UPLogin.ascx" TagName="UPLogin" TagPrefix="uc2" %>
<%@ Register Src="SiteControls/UserProfile/UPValidationSummary.ascx" TagName="UPValidationSummary" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/MasterPages/Main.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftSectionTitleContent" Runat="Server">
	Customer Login
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightSectionTitleContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainBodyContent" Runat="Server">

<uc1:UPValidationSummary ID="UPValidationSummary1" runat="server" />

	<div id="divUserProfile">
	<%----CONTENT START HERE----%>
		<div id="divLogin" runat="server">
			<uc2:UPLogin ID="UPLogin1" runat="server" />
		</div>
		<div id="divResetPassword" runat="server">
			<uc3:UPResetPassword ID="UPResetPassword1" runat="server" />
		</div>
		<div id="divError" runat="server">
			Error?
		</div>
	<%----CONTENT END HERE----%>
	</div>

<script type="text/javascript">
//<![CDATA[

if(typeof(BI_WatchAnchors) == "function")
	BI_WatchAnchors(document.getElementById("divUserProfile"));

//]]>
</script>
</asp:Content>

