﻿//<SCRIPT>
//// SlidingDIV.js ////
//
// ____________________________________________________________________________
//

if(typeof(SBNet) == "undefined")
{
	SBNet={};
}

SBNet.SlidingDiv = {

InitDiv:function(oDiv, bIsVertical, nAnimateSeed_ms, oSizeMeTo, nPadding)
{
	//alert("InitDiv:function(oDiv)");
	//Init the DIV.
	oDiv.moving = false;
	oDiv.timerID = null;
	oDiv.startTime = null;
	oDiv.endWidth = parseInt(oDiv.style.width);
	oDiv.endHeight = parseInt(oDiv.style.height);
	oDiv.isVertical = bIsVertical;
	if(bIsVertical)
		oDiv.direction = (oDiv.style.display == "none") ? "in" : "out";
	else
		oDiv.direction = (oDiv.style.display == "none") ? "up" : "down";
	oDiv.SlideOut = this.SlideOut;
	oDiv.SlideIn = this.SlideIn;
	oDiv.SlideDown = this.SlideOut;
	oDiv.SlideUp = this.SlideIn;
	oDiv.Slide = this.Slide;
	oDiv.animateSpeed = nAnimateSeed_ms;
	oDiv.SizeMeTo = oSizeMeTo;
	oDiv.SizePadding = nPadding;
},

Slide:function()
{
	//alert("Slide:function()");
	if(this.style.display == "none")
		this.SlideOut();
	else
		this.SlideIn();
},

SlideOut:function()
{
	//alert("SlideOut:function()");
	//The 'this' object is the DIV.
	if(this.moving)
		return;
	//Can't side out if already there.
	if(this.style.display == "block")
		return;
	//Start the move.
	this.moving = true;
	if(this.isVertical)
		this.direction = "down";
	else
		this.direction = "out";
	SBNet.SlidingDiv.StartSlide(this);
},

SlideIn:function()
{
	//alert("SlideOut:function()");
	//The 'this' object is the DIV.
	if(this.moving)
		return;
	//Can't side in if already there.
	if(this.style.display == "none")
		return;
	//Start the move.
	this.moving = true;
	if(this.isVertical)
		this.direction = "up";
	else
		this.direction = "in";
	SBNet.SlidingDiv.StartSlide(this);
},

StartSlide:function(oDiv)
{
	//alert("StartSlide:function(oDiv)");
	//Remember the start time.
	oDiv.startTime = (new Date()).getTime();
	//If sliding out or down, start at 1px.
	if(oDiv.direction == "out")
		oDiv.style.width = "1px";
	else
	if(oDiv.direction == "down")
		oDiv.style.height = "1px";
	//Show the DIV
	oDiv.style.display = "block";
	//Raise the z-index.
	oDiv.style.zIndex = "1000";

	//Don't allow overflow when we are sliding.
	// Save the current setting.
	oDiv._overflow = oDiv.style.overflow;
	oDiv.style.overflow = "hidden";

	if(oDiv.SizeMeTo && oDiv.SizePadding)
	{
		//alert(oDiv.SizeMeTo);
		//alert(oDiv.SizePadding);
		if(oDiv.direction == "out")
			oDiv.endWidth = oDiv.SizeMeTo.clientWidth + oDiv.SizePadding;
		else
		if(oDiv.direction == "down")
			oDiv.endHeight = oDiv.SizeMeTo.clientHeight + oDiv.SizePadding;
	}

	//Since we are using a interval timer to do the animation,
	// we need a way to remember our DIV. Using a static list
	// was the only way I could think of...
	SBNet.SlidingDiv.divList[oDiv.id] = oDiv;
	//Start the animation timer.
	oDiv.timerID = setInterval('SBNet.SlidingDiv.SlideTick(\'' + oDiv.id + '\');', SBNet.SlidingDiv.timerLength);
},

SlideTick:function(divId)
{
	//alert("SlideTick:function(divId)");
	//Get the DIV from the list.
	var oDiv = SBNet.SlidingDiv.divList[divId];
	//Calculate the elapsed time.
	var elapsed = (new Date()).getTime() - oDiv.startTime;
	//If we have reached animateSpeed, stop.
	if (elapsed > oDiv.animateSpeed)
		SBNet.SlidingDiv.EndSlide(oDiv);
	else
	{
		//Animate...
		if(oDiv.isVertical)
		{
			var d = Math.round(elapsed / oDiv.animateSpeed * oDiv.endHeight);
			if(oDiv.direction == "up")
				d = oDiv.endHeight - d;
			//Set DIV width
			oDiv.style.height = d + "px";
		}
		else
		{
			var d = Math.round(elapsed / oDiv.animateSpeed * oDiv.endWidth);
			if(oDiv.direction == "in")
				d = oDiv.endWidth - d;
			//Set DIV width
			oDiv.style.width = d + "px";
		}
	}
},

EndSlide:function(oDiv)
{
	//Stop timer.
	clearInterval(oDiv.timerID);
	oDiv.timerID = 0;
	oDiv.moving = false;
	//Hide DIV if sliding in.
	if(oDiv.direction == "in" || oDiv.direction == "out")
	{
		if(oDiv.direction == "in")
			oDiv.style.display = "none";
//		else
//			oDiv.style.overflow = "visible";
		//Make sure we made it.
		oDiv.style.width = oDiv.endWidth + "px";
	}
	else
	if(oDiv.direction == "up" || oDiv.direction == "down")
	{
		if(oDiv.direction == "up")
			oDiv.style.display = "none";
//		else
//			oDiv.style.overflow = "visible";
		//Make sure we made it.
		oDiv.style.height = oDiv.endHeight + "px";
	}

	//Reset the DIVs overflow.
	// Save the current setting.
	oDiv.style.overflow = oDiv._overflow;

	//Remove DIV from static list.
	delete(SBNet.SlidingDiv.divList[oDiv.id]);
	
	//alert(oDiv.endWidth);
}

}
//Declare properties.
SBNet.SlidingDiv.timerLength = 5; 
SBNet.SlidingDiv.divList = new Array(); 

//</SCRIPT>

