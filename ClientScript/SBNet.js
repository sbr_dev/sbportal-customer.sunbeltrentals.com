//<SCRIPT>
//// SBNet.js ////
//
// ____________________________________________________________________________
//

if(typeof(SBNet) == "undefined")
{
	SBNet={};
}

//Global account info.
SBNet.Account = {

//properties
Number:0,
IsCorpLink:false

};


SBNet.Util = {

//// properties. ///
//Hide div list.
divList:new Array(),
//Root web address.
webAddress:"",
webRoot:"",
//DisableUI div.
divHider:null,

//// functions. ///

////////////////////////////////////////////////////////////////////////////
//This function will attach 'eventFunction' to 'eventName' of object 'control'.
// 'control' can optionally be the control ID.
AttachEvent:function(control, eventName, eventFunction)
{
	var oControl;
	
	if(control == null)
		return;
	
	if(typeof(control) == "string")
		oControl = document.getElementById(control);
	else
		oControl = control;

	//alert(typeof(oControl.attachEvent));
	if(typeof(oControl.attachEvent) != "undefined")
		oControl.attachEvent(eventName, eventFunction);
	else
	{
		//If non Microsoft, strip "on" from eventName.
		eventName = eventName.substr(2);
		oControl.addEventListener(eventName, eventFunction, false);
	}
},

////////////////////////////////////////////////////////////////////////////
//

AddToHideOnDocumentClickList:function(oDiv)
{
	//alert("SBNet.Util.AddToHideOnDocumentClickList() - oDiv.id = " + oDiv.id);
	SBNet.Util.divList[oDiv.id] = oDiv;
//	alert(SBNet.Util.divList[oDiv.id]);
},

HideDivList:function()
{
	//alert("SBNet.Util.HideDivList()");
	for (n in SBNet.Util.divList)
	{
		if(typeof(SBNet.Util.divList[n]) == "object")
			SBNet.Util.divList[n].style.display = "none";
	}
	for (n in SBNet.Util.divList)
	{
		if(typeof(SBNet.Util.divList[n]) != "object")
			delete(SBNet.Util.divList[n]);	
	}
	return true;
},

//For OnClientClick with no postback 
HideDivListf:function()
{
	SBNet.Util.HideDivList();
	return false;
},

HideMyDiv:function(oDiv)
{
	if(typeof(oDiv) == "object")
		oDiv.style.display = "none";
},

RemoveFromHideOnDocumentClickList:function(oDiv)
{
	//alert("SBNet.Util.RemoveFromHideOnDocumentClickList()");
	delete(SBNet.Util.divList[oDiv.id]);	
},

DisableUI:function(bAddToHideOnDocumentClickList, nMyZIndex)
{
	var isIE6 = SBNet.Util.isIE6();
	//alert(isIE6);

	if(SBNet.Util.divHider == null)
	{
		if(isIE6)
		{
			SBNet.Util.divHider = document.createElement("iframe");
			SBNet.Util.divHider.src = SBNet.Util.webAddress + "ClientScript/Blank.htm";
			SBNet.Util.divHider.border = 0;
		}
		else
			SBNet.Util.divHider = document.createElement("div");
		document.body.appendChild(SBNet.Util.divHider);
	}	

	if(isIE6)
	{
		SBNet.Util.divHider.bAddToHideOnDocumentClickList = bAddToHideOnDocumentClickList;
		bAddToHideOnDocumentClickList = false;
	}
	else
	{
		SBNet.Util.RemoveFromHideOnDocumentClickList(SBNet.Util.divHider);
		if(bAddToHideOnDocumentClickList)
			SBNet.Util.AddToHideOnDocumentClickList(SBNet.Util.divHider);
	}
	
    SBNet.Util.divHider.style.display = "";
    SBNet.Util.divHider.style.position = "absolute";
    SBNet.Util.divHider.style.left = "0px";
    SBNet.Util.divHider.style.top = "0px";
    
    var size = SBNet.Util.GetClientSize();
    SBNet.Util.divHider.style.width = size[0] + "px";
    SBNet.Util.divHider.style.height = size[1] + "px";

	var z = parseInt(nMyZIndex);
    if(isNaN(z))
		SBNet.Util.divHider.style.zIndex = 10000;
	else
		SBNet.Util.divHider.style.zIndex = z - 1;
    
    SBNet.Util.divHider.style.backgroundColor = "#a0a0a0";
    SBNet.Util.divHider.style.filter = (bAddToHideOnDocumentClickList) ? "alpha(opacity=0)" : "alpha(opacity=45)";
    SBNet.Util.divHider.style.opacity = (bAddToHideOnDocumentClickList) ? "0.0" : "0.45";
},

EnableUI:function()
{
    if(SBNet.Util.divHider != null)
    {
		SBNet.Util.divHider.style.display = "none";
		if(!SBNet.Util.isIE6())
		{
			SBNet.Util.RemoveFromHideOnDocumentClickList(SBNet.Util.divHider);
		}
		else
		{
			//This code will delete the IFRAME. But it is not needed for now. SC 1-16-08
			//document.body.removeChild(SBNet.Util.divHider);
			//delete SBNet.Util.divHider;
			//SBNet.Util.divHider = null;
		}
    }
},

isIE:function()
{
    //Match msie
    return!!navigator.userAgent.match(/msie/i);
},

isIE6:function()
{
	//Match "msie 6."
	return!!navigator.userAgent.match(/msie 6\./i);
},

//Get the version of the IE browser. Returns zero if non-IE.
getIEVersion:function()
{
	//Match "msie x.xx"
	var myregexp = /(msie)\s(\d*.\d*)/i;
	var match = myregexp.exec(navigator.userAgent);
	if (match != null && match.length > 1)
	{
		return parseFloat(match[2]);
	}
	else
		return 0;
},

isSafari:function()
{
	//Match "Safari"
	return!!navigator.userAgent.match(/Safari/i);
},

isOpera:function(userAgent)
{
	//Match "Opera"
	return!!navigator.userAgent.match(/opera/i);
},

//Convert any HTML escape characters back to real characters.
// This is the client side compliment to the server side method
// FleetManagement.Tools.WebTools.HtmlEncode(...).
unescapeHTML:function(sText)
{
	var s = "";
	if(sText != null)
	{
		s = unescape(sText);
		//Do extra stuff.	
		s = s.replace(/&nbsp;/g, " ");
		s = s.replace(/&quot;/g, "\"");
		s = s.replace(/&#8209;/g, "-");
		s = s.replace(/&#39;/g, "'");
		s = s.replace(/&amp;/g, "&");
		s = s.replace(/&lt;/g, "<");
		s = s.replace(/&gt;/g, ">");
		s = s.replace(/<br \/>/g, "\r\n");
	}
//alert("\"" + sText + "\"");
//alert("\"" + s + "\"");
	return s;
},

////////////////////////////////////////////////////////////////////////////
//The follwing functions will get window size and control location.

GetMyWindowXY:function(oControl)
{
	var x = 0;
	var y = 0;
	do
	{
		if(oControl.style.position != "absolute")
		{
			x += oControl.offsetLeft;
			y += oControl.offsetTop;
		}
		oControl = oControl.offsetParent;
	}  while(oControl != null);
	return [x, y];
},

GetScrollXY:function()
{
	var scrOfX = 0;
	var scrOfY = 0;

	if(typeof(window.pageYOffset) == 'number')
	{
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	}
	else
	if(document.body && (document.body.scrollLeft || document.body.scrollTop))
	{
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	}
	else
	if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop))
	{
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return [scrOfX, scrOfY];
},

GetWindowSize:function()
{
	var width = 0;
	var height = 0;
	
	if(typeof(window.innerWidth) == 'number')
	{
		//Non-IE
		width = window.innerWidth;
		height = window.innerHeight;
	}
	else
	if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		//IE 6+ in 'standards compliant mode'
		width = document.documentElement.clientWidth;
		height = document.documentElement.clientHeight;
	}
	else
	if(document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		//IE 4 compatible
		width = document.body.clientWidth;
		height = document.body.clientHeight;
	}
	//alert([width, height]);
	return [width, height];
},

GetClientSize:function()
{
	var size = SBNet.Util.GetWindowSize();
	var width = Math.max(Math.max(document.documentElement.scrollWidth, document.body.scrollWidth), size[0]);
    var height = Math.max(Math.max(document.documentElement.scrollHeight, document.body.scrollHeight),size[1]);
	//alert([width, height]);
	return [width, height];
},

//This is show the window size in an alert box. Handy for development purposes.
ShowWindowSize:function()
{
	var size = SBNet.Util.GetWindowSize();
	alert("width = " + size[0] + "\r\nheight = " + size[1]);
},

CenterControlOnScreen:function(control)
{
	var win = SBNet.Util.GetWindowSize();
	var scrl = SBNet.Util.GetScrollXY();
		
	var x = scrl[0] + ((win[0] - control.offsetWidth) / 2);
	var y = scrl[1] + ((win[1] - control.offsetHeight) / 2);

	control.style.left = x + "px";
	control.style.top = y + "px";
},

////////////////////////////////////////////////////////////////////////////
//This function will return a client side ASP.NET control using the name of the
// control's HTML ID field. This will work for controls that get there client
// side (ClientID) changed by .NET.
FindASPXControl:function(sType, sName, oParent)
{
	var coll;

	//Find the control
	if(oParent != null)
		coll = oParent.getElementsByTagName(sType);
	else
		coll = document.body.getElementsByTagName(sType);

	if(coll != null)
	{
		for(var i = 0; i < coll.length; i++) 
		{
			var obj = coll[i];
			if((obj.id == sName) || (obj.id.indexOf(sName) > -1))
			{
				//alert("FindASPXControl: " + obj.name);
				return obj;
			}
		}
	}
	return null;      
},

//Finds an array of contols of type 'sType' with 'sName' as part
// of their ID. (ie.  FindASPXControls("INPUT", "tbName")
FindASPXControls:function(sType, sName, oParent)
{
	var controls = new Array();
	var coll;

	//Find the controls
	if(oParent != null)
		coll = oParent.getElementsByTagName(sType);
	else
		coll = document.body.getElementsByTagName(sType);
	if(coll != null)
	{
		for(var i = 0; i < coll.length; i++) 
		{
			var obj = coll[i];
			if((obj.id == sName) || (obj.id.indexOf(sName) > -1))
			{
				//alert("FindASPXControls: " + obj.name);
				controls[controls.length] = obj;
			}
		}
	}
	return controls;  
},

//////////////////////////////////////////////////////////////
///	Confirm Box stuff.

_okFunc:null,
_cancelFunc:null,
_divConfirmBox:null,

ConfirmBox:function(title, message, okFunction, cancelFunction, type, okButtonUrl, cancelButtonUrl)
{
	this._okFunc = okFunction;
	this._cancelFunc = cancelFunction;
	
	var okUrl = this.webAddress + "App_Themes/Main/Buttons/ok_60x20_darkred.gif";
	var cancelUrl = this.webAddress + "App_Themes/Main/Buttons/cancel_60x20_darkgray.gif";
	var yesUrl = this.webAddress + "App_Themes/Main/Buttons/yes_60x20_darkred.gif";
	var noUrl = this.webAddress + "App_Themes/Main/Buttons/no_60x20_darkgray.gif";

	//Change the button urls if requested.
	if(okButtonUrl != null)
		okUrl = yesUrl = okButtonUrl;
	if(cancelButtonUrl != null)
		cancelUrl = noUrl = cancelButtonUrl;

	var innerContent = "<table class=\"BlackRoundCorners\" cellpadding=\"0\" cellspacing=\"0\">" + 
		"<tr>" + 
			"<td class=\"rcbTopLeft rcbNoFont\" >&nbsp;</td>" + 
			"<td class=\"rcbTitleText\" >" + title + "</td>" +
			"<td class=\"rcbTopRight rcbNoFont\" >&nbsp;</td>" +
		"</tr>" +
		"<tr>" +
			"<td class=\"rcbLeft rcbNoFont\" >&nbsp;</td>" +
			"<td class=\"rcbCenter\">" +
				"<table style=\"width: 100%;\">" +
					"<tr>" +
						"<td style=\"padding-bottom: 14px; padding-top: 14px; text-align: left;\">" + message + "</td>" +
					"</tr>" + 
					"<tr>" +
						"<td>" +
							"<table style=\"width: 100%; text-align: right;\">" +
								"<tr>" +
									"<td style=\"width: 100%; text-align: right;\">";
	if((type == null) || type == "okcancel")
	{
		innerContent +=					"<input type=\"image\" name=\"btnCancel\" id=\"btnCancel\" src=\"" + cancelUrl + "\" alt=\"Cancel\" style=\"border-width:0px;\" onclick=\"javascript:return SBNet.Util._messageCancelClick();\" />&nbsp;" +
										"<input type=\"image\" name=\"btnOk\" id=\"btnOk\" src=\"" + okUrl + "\" alt=\"OK\" style=\"border-width:0px;\" onclick=\"javascript:return SBNet.Util._messageOkClick();\" />";
	}
	else
	if(type == "yesno")
	{
		innerContent +=					"<input type=\"image\" name=\"btnCancel\" id=\"btnCancel\" src=\"" + noUrl + "\" alt=\"No\" style=\"border-width:0px;\" onclick=\"javascript:return SBNet.Util._messageCancelClick();\" />&nbsp;" +
										"<input type=\"image\" name=\"btnOk\" id=\"btnOk\" src=\"" + yesUrl + "\" alt=\"Yes\" style=\"border-width:0px;\" onclick=\"javascript:return SBNet.Util._messageOkClick();\" />"
	}
	else
	if(type == "okonly")
	{
		innerContent +=					"<input type=\"image\" name=\"btnOk\" id=\"btnOk\" src=\"" + okUrl + "\" alt=\"OK\" style=\"border-width:0px;\" onclick=\"javascript:return SBNet.Util._messageOkClick();\" />";

	}
	innerContent +=					"</td>" +
								"</tr>" +
							"</table>" +
						"</td>" +
					"</tr>" + 
				"</table>" +
			"</td>" +
			"<td class=\"rcbRight rcgNoFont\" >&nbsp;</td>" +
		"</tr>" +
		"<tr>" +
			"<td class=\"rcbBottomLeft rcbNoFont\" >&nbsp;</td>" +
			"<td class=\"rcbBottom rcbNoFont\" >&nbsp;</td>" +
			"<td class=\"rcbBottomRight rcbNoFont\" >&nbsp;</td>" +
		"</tr>" +
	"</table>";
	
	var div = document.createElement("DIV");
	div.innerHTML = innerContent;
	div.style.display = "none";		

	this._divConfirmBox = document.body.appendChild(div);
	
    div.style.zIndex = 99999;

	SBNet.Util.DisableUI(false, div.style.zIndex);
		
    div.style.position = "absolute";		
	div.style.display = "block";		
	SBNet.Util.CenterControlOnScreen(div);
},

_messageOkClick:function()
{
	SBNet.Util.EnableUI();
	SBNet.Util._divConfirmBox.style.display = "none";		
	document.body.removeChild(SBNet.Util._divConfirmBox);
	
	if(SBNet.Util._okFunc != null)
		SBNet.Util._okFunc();
	
	return false;
},

_messageCancelClick:function()
{
	SBNet.Util.EnableUI();
	SBNet.Util._divConfirmBox.style.display = "none";		
	document.body.removeChild(SBNet.Util._divConfirmBox);
	
	if(SBNet.Util._cancelFunc != null)
		SBNet.Util._cancelFunc();

	return false;
},

//////////////////////////////////////////////////////////////
///

isInteger:function(s)
{   
    var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
},

stripCharsInString:function(s, delimiters)
{   
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in delimiters, append to returnString.
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character isn't whitespace.
        var c = s.charAt(i);
        if (delimiters.indexOf(c) == -1) returnString += c;
    }
    return returnString;
},

checkUSPhone:function(strPhone)
{
	// non-digit characters which are allowed in phone numbers
	var phoneNumberDelimiters = "()- ";
	// Minimum no of digits in a phone no.
	var digitsInIPhoneNumber = 10;

	var s=this.stripCharsInString(strPhone,phoneNumberDelimiters);
	return (this.isInteger(s) && s.length == digitsInIPhoneNumber);
},

////////////////////////////////////////////////////////////////////////////
//
formatUSCurrency:function(fAmount, bShowNegative, bNoDecimal)
{
      var n = fAmount;

      if(typeof(n) != "string")
            n = String(n);

      n = parseFloat(n.replace(/,|^\$/g, "")).toFixed(2);
      if(!isNaN(n))
      {
            var rx = /(\d)(\d{3})([.,])/;
            while (rx.test(n))
            {
                  n = n.replace(rx, "$1,$2$3");
            }
            if((bNoDecimal != null) && (bNoDecimal == true)) 
                  n = n.replace(".00", "");
            //Change negative value formating to "($xxx.xx)".
            if((bShowNegative != null) && (bShowNegative == true))
            {
                  n = "$" + n.replace("-", "\x26\x238209\x24");
            }
            else
            {
                  //Remove neg sign.
                  if(fAmount < 0)
                        n = "($" + n.replace(/-/g, "") + ")";
                  else
                        n = "$" + n;
            }
      }
      return n.toString();
}

}

//</SCRIPT>
