//<SCRIPT>
//// Browser.js ////
//
// ____________________________________________________________________________
//


if(typeof(SBNet) == "undefined")
{
	SBNet={};
}

SBNet.Browser = {

getAgent:function()
{
	return navigator.userAgent.toLowerCase();
},

isMac:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/mac/i);
},

isWin:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/win/i);
},

isOpera:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/opera/i);
},

isIE:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/msie/i);
},

isIE6:function()
{
	//Match "msie 6."
	return!!navigator.userAgent.match(/msie 6\./i);
},

isFirefox:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/firefox/i);
},

isSafari:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/Safari/i);
},

isiPhone:function(userAgent)
{
	var agent=userAgent||this.getAgent();
	return!!agent.match(/iPhone/i);
}

};


//</SCRIPT>

