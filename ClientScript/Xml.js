﻿// JScript File

if(typeof(SBNet) == "undefined")
{
	SBNet={};
}

SBNet.XML = function()
{
	this._doc = null;
}

SBNet.XML.prototype = {

LoadXML:function(sXML)
{
	var doc = null;
	
	if(window.ActiveXObject) // code for IE
	{
		doc = new ActiveXObject("Microsoft.XMLDOM");
		doc.async="false";
		doc.loadXML(sXML);
	}
	else // code for Mozilla, Firefox, Opera, etc.
	{
		var parser = new DOMParser();
		doc = parser.parseFromString(sXML, "text/xml");
	}
	
	_doc = doc.documentElement;
	return _doc;
},

Load:function(sFileName)
{
	var doc = null;

	if(window.ActiveXObject) // code for IE
	{
		doc = new ActiveXObject("Microsoft.XMLDOM");
		doc.async = false;
		doc.load(sFileName);
	}
	else // code for Mozilla, Firefox, Opera, etc.
	if(document.implementation && document.implementation.createDocument)
	{
		doc = document.implementation.createDocument("", "", null);
		doc.load(sFileName);
	}

	_doc = doc.documentElement;
	return _doc;
},

GetNodes:function(nodeName)
{
	if(_doc == null)
		throw "Call Load() or LoadXML() before calling this method.";
	return  _doc.getElementsByTagName(nodeName);
},

GetNodeText:function(parent, nodeName)
{
	var nodes = parent.getElementsByTagName(nodeName);
    if(nodes.length > 0)
    {
		if (nodes[0].text != null)
		  return nodes[0].text;
		else
		  return nodes[0].textContent;
	}
	return "";
}
}