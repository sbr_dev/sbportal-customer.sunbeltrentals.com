<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown
		
		try
		{
			//Clean the temp directory...
			string tempDir = Server.MapPath("~/Temp");
			//Get image files...
			string[] files = System.IO.Directory.GetFiles(tempDir, "*.png");
			foreach (string fileName in files)
			{
				System.IO.File.Delete(fileName);
			}
		}
		catch { }

		try
		{
			string tempDir = Server.MapPath("~/Temp/Images");
			//Get image files...
			string[] files = System.IO.Directory.GetFiles(tempDir, "*.png");
			foreach (string fileName in files)
			{
				System.IO.File.Delete(fileName);
			}
		}
		catch { }
		
		try
		{
			//Add this code when needed...
			string tempDir = Server.MapPath("~/Temp/InvoiceReprint");
			//Get image files...
			string[] files = System.IO.Directory.GetFiles(tempDir, "*.pdf");
			foreach (string fileName in files)
			{
				System.IO.File.Delete(fileName);
			}
		}
		catch { }
	}
        
    void Application_Error(object sender, EventArgs e) 
    {
		//This method gets call for unhandled exceptions. It will attemp to 
		// redirect to the error page. If that fails it will display error
		// information on the page.

#if true
		Exception objErr = Server.GetLastError().GetBaseException();

		//Pass this to our error page.
		try
		{
			//Don't redirect, let the web.config error handling do it.		
			//SBNet.Error.HandleError(objErr, true);
			SBNet.Error.HandleError(objErr, false);
		}
		catch (Exception /*e*/)
		{
			//If worst comes to worst...
 #if false
			if (objErr != null)
			{
				string err = "<b>Error Caught in Application_Error event</b><hr><br>" +
					"<br><b>Error in: </b>" + Request.Url.ToString() +
					"<br><b>Error Message: </b>" + objErr.Message.ToString() +
					"<br><b>Stack Trace:</b><br>" +
					objErr.StackTrace.ToString();
				Response.Write(err.ToString());
				Server.ClearError();
			}
 #endif
		}
#endif
	}

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    void Application_PreSendRequestHeaders(object sender, EventArgs e)
    {

    }
       
</script>
