using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using Sunbelt.Security;

public partial class VerifyUser : Sunbelt.Web.WebPage
{
	private enum PageState { Error, Activated, Expired, Resend }
	private PageState State
	{
		get { return (PageState)GetViewState("State", PageState.Error); }
		set { SetViewState("State", value); }
	}
	private string UserName
	{
		get { return (string)GetViewState("UserName", ""); }
		set { SetViewState("UserName", value); }
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			//Check for the query string.
			try
			{
				//Check the query string.
				if (Request.QueryString["wcan"] == null)
				{
					//Record hacker attempt.

					//Transfer to home page.
					Server.Transfer("~/Default.aspx", false);
					return;
				}
				else
				{
					//Ignore ExpiredQueryStringException. Check it manually.
					SecureQueryString sqs = new SecureQueryString(Request.QueryString["wcan"], true);
					UserName = sqs["user"];
					
					//Check if query string has expried.
					if (sqs.HasExpired())
						throw new ExpiredSecureNameValueCollectionException();

					//get user profile.
					SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;

					if (!p.ApproveUser(UserName))
						throw new UserProfileException(UserProfileException.ProfileErrorType.ApproveUserFailed);
					State = PageState.Activated;
				}
			}
			catch (ExpiredSecureNameValueCollectionException /*ex*/)
			{
				State = PageState.Expired;
			}
			catch (InvalidSecureNameValueCollectionException /*ex*/)
			{
				State = PageState.Error;
			}
			catch (UserProfileException /*ex*/)
			{
				State = PageState.Error;
			}
			catch (Exception /*ex*/)
			{
				State = PageState.Error;
			}
		}
		UpdateDisplay();
	}

	private void UpdateDisplay()
	{
		divActivated.Visible = false;
		divExpired.Visible = false;
		divResend.Visible = false;
		divError.Visible = false;
		switch (State)
		{
			case PageState.Activated:
				divActivated.Visible = true;
				break;
			case PageState.Expired:
				divExpired.Visible = true;
				break;
			case PageState.Resend:
				divResend.Visible = true;
				break;
			default: //Error
				divError.Visible = true;
				break;
		}
	}

	protected void btnResendEmail_Click(object sender, EventArgs e)
	{
		UPSendVerifyEmail1.SendActivationEmail(UserName, false);  //TODO:  May need to determine if cash customer
		State = PageState.Resend;
		UpdateDisplay();
	}
}
