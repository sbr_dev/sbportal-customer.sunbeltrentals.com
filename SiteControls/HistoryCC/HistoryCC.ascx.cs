﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SBNet;
using Sunbelt.Tools;
using Sunbelt.Security;
using Sunbelt.BusinessObjects;
using System.Reflection;

public partial class SiteControls_HistoryCC : Sunbelt.Web.UserControl, SBNet.IWebControl
{
	public class EquipmentRentalComparer : IComparer<EquipmentRental>
	{
		private string PropertyName;
		private SortDirection Direction;

		public EquipmentRentalComparer(string propertyName, SortDirection direction)
		{
			PropertyName = propertyName;
			Direction = direction;
		}

		#region IComparer<EquipmentRental> Members

		public int Compare(EquipmentRental a, EquipmentRental b)
		{
			// gets the value of the 'a' property
			PropertyInfo property = a.GetType().GetProperty(PropertyName);
			object valueOf_a = property.GetValue(a, null);

			// gets the value of the 'b' property 
			property = b.GetType().GetProperty(PropertyName);
			object valueOf_b = property.GetValue(b, null);

			// now makes the comparsion 
			// note: the property type must implement the IComparable interface 
			int direction = (Direction == SortDirection.Ascending) ? 1 : -1;
			return ((IComparable)valueOf_a).CompareTo(valueOf_b) * direction;
		}

		#endregion
	}

	public string GridSkinID
	{
		set { gvHistory.SkinID = value; }
		get { return gvHistory.SkinID; }
	}
	public Unit GridWidth
	{
		set { gvHistory.Width = value; }
		get { return gvHistory.Width; }
	}
	public string ScrollingDivHeight
	{
		set { divHistory.Style["height"] = value; }
		get { return divHistory.Style["height"]; }
	}
	public string DivCssClass
	{
		set { divHistory.Attributes["class"] = DivCssClass; }
		get { return divHistory.Attributes["class"]; }
	}

	private SBNet.AccountEventArgs accountEventArgs
	{
		get { return (SBNet.AccountEventArgs)GetViewState("account", null); }
		set { SetViewState("account", value); }
	}
	private EquipmentRentals myList
	{
		get { return (EquipmentRentals)WebTools.GetSessionCache("myList", null); }
		set { WebTools.SetSessionCache("myList", value, new TimeSpan(0, Session.Timeout, 0)); }
	}
	private string SortExpression
	{
		get { return (string)GetViewState("SortE", ""); }
		set { SetViewState("SortE", value); }
	}
	private SortDirection SortDirection
	{
		get { return (SortDirection)GetViewState("SortD", SortDirection.Ascending); }
		set { SetViewState("SortD", value); }
	}

	protected string EmptyDataText = "There is no account history at this time.&nbsp;&nbsp;To create a new reservation, click&nbsp;" +
	"<a href=" + ConfigurationManager.AppSettings["SUNBELT_ROOT"] + " title=\"Create new reservation\">here</a>.";

	protected void Page_Load(object sender, EventArgs e)
    {
    }

	private void LoadGrid()
	{
		if(myList == null)
			myList = CustomerData.GetCashCustomerEquipmentRentals(
				accountEventArgs.DLNumber, accountEventArgs.DLState,
				DateTime.Now.AddMonths(-6));


        if (!string.IsNullOrEmpty(SortExpression))
            myList.Sort(new EquipmentRentalComparer(SortExpression, SortDirection));
	
		gvHistory.DataSource = myList;
		gvHistory.DataBind();
	}

	protected void gvHistory_RowDataBound(object sender, GridViewRowEventArgs e)
	{
        if (e.Row.RowType == DataControlRowType.Header)
        {
            foreach (DataControlField field in gvHistory.Columns)
            {
                e.Row.Cells[gvHistory.Columns.IndexOf(field)].CssClass = "NoSortingApplied";

                if (field.SortExpression == SortExpression)
                {
                    e.Row.Cells[gvHistory.Columns.IndexOf(field)].CssClass =
                        SortDirection == SortDirection.Ascending ? "SortAscending" : "SortDescending";
                }
                else if (gvHistory.Columns[gvHistory.Columns.IndexOf(field)].SortExpression == "") //Check if no sorting applied and apply style.
                {
                    e.Row.Cells[gvHistory.Columns.IndexOf(field)].CssClass = "DefaultHeader";
                }
            }
        }

		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			EquipmentRental er = (EquipmentRental)e.Row.DataItem;

			//Pass everything through the hyperlink.
			HyperLink hl = (HyperLink)e.Row.FindControl("hlContractNumber");
			SecureQueryString sqs = new SecureQueryString();
			sqs.ExpireTime = DateTime.Now.AddMinutes(60);

			sqs["DocNumber"] = er.ContractNumber.ToString(); ;
			sqs["Sequence"] = er.SequenceNumber.ToString();
			sqs["Account"] = "0"; // Cash Customer AccountEventArgs.AccountNumber.ToString();
			sqs["IsCL"] = "false"; // Cash Customer  AccountEventArgs.IsCorpLink.ToString().ToLower();
			sqs["IsQuote"] = "false";
			sqs["SessionID"] = Session.SessionID;
			string wcan = sqs.ToString();

			hl.Text = er.ContractNumber.ToString();
			if (hl.Text != "")
				hl.NavigateUrl = ResolveUrl("~/AccountTools/PDFViewer.aspx?wcan=" + wcan);
			else
				hl.NavigateUrl = string.Empty;

			Label lb = (Label)e.Row.FindControl("lbEquipment");
			lb.Text = er.EquipmentNumber.Trim() + " - " + WebTools.HtmlEncode(er.EquipmentDescription.Trim());

			lb = (Label)e.Row.FindControl("lbDateOut");
			lb.Text = DateTimeTools.ToShortestDateString(er.DateOut);

			lb = (Label)e.Row.FindControl("lbEstReturnDate");
			lb.Text = DateTimeTools.ToShortestDateString(er.EstReturnDate);

			lb = (Label)e.Row.FindControl("lbTotalBilled");
			lb.Text = er.TotalBilled.ToString("C");

			lb = (Label)e.Row.FindControl("lbLastBilledDate");
			lb.Text = DateTimeTools.ToShortestDateString(er.LastBilledDate);
		}
		else
		if (e.Row.RowType == DataControlRowType.Header)
		{
#if false
			foreach (DataControlField field in gvHistory.Columns)
			{
				int index = gvHistory.Columns.IndexOf(field);
				TableCell cell = e.Row.Cells[index];
				
				cell.CssClass = "NoSortingApplied";

				Image img = new Image();
				img.ImageUrl = "~/Images/sort_asc.gif";
				cell.Controls.Add(img); 
				
				if (field.SortExpression == SortExpression)
				{
					cell.CssClass =
						SortDirection == SortDirection.Ascending ? "SortAscending" : "SortDescending";
				}
				else
				if (field.SortExpression == "") //Check if no sorting applied and apply style.
				{
					cell.CssClass = "DefaultHeader";
				}
			}
#endif
		}
	}

    protected void gvHistory_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (e.SortExpression == SortExpression)
            SortDirection = (SortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending;
        else
            SortDirection = SortDirection.Ascending;
        SortExpression = e.SortExpression;
        LoadGrid();
    }

	#region IWebControl Members

	public void UpdateData(AccountEventArgs e)
	{
		if (!IsPostBack || (accountEventArgs == null) ||
			(accountEventArgs.DLStateNumber != e.DLStateNumber))
		{
			myList = null;
			accountEventArgs = e;
			LoadGrid();
		}
	}

	#endregion
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string s = Request.Params["__EVENTARGUMENT"];
        s = Request.Params["__EVENTTARGET"];

        bool blnDifferentColumn = false;
        LinkButton lnk = (LinkButton)Page.FindControl(s);

        if (lnk != null)
        {
            if (!string.IsNullOrEmpty(SortExpression) && SortExpression != lnk.CommandArgument)
                blnDifferentColumn = true;

            SortExpression = lnk.CommandArgument;

            if (!string.IsNullOrEmpty(SortExpression))
            {
                if (SortDirection == SortDirection.Ascending && !blnDifferentColumn)
                {
                    LoadGrid();
                    SortDirection = SortDirection.Descending;
                }
                else if (SortDirection == SortDirection.Descending && !blnDifferentColumn)
                {
                    LoadGrid();
                    SortDirection = SortDirection.Ascending;
                }
                else
                {
                    SortDirection = SortDirection.Ascending;
                    LoadGrid();
                }
            }
        }
    }
}
