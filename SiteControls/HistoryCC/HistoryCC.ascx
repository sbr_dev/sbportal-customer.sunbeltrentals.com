﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HistoryCC.ascx.cs" Inherits="SiteControls_HistoryCC" %>

<script language="javascript" type="text/javascript">
//<![CDATA[
   
    function Sort(lbId, sortExpression)
    {
        var linkbutton = document.getElementById(lbId);
        __doPostBack(lbId, sortExpression);
    }

//]]>
</script>

<table id="tblHistory" class="AccountTools" cellpadding="0" cellspacing="0">
<tr>
	<td style="vertical-align: top;"><div id="divHistory" runat="server" class="AccountToolsScrollingDIV">
		<asp:GridView ID="gvHistory" AutoGenerateColumns="false" runat="server" AllowSorting="false" 
			SkinID="Gray" onrowdatabound="gvHistory_RowDataBound" OnSorting="gvHistory_Sorting">
			<EmptyDataTemplate>
				<asp:Label ID="lbEmptyData" runat="server" Text='<%# EmptyDataText %>' Width="600px"></asp:Label>
			</EmptyDataTemplate>
			<Columns>
		        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="ContractNumber">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkHeaderContract" runat="server" Text="Contract" OnClick="LinkButton1_Click" CommandArgument="ContractNumber" style="margin-right: 8px;"/>
		            </HeaderTemplate>
		            <ItemTemplate>
                        <asp:HyperLink ID="hlContractNumber" runat="server" Target="_blank"></asp:HyperLink>
		            </ItemTemplate>
		        </asp:TemplateField>
				<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="EquipmentNumber">
					<HeaderTemplate>
					    <asp:LinkButton ID="lnkHeaderEquipment" runat="server" Text="Equipment" OnClick="LinkButton1_Click" CommandArgument="EquipmentNumber" style="margin-right: 8px;" />
					</HeaderTemplate>
					<ItemTemplate>
						<asp:Label ID="lbEquipment" runat="server" Text="lbEquipment"></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="Quantity">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkQty" runat="server" Text="Qty" OnClick="LinkButton1_Click" CommandArgument="Quantity" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbQty" runat="server" Text='<%# Eval("Quantity")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
		        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="DateOut">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkDateOut" runat="server" Text="Date Out" OnClick="LinkButton1_Click" CommandArgument="DateOut" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbDateOut" runat="server" Text='<%# Eval("DateOut")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
		        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="DaysRented">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkDaysRented" runat="server" Text="Days" OnClick="LinkButton1_Click" CommandArgument="DaysRented" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbDaysRented" runat="server" Text='<%# Eval("DaysRented")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="EstReturnDate">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkEstReturnDate" runat="server" Text="Return Date" OnClick="LinkButton1_Click" CommandArgument="EstReturnDate" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbEstReturnDate" runat="server" Text='<%# Eval("EstReturnDate")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="TotalBilled">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkTotalBilled" runat="server" Text="Total Billed" OnClick="LinkButton1_Click" CommandArgument="TotalBilled" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbTotalBilled" runat="server" Text='<%# Eval("TotalBilled")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
		        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="LastBilledDate">
		            <HeaderTemplate>
		                <asp:LinkButton ID="lnkLastBilledDate" runat="server" Text="Lasted Billed" OnClick="LinkButton1_Click" CommandArgument="LastBilledDate" style="margin-right: 8px;" />
		            </HeaderTemplate>
		            <ItemTemplate>
		                <asp:Label ID="lbLastBilledDate" runat="server" Text='<%# Eval("TotalBilled")%>' ></asp:Label>
		            </ItemTemplate>
		        </asp:TemplateField>
			</Columns>
		</asp:GridView>
	</div></td>
</tr>
</table>
<asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click"></asp:LinkButton>

