﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserAccess.ascx.cs" Inherits="SiteControls_UserAdministration_WebUserControl" %>
<table style="width: 100%; height: 100%;">
    <tr>
        <td id="tblCell" runat="server" class="UserAccess">
            You currently do not have access to <asp:Label ID="lblAdministrationType" runat="server" />.  Please contact your site administrator.
        </td>
    </tr>
</table>