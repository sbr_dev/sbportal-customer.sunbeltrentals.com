﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SiteControls_UserAdministration_WebUserControl : System.Web.UI.UserControl
{
    public string SecurityAccessObject
    {
        get { return lblAdministrationType.Text; }
        set { lblAdministrationType.Text = value.ToLower(); }
    }
    public FontUnit FontSize
    {
        set
        {
            lblAdministrationType.Font.Size = value;
            tblCell.Style.Add("font-size", value.ToString());
            tblCell.Attributes.Add("font-size", value.ToString());
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
