﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationAddressAndHours.ascx.cs" Inherits="SiteControls_LocationAddressAndHours" %>

<div id="divPCAddressAndHours" visible="true" runat="server">
    <table id="tbldivPCAddressAndHours" border="0" cellspacing="1" cellpadding="1" runat="server">
        <tr>
            <td id="tdLocationAddressHours" colspan="4" valign="top" style="font-weight:bold;height:10px;" class="SmallBodyText" align="left"  runat="server">
                <asp:Label ID="lblRentalLocationHeading" runat="server" ForeColor="Black" Text="Rental Location:"/>
            </td>
        </tr>
        <tr>
            <td align="left" valign='top' style="padding-right: 7px;" class="SmallBodyText"  >
                <asp:Label ID="lbLocationName" runat='server'/>
                <br />
                <asp:Label ID='lbAddress' runat='server'/>
                <br />
                <asp:Label ID='lbCityStateZip' runat='server'/>
                <br />
                <asp:Label ID='lbPhone' runat='server'/>
            </td>
            <td id="tdAddressHoursSeparator" rowspan='3' style="border-left: 1px solid #ffffff; padding-right: 2px;" runat="server">
                &nbsp;
            </td>
            <td valign='top'>
                <table cellpadding='0' cellspacing='0' class="SmallBodyText">
                    <tr>
                        <td class="SmallBodyText">
                            <asp:Label ID='lbWeekDays' runat='server' Text=''/>
                        </td>
                        <td class="SmallBodyText">
                            <asp:Label ID='filler' runat='server' Text='&nbsp;&nbsp;'/>
                        </td>
                        <td class="SmallBodyText" align="left">
                            <asp:Label ID='lbWeekHours' runat='server' Text=''/>
                        </td>
				    </tr>
		    </table>
		    </td>
    </tr> 
    </table>
</div>