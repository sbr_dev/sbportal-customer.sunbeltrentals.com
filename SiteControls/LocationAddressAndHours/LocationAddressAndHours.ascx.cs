﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sunbelt.BusinessObjects.Web;
using System.Text;
using SBNet.Extensions;



using System.Web.UI.HtmlControls;
using System.IO;
using System.Globalization;
using Sunbelt.Tools;

public partial class SiteControls_LocationAddressAndHours : System.Web.UI.UserControl
{
	public string LocationName { get; set; }
	public string Address      { get; set; }
	public string City         { get; set; }
	public string State        { get; set; }
	public string Zip          { get; set; }
	public string Phone        { get; set; }
	public string HoursMF      { get; set; }
	public string HoursSat     { get; set; }
	public string HoursSun     { get; set; }
	public Label RentalLocationHeading 	{ get { return lblRentalLocationHeading; } 	}
    public HtmlTableCell RentalLocationHeadingCell { get { return tdLocationAddressHours; } }



    protected void Page_Load(object sender, EventArgs e)
    {

    }
	public void SetRentalLocation(string rentalLocation)
	{
		Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Locations.ToString(),"Loading days and hours of operation for PC:" + rentalLocation.ToString());
		try
		{
			Location RentalLocation = new Location(rentalLocation);
			System.Collections.Generic.List<Schedule> RentalLocationSchedule = RentalLocation.HoursOfOperation;

			LocationName = RentalLocation.PCName.ToLeadingCaps() + "<br/>Branch #" + RentalLocation.PCNumber;
			Address = RentalLocation.ContactInformation.Address.Street.ToLeadingCaps();
			City = RentalLocation.ContactInformation.Address.City.ToLeadingCaps();
			State = RentalLocation.ContactInformation.Address.State;
			Zip = RentalLocation.ContactInformation.Address.Zip;
			Phone = RentalLocation.ContactInformation.PrimaryPhone;

			lbLocationName.Text = LocationName;
			lbAddress.Text = Address;
			lbCityStateZip.Text = City + ", " + State + " " + Zip;
			lbPhone.Text = Phone;

			bool isWeekDaysHourDiffer = false;
			int o = 0;
			int c = 0;
			int day;
			for (day = (int)DayOfWeek.Monday; day <= (int)DayOfWeek.Friday; day++)
			{
				if (RentalLocationSchedule[day].IsOpen == false)
				{
					isWeekDaysHourDiffer = true;
					break;
				}
				if (o == 0)
					o = RentalLocationSchedule[day].Open.Hour;
				else
					if (o != RentalLocationSchedule[day].Open.Hour)
					{
						isWeekDaysHourDiffer = true;
						break;
					}

				if (c == 0)
					c = RentalLocationSchedule[day].Close.Hour;
				else
					if (c != RentalLocationSchedule[day].Close.Hour)
					{
						isWeekDaysHourDiffer = true;
						break;
					}
			}

			StringBuilder weekDayHours = new StringBuilder("");
			StringBuilder weekDays = new StringBuilder("");
			if (isWeekDaysHourDiffer == true)
			{
				for (day = (int)DayOfWeek.Monday; day <= (int)DayOfWeek.Saturday; day++)
				{
					if (day == (int)DayOfWeek.Saturday)
					{
						weekDays.Append("<hr>");
						weekDayHours.Append("<hr>");
					}

					weekDays.Append(((DayOfWeek)day).ToString() + ":<br />");

					if (RentalLocationSchedule[day].IsOpen)
						weekDayHours.Append(RentalLocationSchedule[day].Open.ToString("h:mm tt").ToLower() + " - "
											+ RentalLocationSchedule[day].Close.ToString("h:mm tt").ToLower() + "<br />");
					else
						weekDayHours.Append("Closed" + "<br />");
				}
			}
			else
			{
				for (day = (int)DayOfWeek.Friday; day <= (int)DayOfWeek.Saturday; day++)
				{
					if (day == (int)DayOfWeek.Friday)
						weekDays.Append("M-F:<br />");
					else
						weekDays.Append(((DayOfWeek)day).ToString() + ":<br />");

					if (RentalLocationSchedule[day].IsOpen)
						weekDayHours.Append(RentalLocationSchedule[day].Open.ToString("h:mm tt").ToLower() + " - "
											+ RentalLocationSchedule[day].Close.ToString("h:mm tt").ToLower() + "<br />");
					else
						weekDayHours.Append("Closed" + "<br />");
				}
			}

			day = (int)DayOfWeek.Sunday;
			weekDays.Append(((DayOfWeek)day).ToString() + ": <br />");
			if (RentalLocationSchedule[day].IsOpen)
				weekDayHours.Append(RentalLocationSchedule[day].Open.ToString("h:mm tt").ToLower() + " - "
									+ RentalLocationSchedule[day].Close.ToString("h:mm tt").ToLower() + "<br />");
			else
				weekDayHours.Append("Closed" + "<br />");

			lbWeekHours.Text = weekDayHours.ToString();
			lbWeekDays.Text = weekDays.ToString();

			Sunbelt.Log.LogActivity(Sunbelt.Log.Sections.Locations.ToString(), "Loading days and hours of operation completed for PC:" + rentalLocation.ToString());
		}
		catch (Exception ex)
		{
			Sunbelt.Log.LogError(ex, "Days and hours of operation failed for PC:" + rentalLocation.ToString());
		}
	}

    public void FormatForEquipmentPickUpConfirmation()
    {
        RentalLocationHeading.Text = "Rental Location";
        RentalLocationHeadingCell.VAlign = "bottom";
        tdAddressHoursSeparator.Style["border-left"] = "solid 1px gray;";
        tdLocationAddressHours.Style["border-bottom"] = "solid 1px gray";
        tbldivPCAddressAndHours.Style["margin-top"] = "0px";
    }
}
