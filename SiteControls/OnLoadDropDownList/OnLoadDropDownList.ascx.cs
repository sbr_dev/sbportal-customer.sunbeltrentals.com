﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sunbelt.Tools;

public partial class UserControl_OnLoadDropDownList : Sunbelt.Web.UserControl, ICallbackEventHandler
{
	public delegate void BindDDLDelegate(out object dataSource, out string dataText, out string dataValue);
	public BindDDLDelegate OnBindDDLDelegate;

	public delegate bool LoadOnClientCallDelegate();
	public LoadOnClientCallDelegate OnLoadOnClientCall;
	
	public EventHandler SelectedIndexChanged;

	public string DropDownListID
	{
		set { sel1.ID = value; }
		get { return sel1.ID; }
	}
	public string DropDownListClientID
	{
		get { return sel1.ClientID; }
	}
    public string DropDownListUniqueID
    {
        get { return sel1.UniqueID; }
    }
	public string SelectedValue
	{
		set { SetViewState("sv", value); }
		get { return (string)GetViewState("sv", ""); }
	}
	public bool Enabled
	{
		set { SetViewState("en", value); }
		get { return (bool)GetViewState("en", true); }
	}
	public bool DelayLoad
	{
		set { SetViewState("dl", value); }
		get { return (bool)GetViewState("dl", true); }
	}
	public bool AutoDisable
	{
		set { SetViewState("ad", value); }
		get { return (bool)GetViewState("ad", true); }
	}
	public string Width
	{
		set { sel1.Style["width"] = value; }
		get { return sel1.Style["width"]; }
	}
    public string DefaultItemText
    {
		set { SetViewState("dit", value); }
		get { return (string)GetViewState("dit", ""); }
    }
    public string DefaultItemValue
    {
		set { SetViewState("div", value); }
		get { return (string)GetViewState("div", ""); }
	}

	private bool loadOnClientCall = false;
	
	protected void Page_Load(object sender, EventArgs e)
    {
		if (IsPostBack)
		{
			//To know the selected value, we have to look at the form.
			string formValue = Page.Request.Form[sel1.UniqueID];
			if (formValue != null &&
				formValue != "[_LOAD_]" &&
				formValue != SelectedValue)
			{
				SelectedValue = formValue;

				EventHandler eh = SelectedIndexChanged;
				if (eh != null)
					eh(this, EventArgs.Empty);
			}
		}
	}

	protected override void OnPreRender(EventArgs e)
	{
		if (OnLoadOnClientCall != null)
			loadOnClientCall = OnLoadOnClientCall();

		ReqisterClentScript();
		base.OnPreRender(e);
	}

	private string xmlEncode(string text)
	{
		StringBuilder sb = new StringBuilder(text);
		sb.Replace("&", "&amp;");
		sb.Replace("<", "&lt;");	
		sb.Replace("\"", "&quot;");	
		sb.Replace("'", "&apos;");	
		sb.Replace(">", "&gt;");
		return sb.ToString();
	}
	
	private void ReqisterClentScript()
	{
		sel1.Disabled = !Enabled;
		if (DelayLoad)
		{
			if (!Enabled)
			{
				opt1.Value = "";
				opt1.Text = "";
				opt1.Attributes["style"] = "";
				if (string.IsNullOrEmpty(Width))
					sel1.Style["width"] = "100px";
				return;
			}
			else
			{
				opt1.Value = "[_LOAD_]";
				opt1.Text = "Loading...";
				opt1.Attributes["style"] = "background-color:#e0e0e0;";
			}
		}

		//If not delay load, it works like a standard DropDownList.
		if (!DelayLoad)
		{
			//Only need to get the data the first time. The items will be
			// saved in view state.
			if (!IsPostBack)
			{
				if (OnBindDDLDelegate == null)
					throw new ArgumentException("OnBindDDLDelegate not set.");

				object dataSource;
				string dataText;
				string dataValue;
				OnBindDDLDelegate(out dataSource, out dataText, out dataValue);

				sel1.DataSource = dataSource;
				sel1.DataTextField = dataText;
				sel1.DataValueField = dataValue;


                if (DefaultItemText != "" && DefaultItemValue != "")
                {
                    sel1.Items.Insert(0, new ListItem(DefaultItemText, DefaultItemValue));
                    sel1.DataBind();
                }
                else
                    sel1.DataBind();

				if (!string.IsNullOrEmpty(SelectedValue))
				{
					sel1.Value = SelectedValue;
				}
			}
		}
		else //Delay load...
		{
			/*** Generate all client script for the control ***/
	
			//Include SBNet XML code.
			SBNet.Tools.IncludeXMLClientScript(Page);

			//Register the OnLoadDropDownList.js file.
			if (!Page.ClientScript.IsClientScriptIncludeRegistered("OnLoadDropDownList.js"))
				Page.ClientScript.RegisterClientScriptInclude("OnLoadDropDownList.js",
					ResolveUrl(AppRelativeTemplateSourceDirectory + "OnLoadDropDownList.js"));

			ClientScriptManager cs = Page.ClientScript;
			
			if (!cs.IsClientScriptBlockRegistered("OLDDL" + ClientID))
			{
				StringBuilder sb = new StringBuilder();
				string serverCall = "ServerCall" + ClientID;

				sb.Append("if(typeof(Sys) != 'undefined')\r\n");
				sb.Append("  Sys.Application.add_load(OnLoad" + ClientID + ")\r\n");
				sb.Append("else\r\n");
				sb.Append("  SBNet.Util.AttachEvent(window, 'onload', OnLoad" + ClientID + ");\r\n");

				sb.Append("function OnLoad" + ClientID + "()\r\n");
				sb.Append("{\r\n");
				//sb.Append("  alert('OnLoad" + ClientID + "()');\r\n");
				sb.Append("  var ddl = document.getElementById('" + sel1.ClientID + "');\r\n");
                if (!string.IsNullOrEmpty(SelectedValue))
                {
                    //Must check for single quotes in selected value so it does not generate a javascript error.
                    if (SelectedValue.Contains("'"))
                        SelectedValue = SelectedValue.Replace("'", "' + '");
                    sb.Append("  ddl._selectedValue = '" + SelectedValue + "';\r\n");
                }
				sb.Append("  ddl._loadOnClientCall = " + loadOnClientCall.ToString().ToLower() + ";\r\n");
				sb.Append("  ddl._ServerCall = " + serverCall + ";\r\n");
				sb.Append("  OLDDL.AddControl(ddl);\r\n");
				sb.Append("  if(!ddl.disabled && !ddl._loadOnClientCall)\r\n");
				sb.Append("    " + serverCall + "('LOAD', '" + ClientID + "');\r\n");
				sb.Append("}\r\n");

				sb.Append("function ClientCall" + ClientID + "(rvalue, context)\r\n");
				sb.Append("{\r\n");
				//sb.Append("  alert('rvalue = '+ rvalue);\r\n");
				sb.Append("  OLDDL.FillDropDown(rvalue, '" + sel1.ClientID + "');\r\n");
				sb.Append("}\r\n");

				sb.Append("function ErrorCall" + ClientID + "(message)\r\n");
				sb.Append("{\r\n");
				sb.Append("  alert(message);\r\n");
				sb.Append("}\r\n");

				String cbReference = cs.GetCallbackEventReference(this,
					"arg",
					"ClientCall" + ClientID,
					"context",
					"ErrorCall" + ClientID,
					true);

				sb.Append("function " + serverCall + "(arg, context) { " + cbReference + "} ;");
				cs.RegisterClientScriptBlock(this.GetType(),
					"OLDDL" + ClientID,
					sb.ToString(),
					true);
			}
		}
	}

	#region ICallbackEventHandler Members

	private string EventArgument; 
	
	public string GetCallbackResult()
	{
		if (EventArgument != "LOAD")
			throw new ArgumentException("Invalid EventArgument (" + EventArgument + ").");
		if (OnBindDDLDelegate == null)
			throw new ArgumentException("OnBindDDLDelegate not set.");

		object dataSource;
		string dataText;
		string dataValue;
		OnBindDDLDelegate(out dataSource, out dataText, out dataValue);

		DropDownList ddl = new DropDownList();
		Sunbelt.Tools.ControlTools.BindListControl(ddl, dataSource, dataText, dataValue);

		if (DefaultItemText != "" && DefaultItemValue != "")
        {
            ddl.Items.Insert(0, new ListItem(DefaultItemText, DefaultItemValue));
            //sel1.DataBind();
        }

		//Convert the items to xml.
		StringBuilder sb = new StringBuilder();

		sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?><OLDDL>");

		//Add settings.
		sb.Append("<settings>");

        //Need to apply the old settings back.  See register client script method.  
        //We had to strip out the single quote to avoid javascript errors, its a hack but works.
        if (SelectedValue.Contains("' + '"))
            SelectedValue = SelectedValue.Replace("' + '", "'");
		sb.Append(string.Format("<selectedvalue>{0}</selectedvalue>",  SelectedValue));
		sb.Append(string.Format("<autodisable>{0}</autodisable>", AutoDisable.ToString().ToLower()));
		sb.Append(string.Format("<loadonclientcall>{0}</loadonclientcall>", loadOnClientCall.ToString().ToLower()));
		
		sb.Append("</settings>");

		//Add items.
		sb.Append("<items>");
		foreach (ListItem li in ddl.Items)
		{
			sb.Append(string.Format("<item><text>{0}</text><value>{1}</value></item>",
				xmlEncode(li.Text.Trim()),
				xmlEncode(li.Value.Trim())));
		}
		sb.Append("</items>");

		//Close root tag.
		sb.Append("</OLDDL>");
		return sb.ToString();
	}

	public void RaiseCallbackEvent(string eventArgument)
	{
		EventArgument = eventArgument;
	}

	#endregion
}
