﻿
// OnLoadDropDownList.js

var OLDDL = {

	controlList:null,
	
	AddControl:function(control)
	{
		if(!this.controlList)
			this.controlList = new Array();
		this.controlList[control.id] = control;
	},

	FillDropDown:function(sXML, controlID)
	{
		//NOTE: Requires SBNet.XML.
		var xmlObj = new SBNet.XML();
		var xdoc = xmlObj.LoadXML(sXML);
		var ddl = document.getElementById(controlID);
		//Clear the option list.
		ddl.options.length = 0;
		//Get the settings.
		var settings = xdoc.getElementsByTagName('settings');
		ddl._selectedValue = xmlObj.GetNodeText(settings[0], 'selectedvalue');
		ddl._autoDisable = (xmlObj.GetNodeText(settings[0], 'autodisable') == "true");
		ddl._loadOnClientCall = (xmlObj.GetNodeText(settings[0], 'loadonclientcall') == "true");
		//Get the items.
		var itemList = xdoc.getElementsByTagName('item');
		for(var i = 0; i < itemList.length; i++)
		{
			var opt = document.createElement('OPTION');
			ddl.options.add(opt);
			opt.text = xmlObj.GetNodeText(itemList[i], 'text');
			opt.value = xmlObj.GetNodeText(itemList[i], 'value');
			if(opt.value == ddl._selectedValue)
				opt.selected = true;
		}
		//Disable the select if not items.
		ddl.disabled = (itemList.length == 0 && ddl._autoDisable);
	},
	
	DoClientLoad:function()
	{
		for (n in this.controlList)
		{
			var ddl = this.controlList[n];
			
			if(ddl._loadOnClientCall)
			{
				//Reset master page session timeout.
				if(typeof(masterPage) != "undefined")
					masterPage.RestartTimer();

				ddl._ServerCall('LOAD', ddl.id);
			}
		}
	}
}
