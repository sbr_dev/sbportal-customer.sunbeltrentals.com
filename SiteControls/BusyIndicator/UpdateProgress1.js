//<SCRIPT>
//// UpdateProgress1.js ////
//
// ____________________________________________________________________________
//

var BI_UP1 = {

ReloadImage:function(img)
{
	BI_UP1.img = img;
	BI_UP1.timerID = setTimeout('_BI_UP1_StartImage()', 1);
},

HideDiv:function(div)
{
	BI_UP1.div = div;
	//Hide the DIV after 10 seconds.
	BI_UP1.timerID = setTimeout('_BI_UP1_HideDiv()', 10000);
}
};

BI_UP1.timerID = 0;
BI_UP1.img = null;
BI_UP1.div = null;

function _BI_UP1_StartImage()
{
	if(BI_UP1.timerID)
	{
		clearTimeout(BI_UP1.timerID);
		BI_UP1.timerID = 0;
		BI_UP1.img.src = SBNet.Util.webAddress + "SiteControls/BusyIndicator/SunbeltGear_Rotating.gif";
	}
}

function _BI_UP1_HideDiv()
{
	if(BI_UP1.timerID)
	{
		clearTimeout(BI_UP1.timerID);
		BI_UP1.timerID = 0;
		BI_UP1.div.style.display = "none";
	}
}

//</SCRIPT>
