using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

public partial class UserControls_BusyIndicator2 : System.Web.UI.UserControl
{
	/// <summary>
	/// Sets the style for the text box.
	/// </summary>
	[DefaultValue("BusyIndicator")]
	public string CssClass
	{
		//set { lblText.CssClass = value; }
		//get { return lblText.CssClass; }
		set { divBusy.Attributes["class"] = value; }
		get { return divBusy.Attributes["class"]; }
	}

	//[DefaultValue("#f5f5f5")]
	//public System.Drawing.Color BackColor
	//{
	//    set { divBusy.Style["background-color"] = Sunbelt.Tools.ColorTools.ToWebString(value); }
	//    get { return Sunbelt.Tools.ColorTools.FromWebString(divBusy.Style["background-color"]); }
	//}

	/// <summary>
	/// Sets the text.
	/// </summary>
	[DefaultValue("Please Wait...")]
	public string Text
	{
		set { lblText.Text = HtmlEncode(value, true, true); }
		get { return lblText.Text; }
	}

	/// <summary>
	/// Sets the image.
	/// </summary>
	[DefaultValue("SunbeltGear_Rotating.gif")]
	public string ImageUrl
	{
		set { imgBusy.ImageUrl = value; }
		get { return imgBusy.ImageUrl; }
	}

	[DefaultValue("False")]
	public bool EnableForUpdatePanels
	{
		set { ViewState["efup"] = value; }
		get
		{
			if (ViewState["efup"] == null)
				return false;
			else
				return Convert.ToBoolean(ViewState["efup"]);
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		frameBusy.Attributes["src"] = ResolveUrl(AppRelativeTemplateSourceDirectory + "Blank.htm");
		frameBusy.Visible = isIE6();
	}

	public bool isIE6()
	{
		return (HttpContext.Current.Request.Browser.Browser == "IE" &&
			HttpContext.Current.Request.Browser.MajorVersion == 6);
	}

	protected override void OnPreRender(EventArgs e)
	{
		//HttpBrowserCapabilities bc = Request.Browser;
		//if (bc.Browser.ToUpper().IndexOf("IE") > -1)
		//{
		//    divBusy.Style["display"] = "none";
		//}

		divBusy.Style["filter"] = "alpha(opacity=0)";
		divBusy.Style["opacity"] = "0.0";

		//Register the BusyIndicator2.js file.
		if (!Page.ClientScript.IsClientScriptIncludeRegistered("BusyIndicatorJS"))
			Page.ClientScript.RegisterClientScriptInclude("BusyIndicatorJS", ResolveUrl("BusyIndicator2.js"));

		base.OnPreRender(e);
	}
	
	public string HtmlEncode(string text, bool doSpaces, bool doNewLines)
	{
		//Apply default html encoding first.
		System.Text.StringBuilder sb = new System.Text.StringBuilder(Server.HtmlEncode(text));

		//Do tabs.
		sb.Replace("\t", "    ");
		//Handle single quot mark.
		sb.Replace("\'", "&#39;");
		if (doSpaces)
		{
			//If doing spaces do hyphens too.
			sb.Replace(" ", "&nbsp;");
			sb.Replace("-", "&#8209;"); //Non-breaking hyphens.
		}
		if (doNewLines)
		{
			sb.Replace("\r\n", "<br />");
		}
		return sb.ToString();
	}
}
