<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BusyIndicator2.ascx.cs" Inherits="UserControls_BusyIndicator2" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>
<script type="text/javascript">
//<![CDATA[

BI_AttachControlEvent(window, "onload", BI_OnBusyIndicatorLoad);

///////////////////////////////////////////////////////////////////

//The script code in this file needs access to sever-side stuff. That is why
// it is not in the BusyIndicator2.js file.

var BI_sftcClockID = 0;
var BI_enableForUpdatePanels = <% =EnableForUpdatePanels.ToString().ToLower() %>;
var BI_isVisible = false;

function BI_ShowBusyIndicator(bNoDelay)
{
	var div = document.getElementById("<% =divBusy.ClientID %>");
	var frame = document.getElementById("<% =frameBusy.ClientID %>");
	
	if(BI_isIE6())
	{
		var oRemoved = div.removeNode(true);
		div = document.body.appendChild(oRemoved);

		oRemoved = frame.removeNode(true);
		frame = document.body.appendChild(oRemoved);
	}

	var win = BI_GetWindowSize();
	var loc = BI_GetMyWindowXY(div);
	var scrl = BI_GetScrollXY();
	
	div.style.filter = "alpha(opacity=100)";
	div.style.opacity = "100.0";
	div.style.position = "absolute";
	div.style.display = "block";
	div.style.zIndex = "10001";

	var x = scrl[0] + ((win[0] - div.offsetWidth) / 2);
	var y = scrl[1] + ((win[1] - div.offsetHeight) / 2);
	div.style.left = x + "px";
	div.style.top = y + "px";

	if(BI_isIE6())
	{
		frame.style.position = "absolute";
		frame.style.display = "block";
		frame.style.zIndex = "10000";

		frame.style.left = div.style.left;
		frame.style.top = div.style.top;
		frame.style.width = div.offsetWidth + "px";
		frame.style.height = div.offsetHeight + "px";
	}
	
	document.getElementById("<% =imgBusy.ClientID %>").src = "<% =ResolveUrl(imgBusy.ImageUrl) %>";


	if(typeof(bNoDelay) == "undefined" || bNoDelay == false)
		BI_sftcClockID = setTimeout('BI_StartBusyIndicator()', 1);
	else
		BI_StartBusyIndicator();

	BI_isVisible = true;	
}

function BI_HideBusyIndicator()
{
	document.getElementById("<% =divBusy.ClientID %>").style.display = "none";
	if(BI_isIE6())
		document.getElementById("<% =frameBusy.ClientID %>").style.display = "none";
	BI_isVisible = false;	
}

function BI_StartBusyIndicator()
{
	if(BI_sftcClockID)
	{
		clearTimeout(BI_sftcClockID);
		sftcClockID = 0;
	}
	document.getElementById("<% =imgBusy.ClientID %>").src = "<% =ResolveUrl(imgBusy.ImageUrl) %>";
}

function BI_SetText(text)
{
	document.getElementById("<% =lblText.ClientID %>").innerText = text;
}

//]]>
</script>
<iframe id="frameBusy" runat="server" src="Blank.htm" frameborder="0" marginheight="0" marginwidth="0" style="display: none; position: absolute; width: 104px; height: 74px;"></iframe>
<cc1:RoundedCornerPanel ID="divBusy" runat="server" SkinID="rcpBlackNoTitle" CssClass="BusyIndicator2">
	<table cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td>
			<asp:Image ID="imgBusy" runat="server" ImageUrl="SunbeltGear_Rotating.gif" />
		</td>
		<td>
			<asp:Label ID="lblText" runat="server" Text="Please Wait..." CssClass="SmallBlueText"></asp:Label>
		</td>
	</tr>
	</table>
</cc1:RoundedCornerPanel>
