using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

public partial class SiteControls_BusyIndicator_UpdateProgress1 : System.Web.UI.UserControl
{
	/// <summary>
	/// Sets the text.
	/// </summary>
	public string Text
	{
		set { lblText.Text = Sunbelt.Tools.WebTools.HtmlEncode(value); }
		get { return lblText.Text; }
	}

	protected void Page_Load(object sender, EventArgs e)
    {
		//Register the UpdateProgress1.js file.
		if (!Page.ClientScript.IsClientScriptIncludeRegistered("UpdateProgress1JS"))
			Page.ClientScript.RegisterClientScriptInclude("UpdateProgress1JS", ResolveUrl("UpdateProgress1.js"));
	}
}
