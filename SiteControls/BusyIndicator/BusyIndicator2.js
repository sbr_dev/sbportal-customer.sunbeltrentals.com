//<SCRIPT>
//// BusyIndicator2.js ////
//
// ____________________________________________________________________________
//

//To show the busy indicator on	__doPostBack calls, you must hook the __doPostBack
// function and inject a BI_ShowBusyIndicator call. This code does it.
if(typeof(__doPostBack) != "undefined")
	BI_AddFunctionTo__doPostBack("BI_ShowBusyIndicator");

//This code gets called on the window.onload event. It is set in BusyIndicator2.cs.
function BI_OnBusyIndicatorLoad()
{
	BI_HideBusyIndicator();

	//Check to see if any AJAX partial-page rendering control are being used. If so,
	// hook the AJAX client events.
	if(typeof(Sys) != "undefined" && typeof(Sys.WebForms.PageRequestManager) == "function")
	{
		//Add a property for this.
		Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BI_ajax_beginRequest);
		Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(BI_ajax_pageLoaded);
		//Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(BI_ajax_initializeRequest);
	}

	//Check if there are any validation controls being used. If so, hook the 
	// ValidatorCommonOnSubmit function. Else, hook the form onsubmit function.
	if(typeof(ValidatorCommonOnSubmit) != "undefined")
		BI_AddFunctionTo_ValidatorCommonOnSubmit("BI_ValidatorCommonOnSubmit");
	else
		BI_AttachControlEvent(document.forms[0], "onsubmit", BI_onSubmit);
}
        
var BI_ajax_Request = false;

function BI_ajax_beginRequest(sender, args)
{
	//alert("BI_ajax_beginRequest");

	BI_ajax_Request = true;

	//Check for update panel use.
	var prm = Sys.WebForms.PageRequestManager.getInstance();
	if(prm._postBackSettings != null)
	{
		//If we get here a update panel is updating.
		if(BI_enableForUpdatePanels)
			BI_ShowBusyIndicator();
		else
			BI_HideBusyIndicator();
		
		BI_isVisible = true;
	}
	else
		BI_ShowBusyIndicator();
}

function BI_ajax_pageLoaded(sender, args)
{
	//alert("BI_ajax_pageLoaded");
	BI_HideBusyIndicator();
	
	BI_ajax_Request = false;
}

//function BI_ajax_initializeRequest(sender, args)
//{
//	//alert("BI_ajax_initializeRequest");
//	BI_HideBusyIndicator();
//}

function BI_isIE6()
{
	//Match "msie 6."
	return!!navigator.userAgent.match(/msie 6\./i);
}

//function BI_isSafari()
//{
//	//Match "Safari"
//	return!!navigator.userAgent.match(/Safari/i);
//}

//function BI_isFirefox()
//{
//	//Match "Firefox"
//	return!!navigator.userAgent.match(/Firefox/i);
//}

function BI_WatchAnchors(parent)
{
	//This relies on document.readyState.
	if(typeof(document.readyState) == "string")
	{
		var coll;
		//Find the controls
		if(parent != null)
			coll = parent.getElementsByTagName("A");
		else
			coll = document.body.getElementsByTagName("A");
		if(coll != null)
		{
			for(var i = 0; i < coll.length; i++) 
			{
				BI_AttachControlEvent(coll[i], "onclick", BI_OnClickCheckLoad);
			}
		}
	}
}

var BI_checkTimer = 0;
var BI_checkCount = 0;

function BI_OnClickCheckLoad()
{
	//alert("BI_OnClickCheckLoad");
	if(BI_checkTimer == 0)
		BI_checkTimer = setInterval('BI_DoCheckLoad()', 250);
}

function BI_DoCheckLoad()
{
	//if(document.readyState != "complete")
	//	alert(document.readyState);

	BI_checkCount++;

	if((BI_checkCount > 20) || (document.readyState == "loading"))
	{
		clearInterval(BI_checkTimer);
		BI_checkTimer = 0;
		BI_checkCount = 0;
		
		if(document.readyState == "loading")
			BI_ShowBusyIndicator(true);
	}
}

function BI_AttachControlEvent(oControl, eventName, eventFunction)
{
	//alert(typeof(oControl.attachEvent));
	if(typeof(oControl.attachEvent) != "undefined")
		oControl.attachEvent(eventName, eventFunction);
	else
	{
		//If non Microsoft, strip "on" from eventName.
		eventName = eventName.substr(2);
		oControl.addEventListener(eventName, eventFunction, false);
	}
}

function BI_AddFunctionTo_ValidatorCommonOnSubmit(sFunctionName)
{
	var ev = ValidatorCommonOnSubmit;
	if(typeof(ev) == "function")
	{
		ev = ev.toString();
		ev = ev.substring(ev.indexOf("{") + 1, ev.lastIndexOf("}"));
	}
	else
		ev = "";
	ValidatorCommonOnSubmit = new Function(sFunctionName + "(); " + ev);
}

function BI_AddFunctionTo__doPostBack(sFunctionName)
{
	var ev = __doPostBack;
	if(typeof(ev) == "function")
	{
		ev = ev.toString();

		//if(ev.indexOf("method.apply") != -1)
		if(ev.indexOf("__doPostBack") == -1)
			return;

		ev = ev.substring(ev.indexOf("{") + 1, ev.lastIndexOf("}"));
	}
	else
		ev = "";
	__doPostBack = new Function("eventTarget", "eventArgument", sFunctionName + "(); " + ev);
}

function BI_ValidatorCommonOnSubmit()
{
	if(!Page_BlockSubmit)
	{
		if(typeof(BI_ShowBusyIndicator) == "function")
			BI_ShowBusyIndicator();
	}
}

function BI_onSubmit()
{
	if(typeof(theForm) == "undefined")
		theForm = document.forms[0];

	if (!theForm.onsubmit || (theForm.onsubmit() != false))
	{
		if(typeof(BI_ShowBusyIndicator) == "function")
		{
			if(BI_ajax_Request == false || (BI_ajax_Request == true && BI_enableForUpdatePanels == true))
				BI_ShowBusyIndicator();
		}
	}
}

///////////////////////////////////////////////////////////////////

function BI_GetMyWindowXY(oControl)
{
	var x = 0;
	var y = 0;
	do
	{
		x += oControl.offsetLeft;
		y += oControl.offsetTop;
		oControl = oControl.offsetParent;
	}  while(oControl != null);
	return [x, y];
}

function BI_GetScrollXY()
{
	var scrOfX = 0;
	var scrOfY = 0;

	if(typeof(window.pageYOffset) == 'number')
	{
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	}
	else
	if(document.body && (document.body.scrollLeft || document.body.scrollTop))
	{
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	}
	else
	if(document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop))
	{
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return [scrOfX, scrOfY];
}

function BI_GetWindowSize()
{
	var myWidth = 0;
	var myHeight = 0;
	
	if(typeof(window.innerWidth) == 'number')
	{
		//Non-IE
		myWidth = window.innerWidth;
		myHeight = window.innerHeight;
	}
	else
	if(document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		//IE 6+ in 'standards compliant mode'
		myWidth = document.documentElement.clientWidth;
		myHeight = document.documentElement.clientHeight;
	}
	else
	if(document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		//IE 4 compatible
		myWidth = document.body.clientWidth;
		myHeight = document.body.clientHeight;
	}
	//window.alert( 'Width = ' + myWidth );
	//window.alert( 'Height = ' + myHeight );
	return [myWidth, myHeight];
}

//</SCRIPT>
