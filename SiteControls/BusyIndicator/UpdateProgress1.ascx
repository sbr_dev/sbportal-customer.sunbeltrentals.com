<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UpdateProgress1.ascx.cs" Inherits="SiteControls_BusyIndicator_UpdateProgress1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>

<cc1:RoundedCornerPanel ID="rcpBusy" runat="server" SkinId="rcpBlackNoTitle">
	<table id="tlbBusy" runat="server" cellpadding="5" cellspacing="0" border="0">
	<tr>
		<td>
			<asp:Image ID="imgBusy" runat="server" ImageUrl="SunbeltGear_Rotating.gif" />
		</td>
		<td>
			<asp:Label ID="lblText" runat="server" Text="Please&nbsp;Wait..."></asp:Label>
		</td>
	</tr>
	</table>
</cc1:RoundedCornerPanel>


