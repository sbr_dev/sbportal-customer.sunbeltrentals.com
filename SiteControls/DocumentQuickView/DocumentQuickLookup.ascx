﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentQuickLookup.ascx.cs" Inherits="Test_DocumentQuickLookup" %>
<%@ Register src="~/SiteControls/QueryControl/QueryControl.ascx" tagname="QueryControl" tagprefix="uc1" %>
<%@ Register src="../QueryAccountControl/QueryAccountControl.ascx" tagname="QueryAccountControl" tagprefix="uc2" %>

<table cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <uc2:QueryAccountControl ID="QueryControl1" runat="server" QueryUrl="~/SiteControls/DocumentQuickView/QueryInvoiceContracts.aspx" 
                Width="10em" ClientOnQuery="_DQVL.ContractInvoiceOnQuery" FontSize="9pt" SearchText="" SearchPadding="1px 10px" 
                ValueCssClass="SuggestValueText" ClientCallServer="_DQVL_CheckInvoiceNumber" ClientCallBack="_DQVL.ReceiveInvoiceData" 
                ClientErrorCallBack="_DQVL.OnServerError" ClientOnGoClick="_DQVL.OnGoClick" />
        </td>
    </tr>
    <tr>
    <td style="padding-left: 5px; vertical-align: top; font-style: italic; font-size: 8pt;">Enter Contract# or Invoice#</td>
    </tr>
</table>
<script type="text/javascript" language="javascript">
//<![CDATA[

var _DQVL = {

    ContractInvoiceOnQuery:function(sQuery, bShowMore)
    {
        //alert("ContractInvoiceOnQuery(&accountnumber=" + SBNet.Account.Number + "&iscorplink=" + SBNet.Account.IsCorpLink + ")");
        if (sQuery.length > 3)
	        return "&accountnumber=" + SBNet.Account.Number+ "&iscorplink=" + SBNet.Account.IsCorpLink;
	    else
	        return null;
    },

	OnGoClick:function()
	{
		var textBox = document.getElementById("<% =QueryControl1.TextBoxControl.ClientID %>");
		if(textBox.value.length > 0)
		{
			_DQVL_CheckInvoiceNumber(textBox.value, "CHECK");
		}
		return false;
	},

	ReceiveInvoiceData:function(rvalue, context)
	{
		if(context == "CHECK")
		{
			if(rvalue.length == 0)
			{
				SBNet.Util.ConfirmBox("Document Lookup", "The document number is invalid.  Please verify that this document exists.", null, null, "okonly");
			}
			else
			{
                window.open(rvalue);
			}
		}
		else
			alert("Unknown context: " + context + " - " + rvalue);
	},

	OnServerError:function(message, context)
	{
		alert(message);
	}
}

//]]>
</script>

