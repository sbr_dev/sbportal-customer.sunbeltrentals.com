﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using SBNet;
using Sunbelt.Security;
using Sunbelt.Tools;

public partial class Test_DocumentQuickLookup : Sunbelt.Web.UserControl
{
    public SBNet.AccountEventArgs accountEventArgs
    {
        get { return (SBNet.AccountEventArgs)GetViewState("account", null); }
        set { SetViewState("account", value); }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        QueryControl1.ValidateTextBoxValue = CheckInvoiceNumber;
        //Master.AccountChanged += ChangeAccount;
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Request.QueryString["accountnumber"] != null)
        {
            QueryControl1.QueryUrl = ResolveUrl("~/SiteControls/DocumentQuickView/QueryInvoiceContracts.aspx?accountnumber="
                + accountEventArgs.AccountNumber.ToString() + "&iscorplink=" + accountEventArgs.IsCorpLink.ToString());
        }
    }

    protected string CheckInvoiceNumber(string InvoiceContractNumber)
    {
        //Set the session cache key
        string encryption = QueryControl1.Text + DateTimeTools.ToMilitaryTimeString(DateTime.Now);

        //Set the wcan secure string passed in the url
        SecureQueryString sqs = new SecureQueryString();
        sqs.Add("pdfviewer", HttpUtility.UrlDecode(encryption));
        string wcan = sqs.ToString();

        PDFView pv = new PDFView();
        pv.AccountNumber = accountEventArgs.AccountNumber;
        pv.IsCorpLink = accountEventArgs.IsCorpLink;

        if (InvoiceContractNumber.Contains("(I)"))
        {
            int i = InvoiceContractNumber.IndexOf('-');
            pv.InvoiceContractNumber = ConvertTools.ToInt32(InvoiceContractNumber.Remove(i), 0);
            string strValue2 = InvoiceContractNumber.Remove(0, i + 1);
            strValue2 = strValue2.Remove(4);
            pv.Sequence = ConvertTools.ToInt32(strValue2.TrimStart('0'), 0);

            //Set the session cache
            WebTools.SetSessionCache(encryption, pv, new TimeSpan(0, 5, 0));
        }
        else if (InvoiceContractNumber.Contains("(C)"))
        {
            int i = InvoiceContractNumber.IndexOf('-');
            pv.InvoiceContractNumber = ConvertTools.ToInt32(InvoiceContractNumber.Remove(i), 0);
            string strValue2 = InvoiceContractNumber.Remove(0, i + 1);
            pv.Sequence = ConvertTools.ToInt32(strValue2.Remove(4), 0);

            //Set the session cache
            WebTools.SetSessionCache(encryption, pv, new TimeSpan(0, 5, 0));
        }
        else //If you just put in the invoice or contract number.
        {
            int i = InvoiceContractNumber.IndexOf('-');
            if (i != -1)
            {
                pv.InvoiceContractNumber = ConvertTools.ToInt32(InvoiceContractNumber.Remove(i), 0);
                pv.Sequence = ConvertTools.ToInt32(InvoiceContractNumber.Remove(0, i + 1));
            }
            else
            {
                pv.InvoiceContractNumber = ConvertTools.ToInt32(InvoiceContractNumber, 0);
                pv.Sequence = 0;
            }

            //Set the session cache
            WebTools.SetSessionCache(encryption, pv, new TimeSpan(0, 5, 0));
        }

        if (PDFView.DoesUserHaveAccessToInvoiceOrContract(pv.AccountNumber, pv.IsCorpLink, pv.InvoiceContractNumber, pv.IsQuote))
        {
            string url = ResolveUrl("~/AccountTools/PDFViewer.aspx?wcan=" + wcan);
            return url;
        }
        else
            return "";
    }

    protected void btnDocumentQuickView_Click(object sender, ImageClickEventArgs e)
    {

    }
}
