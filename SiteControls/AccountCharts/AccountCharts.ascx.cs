using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;  // Color

using dotnetCHARTING;
using Sunbelt.Security;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;   //Aging Summary
using Reports;


public partial class controls_useragingsummary : Sunbelt.Web.UserControl, SBNet.IWebControl
{
	public TimeSpan LoadTime;
	
	private SBNet.AccountEventArgs AccountArgs
    {
        get { return (SBNet.AccountEventArgs) GetViewState("AccountEventArgs", null ); }
        set { SetViewState("AccountEventArgs", value); }
    }
	private InvoiceAging InvoiceAgingList
	{
		get { return (InvoiceAging)WebTools.GetSessionCache("UAGE_list", null); }
		set { WebTools.SetSessionCache("UAGE_list", value, new TimeSpan(0, Session.Timeout, 0)); }
	}
	private int AgingAccountNumber
	{
		get { return (int)Convert.ToInt32(WebTools.GetSessionCache("UAGE_account", 0)); }
		set { WebTools.SetSessionCache("UAGE_account", value, new TimeSpan(0, Session.Timeout, 0)); }
	}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }
        Chart.TempDirectory = ResolveUrl(ConfigurationManager.AppSettings["TempImageDirectory"]);
    }

    private void LoadAgingSummaryChart()
    {
        try
        {
            // Chart create and define
            Chart.TempDirectory = ResolveUrl(ConfigurationManager.AppSettings["TempImageDirectory"]);
            Chart.CleanupPeriod = Convert.ToInt32(ConfigurationManager.AppSettings["Chart_CleanupPeriod"]);

            Chart.Type = ChartType.Donut;
            Chart.DonutHoleSize = 35;
            Chart.Background.Color = System.Drawing.Color.LightGray;
            Chart.ChartArea.ClearColors();
            Chart.Use3D = true;
            Chart.Width = 238;
            Chart.Height = 313;

            // Legend positioning and formatting
            Chart.LegendBox.ListTopToBottom = true;

            Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            Chart.LegendBox.Template = "%Icon%Name";  // display only icon(color) and name of each series name


            // Lbels for pie segments labels
            Chart.DefaultElement.ShowValue = true;
            Chart.PieLabelMode = PieLabelMode.Inside;

            // Data load into series
            SeriesCollection mySC = LoadAgingSummaryData();

            // Series load in chart collection
            Chart.SeriesCollection.Add(mySC);

            // Add Click events for pie segments only if data exists.
            if (AccountArgs != null)  // Account does exist for user
            {
                // Add hotspot linking only if TotalAmount is not zero.  If zero we force doughnut to 100% without hotspot linking.
                if( InvoiceAgingList.TotalAmount != 0 )
                {
					//Make a secure query string to passs the account info with.
					SecureQueryString sqs = new SecureQueryString();
					sqs.ExpireTime = DateTime.Now.AddHours(8);
					sqs["Acnt"] = AccountArgs.AccountNumber.ToString();
					sqs["IsCL"] = AccountArgs.IsCorpLink.ToString();

					//the wcan url parameteter will get pass to the chart object. The chart object
					// uses % signs, so remove them from wcan.
					string wcan = sqs.ToString().Replace("%", "_p");

					Chart.DefaultElement.Hotspot.URL = ResolveClientUrl("~/AccountReports/ReportBuilder/InvoiceAging.aspx") + "?wcan=" + wcan + "&AgingDays=%Name";
                    
                    Chart.DefaultElement.Hotspot.URLTarget = "_blank";
                    Chart.DefaultElement.Hotspot.ToolTip = "%Name: $%Value";
                }
            }

        }
        catch (Exception ex)
        {
            ex.Source="controls_useragingsummary:LoadAgingSummaryChart()";
        }
    }

    private SeriesCollection LoadAgingSummaryData()
    {
        string[] chartCategoriesArr ={ "Current", "30", "60", "90" };
        int[] chartValuesArr = { 1, 1, 1, 1 };  // Testing values which are reloaded from GetCustomerInvoiceAging()
        Color[] donutColors = { Color.Green, Color.Yellow, Color.Orange, Color.Red };

        // if TotalAmount is 0 then account is paid in full and adjust the map to show 100%
        if (InvoiceAgingList.TotalAmount == 0)
            InvoiceAgingList.CurrentAmount = 1;

        chartValuesArr[0] = (Int32)InvoiceAgingList.CurrentAmount;
        chartValuesArr[1] = (Int32)InvoiceAgingList.Over30DaysAmount;
        chartValuesArr[2] = (Int32)InvoiceAgingList.Over60DaysAmount;
        chartValuesArr[3] = (Int32)InvoiceAgingList.Over90DaysAmount;

        SeriesCollection SC = new SeriesCollection();
        Series s = new Series();
        for (int ii = 0; ii < chartValuesArr.Length; ii++)
        {
            Element e = new Element();
            e.Name = chartCategoriesArr[ii];
            e.YValue = chartValuesArr[ii];
            e.Color = donutColors[ii];
            s.Elements.Add(e);
            SC.Add(s);
        }
        // Required inorder to get the hotspot links to work.
        Chart.Type = ChartType.Donuts;  

        return SC;
    }

	#region IWebControl Members

	public void UpdateData(SBNet.AccountEventArgs e)
	{
        if( !IsPostBack || (AccountArgs == null) || (AccountArgs.AccountNumber != e.AccountNumber))
        {
            if (e.AccountNumber != 0)
            {
                try
                {
					DateTime start = DateTime.Now;

                    if( !IsPostBack || InvoiceAgingList == null ||
						e.AccountNumber != AgingAccountNumber)
                        InvoiceAgingList = Invoices.GetCustomerInvoiceAging(e.AccountNumber, e.IsCorpLink);

					DateTime end = DateTime.Now;
					LoadTime = end - start;

					AccountArgs = e;
					AgingAccountNumber = e.AccountNumber;
                }
                catch (Exception /*ex*/) { }
            }
            else
            {
                if (InvoiceAgingList == null)
                    InvoiceAgingList = new InvoiceAging();
                InvoiceAgingList.CurrentAmount = 0;
                InvoiceAgingList.Over30DaysAmount = 0;
                InvoiceAgingList.Over60DaysAmount = 0;
                InvoiceAgingList.Over90DaysAmount = 0;
            }
        }
        LoadAgingSummaryChart();
    }

	#endregion
}
