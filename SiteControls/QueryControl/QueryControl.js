﻿// JScript File

var _QueryControlList = new Array();
var _QueryControlNextID  = 0;

function _QueryControl_getSpanById(sID, oParent)
{
	var coll = oParent.getElementsByTagName("SPAN");
	for(var i = 0; i < coll.length; i++) 
	{
		if(coll[i].id == sID)
		{
			return coll[i];
		}
	}
	return null;      
}

function _QueryControl_Node(name, value)
{
    this.name = name;
    this.value = value;
}

function _QueryControl_doQuery(qcID)
{
	//alert("_QueryControl_doQuery('" + qcID + "')");
	var qc = _QueryControlList[qcID];
	//alert(qc);

	var text = SBNet.EC.entryTrim(qc.tbText.value);
	//alert(text);
	
	//check cache.
    var cacheResult = qc.cache[text];
    if(cacheResult)
    {
		//alert("In cache!");
		qc.showQueryDiv(cacheResult);
		return;
	}

	if(qc.xmlHttp)
		qc.xmlHttp.abort();
	qc.xmlHttp = null;
	
	var xmlHttp = null;
	if(window.ActiveXObject) // code for IE
	{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else // code for Mozilla, Firefox, Opera, etc.
	if(XMLHttpRequest)
	{
		xmlHttp = new XMLHttpRequest();
	}
	
	//alert(xmlHttp);
	if(xmlHttp)
	{
		qc.xmlHttp = xmlHttp;
		// What do we do when the response comes back?
		xmlHttp.onreadystatechange =
		function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status == 200)
				{
					//alert(xmlHttp.responseText);
					//alert(xmlHttp.responseXML);
					//NOTE: Can not use the xmlHttp.responseXML object.
					// It is null in some browsers.
					var xobj = new SBNet.XML();
					var xdoc = xobj.LoadXML(xmlHttp.responseText);
					//alert(xdoc);
					
					var itemList = xdoc.getElementsByTagName("item");
					var items = new Array();
					if(itemList.length > 0)
					{
						for(var i = 0; i < itemList.length; i++)
						{
							var nodes = new Array();
							
							for(var j = 0; j < itemList.item(i).childNodes.length; j++)
							{
								var node = itemList.item(i).childNodes[j];
								var value = "";
								if(node.text != null) //IE
									value = node.text;
								else          //Others
									value = node.textContent;
								
								nodes[nodes.length] = new _QueryControl_Node(node.tagName, value);
							}
							items[items.length] = nodes;
						}
						//alert(items.length);
						qc.addToCache(text, items);
					}
					if(qc.xmlHttp != null)
						qc.showQueryDiv(items);
				}
				else
				if(xmlHttp.status != 0)
				{
					alert("Problem retrieving XML data. xmlHttp.status = " + xmlHttp.status);
				}
				qc.xmlHttp = null;
			}
		}
		var sAddedQuery = "";
		if(qc.clientOnQuery)
			sAddedQuery = qc.clientOnQuery(text, qc.showMoreMode);	
	
		//If this is null the user request no query.
		if(sAddedQuery != null)
		{
			"?search=" + text;
			
			xmlHttp.open("GET", qc.queryUrl + "?search=" + encodeURIComponent(text) + sAddedQuery, true);
			qc.showSearchDiv();
			xmlHttp.send(null);

			//Reset master page session timeout.
			if(typeof(masterPage) != "undefined")
				masterPage.RestartTimer();
		}
		else
			qc.showQDiv(false);

	}
}

QueryControl = function()
{
	this.cache = new Array();
	this.tbText = null;
	this.sTexNodeName = "value";
	this.hidValue = null;
	this.divList = null;
	this.divSearch = null;
	this.queryUrl = null;
	this.selectedDiv = null;
	this.mouseoverDiv = false;
	this.mouseDown = false;
	this.valueCssClass = null;
	this.noHide = false;
	this.showMoreMode = false;
	this.enableShowMore = false;
	this.searchText = "";
	this.searchPadding = "1px 1px";
	
	this.xmlHttp = null;
		
	this.clientOnQuery = null;
	this.clientOnChange = null;
	this.clientOnRowBound = null;

	this.timerID = 0;
	this.myID = "QC" + (++_QueryControlNextID);
	_QueryControlList[this.myID] = this;
}

QueryControl.prototype = {

Initialize:function(tbTextID, sTextNodeName, hidValueID, divListID, frmListID, divSearchID,
	queryUrl, bDoShowMore, sSearchText, sSearchPadding)
{
	//alert("QueryControl.Initialize()");
	this.queryUrl = queryUrl;

	//Get our controls.
	this.tbText = document.getElementById(tbTextID);
	this.hidValue = document.getElementById(hidValueID);
	this.divList = document.getElementById(divListID);
	this.frmList = document.getElementById(frmListID);	

	this.sTexNodeName = sTextNodeName;
	
	this.divSearch = document.getElementById(divSearchID);	
	this.divSearch._isInit = false;

	this.enableShowMore = bDoShowMore;
	this.searchText = sSearchText;
	this.searchPadding = sSearchPadding;
	
	this.positionQDiv();
	this.positionSDiv();

	//Add a reference to 'this' to each control.
	this.tbText._QueryControl = this;
	this.divList._QueryControl = this;

	//SBNet.Util.AttachEvent(this.tbText, "onkeydown", this.keydownHandler);
	this.tbText.onkeydown = this.keydownHandler;	
	this.tbText.onkeyup = this.keyupHandler;
	this.tbText.onblur = this.onblurHandler;	
},
	
showSearchDiv:function()
{
	if(!this.divSearch._isInit)
	{		
		var div = this.divSearch;

		div.style.display = "none";
		div.style.position = "absolute";
		div.style.padding = this.searchPadding;
		div.style.border = "#A5ACB2 1px solid";
		div.style.backgroundColor = "#F0F0F0";

		if(div.innerHTML == "")
		{
			//Create a img to show the 'searhing' 
			var img = document.createElement("IMG");
			img.align = "absmiddle";
			img.src = SBNet.Util.webAddress + "SiteControls/QueryControl/Images/progressindicator.gif";

			//Add child
			div.appendChild(img);

			if(this.searchText.length > 0)
			{
				var span = document.createElement("SPAN");
				span.style.fontSize = "7pt";
				span.innerHTML = this.searchText;

				//Add child
				div.appendChild(span);
			}
			this.divSearch._isInit = true;
		}
	}
	this.showSDiv(true);
},

showSDiv:function(bShow)
{
	if(bShow)
	{
		this.positionSDiv();
		this.divSearch.style.display = "block";
	}
	else
	{
		if(this.divSearch != null)
			this.divSearch.style.display = "none";
	}
},

positionSDiv:function()
{
	if(this.divSearch != null)
	{
		var tbLoc = SBNet.Util.GetMyWindowXY(this.tbText);
		//alert(loc);
		this.divSearch.style.position = "absolute";
		this.divSearch.style.left = tbLoc[0] + this.tbText.offsetWidth + "px";
		this.divSearch.style.top = tbLoc[1] + "px";
	}
},

showQueryDiv:function(keyValueArray)
{
	this.selectedDiv = null;
	var div = this.divList;
  
	//Remove any results that are already there.
	while(div.childNodes.length > 0)
		div.removeChild(div.childNodes[0]);
  
	//Add an entry for each of the results in the resultArray
	var rowCount = keyValueArray.length;
	if(this.enableShowMore)
	{
		if(this.showMoreMode)
			rowCount = keyValueArray.length;
		else
			rowCount = Math.min(10, keyValueArray.length);
	}

	for(var i = 0; i < rowCount; i++)
	{
		//Each item will be contained within its own DIV.
		var iDiv = document.createElement("DIV");
		iDiv.style.cursor = "pointer";
		iDiv.style.padding = "1px 2px 1px 2px";
		iDiv.style.whiteSpace = "nowrap";

		iDiv._QueryControl = this;
		iDiv.onmousedown = this.mousedownHandler;
		iDiv.onmouseup = this.mouseupHandler;
		iDiv.onmouseover = this.mouseoverHandler;
		iDiv.onmouseout = this.mouseoutHandler;

		//Add each node as a SPAN.
		var nodeList = keyValueArray[i];
		for(var j = 0; j < nodeList.length; j++)
		{
			//_QueryControl_Node object.
			var qnode = nodeList[j]; 

			var span = document.createElement("SPAN");
			span.id = qnode.name;
			if(span.innerText != null)
				span.innerText = qnode.value;				
			else
				span.textContent = qnode.value;				
			
			//Hide all but value field.
			if(qnode.name != this.sTexNodeName)
				span.style.display = "none";
			else
			if(this.valueCssClass != null)
				span.className = this.valueCssClass;

			//Add child
			iDiv.appendChild(span);
		}
		//Add child
		div.appendChild(iDiv);

		if(this.clientOnRowBound != null)
			this.clientOnRowBound(iDiv);
	}
	
	if(this.enableShowMore && !this.showMoreMode && (keyValueArray.length > rowCount))
	{
		//Each item will be contained within its own DIV.
		var iDiv = document.createElement("DIV");
		iDiv._QueryControl = this;
		iDiv.style.padding = "0px 4px 0px 0px";
		iDiv.style.whiteSpace = "nowrap";
		iDiv.style.textAlign = "right";
		iDiv.onmousedown = this.mousedownMore;
		iDiv.onmouseup = this.mousedownMore;

		var anchor = document.createElement("A");
		anchor._QueryControl = this;
		//anchor.href = "javascript:return 0;";
		anchor.href = "javascript:function returnZero(){return 0;}";
		anchor.onclick = this.onclickMore;		
		anchor.style.fontSize = "7pt";
		anchor.innerHTML = "more..." ;

		//Add child
		iDiv.appendChild(anchor);

		//Add child
		div.appendChild(iDiv);
	}

	// display the div if we had at least one result
	this.showQDiv(keyValueArray.length > 0);
},

mousedownMore:function(mouseEvent)
{
	this._QueryControl.noHide = true;

	if(window.event)
		event.cancelBubble = true;
	else
		mouseEvent.returnvalue = false;
},

onclickMore:function()
{
	this._QueryControl.showMoreMode = true;
	this._QueryControl.clearCache();
	_QueryControl_doQuery(this._QueryControl.myID);
	this._QueryControl.tbText.focus();
},

showQDiv:function(bShow)
{
	this.showSDiv(false);
	if(bShow)
	{
		this.positionQDiv();
		this.divList.style.display = "block";
		if(this.isIE6())
			this.frmList.style.display = "block";
		this.adjustFrame();
	}
	else
	{
		if(this.isIE6())
			this.frmList.style.display = "none";
		this.divList.style.display = "none";
	}
},

isQDivVisible:function()
{
	return (this.divList.style.display != "none");
},

positionQDiv:function()
{
	var loc = SBNet.Util.GetMyWindowXY(this.tbText);
	//alert(loc);
	this.divList.style.position = "absolute";
	this.divList.style.left = loc[0] + 2 + "px";
	this.divList.style.top = loc[1] + this.tbText.offsetHeight - 2 + "px";
	//Reset width.
	this.divList.style.width = "";
	
	if(this.isIE6())
	{
		this.frmList.style.position = "absolute";
		this.frmList.style.left = this.divList.style.left;
		this.frmList.style.top = this.divList.style.top;
		this.frmList.style.zIndex = this.divList.style.zIndex - 1;
	}
},

adjustFrame:function()
{
	//Make DIV as wide as INPUT if needed.
	if(this.divList.offsetWidth < this.tbText.offsetWidth)
		this.divList.style.width = this.tbText.offsetWidth + "px";

	if(this.isIE6())
	{
		this.frmList.style.width = this.divList.offsetWidth + "px";
		this.frmList.style.height = this.divList.offsetHeight + "px";
    }
},

mouseoverHandler:function()
{
	this._QueryControl.mouseoverDiv = true;
	this._QueryControl.highlightItem(this);
},

highlightItem:function(div)
{
	var qc = div._QueryControl;

	if(qc.selectedDiv)
	{
		qc.unhighlightItem(qc.selectedDiv);
		qc.selectedDiv = null;
	}
	div.style.backgroundColor = "#B2B4BF";
},

mouseoutHandler:function()
{
	this._QueryControl.mouseoverDiv = false;
	this._QueryControl.unhighlightItem(this);
},

unhighlightItem:function(div)
{
  div.style.backgroundColor = "";
},

mousedownHandler:function(mouseEvent)
{
	if(window.event)
		event.cancelBubble = true;
	else
		mouseEvent.returnvalue = false;
	this._QueryControl.mouseDown = true;
},

mouseupHandler:function(mouseEvent)
{
	this._QueryControl.selectItem(this, true);

	if(window.event)
		event.cancelBubble = true;
	else
		mouseEvent.returnvalue = false;
	this._QueryControl.mouseDown = false;
},

isIE6:function()
{
	//Match "msie 6."
	return!!navigator.userAgent.match(/msie 6\./i);
},

selectItem:function(div, bHideDiv)
{
	var qc = div._QueryControl;
	
	qc.tbText.focus();
	//qc.tbText.value = div._value;
	qc.tbText.value = qc.getSpanText(this.sTexNodeName, div);
	qc.hidValue.value = qc.getSpanText("value", div); 
	if(bHideDiv)
	{
		qc.showQDiv(false);
	}	
	else
	{
		qc.highlightItem(div);
		qc.selectedDiv = div;
	}

	if(qc.clientOnChange != null)
		qc.clientOnChange(div);
},

getSpanText:function(sName, oParent)
{
	var coll = oParent.getElementsByTagName("SPAN");
	for(var i = 0; i < coll.length; i++) 
	{
		if(coll[i].id == sName)
		{
			var span = coll[i];
			if(span.innerText != null)
				return span.innerText;
			else
				return span.textContent;
		}
	}
	return "";      
},


selectNextDiv:function()
{
	if(this.selectedDiv)
	{
		if(this.selectedDiv.nextSibling)
			this.selectItem(this.selectedDiv.nextSibling, false);
	}
	else
	{
		this.selectItem(this.divList.childNodes[0], false);
	}
},

selectPreviousDiv:function()
{
	if(this.selectedDiv)
	{
		if(this.selectedDiv.previousSibling)
			this.selectItem(this.selectedDiv.previousSibling, false);
	}
	else
	{
		this.selectItem(this.divList.childNodes[this.divList.childNodes.length - 1], false);
	}
},

addToCache:function(text, keyValueList)
{
	this.cache[text] = keyValueList;
},
	
clearCache:function()
{
	delete this.cache;
	this.cache = new Array();
},

onblurHandler:function()
{
	var qc = this._QueryControl;

	if(!qc.noHide)
	{
		if(!qc.mouseoverDiv)
		{
			if(qc.xmlHttp)
				qc.xmlHttp.abort();
			qc.xmlHttp = null;	
			
			qc.showQDiv(false);
		}
	}
	qc.noHide = false;
	
	//This code will try to find the text in the cache
	// and fill the 'hidValue' field.
	//If the mouse is being pressed or their already
	// a value in 'hidValue' ignore this.
	if(!qc.mouseDown && (qc.hidValue.value == ""))
	{
		var text = qc.tbText.value;
		var keyValueArray = qc.cache[text];
		if(keyValueArray)
		{
			var rowCount = keyValueArray.length;
			for(var i = 0; i < rowCount; i++)
			{
				var value = "";
				var name = "";
				var nodeList = keyValueArray[i];
				for(var j = 0; j < nodeList.length; j++)
				{
					//_QueryControl_Node object.
					var qnode = nodeList[j]; 
					
					if(qnode.name == "value")
						value = qnode.value;

					if((qnode.name == qc.sTexNodeName) &&
						(qnode.value == text))
						name = qnode.value;
				
					//If have a name and a value set 'hidValue'.
					if(name != "" && value != "")
					{
						qc.hidValue.value = value;
						return;	
					}
				}
			}
		}
	}
},

keydownHandler:function(keyEvent)
{
	//alert("QueryControl.keydownHandler()");
	var oControl = this;
	var qc = oControl._QueryControl;
	
	//Kill timer if running.
	if(qc.timerID != 0)
		clearTimeout(qc.timerID);
	qc.timerID == 0;

	//Don't do anything if the div is hidden
	if(qc.divList.style.display == "none")
		return true;

	var key = SBNet.EC.GetKeyCode(keyEvent);
	//alert("key = " + key + " (" + String.fromCharCode(key) + ")");

	 //Handle only these keys:
	 // UpArrow = 38, DownArrow = 40, Enter = 13, Tab = 9;
	if ((key != 38) && (key != 40) && (key != 13) && (key != 9))
		return true;

	if(key == 40)
		qc.selectNextDiv();
	else
	if(key == 38)
		qc.selectPreviousDiv();
	else
	if((key == 13) || (key == 9))
	{
		if(qc.isQDivVisible())
		{
			qc.showQDiv(false);
			if(key == 13)
			{
				if(window.event)
					event.cancelBubble = true;
				else
					keyEvent.returnvalue = false;
				return false;
			}
		}
	}
	return true;
},

keyupHandler:function(keyEvent)
{
	var oControl = this;
	var qc = oControl._QueryControl;

	//Kill timer if running.
	if(qc.timerID != 0)
		clearTimeout(qc.timerID);
	qc.timerID == 0;

	var key = SBNet.EC.GetKeyCode(keyEvent);
	//alert("key = " + key + " (" + String.fromCharCode(key) + ")");

	 //Ingnor these keys:
	 // UpArrow = 38, DownArrow = 40, Tab = 9;
	if ((key == 38) || (key == 40) || (key == 13) || (key == 9))
		return true;

	qc.hidValue.value = "";
	
	var text = SBNet.EC.entryTrim(qc.tbText.value);
	if(text.length > 0)
		qc.timerID = setTimeout("_QueryControl_doQuery('" + qc.myID + "')", 500);
	else
		qc.showQDiv(false);

	return true;
}

}