using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

public partial class SiteControls_QueryControl : Sunbelt.Web.UserControl
{
	public TextBox TextBoxControl
	{
		get { return tbText; }
	}
	public string TextBoxNodeName
	{
		set { SetViewState("tbnn", value); }
		get { return Convert.ToString(GetViewState("tbnn", "value")); }
	}
	public HiddenField ValueControl
	{
		get { return hidValue; }
	}
	public int MaxLength
	{
		set { tbText.MaxLength = value; }
		get { return tbText.MaxLength; }
	}
	public Unit Width
	{
		set { tbText.Width = value; }
		get { return tbText.Width; }
	}
	public string Text
	{
		set { tbText.Text = value; }
		get { return tbText.Text; }
	}
	public string FontSize
	{
	    set { tbText.Font.Size = (FontUnit)TypeDescriptor.GetConverter(typeof(FontUnit)).ConvertFromString(value); }
	    get { return (string)TypeDescriptor.GetConverter(typeof(FontUnit)).ConvertToString(tbText.Font.Size); }
	}
	public int Columns
	{
	    set { tbText.Columns = value; }
	    get { return tbText.Columns; }
	}
	public bool IsRequired
	{
		set
		{
			rfvText.Enabled = value;
			rfvText.Visible = value;
		}
		get { return rfvText.Enabled; }
	}
	public string ErrorMessage
	{
		set
		{
			rfvText.ErrorMessage = value;
			rfvText.ToolTip = value;
		}
		get { return rfvText.ErrorMessage; }
	}
	public string QueryUrl
	{
		set { SetViewState("qurl", value); }
		get { return (string)GetViewState("qurl", ""); }
	}
	public string ValueCssClass
	{
		set { SetViewState("valcss", value); }
		get { return (string)GetViewState("valcss", ""); }
	}
	public string ClientOnQuery
	{
		set { SetViewState("onquery", value); }
		get { return (string)GetViewState("onquery", ""); }
	}
	public string ClientOnChange
	{
		set { SetViewState("onchange", value); }
		get { return (string)GetViewState("onchange", ""); }
	}
	public string ClientOnRowBound
	{
		set { SetViewState("onbound", value); }
		get { return (string)GetViewState("onbound", ""); }
	}
    public bool ReadOnly
    {
        set { tbText.ReadOnly = value; }
        get { return tbText.ReadOnly; }
    }
    public bool DoShowMore
    {
        set { SetViewState("showMore", value); }
        get { return (bool)GetViewState("showMore", false); }
    }
	public string SearchText
	{
		set { SetViewState("searchText", value); }
		get { return (string)GetViewState("searchText", ""); }
	}
	public string SearchPadding
	{
		set { SetViewState("padding", value); }
		get { return (string)GetViewState("padding", ""); }
	}
	public int ZIndex
	{
		set { SetViewState("ZIndex", value); }
		get { return (int)GetViewState("ZIndex", 10000); }
	}

	protected void Page_Load(object sender, EventArgs e)
    {
		//Turn off auto complete.
		tbText.Attributes["autocomplete"] = "off";
		
		frmList.Attributes["src"] = ResolveUrl(AppRelativeTemplateSourceDirectory + "Blank.htm");
		frmList.Visible = isIE6();
    }

	public bool isIE6()
	{
		return (HttpContext.Current.Request.Browser.Browser == "IE" &&
			HttpContext.Current.Request.Browser.MajorVersion == 6);
	}

    protected override void OnPreRender(EventArgs e)
    {
        SBNet.Tools.IncludeEntryControlsClientScript(Page);
        SBNet.Tools.IncludeXMLClientScript(Page);

		divList.Style["z-index"] = (ZIndex + 1).ToString();
		divSearch.Style["z-index"] = (ZIndex + 2).ToString();
		
		//Register the EntryControl.js file.
        if (!Page.ClientScript.IsClientScriptIncludeRegistered("QueryControl.js"))
            Page.ClientScript.RegisterClientScriptInclude("QueryControl.js", ResolveUrl("QueryControl.js"));

        string qcName = "qc" + ClientID;
        if (!Page.ClientScript.IsStartupScriptRegistered(qcName))
        {
            System.Text.StringBuilder sbCode = new System.Text.StringBuilder();

            sbCode.Append("var " + qcName + " = new QueryControl();\r\n");
            sbCode.Append(qcName + ".Initialize(");
				sbCode.Append("\"" + tbText.ClientID + "\"");
				sbCode.Append(", \"" + TextBoxNodeName + "\"");
				sbCode.Append(", \"" + hidValue.ClientID + "\"");
				sbCode.Append(", \"" + divList.ClientID + "\"");
				sbCode.Append(", \"" + frmList.ClientID + "\"");
				sbCode.Append(", \"" + divSearch.ClientID + "\""); 
				sbCode.Append(", \"" + Sunbelt.Tools.WebTools.ToWebAddress(QueryUrl) + "\"");
				sbCode.Append(", " + DoShowMore.ToString().ToLower()); 
				sbCode.Append(", \"" + SearchText + "\"");
				sbCode.Append(", \"" + SearchPadding + "\"");
				sbCode.Append(");\r\n");
            if (!string.IsNullOrEmpty(ValueCssClass))
                sbCode.Append(qcName + ".valueCssClass = \"" + ValueCssClass + "\";\r\n");

			if (!string.IsNullOrEmpty(ClientOnQuery))
				sbCode.Append(qcName + ".clientOnQuery = " + ClientOnQuery + ";\r\n");

			if (!string.IsNullOrEmpty(ClientOnChange))
                sbCode.Append(qcName + ".clientOnChange = " + ClientOnChange + ";\r\n");

            if (!string.IsNullOrEmpty(ClientOnRowBound))
                sbCode.Append(qcName + ".clientOnRowBound = " + ClientOnRowBound + ";\r\n");

            Page.ClientScript.RegisterStartupScript(this.GetType(), qcName, sbCode.ToString(), true);
        }

        base.OnPreRender(e);
    }
}
