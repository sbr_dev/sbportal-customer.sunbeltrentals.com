<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QueryControl.ascx.cs" Inherits="SiteControls_QueryControl" %>
<asp:TextBox ID="tbText" runat="server" MaxLength="30"></asp:TextBox><asp:RequiredFieldValidator ID="rfvText" runat="server" ControlToValidate="tbText"
	ErrorMessage="Field is required" Display="Dynamic" Enabled="False"><asp:Image ID="imgWarn" runat="server" ImageUrl="~/SiteControls/QueryControl/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Field is required" /></asp:RequiredFieldValidator>
<div id="divList" runat="server"  onmousedown="javascript:event.cancelBubble = true" style="display:none; border-right: #000000 1px solid; border-top: #b2b2b2 1px solid; border-left: #b2b2b2 1px solid; border-bottom: #000000 1px solid; background-color: #ffffff;"></div>
<iframe id="frmList" runat="server" src="Blank.htm" frameborder="0" scrolling="no" style="display:none"></iframe>
<div id="divSearch" runat="server" style="display:none;"></div>
<asp:HiddenField ID="hidValue" runat="server" />

