﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Sunbelt.Tools;
using System.Collections.Generic;

public partial class SiteControls_AdditionalEmailRecipients : Sunbelt.Web.UserControl
{
    public string Emails
    {    // this needs to jive with the Session Cache name in the SiteVisitor Cart code page
        get { return (string)WebTools.GetSessionCache("AdditionalEmailRecipients_Emails", ""); }
        set { WebTools.SetSessionCache("AdditionalEmailRecipients_Emails", value, new TimeSpan(0, Session.Timeout, 0)); }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        BindToEmailRepeater(Emails);
    }

    protected void btnEmailAdd_Click(object sender, ImageClickEventArgs e)
    {
        if (txtEmailAddress.Text == string.Empty)
        {
            cvEmailAddressMissing.ErrorMessage = "Email address is missing";
            cvEmailAddressMissing.IsValid = false;
        }
        else if( Emails.IndexOf(txtEmailAddress.Text) < 0)
        {
            Emails = txtEmailAddress.Text + (Emails.Length > 0 ? "," : "") + Emails;
            txtEmailAddress.Text = string.Empty;
        }
        else
        {
            cvEmailAddressDuplicate.ErrorMessage = "Email address already exists";
            cvEmailAddressDuplicate.IsValid = false;
        }

        BindToEmailRepeater(Emails);
    }
    protected void lnkClearAll_Click(object sender, EventArgs e)
    {
        Emails = string.Empty;
        txtEmailAddress.Text = string.Empty;
    }

    public static List<string> SpecialSplit30(string s)
    {
        if (s == string.Empty)
            return null;

        return new List<string>(from part in s.Split(',')
                                select
                                    part.Trim());
    }
    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        RemoveEmailAddress(e.Item.ItemIndex);
    }

    private void RemoveEmailAddress( int repeaterItemIndex )
    {
        int index = -1;
        string newEmails = string.Empty;

        string[] emails = Emails.Split(',');
        for ( index = 0; index < emails.Length; index++)
        {
            if (index == repeaterItemIndex)
                continue;
            newEmails += (newEmails.Length > 0 ? ", " : "") + emails[index];
        }
        Emails = newEmails;

        BindToEmailRepeater(Emails);
    }
    private void BindToEmailRepeater(string emails)
    {
        if (Emails.Length > 0)
            Repeater1.DataSource = SpecialSplit30(Emails).ToArray();
        else
            Repeater1.DataSource = null;
        Repeater1.DataBind();
    }

}
