﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdditionalEmailRecipients.ascx.cs" Inherits="SiteControls_AdditionalEmailRecipients" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<table border="0" style="text-align: left; width: 540px; font-size: 8pt;" cellpadding="0" cellspacing="0" id="tblEmailAddress" runat="server">
<tr valign="top">
    <td style="width:205px; height: 28px;" valign="middle">
        <asp:TextBox ID="txtEmailAddress" runat="server" Width="197px" Visible="true" />
        <Ajax:TextBoxWatermarkExtender ID="AjaxProjectTitle" TargetControlID="txtEmailAddress" runat="server" WatermarkCssClass="WatermarkText" WatermarkText="Enter Email Address"></Ajax:TextBoxWatermarkExtender>
    </td>
    <td valign="middle">
        <asp:ImageButton ID="btnEmailAdd" runat="server" ImageUrl="Images/button_add.png" onclick="btnEmailAdd_Click" />
    </td>
</tr>
<tr>
    <td colspan="2">
        <asp:CustomValidator ID="cvEmailAddressMissing" ClientValidationFunction="" runat="server"  ErrorMessage="Email address is missing" Display="Dynamic" />
        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailAddress" Display="Dynamic" ErrorMessage="Email adddress entered is not valid" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">	</asp:RegularExpressionValidator>         
        <asp:CustomValidator ID="cvEmailAddressDuplicate" ClientValidationFunction="" runat="server"  ErrorMessage="Email address already exists" Display="Dynamic" />
    </td>
</tr>
</table>
<asp:Repeater ID="Repeater1" runat="server" onitemcommand="Repeater1_ItemCommand">
    <ItemTemplate><b></ItemTemplate>
    <ItemTemplate>
    <table>
    <tr>
        <td style="width:50px"><%# Container.ItemIndex == 0 ? "<b>Added:&nbsp;<b/>" : "" %></b></td>
        <td style="width:160px"><%# Container.DataItem %><br /></td>
        <td style="width:5px"><b><asp:LinkButton ID="lnkDeleteEmail" runat="server"><b>X</b></asp:LinkButton></td>
    </tr></table>
    </ItemTemplate>
</asp:Repeater>
