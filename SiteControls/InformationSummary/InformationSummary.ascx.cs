using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteControls_InformationSummary : System.Web.UI.UserControl
{
	private List<string> infoList;

	protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
		}
    }

	public void AddInfo(string text)
	{
		if (infoList == null)
			infoList = new List<string>();

		infoList.Add(text);
	}

	protected override void OnPreRender(EventArgs e)
	{
		if (infoList == null)
			infoList = new List<string>();

		divInfoSummary.Visible = (infoList.Count > 0);
		
		repInfo.DataSource = infoList;
		repInfo.DataBind();

		base.OnPreRender(e);
	}
	protected void repInfo_ItemDataBound(object sender, RepeaterItemEventArgs e)
	{
		if (e.Item.ItemType == ListItemType.Item ||
			e.Item.ItemType == ListItemType.AlternatingItem)
		{
			Label lb = (Label)e.Item.FindControl("lbInfo");

			lb.Text = (string)e.Item.DataItem;
		}
	}
}
