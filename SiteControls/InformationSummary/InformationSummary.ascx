<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InformationSummary.ascx.cs" Inherits="SiteControls_InformationSummary" %>
<div id="divInfoSummary" runat="server" style="color: #404040;">
	<ul>
		<asp:Repeater ID="repInfo" runat="server" OnItemDataBound="repInfo_ItemDataBound">
			<ItemTemplate>
				<li><asp:Label ID="lbInfo" runat="server" Text="[lbInfo]"></asp:Label></li>
			</ItemTemplate>
		</asp:Repeater>
	</ul>
</div>