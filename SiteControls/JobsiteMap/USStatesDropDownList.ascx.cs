﻿using System;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


[ValidationPropertyAttribute("SelectedValue")]
public partial class SiteControls_JobsiteMap_USStates : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			if (shortStateName)
			{
				// ----------------------------------------------------
				// If the ShortStateName property is set to true
				// use the value as the display text. E.g. NC
				// ----------------------------------------------------
				foreach (ListItem theItem in StateDropDown.Items)
				{
					theItem.Text = theItem.Value;
				}
			}
		}
    }
    public string SelectedValue
    {
        set { StateDropDown.SelectedValue = value; }
        get { return StateDropDown.SelectedValue; }
    }

    public string SelectedItem
    {
        get { return StateDropDown.SelectedItem.Text; }
    }

    private bool shortStateName = false;
    public bool ShortStateName
    {
        get { return shortStateName; }
        set { shortStateName = value; }
    }

	public string CssClass
    {
        get { return StateDropDown.CssClass; }
        set { StateDropDown.CssClass = value; }
    }

    public DropDownList StateList
    {
        get { return StateDropDown; }

    }

    public bool UseValidator
	{
		get { return rfvStateDropDown.Visible; }
		set { rfvStateDropDown.Visible = value; }
	}

    public RequiredFieldValidator ValidatorControl
	{
		get { return rfvStateDropDown; }
	}

	public Image ValidatorImage
	{
		get { return imgStateDropDown; }
	}
}
