﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="USStatesDropDownList.ascx.cs" Inherits="SiteControls_JobsiteMap_USStates" %>
<asp:DropDownList ID="StateDropDown" runat="server">
	<asp:ListItem Text="--" Value="" />
	<asp:ListItem Text="Alabama" Value="AL" /> 
	<asp:ListItem Text="Alaska" Value="AK" />
	<asp:ListItem Text="Arkansas" Value="AR"/> 
	<asp:ListItem Text="Arizona" Value="AZ"/> 
	<asp:ListItem Text="California" Value="CA"/> 
	<asp:ListItem Text="Colorado" Value="CO"/> 
	<asp:ListItem Text="Connecticut" Value="CT"/> 
	<asp:ListItem Text="DC" Value="DC"/> 
	<asp:ListItem Text="Delaware" Value="DE"/> 
	<asp:ListItem Text="Florida" Value="FL"/> 
	<asp:ListItem Text="Georgia" Value="GA"/> 
	<asp:ListItem Text="Hawaii" Value="HI"/> 
	<asp:ListItem Text="Iowa" Value="IA"/> 
	<asp:ListItem Text="Idaho" Value="ID"/> 
	<asp:ListItem Text="Illinois" Value="IL"/> 
	<asp:ListItem Text="Indiana" Value="IN"/> 
	<asp:ListItem Text="Kansas" Value="KS"/> 
	<asp:ListItem Text="Kentucky" Value="KY"/> 
	<asp:ListItem Text="Louisiana" Value="LA"/> 
	<asp:ListItem Text="Massachusetts" Value="MA"/> 
	<asp:ListItem Text="Maryland" Value="MD"/> 
	<asp:ListItem Text="Maine" Value="ME"/> 
	<asp:ListItem Text="Michigan" Value="MI"/> 
	<asp:ListItem Text="Minnesota" Value="MN"/> 
	<asp:ListItem Text="Missouri" Value="MO"/> 
	<asp:ListItem Text="Mississippi" Value="MS"/> 
	<asp:ListItem Text="Montana" Value="MT"/> 
	<asp:ListItem Text="North Carolina" Value="NC"/> 
	<asp:ListItem Text="North Dakota" Value="ND"/> 
	<asp:ListItem Text="Nebraska" Value="NE"/> 
	<asp:ListItem Text="New Hampshire" Value="NH"/> 
	<asp:ListItem Text="New Jersey" Value="NJ"/> 
	<asp:ListItem Text="New Mexico" Value="NM"/> 
	<asp:ListItem Text="Nevada" Value="NV"/> 
	<asp:ListItem Text="New York" Value="NY"/> 
	<asp:ListItem Text="Ohio" Value="OH"/> 
	<asp:ListItem Text="Oklahoma" Value="OK"/> 
	<asp:ListItem Text="Oregon" Value="OR"/> 
	<asp:ListItem Text="Pennsylvania" Value="PA"/> 
	<asp:ListItem Text="Rhode Island" Value="RI"/> 
	<asp:ListItem Text="South Carolina" Value="SC"/> 
	<asp:ListItem Text="South Dakota" Value="SD"/> 
	<asp:ListItem Text="Tennessee" Value="TN"/> 
	<asp:ListItem Text="Texas" Value="TX"/> 
	<asp:ListItem Text="Utah" Value="UT"/> 
	<asp:ListItem Text="Virginia" Value="VA"/> 
	<asp:ListItem Text="Vermont" Value="VT"/> 
	<asp:ListItem Text="Washington" Value="WA"/> 
	<asp:ListItem Text="Wisconsin" Value="WI"/> 
	<asp:ListItem Text="West Virginia" Value="WV"/> 
	<asp:ListItem Text="Wyoming" Value="WY"/> 
</asp:DropDownList>
<asp:RequiredFieldValidator ID="rfvStateDropDown" runat="server" Visible="false" 
	ControlToValidate="StateDropDown" ErrorMessage="State is required." 
	Display="Dynamic"><asp:Image ID="imgStateDropDown" runat="server"
	ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle"
	ToolTip="State is required" /></asp:RequiredFieldValidator>
