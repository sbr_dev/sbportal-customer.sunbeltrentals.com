﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public class QueryAccountControlEventArgs : EventArgs
{
	public QueryAccountControlEventArgs(string textboxValue, int companyID=1)
	{
		TextboxValue = textboxValue;
        CompanyID = companyID;
    }
	public string TextboxValue;
    public int CompanyID;
}

public partial class SiteControls_QueryAccountControl_QueryAccountControl : Sunbelt.Web.UserControl, ICallbackEventHandler
{
	public EventHandler<QueryAccountControlEventArgs> GoClick;

	public delegate string TextBoxValueDelegate(string textBoxValue);
	public TextBoxValueDelegate ValidateTextBoxValue;
 
	public string ClientCallServer
	{
		set { SetViewState("ccs", value); }
		get { return (string)GetViewState("ccs", ""); }
	}
	public string ClientCallBack
	{
		set { SetViewState("ccb", value); }
		get { return (string)GetViewState("ccb", ""); }
	}
	public string ClientErrorCallBack
	{
		set { SetViewState("cecb", value); }
		get { return (string)GetViewState("cecb", ""); }
	}
	public string ClientOnGoClick
	{
		set { SetViewState("cogc", value); }
		get { return (string)GetViewState("cogc", ""); }
	}

	public ImageButton GoButtonControl
	{
		get { return btnGo; }
	}

	/////////////////////////////////////////
	/// From QueryControl
	public TextBox TextBoxControl
	{
		get { return qcAccount.TextBoxControl; }
	}
	public Unit Width
	{
		set { qcAccount.Width = value; }
	}
	public string Text
	{
		set { qcAccount.Text = value; }
		get { return qcAccount.Text; }
	}
	public string FontSize
	{
		set { qcAccount.FontSize = value; }
	}
	public int Columns
	{
		set { qcAccount.Columns = value; }
	}
	public string QueryUrl
	{
		set { qcAccount.QueryUrl = value; }
	}
	public string ValueCssClass
	{
		set { qcAccount.ValueCssClass = value; }
	}
	public string ClientOnQuery
	{
		set { qcAccount.ClientOnQuery = value; }
	}
	public string ClientOnChange
	{
		set { qcAccount.ClientOnChange = value; }
	}
	public string ClientOnRowBound
	{
		set { qcAccount.ClientOnRowBound = value; }
	}
	public bool DoShowMore
	{
		set { qcAccount.DoShowMore = value; }
	}
	public string SearchText
	{
		set { qcAccount.SearchText = value; }
	}
	public string SearchPadding
	{
		set { qcAccount.SearchPadding = value; }
	}
	public int ZIndex
	{
		set { qcAccount.ZIndex = value; }
	}

	/////////////////////////////////////////
		
	protected void Page_Load(object sender, EventArgs e)
    {
	}

	protected override void OnPreRender(EventArgs e)
	{
		ReqisterClentScript();

		base.OnPreRender(e);
	}

	private void ReqisterClentScript()
	{
		//Register common scripts.
		SBNet.Tools.IncludeSBNetClientScript(Page);

		if (!string.IsNullOrEmpty(ClientOnGoClick))
			btnGo.OnClientClick = "javascript:return " + ClientOnGoClick + "();";

		if (!string.IsNullOrEmpty(ClientCallServer))
		{
			ClientScriptManager cs = Page.ClientScript;

			String cbReference = cs.GetCallbackEventReference(this,
				"arg",
				ClientCallBack,
				"context",
				ClientErrorCallBack,
				false);

			String callbackScript = "function " + ClientCallServer + "(arg, context) { " + cbReference + "} ;";
			cs.RegisterClientScriptBlock(this.GetType(),
				ClientCallServer,
				callbackScript,
				true);
		}
	}

	protected void btnGo_Click(object sender, ImageClickEventArgs e)
	{
		EventHandler<QueryAccountControlEventArgs> eh = GoClick;
		if (eh != null)
			eh(sender, new QueryAccountControlEventArgs(qcAccount.Text));
	}

	#region ICallbackEventHandler Members

	private string EventArgument;
	
	public string GetCallbackResult()
	{
		if (ValidateTextBoxValue != null)
			return ValidateTextBoxValue(EventArgument);
		else
			return EventArgument;
	}

	public void RaiseCallbackEvent(string eventArgument)
	{
		EventArgument = eventArgument;
	}

	#endregion
}
