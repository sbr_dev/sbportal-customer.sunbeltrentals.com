<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPLogin.ascx.cs" Inherits="SiteControls_UserProfile_UPLogin" %>

<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc1" %>

<div id="divLogin" runat="server">
<asp:Panel ID="panLogin" runat="server" DefaultButton="btnLogin">
<cc1:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:460px;">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2" class="h2">LOG IN</td>
		</tr>
		<tr>
			<td class="left">
				Email:</td>
			<td class="right">
				<asp:TextBox ID="tbEmail" runat="server" Columns="40" MaxLength="100"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is required" ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image8" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is required" /></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email is not a valid email address"
					ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image7" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is not a valid email address" /></asp:RegularExpressionValidator>
				</td>
		</tr>
		<tr>
			<td class="left">
				Password:</td>
			<td class="right">
				<asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="tbPassword"
					ErrorMessage="Password is required" Display="Dynamic"><asp:Image ID="Image4" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Password is required" /></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revPassword" runat="server" ErrorMessage="Password is not valid" ControlToValidate="tbPassword" Display="Dynamic" ValidationExpression="[\d\w_-]{4,12}"><asp:Image ID="Image11" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Password is not valid" /></asp:RegularExpressionValidator>&nbsp;<asp:LinkButton ID="btnForgotPassword" runat="server" OnClick="btnForgotPassword_Click" CausesValidation="False">Forgot Your Password?</asp:LinkButton></td>
		</tr>
		<tr>
			<td class="left">&nbsp;</td>
			<td class="right"><asp:CheckBox ID="cbRememberMe" runat="server" Text="Remember my email" /></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>
	</cc1:RoundedCornerPanel>

	<br />
	
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2">
			<table cellpadding="0" cellspacing="0" style="width: 460px; text-align: center;">
				<tr>
					<td><asp:HyperLink ID="hlNewUser" runat="server"><b>New User?</b>&nbsp;&nbsp;<i>Register Here!</i></asp:HyperLink><%--<asp:LinkButton ID="btnNewUser" runat="server" OnClick="btnNewUser_Click" CausesValidation="False"><b>New User?</b>&nbsp;&nbsp;<i>Register Here!</i></asp:LinkButton>--%><br />
					</td>
					<td style="text-align: right; vertical-align: top;">
						<asp:ImageButton ID="btnLogin" runat="server" AlternateText="Login" ImageUrl="~/App_Themes/Main/Buttons/login_80x20_darkred.gif" ImageAlign="AbsMiddle" OnClick="btnLogin_Click" /></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

	<table cellpadding="0" cellspacing="0" style="width:460px;">
		<tr>
			<td colspan="2" class="right"><hr style="width:460px" /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</asp:Panel>
</div>

<div id="divLockedOut" runat="server">
	<cc1:RoundedCornerPanel ID="RoundedCornerPanel1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:460px;">
	Your account is locked out. Contact customer support.
	</cc1:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width:460px;">
		<tr>
			<td colspan="2" class="right"><hr style="width:460px" /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome2" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>

<div id="divNotApproved" runat="server">
	<cc1:RoundedCornerPanel ID="RoundedCornerPanel2" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:460px;">
	Your account is not activated. Contact customer support.
	</cc1:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width:460px;">
		<tr>
			<td colspan="2" class="right"><hr style="width:460px" /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome3" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>

<div id="divError" runat="server">
	<cc1:RoundedCornerPanel ID="RoundedCornerPanel3" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:460px;">
	An error has occurred. Contact customer support.
	</cc1:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width:460px;">
		<tr>
			<td colspan="2" class="right"><hr style="width:460px" /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome4" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>
