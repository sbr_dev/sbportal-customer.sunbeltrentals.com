<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPValidationSummary.ascx.cs" Inherits="UserProfile_UPValidationSummary" %>
<script type="text/javascript">
//<![CDATA[

var UPVS = {

// Method for displaying client side error messages
// in the validation summary.
DisplayError:function(errorMessage)
{
	//This code mimics how a server side validation
	// control renders itself client side.
	
	//Add a span object.
	var obj = document.createElement("SPAN");

	//Create an ID.
	var id = "errorMsg" + Page_Validators.length.toString();
	//alert(id);
	obj.id = id;
	//Add needed validation stuff.
	obj.errormessage = errorMessage;
	obj.display = "None";
	obj.isvalid = "False";
	obj.evaluationfunction = "CustomValidatorEvaluateIsValid";
	obj.clientvalidationfunction = "UPVS.DefaultValidate";
	//Add this so we know we it's one of ours.
	obj.IsVSControl = true;

	document.forms[0].appendChild(obj);
	
	//Add it to the Page_Validators list.
	Page_Validators[Page_Validators.length] = obj; 

	//Call this to hook it up.
	ValidatorOnLoad();
	
	//Make sure all the ones we have added (server and client)
	// are invalid.
	for(var i = 0; i < Page_Validators.length; i++)
	{
		var val = Page_Validators[i];
		if(val.IsVSControl == true || val.IsVSControl == "true")
		{
			//alert(val.IsVSControl + " - " + val.errormessage);
			//ValidatorEnable(val, true);
			//ValidatorValidate(val);
			//val.enabled = true;
			val.isvalid = false;
		}
	}

	//Update screen.
	ValidationSummaryOnSubmit();
	//return the id.
	return id;
},

VSC_ClearAll:function()
{
	var arr = new Array();
	
	for(var i = 0; i < Page_Validators.length; i++)
	{
		var val = Page_Validators[i];
		if(val.IsVSControl == true || val.IsVSControl == "true")
		{
			ValidatorEnable(val, false);
		}
		else
			arr[arr.length] = Page_Validators[i];
	}
	
	//Update screen.
	ValidationSummaryOnSubmit();
	Page_Validators = arr;
},

// Default custom validator call for server and client
// added errors.
DefaultValidate:function(source, arguments)
{
	//As long as there are other validation errors
	// (from validation controls) keep our added
	// errors visible, else it will keep the page
	// from posting back.
	//alert("defaultValidate");
	var bIsValid = true;
	for(var i = 0; i < Page_Validators.length; i++)
	{
		var val = Page_Validators[i];
		if(val.IsVSControl == null)
		{
			ValidatorValidate(val);
			//alert("val.id = " + val.id + "  val.isvalid = " + val.isvalid);
			if(!val.isvalid)
			{
				bIsValid = false;
				break;
			}
		}
	}
	//alert("bIsValid = " + bIsValid);
	arguments.IsValid = bIsValid;
}
}

//]]>
</script>
<asp:ValidationSummary ID="validationSummary" runat="server" />
<asp:PlaceHolder ID="phValidators" runat="server">
</asp:PlaceHolder><asp:CustomValidator ID="cvDefault" runat="server" Enabled="False"
	EnableTheming="False" EnableViewState="False" ErrorMessage="DefaultCustomValidator" ClientValidationFunction="UPVS.DefaultValidate" Display="None"></asp:CustomValidator>
