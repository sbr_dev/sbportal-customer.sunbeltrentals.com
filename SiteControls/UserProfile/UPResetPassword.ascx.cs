using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using CaptchaWebControl;


public partial class SiteControls_UserProfile_UPResetPassword : Sunbelt.Web.UserControl
{
	private enum PageState { PasswordReset, EmailSent, LockedOut, NotApproved }
	private PageState State
	{
		get { return (PageState)GetViewState("State", PageState.PasswordReset); }
		set { SetViewState("State", value); }
	}
	private string UserName
	{
		get { return (string)GetViewState("UserName", ""); }
		set { SetViewState("UserName", value); }
	}
	public bool TooManyLoginAttempts
	{
		get { return (bool)GetViewState("ToMany", false); }
		set 
		{ 
			SetViewState("ToMany", value);
			pAttemptsMsg.Visible = value;
		}
	}

	public event EventHandler<UserErrorEvent> UserErrorRasied;
	public event EventHandler<UserEvent> LogMeInClicked;

	protected void Page_Load(object sender, EventArgs e)
	{
		UpdateDisplay();
		hlHome.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
	}

	private void RaiseUserError(string message)
	{
		EventHandler<UserErrorEvent> eh = UserErrorRasied;
		if (eh != null)
			eh(this, new UserErrorEvent(message));
		else
			throw new Exception("UserErrorRasied event not handled.");
	}

	private void UpdateDisplay()
	{
		divPasswordReset.Visible = false;
		divLockedOut.Visible = false;
		divNotApproved.Visible = false;
		divEmailSent.Visible = false;
		divError.Visible = false;
		switch (State)
		{
			case PageState.PasswordReset:
				Page.Form.DefaultButton = btnResetPassword.UniqueID;
				divPasswordReset.Visible = true;
				break;
			case PageState.EmailSent:
				divEmailSent.Visible = true;
				break;
			case PageState.LockedOut:
				divLockedOut.Visible = true;
				break;
			case PageState.NotApproved:
				divNotApproved.Visible = true;
				break;
			default: //Error
				divError.Visible = true;
				break;
		}
	}

	protected void btnResetPassword_Click(object sender, ImageClickEventArgs e)
	{
		//Always check for validation!
		if (Page.IsValid)
		{
			string userName = tbEmail.Text.Trim();

			//get user profile.
			MembershipUser mu = Membership.GetUser(userName, false);
			if (mu != null && mu.IsLockedOut)
			{
				State = PageState.LockedOut;
			}
			else
				if (mu != null && !mu.IsApproved)
				{
					State = PageState.NotApproved;
				}
				else
				{
					if (mu != null)
					{
						//Try to send the email...
						State = PageState.EmailSent;
						UPSendVerifyEmail1.SendPasswordResetEmail(userName);
					}
					else
					{
#if DEBUG
						RaiseUserError("DEBUG - User not in database.");
#endif
						RaiseUserError("Email not found. Please check your email address and try again.");
					}
				}
			UpdateDisplay();
		}
	}
	
	protected void btnLogin_Click(object sender, EventArgs e)
	{
		//Switch to login view.
		EventHandler<UserEvent> eh = LogMeInClicked;
		if (eh != null)
			eh(this, new UserEvent());
		else
			throw new Exception("LogMeInClicked event not handled.");
	}
}
