<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPSendVerifyEmail.ascx.cs" Inherits="UserProfile_UPSendVerifyEmail" %>
<asp:Panel ID="panEmailSent" runat="server" Visible="false">
<p>A activation email has been sent to your email address.</p>
<p>Please follow the email instructions to activate your account.</p>
</asp:Panel>
<asp:Panel ID="panEmailError" runat="server" Visible="false">
<p>There was an error trying to send your activation email.  Please contact customer support.</p>
<asp:Label ID="lbError" runat="server"></asp:Label>
</asp:Panel>
<asp:Panel ID="panNotSend" runat="server" Visible="true">
<p>SendEmail() has not been called.</p>
</asp:Panel>