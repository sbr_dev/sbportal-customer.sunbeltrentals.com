﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPAccountType.ascx.cs" Inherits="UserProfile_UPAccountType" %>

<%@ Register src="../JobsiteMap/USStatesDropDownList.ascx" tagname="USStatesDropDownList" tagprefix="uc1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc2" %>

<script type="text/javascript">
//<![CDATA[

function ValidateDUNSNumberAndTaxId(sender, args)
{
	//alert('ValidateDUNSNumber');
	var tbDuns = document.getElementById('<% =tbDunsId.ClientID %>');
	var tbTax =  document.getElementById('<% =tbTaxId.ClientID %>');
	if(SBNet.EC.entryTrim(tbDuns.value) == "" &&
		SBNet.EC.entryTrim(tbTax.value) == "")
		args.IsValid = false;
	else
		args.IsValid = true;
}

//]]>
</script>   


<cc2:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 660px">
	<table cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td class="right h2">EXISTING CREDIT ACCOUNT?</td>
			<td class="left" style="width: 100%; text-align:right"><cc2:HelpLink ID="HelpLink1" runat="server" HelpTagName="TOP" HelpUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx" NavigateUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx" SkinID="ContentTopRight">Need Help?</cc2:HelpLink></td>
		</tr>
	</table>	
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td style="padding-top: 4px;"><asp:RadioButton ID="radCashCustomer" 
					GroupName="grpAccount" runat="server"
				Text="&nbsp;<b>I do not have</b> a credit account with Sunbelt Rentals." 
					AutoPostBack="True" oncheckedchanged="radCashCustomer_CheckedChanged" /></td>
		</tr>
		<tr>
			<td style="padding-bottom: 4px;"><asp:RadioButton ID="radCreditCustomer" 
					GroupName="grpAccount" runat="server"
				Text="&nbsp;<b>I do have</b> a credit account with Sunbelt Rentals." 
					AutoPostBack="True" oncheckedchanged="radCreditCustomer_CheckedChanged" /></td>
		</tr>
	</table>

	<div ID="divCreditCustomer" runat="server">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3" class="h3">ACCOUNT INFORMATION</td>
			</tr>
			<tr>
				<td style="font-size:smaller;">&nbsp;</td>
				<td colspan="2" class="right" style="font-size:smaller;">* Indicates a required field</td>
			</tr>
			<tr>
				<td class="left">Company Name*:</td>
				<td class="right"><asp:TextBox ID="tbCompanyName" runat="server" MaxLength="40" Columns="40"></asp:TextBox>
					<asp:RequiredFieldValidator ID="rfvCompanyName"	runat="server" ErrorMessage="Company name is required" ControlToValidate="tbCompanyName" Display="Dynamic"><asp:Image ID="Image8" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Company is required" /></asp:RequiredFieldValidator></td>
				<td rowspan="2" align="left" class="instruction" style="width: 180px; padding:4px 8px;">▪ As it appears on a Sunbelt invoice 
					or statement.</td>
			</tr>
			<tr>
				<td class="left">Account Number*:</td>
				<td class="right"><asp:TextBox ID="tbAccountNumber" runat="server" MaxLength="9"></asp:TextBox>
					<asp:RequiredFieldValidator ID="rfvAccountNumber" runat="server" ErrorMessage="Account number is required" ControlToValidate="tbAccountNumber" Display="Dynamic"><asp:Image ID="Image1" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Account is required" /></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
			    <td class="left">D&amp;B D-U-N-S Number*:
			    </td>
			    <td class="right" colspan="2"><asp:TextBox ID="tbDunsId" runat="server" MaxLength="9"></asp:TextBox>
					<asp:CustomValidator ID="cvDUNSNumber" runat="server" ValidateEmptyText="true" 
						ControlToValidate="tbDunsId" EnableClientScript="true" ClientValidationFunction="ValidateDUNSNumberAndTaxId"
					ErrorMessage="You must provide either a D&B Number or a Tax ID" 
						onservervalidate="cvDUNSNumber_ServerValidate"><asp:Image ID="Image3" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="D&B Number and/or Tax ID required" /></asp:CustomValidator></td>
			</tr>
			<tr>
			    <td colspan='2' class="center">-- OR --</td>
			    <td>&nbsp;</td>
			</tr>
			<tr>
			    <td class="left" valign="top">Tax ID Number*:</td>
			    <td colspan="2">
			        <table>
			            <tr>
			                <td class="right" valign="top"><asp:TextBox ID="tbTaxId" runat="server" MaxLength="9"></asp:TextBox>
					            <asp:CustomValidator ID="cvTaxId" runat="server" ValidateEmptyText="true" Display="Dynamic"
						            ControlToValidate="tbTaxId" EnableClientScript="true" ClientValidationFunction="ValidateDUNSNumberAndTaxId"
					                ErrorMessage="You must provide either a Tax ID or D&B Number" 
						            onservervalidate="cvTaxId_ServerValidate"><asp:Image ID="Image13" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Tax ID and/or D&B Number required" /></asp:CustomValidator></td>
			                <td align="left" class="instruction" style="width: 210px; align: left; padding:4px 8px;">▪ Tax ID cannot contain any dashes and must be a valid Tax ID number.</td>
			            </tr>
			        </table>
			    </td>				
			</tr>
			<tr>
			    <td colspan="2" >&nbsp;</td> 
			</tr>
		</table>
	</div>

	<div ID="divCashCustomer" runat="server">
	</div>
</cc2:RoundedCornerPanel>
