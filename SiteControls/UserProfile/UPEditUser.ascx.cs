using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Sunbelt.Security;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using Sunbelt.AS400Access;
using SBNet;
using SBNet.Extensions;

public partial class UserProfile_UPEditUser : Sunbelt.Web.UserControl
{
	private enum PageState { Edit, Error, EmailSent, Updated }
	private PageState State
	{
		get { return (PageState)GetViewState("State", PageState.Edit); }
		set { SetViewState("State", value); }
	}
	private int EbClock
	{
		set { SetViewState("EbClock", value); }
		get { return (int)GetViewState("EbClock", 0); }
	}
	private bool IsNewUser
	{
		get { return (bool)GetViewState("IsNewUser", false); }
		set { SetViewState("IsNewUser", value); }
	}
	public bool IsCashCustomer
	{
		get { return (bool)GetViewState("IsCashCustomer", true); }
		set { SetViewState("IsCashCustomer", value); }
	}
	private string UserName
	{
		get { return (string)GetViewState("UserName", null); }
		set { SetViewState("UserName", value); }
	}
	private string UserKey
	{
		get { return (string)GetViewState("UserKey", ""); }
		set { SetViewState("UserKey", value.ToString()); }
	}
	private int ErrorLogId
	{
		//get { return (int)GetViewState("ErrorLogId", 0); }
		//set { SetViewState("ErrorLogId", value.ToString()); }
		get; set;
	}
	private bool InitMethodHasBeenCalled
	{
		get { return (bool)GetViewState("Init", false); }
		set { SetViewState("Init", value); }
	}

	/// <summary>
	/// Rasied when an error occurs.
	/// </summary>
	public event EventHandler<UserErrorEvent> UserErrorRasied;
	/// <summary>
	/// Rasied when an unknow execption is caugth.
	/// </summary>
	public event EventHandler<ExecptionCaughtEvent> ExecptionCaught;
	/// <summary>
	/// Rasied before the user information is updated. Set the UserEvent.CancelEvent propertery.
	/// </summary>
	public event EventHandler<UserEvent> UserUpdating;
	/// <summary>
	/// Rasied after the user information is updated.
	/// </summary>
	public event EventHandler<UserEvent> UserUpdated;
	

	protected void Page_Load(object sender, EventArgs e)
    {
		UpdateDisplay();

        if (!IsPostBack)
        {
            if (!InitMethodHasBeenCalled)
                throw new Exception("CreateNewUser(...) or EditUserProfile(...) must be called before first page load event.");

			hlHome1.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
			hlHome2.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
			hlHome3.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
			hlHome4.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];

            HyperLink3.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(HyperLink3.NavigateUrl, false);
            HyperLink6.NavigateUrl = Sunbelt.Tools.WebTools.ResolveServerUrl(HyperLink6.NavigateUrl, false);

            ShowDiverLicenseInformation(IsNewUser && IsCashCustomer);
            ShowJobTitle(!IsCashCustomer && (EbClock == 0));

            //Change the default validator message.
            ddlDLState.ValidatorControl.ErrorMessage = "Drivers License State is required";
            ddlState.ValidatorControl.ErrorMessage = "State is required";

            if (Request.QueryString["ucan"] != null)
            {
                try
                {
                    Guid userGuid = new Guid(Request.QueryString["ucan"]);
                    MembershipUser mu = Membership.GetUser(userGuid, true);
                    if (mu != null)
                    {
                        UserKey = userGuid.ToString();
                        //Remember the use name.
                        UserName = mu.UserName;

                        if (mu.IsApproved)
                        {
							//We are in a try/catch so, set second parameter to 'false' so we don't throw
							// a 'Thread was being aborted' exception.
							Response.Redirect("~/Login.aspx", false);
							return;
                        }
                        else
                        {
                            tbEmail.Text = mu.UserName;
                            tbConfrimEmail.Text = mu.UserName;
                            spanEmailMsg.Visible = false;
                            trEditLogin2.Visible = false;
                        }
                    }
                }
                catch {/*Do nothing.*/}
            }
            else
            {
                if ((Page.Request.UrlReferrer == null) ||
                    string.IsNullOrEmpty(Page.Request.UrlReferrer.AbsoluteUri) ||
                    (hlCancel.NavigateUrl == Page.Request.Url.AbsoluteUri))
                    hlCancel.NavigateUrl = ConfigurationManager.AppSettings["CUSTOMER_ROOT"];
                else
                    hlCancel.NavigateUrl = Page.Request.UrlReferrer.AbsoluteUri;
                hlCancel.Visible = true;

                litCancel.Text = "&nbsp;";
                litCancel.Visible = true;
            }

            if (IsNewUser == true && EbClock == 0)
                trUserAgreement.Visible = true;
            else if (string.IsNullOrEmpty(UserKey) && State != PageState.Edit)
                trUserAgreement.Visible = true;
            else
                trUserAgreement.Visible = false;
        }
        else
        {
            tbPassword.Attributes["value"] = tbPassword.Text;
            tbConfirmPassword.Attributes["value"] = tbPassword.Text;
        }
	}

	protected override void OnPreRender(EventArgs e)
	{
		//Register the client javascript.
		SBNet.Tools.IncludeEntryControlsClientScript(this.Page);

		if (!Page.ClientScript.IsStartupScriptRegistered("UPEditUser"))
		{
			StringBuilder sbCode = new StringBuilder();
			
			sbCode.Append("var UPEU = {\r\n");

			sbCode.Append("WindowOnLoad:function()\r\n");
			sbCode.Append("{\r\n");
			//sbCode.Append("  alert('UPEU.WindowOnLoad');\r\n");
			sbCode.Append("  if(document.getElementById('" + tbFirstName.ClientID + "') != null)\r\n");
			sbCode.Append("  {\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbFirstName.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbLastName.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbAddress1.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbAddress2.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbAddress3.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbCity.ClientID + "'), true);\r\n");
            sbCode.Append("    SBNet.EC.ZipEntry(document.getElementById('" + tbPostalCode.ClientID + "'), true);\r\n");
			//sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbPhone.ClientID + "'), true);\r\n");
			//sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbFax.ClientID + "'), true);\r\n");
			sbCode.Append("  }\r\n");

			sbCode.Append("  if(document.getElementById('" + tbDLNumber.ClientID + "') != null)\r\n");
			sbCode.Append("  {\r\n");
			sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbDLNumber.ClientID + "'), true);\r\n");
            sbCode.Append("    SBNet.EC.UpperCaseTextEntry(document.getElementById('" + tbDLNumber.ClientID + "'), true);\r\n");
			sbCode.Append("    SBNet.EC.DateEntry(document.getElementById('" + tbDLDateOfBirth.ClientID + "'), true);\r\n");
			//sbCode.Append("    SBNet.EC.AllowNoValue(document.getElementById('" + tbDLDateOfBirth.ClientID + "'));\r\n");
			sbCode.Append("  }\r\n");

			sbCode.Append("},\r\n");
	
			//Date Validator
			sbCode.Append("DateValidator:function(source, arguments)\r\n");
			sbCode.Append("{\r\n");
			//sbCode.Append("  alert('UPEU.DateValidator');\r\n");
			sbCode.Append("  var control = document.getElementById('" + tbDLDateOfBirth.ClientID + "');\r\n");
			sbCode.Append("  SBNet.EC.TestControl(control);\r\n");
			sbCode.Append("  arguments.IsValid = control.ValidFormat && (control.value != \"\");\r\n");
			//sbCode.Append("  alert(arguments.IsValid);\r\n");
			sbCode.Append("},\r\n");

            //Zip Validator
			sbCode.Append("ZipValidator:function(source, arguments)\r\n");
            sbCode.Append("{\r\n");
            //sbCode.Append("  alert('UPEU.ZipValidator');\r\n");
            sbCode.Append(" var control = document.getElementById('" + tbPostalCode.ClientID + "');\r\n");
            //sbCode.Append(" var format = SBNet.EC.formatZip(control.value);\r\n");
            //sbCode.Append(" if (format != null && format != '')\r\n{\r\n  arguments.IsValid = true \r\n }\r\n else \r\n { \r\n arguments.IsValid = false \r\n }");
			sbCode.Append("  SBNet.EC.TestControl(control);\r\n");
			sbCode.Append("  arguments.IsValid = control.ValidFormat && (control.value != \"\");\r\n");
			sbCode.Append("}\r\n");

            //Closing Bracket for UPEU remember additional , for each added function!
            sbCode.Append("}\r\n");
	
			sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
			sbCode.Append("  Sys.Application.add_load(UPEU.WindowOnLoad)\r\n");
			sbCode.Append("else\r\n");
			sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', UPEU.WindowOnLoad);\r\n");
			Page.ClientScript.RegisterStartupScript(this.GetType(), "UPEditUser", sbCode.ToString(), true);
		}
		base.OnPreRender(e);
	}
	
	public void ShowDiverLicenseInformation(bool opt)
	{
		trDL1.Visible = opt;
		trDL2.Visible = opt;
		trDL3.Visible = opt;
	}

	public void ShowJobTitle(bool opt)
	{
		trJT1.Visible = opt;
	}
	
	public bool IsNewUserMode()
	{
		return IsNewUser;
	}

	public bool IsEditUserMode()
	{
		return !IsNewUser;
	}

	/// <summary>
	/// Creates the new user.	NOTE: This or EditUserProfile(...) must get
	/// called before first page load event.
	/// </summary>
	/// <param name="ebClock">The eb clock.</param>
	public void CreateNewUser(int ebClock)
	{
		InitMethodHasBeenCalled = true;
		EbClock = ebClock;
		IsNewUser = true;

		//if (EbClock > 0)
		//{
		//    ShowDiverLicenseInformation(false);
		//    ShowJobTitle(false);
		//}
		//UpdateDisplay();
	}

	/// <summary>
	/// Edits the user profile.	NOTE: This or CreateNewUser(...) must get
	/// called before first page load event.
	/// </summary>
	/// <param name="userName">Name of the user.</param>
	public void EditUserProfile(string userName)
	{
		InitMethodHasBeenCalled = true;
		IsNewUser = false;
		//ShowDiverLicenseInformation(IsNewUser);
		//UpdateDisplay();
		
		//get user profile.
		SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;

		MembershipUser mu = p.GetUser(userName, true);
		UserProfile up = p.GetUserProfile(userName);

		//Remember the use name.
		UserName = mu.UserName;

		lbEmail.Text = mu.Email;
		//lblSecurityQuestion.Text = mu.PasswordQuestion;
		//tbPrefix.Text = up.Prefix;
        if (SBNet.SiteUser.IsCashCustomer)
        {
            tbFirstName.ReadOnly = true;
			tbFirstName.CssClass = "ReadOnly";
			tbLastName.ReadOnly = true;
			tbLastName.CssClass = "ReadOnly";
        }
		tbFirstName.Text=  up.firstName;
		//tbMiddleInitial.Text = up.MiddleInitial;
		tbLastName.Text = up.lastName;
		//tbSuffix.Text = up.Suffix;
		tbAddress1.Text = up.address1;
		tbAddress2.Text = up.address2;
		tbAddress3.Text = up.address3;
		tbCity.Text = up.city;
        try { ddlJobTitle.SelectedValue = up.jobTitle; }
        catch { ddlJobTitle.SelectedValue = ""; }
        cbSentMePromotions.Checked = up.receivePromotionalInfo.Value;
		try { ddlState.SelectedValue = up.state; }
		catch { ddlState.SelectedValue = ""; }
		tbPostalCode.Text = up.zip;
		//tbCountry.Text = up.Country;
		tbPhone.Text = up.phone;
		tbFax.Text = up.fax;

		EbClock = up.ebClock.Value;
	}

	private void RaiseUserError(MembershipCreateStatus status, string message)
	{
		EventHandler<UserErrorEvent> eh = UserErrorRasied;
		if (eh != null)
			eh(this, new UserErrorEvent(status, message));
		else
			throw new Exception("RaiseUserError event not handled.");
	}

	/// <summary>
	/// Raises the user updating. Returns UserEvent.CancelEvent.
	/// </summary>
	/// <returns></returns>
	private UserEvent RaiseUserUpdating()
	{
		UserEvent ue = new UserEvent(UserEvent.EventType.Updating);
		EventHandler<UserEvent> eh = UserUpdating;
		if (eh != null)
		{
			eh(this, ue);
		}
		return ue;
	}

	private void RaiseUserUpdated()
	{
		EventHandler<UserEvent> eh =  UserUpdated;
		if (eh != null)
			eh(this, new UserEvent(UserEvent.EventType.Updated));
	}

	private void RaiseExecptionCaught(Exception ex, int logId)
	{
		EventHandler<ExecptionCaughtEvent> eh = ExecptionCaught;
		if (eh != null)
			eh(this, new ExecptionCaughtEvent(ex, logId));
	}
	
	
	private void UpdateDisplay()
	{
		divEditUser.Visible = false;
		divUserUpdated.Visible = false;
		divEmailSent.Visible = false;
		divError.Visible = false;
		
		panAddUser.Visible = IsNewUser;
		panEditUser.Visible = !IsNewUser;
		btnAddUser.Visible = IsNewUser;
		btnUpdateUser.Visible = !IsNewUser;
	
		lbEmail.Text = string.IsNullOrEmpty(Page.User.Identity.Name) ? "User.Identity.Name is empty" : Page.User.Identity.Name;

		trEditLogin.Visible = !IsNewUser;
		trAccountTools.Visible = !IsNewUser;
		
		switch (State)
		{
			case PageState.Edit:
				Page.Form.DefaultButton = (IsNewUser) ? btnAddUser.UniqueID : btnUpdateUser.UniqueID;
				divEditUser.Visible = true;
				break;
			case PageState.EmailSent:
				divEmailSent.Visible = true;
				break;
			case PageState.Updated:
				divUserUpdated.Visible = true;
				break;
			default:
				lbErrorCode.Text = ErrorLogId.ToString();
				divError.Visible = true;
				break;
		}
	}

	protected void btnUpdateUser_Click(object sender, ImageClickEventArgs e)
	{
		btnAddUser_Click(sender, e);
	}

	protected void btnAddUser_Click(object sender, ImageClickEventArgs e)
	{
	}

	protected void cvDate_ServerValidate(object source, ServerValidateEventArgs args)
	{
		DateTime dt = Sunbelt.Tools.ConvertTools.ToDateTime(tbDLDateOfBirth.Text, DateTime.MinValue);

		//args.IsValid = (dt != DateTime.MinValue);
		args.IsValid = (dt < DateTime.Now.AddYears(-16) && dt > DateTime.Now.AddYears(-100));
	}

    protected void cvZip_ServerValidate(object source, ServerValidateEventArgs args)
    {
		//Nothing to check server side.
		args.IsValid = true;
    }
}
