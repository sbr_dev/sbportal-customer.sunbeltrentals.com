using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Sunbelt.Tools;
using Sunbelt.Security;


public partial class UserProfile_UPSendVerifyEmail : Sunbelt.Web.UserControl
{
	public enum EmailStatus { Sent, NotSent, Error	}

	private EmailStatus Status
	{
		get { return (EmailStatus)GetViewState("Status", EmailStatus.NotSent); }
	}
	
	protected void Page_Load(object sender, EventArgs e)
    {

    }

	public void SendActivationEmail(string userName, bool IsCashCustomer)
	{
		try
		{
            string emailTemplatefile;
            if (IsCashCustomer)
                emailTemplatefile = "VerifyUserCC.htm";
            else
                emailTemplatefile = "VerifyUser.htm";

            //Send email here...

			SecureQueryString sqs = new SecureQueryString();
			sqs.ExpireTime = DateTime.Now.AddDays(5);
			sqs["user"] = userName;

			StringBuilder sb = new StringBuilder();
            sb.Append(Sunbelt.Email.ReadTextFile(Server.MapPath("~/SiteControls/UserProfile/EmailTemplates/" + emailTemplatefile)));

			sb.Replace("[LINK_HERE]", WebTools.ToWebAddress("~/VerifyUser.aspx") + "?wcan=" + sqs.ToString());
			Sunbelt.Email.SendEmail(userName, ConfigurationManager.AppSettings["CustomerService_Email"],
				"", ConfigurationManager.AppSettings["SunbeltEmail"].ToString(),
				"Welcome New SunbeltRentals.com User", sb.ToString(), true);

			SetViewState("Status", EmailStatus.Sent);

			Sunbelt.Log.LogActivity(SBNet.Log.Sections.Email.ToString(),
				"SendActivationEmail - Sent to " + userName);
		}
		catch (Exception ex)
		{
#if DEBUG
			lbError.Text = ex.Message;
#endif
			SetViewState("Status", EmailStatus.Error);
		}
		UpdateDisplay();
	}

    public void SendPasswordResetEmail(string userName)
    {
        try
        {
            //Send email here...

            SecureQueryString sqs = new SecureQueryString();
            sqs.ExpireTime = DateTime.Now.AddDays(5);
			sqs["user"] = userName;
            sqs["dt"] = DateTime.Now.ToString();

            StringBuilder sb = new StringBuilder();
            sb.Append(Sunbelt.Email.ReadTextFile(Server.MapPath("~/SiteControls/UserProfile/EmailTemplates/PasswordReset.htm")));

            sb.Replace("[LINK_HERE]", WebTools.ToWebAddress("~/EditLogin.aspx") + "?wcan=" + sqs.ToString());

			Sunbelt.Email.SendEmail(userName, "support@sunbeltrentals.com",
                "", "", "Forgotten Password Reset", sb.ToString(), true);

            ViewState["Status"] = EmailStatus.Sent;

			Sunbelt.Log.LogActivity(SBNet.Log.Sections.Email.ToString(),
				"SendPasswordResetEmail - Sent to " + userName);
		}
        catch (Exception ex)
        {
#if DEBUG
			lbError.Text = ex.Message;
#endif
			ViewState["Status"] = EmailStatus.Error;
        }
        UpdateDisplay();
    }


	private void UpdateDisplay()
	{
		panNotSend.Visible = false;
		panEmailSent.Visible = false;
		panEmailError.Visible = false;

		switch (Status)
		{
			case EmailStatus.Sent:
				panEmailSent.Visible = true;
				break;
			case EmailStatus.Error:
				panEmailError.Visible = true;
				break;
			default:
				panNotSend.Visible = true;
				break;
		}
	}
}
