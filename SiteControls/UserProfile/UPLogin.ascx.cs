using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SBNet;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;
using System.Collections.Generic;

public partial class SiteControls_UserProfile_UPLogin : Sunbelt.Web.UserControl
{
	private enum PageState { Login, Error, LockedOut, NotApproved }
	private PageState State
	{
		get { return (PageState)GetViewState("State", PageState.Login); }
		set { SetViewState("State", value); }
	}
	private int LoginAttempts
	{
		get { return (int)GetViewState("Attempts", 0); }
		set { SetViewState("Attempts", value); }
	}

	public string NewUserNavigateUrl
	{
		get { return hlNewUser.NavigateUrl; }
		set { hlNewUser.NavigateUrl = value; }
	}
	
	public event EventHandler<UserEvent> UserLoggedIn;
	//public event EventHandler<UserEvent> NewUserClicked;
	public event EventHandler<UserEvent> ForgotPasswordClicked;
	public event EventHandler<UserEvent> TooManyLoginAttempts;
	public event EventHandler<UserErrorEvent> UserErrorRasied;

	protected void Page_Load(object sender, EventArgs e)
    {
		UpdateDisplay();

		if (!IsPostBack)
		{
			cbRememberMe.Checked = ConvertTools.ToBoolean(WebTools.GetCookie("Login", "Remember", "false"));
			if (cbRememberMe.Checked)
			{
				tbEmail.Text = WebTools.GetCookie("Login", "Email", "");
				tbPassword.Focus();
			}
			else
				tbEmail.Focus();
		}

		hlHome.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
		hlHome2.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
		hlHome3.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
		hlHome4.NavigateUrl = ConfigurationManager.AppSettings["SUNBELT_ROOT"];
	}

	private void UpdateDisplay()
	{
		divLogin.Visible = false;
		divLockedOut.Visible = false;
		divNotApproved.Visible = false;
		divError.Visible = false;
		switch (State)
		{
			case PageState.Login:
				//Page.Form.DefaultButton = btnLogin.UniqueID;
				divLogin.Visible = true;
				break;
			case PageState.LockedOut:
				divLockedOut.Visible = true;
				break;
			case PageState.NotApproved:
				divNotApproved.Visible = true;
				break;
			default: //Error
				divError.Visible = true;
				break;
		}
	}

	private void RaiseUserError(string message)
	{
        EventHandler<UserErrorEvent> eh = UserErrorRasied;
		if (eh != null)
			eh(this, new UserErrorEvent(message));
		else
			throw new Exception("UserErrorRasied event not handled.");
	}
	
	//protected void btnNewUser_Click(object sender, EventArgs e)
	//{
	//    EventHandler<UserEvent> eh = NewUserClicked;
	//    if (eh != null)
	//        eh(this, new UserEvent());
	//    else
	//        throw new Exception("NewUserClicked event not handled.");
	//}
	
	protected void btnForgotPassword_Click(object sender, EventArgs e)
	{
		EventHandler<UserEvent> eh = ForgotPasswordClicked;
		if (eh != null)
			eh(this, new UserEvent());
		else
			throw new Exception("ForgotPasswordClicked event not handled.");
	}
	
	protected void  btnLogin_Click(object sender, ImageClickEventArgs e)
	{
		if (Page.IsValid)
		{
            LoginAttempts++;

			DateTime start = DateTime.Now;

			string userName = tbEmail.Text.Trim();
			string password = tbPassword.Text.Trim();

			SBNetSqlMembershipProvider p = (SBNetSqlMembershipProvider)Membership.Provider;
			if (Membership.ValidateUser(userName, password))
			{
                Sunbelt.Log.LogActivity("Login", "User, " + userName + " logged into Customer site");
                //Get the user's profile. If a Sunbelt empolyee, verify they are valid.
				UserProfile up = p.GetUserProfile(userName);

				if (up.ebClock > 0)
				{
					try
					{
						Sunbelt.Security.SecureNameValueCollection snvc = new Sunbelt.Security.SecureNameValueCollection();
						snvc["ClockNumber"] = up.ebClock.ToString();

						SunbeltWebServices.Authentication auth = new SunbeltWebServices.Authentication();
						SunbeltWebServices.UserInfo ui = auth.GetUserByClockID(snvc.ToString());
						if (ui.TerminationDate <= DateTime.Now)
						{
							//Log this.
							Sunbelt.Log.LogError(new Exception("SunbeltWebServices.UserInfo.TerminationDate <= DateTime.Now."),
								"User: " + userName + " termination date is " + ui.TerminationDate.ToLongDateString());

#if DEBUG
							RaiseUserError("DEBUG - SunbeltWebServices.UserInfo.TerminationDate <= DateTime.Now.");
#endif
							RaiseUserError("Login failed. Please check your email address and password and try again.");
							return;
						}
					}
					catch (Exception ex)
					{
						//For now ignore the "(SAMAccountName=) search filter is invalid" error.
						if(ex.Message.IndexOf("(SAMAccountName=) search filter is invalid") == -1)
						{						
							//For now, just log the error.
							Sunbelt.Log.LogError(ex, "SunbeltWebServices.Authentication.GetUserByClockID failed for " + userName + ":" + up.ebClock.ToString());
						}
					}
				}
                else
                {
                    if (isCustomerAccountInCollections(up) == true)
                    {
                        State = PageState.LockedOut;
                        UpdateDisplay();
                        return;
                    }
                }

				//Save cookie if needed.
				WebTools.SetCookie("Login", "Remember", cbRememberMe.Checked.ToString().ToLower());
				if (cbRememberMe.Checked)
					WebTools.SetCookie("Login", "Email", userName, 30);

				//Raise event.
				EventHandler<UserEvent> eh = UserLoggedIn;
                if (eh != null)
                    eh(this, new UserEvent(UserEvent.EventType.LoggedIn, userName));
                else
                    throw new Exception("UserLoggedIn event not handled.");
				//done
				Session["UserLoginTime"] = DateTime.Now - start;

				return;
			}
			else //Login failed.
			{
                Sunbelt.Log.LogActivity("Login", "User, " + userName + " log in failed");

				MembershipUser mu = Membership.GetUser(userName, false);
				if (mu != null && mu.IsLockedOut)
				{
					State = PageState.LockedOut;
				}
				else
				if (mu != null && !mu.IsApproved)
				{
					State = PageState.NotApproved;
				}
				else
				{
					if (LoginAttempts > 4)
					{
						//If they are about to get locked out because of too many attempts,
						// send notify our parent page so they can let them know
						EventHandler<UserEvent> eh = TooManyLoginAttempts;
						if (eh != null)
						{
							eh(this, new UserEvent(UserEvent.EventType.LoginAttempts));
							LoginAttempts = 0;
							p.ResetFailedPasswordCount(userName);
							return;
						}
					}
#if DEBUG
					if(mu == null)
						RaiseUserError("DEBUG - User not in database.");
#endif

					RaiseUserError("Login failed. Please check your email address and password and try again.");
					if (LoginAttempts > 2)
						RaiseUserError("Is Caps Lock on?  Having Caps Lock on may cause you to enter your password incorrectly.");
					
					tbPassword.Focus();
				}
				UpdateDisplay();
			}
		}
	}
    private bool isCustomerAccountInCollections(UserProfile up)
    {
        bool sentToCollections = false;
        try
        {
            List<SecurityAccess> accountList = UserSecurity.GetAccessByUser(up.email);
            foreach (SecurityAccess sa in accountList)
            {
                Sunbelt.BusinessObjects.Customer c = Sunbelt.BusinessObjects.CustomerData.GetCustomer(sa.AccountNumber, 1);
                if (c == null)
                    continue;
				//That is not good, we should be mirroring what Wynne does. It is a hard halt for B,C, F, and S.
				//Thanks-Lisa
                // S has been removed as a halt via Lisa, to many accounts with this status

				//if (c.Status.ToUpper() == "C" )
				if (c.Status.ToUpper() == "C" || c.Status.ToUpper() == "B" )
                {
                    sentToCollections = true;
					Sunbelt.Email.SendEmail("michael.evanko@sunbeltrentals.com; Lisa.Coakley@sunbeltrentals.com", "noreply@sunbeltrentals.com", "", "", "Sunbelt Rentals Collections(or hard halt) Login", "Customer, " + up.email + " attempted login for account# " + sa.AccountNumber.ToString() + ", Account Status: " + c.Status.ToUpper(), true);
                }
            }
        }
        catch (Exception ex)
        {
            Sunbelt.Log.LogError(ex, "Collections validation of customer");
        }
        return sentToCollections;
    }
}
