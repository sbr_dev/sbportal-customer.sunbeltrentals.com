﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPResetPassword.ascx.cs" Inherits="SiteControls_UserProfile_UPResetPassword" %>
<%@ Register Src="UPSendVerifyEmail.ascx" TagName="UPSendVerifyEmail" TagPrefix="uc1" %>
<%@ Register Namespace="CaptchaWebControl" Assembly="CaptchaWebControl" TagPrefix="cc1" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc2" %>


<div ID="divPasswordReset" runat="server">
	<cc2:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:540px;">
		<p id="pAttemptsMsg" runat="server" style="margin-bottom:0.5em; font-style:italic; font-weight:bold;" visible="false">It appears you have forgotten your password. You can reset it here!</p>
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td colspan="3" class="h2">Password Reset</td>
			</tr>
			<tr>
				<td style="font-size:smaller;">&nbsp;</td>
				<td colspan="2" class="right" style="font-size:smaller;">* Indicates a required field</td>
			</tr>
			<tr>
				<td class="left">
					Email*:</td>
				<td class="right">
					<asp:TextBox ID="tbEmail" runat="server" Columns="40" MaxLength="100"></asp:TextBox>
					<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is required" ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image8" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is required" /></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email is not a valid email address"
						ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image7" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is not a valid email address" /></asp:RegularExpressionValidator>
				</td>
				<td rowspan="2" class="instruction" style="width: 260px; padding:4px 8px;"><span id="spanEmailMsg" runat="server">
					▪ You will receive an email to complete the reset process.</span>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" class="right"><cc1:captchacontrol id="CaptchaControl1" runat="server" ErrorImage="~/SiteControls/UserProfile/Images/icon_warn2.gif"></cc1:captchacontrol></td>
			</tr>	 
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
		</table>
	</cc2:RoundedCornerPanel>
	<br />
	<table cellpadding="0" cellspacing="0" style="width:540px;">
		<tr>
			<td class="right"><asp:LinkButton ID="btnLogin" runat="server" CausesValidation="False" OnClick="btnLogin_Click">I Remember My Password</asp:LinkButton></td>
			<td style="text-align: right; vertical-align: top;"><asp:ImageButton ID="btnResetPassword" runat="server" AlternateText="Reset Password"
					ImageAlign="AbsMiddle" ImageUrl="~/App_Themes/Main/Buttons/reset_password_100x20_darkred.gif" OnClick="btnResetPassword_Click" /></td>
		</tr>
	</table>
</div>

<cc2:RoundedCornerPanel ID="divEmailSent" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:540px;">
	<uc1:UPSendVerifyEmail ID="UPSendVerifyEmail1" runat="server" />
</cc2:RoundedCornerPanel>
<cc2:RoundedCornerPanel ID="divLockedOut" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:540px;">
	Your account is locked out. Please contact customer support.
</cc2:RoundedCornerPanel>
<cc2:RoundedCornerPanel ID="divNotApproved" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:540px;">
	Your account is not activated. Please contact customer support.
</cc2:RoundedCornerPanel>
<cc2:RoundedCornerPanel ID="divError" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width:540px;">
	An error has occurred. Contact customer support.
</cc2:RoundedCornerPanel>

<table cellpadding="0" cellspacing="0" style="width:540px;">
	<tr>
		<td class="right" colspan="2"><hr /></td>
	</tr>
	<tr>
		<td colspan="2" class="right"><asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
	</tr>
</table>
