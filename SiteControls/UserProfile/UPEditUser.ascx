﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UPEditUser.ascx.cs" Inherits="UserProfile_UPEditUser" %>
<%@ Register Assembly="SBNetWebControls" Namespace="SBNetWebControls" TagPrefix="cc2" %>
<%@ Register Src="UPSendVerifyEmail.ascx" TagName="UPSendVerifyEmail" TagPrefix="uc1" %>
<%@ Register Assembly="CaptchaWebControl" Namespace="CaptchaWebControl" TagPrefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc3" %>
<%@ Register src="../JobsiteMap/USStatesDropDownList.ascx" tagname="USStatesDropDownList" tagprefix="uc2" %>

<div ID="divEditUser" runat="server">
	<cc2:RoundedCornerPanel ID="rcp1" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 660px">
	<asp:Panel ID="panAddUser" runat="server">
	<table cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td class="right h2">MY LOGIN INFORMATION</td>
			<td class="left" style="width: 100%; text-align:right"><%--<cc2:HelpLink ID="HelpLink1" runat="server" HelpTagName="TOP" HelpUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx" NavigateUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx" SkinID="ContentTopRight">Need Help?</cc2:HelpLink>--%></td>
		</tr>
	</table>	
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td style="font-size:smaller;">&nbsp;</td>
			<td class="right" style="font-size:smaller;">* Indicates a required field</td>
		</tr>
		<tr>
			<td class="left">
				Email*:</td>
			<td class="right">
				<asp:TextBox ID="tbEmail" runat="server" Columns="40" MaxLength="100"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email is required." ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image8" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is required" /></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Email is not a valid email address."
					ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="tbEmail" Display="Dynamic"><asp:Image ID="Image7" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email is not a valid email address" /></asp:RegularExpressionValidator>
			</td>
			<td rowspan="2" class="instruction" style="width: 200px; padding:4px 8px;"><span id="spanEmailMsg" runat="server">▪ An email will be sent to this address to complete the activation process.</span>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
				Confirm Email*:</td>
			<td rowspan="2" class="right">
				<asp:TextBox ID="tbConfrimEmail" runat="server" Columns="40" MaxLength="100"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvConfirmEmail" runat="server" ControlToValidate="tbConfrimEmail"
					ErrorMessage="Confirm email is required." Display="Dynamic"><asp:Image ID="Image6" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Confirm email is required" /></asp:RequiredFieldValidator><asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="tbEmail"
					ControlToValidate="tbConfrimEmail" ErrorMessage="Email and confirmation email must match." Display="Dynamic"><asp:Image ID="Image5" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Email and confirmation email must match" /></asp:CompareValidator></td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="left">
				Password*:</td>
			<td class="right">
				<asp:TextBox ID="tbPassword" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="tbPassword"
					ErrorMessage="Password is required." Display="Dynamic"><asp:Image ID="Image4" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Password is required" /></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="revPassword" runat="server" ErrorMessage="Password is not valid." ControlToValidate="tbPassword" Display="Dynamic" ValidationExpression="[\d\w_-]{4,12}"><asp:Image ID="Image11" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Password is not valid" /></asp:RegularExpressionValidator><br />
			</td>
			<td rowspan="2" class="instruction" style="width: 200px; padding:4px 8px;">▪ Between 4 and 12 characters; number and letters; case sensitive</td>
		</tr>
		<tr>
			<td class="left">
				Confirm Password*:</td>
			<td class="right">
				<asp:TextBox ID="tbConfirmPassword" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" ControlToValidate="tbConfirmPassword"
					ErrorMessage="Confirm password is required." Display="Dynamic"><asp:Image ID="Image3" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Confirm password is required" /></asp:RequiredFieldValidator><asp:CompareValidator ID="cvPassword" runat="server" ControlToCompare="tbPassword"
					ControlToValidate="tbConfirmPassword" ErrorMessage="Password and confirmation password must match." Display="Dynamic"><asp:Image ID="Image1" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Password and confirmation password must match" /></asp:CompareValidator></td>
		</tr>
		<%--<tr>
			<td class="left">
				Security Question*:</td>
			<td class="right">
				<asp:TextBox ID="tbPasswordQuestion" runat="server" Columns="50" MaxLength="250"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPasswordQuestion" runat="server" ErrorMessage="Security Question is required." ControlToValidate="tbPasswordQuestion" Display="Dynamic"><asp:Image ID="Image10" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Security Question is required" /></asp:RequiredFieldValidator></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
				Security Answer*:</td>
			<td class="right">
				<asp:TextBox ID="tbPasswordAnswer" runat="server" Columns="40" MaxLength="120"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPasswordAnswer" runat="server" ErrorMessage="Security Answer is required." ControlToValidate="tbPasswordAnswer" Display="Dynamic"><asp:Image ID="Image9" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Security Answer is required" /></asp:RequiredFieldValidator></td>
			<td>&nbsp;</td>
		</tr>--%>
	</table>
	</asp:Panel>
	<asp:Panel ID="panEditUser" runat="server">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2" class="h2">MY LOGIN INFORMATION&nbsp;&nbsp;&nbsp;<asp:HyperLink ID="hlChangeLogin" runat="server" Font-Size="X-Small" NavigateUrl="../../EditLogin.aspx" Font-Bold="false">(Change My Login)</asp:HyperLink></td>
		</tr>
		<tr>
			<td class="left">Email:</td>
			<td class="right"><asp:TextBox ID="lbEmail" runat="server" Columns="40" ReadOnly="true" CssClass="ReadOnly"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="left">Password:</td>
			<td class="right ReadOnlyCell"><asp:TextBox ID="lbPassword" runat="server" ReadOnly="true" CssClass="ReadOnly" Text="••••••••••"></asp:TextBox></td>
		</tr>
	</table>
	</asp:Panel>
	</cc2:RoundedCornerPanel>

	<br />	
	<cc2:RoundedCornerPanel ID="rcp2" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 660px">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="5" class="h2">MY PROFILE</td>
		</tr>
		<tr>
			<td style="font-size:smaller;">&nbsp;</td>
			<td colspan="4" class="right" style="font-size:smaller;">* Indicates a required field</td>
		</tr>
		<tr>
			<td class="left">
				First Name*:</td>
			<td colspan="4" class="right">
				<asp:TextBox ID="tbFirstName" runat="server" MaxLength="40"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="tbFirstName"
					ErrorMessage="Frist Name is required." Display="Dynamic"><asp:Image ID="Image2" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif"
					ImageAlign="AbsMiddle" ToolTip="Frist Name is required" /></asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="left">
				Last Name*:</td>
			<td colspan="4" class="right">
				<asp:TextBox ID="tbLastName" runat="server" MaxLength="40"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="tbLastName"
					ErrorMessage="Last Name is required." Display="Dynamic">
					<asp:Image ID="Image12" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Last Name is required" />
				</asp:RequiredFieldValidator></td>
		</tr>
		<tr id="trJT1" runat="server">
			<td class="left">
				Job Title*:</td>
			<td colspan="4" class="right"><asp:DropDownList ID="ddlJobTitle" runat="server">
					<asp:ListItem Text="-- select one --" Value="" />
					<asp:ListItem Text="CEO / President" Value="CEO" />
					<asp:ListItem Text="Chief Financial Officer" Value="CFO" />
					<asp:ListItem Text="Chief Operating Officer" Value="COO" />
					<asp:ListItem Text="Controller" Value="Controller" />
					<asp:ListItem Text="Foreman" Value="Foreman" />
					<asp:ListItem Text="Manager" Value="Manager" />
					<asp:ListItem Text="Supervisor" Value="Supervisor" />
					<asp:ListItem Text="Superintendent" Value="Superintendent" />
					<asp:ListItem Text="Accounts Payable" Value="AP" />
					<asp:ListItem Text="Purchasing" Value="Purchasing" />

				</asp:DropDownList>
				<asp:RequiredFieldValidator ID="rfvJobTitle" runat="server" ControlToValidate="ddlJobTitle"
					ErrorMessage="Job Title is required." Display="Dynamic">
					<asp:Image ID="Image10" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Job Title is required" />
				</asp:RequiredFieldValidator></td>
		</tr>
		<tr id="trDL1" runat="server">
			<td colspan="5" class="h3">DRIVERS LICENSE INFORMATION</td>
		</tr>
		<tr id="trDL2" runat="server">
			<td class="left">State*:</td>
			<td><uc2:USStatesDropDownList ID="ddlDLState" runat="server" ShortStateName="true" UseValidator="true" /></td>
			<td>&nbsp;&nbsp;&nbsp;<span class="left">Number*:</span></td>
			<td><asp:TextBox ID="tbDLNumber" runat="server" MaxLength="20"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvDLNumber" runat="server" ControlToValidate="tbDLNumber"
					ErrorMessage="Drivers License Number is required." Display="Dynamic"><asp:Image ID="Image14" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Number is required" /></asp:RequiredFieldValidator></td>
			<td rowspan="2" class="instruction" style="padding: 4px 0px 0px 8px;"><%--<cc2:HelpLink ID="HelpLink2" runat="server" HelpTagName="TOP" 
					HelpUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx" 
					NavigateUrl="~/SiteControls/UserProfile/UserProfileHelp.aspx"
					EnableJavaScript="True" Target="_blank" WindowHeight="500" WindowWidth="500">▪ Why does Sunbelt need this?</cc2:HelpLink>--%>▪ We take your privacy very seriously!&nbsp; Your driver license 
				information is only used when you make a reservation and is never shared with 
				third parties.</td>
		</tr>
		<tr id="trDL3" runat="server">
			<td class="left">Date of Birth*:</td>
			<td colspan="3"><asp:TextBox ID="tbDLDateOfBirth" runat="server" MaxLength="15" Columns="12"></asp:TextBox>
			<asp:CustomValidator ID="cvDate" runat="server" 
			ErrorMessage="Date of birth is required." ClientValidationFunction="UPEU.DateValidator" 
			ControlToValidate="tbDLDateOfBirth" onservervalidate="cvDate_ServerValidate" ValidateEmptyText="True"><asp:Image ID="Image13" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="DOB is required" /></asp:CustomValidator></td>
		</tr>
		</table>
		</cc2:RoundedCornerPanel>
		
		<br />
		<cc2:RoundedCornerPanel ID="rcp3" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 660px">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2" class="h2">CONTACT INFORMATION</td>
		</tr>
		<tr>
			<td style="font-size:smaller; width:1%;">&nbsp;</td>
			<td class="right" style="font-size:smaller;">* Indicates a required field</td>
		</tr>
		<tr>
			<td class="left">Phone*:</td>
			<td class="right"><asp:TextBox ID="tbPhone" runat="server" MaxLength="20"></asp:TextBox>
			<asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="tbPhone"
					ErrorMessage="Phone is required." Display="Dynamic"><asp:Image ID="Image17" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Phone is required" /></asp:RequiredFieldValidator><asp:CustomValidator ID="cvPhone" runat="server" ErrorMessage="Invalid Phone Number. Please include area code." Text="*"></asp:CustomValidator>
		        <cc3:MaskedEditExtender ID="PhoneNumMaskedEditExtender" runat="server" AutoComplete="false" 
		            Mask="(999)-999-9999" MaskType="Number" TargetControlID="tbPhone" >
                </cc3:MaskedEditExtender>	        
		    </td>
		</tr>
		<tr>
			<td class="left">Fax:</td>
			<td class="right"><asp:TextBox ID="tbFax" runat="server" MaxLength="20"></asp:TextBox>
			<asp:CustomValidator ID="cvFax" runat="server" ErrorMessage="Invalid Fax Number. Please include area code." Text="*"></asp:CustomValidator>
		        <cc3:MaskedEditExtender ID="FaxNumMaskedEditExtender" runat="server" AutoComplete="false" 
		            Mask="(999)-999-9999" MaskType="Number" TargetControlID="tbFax" ></cc3:MaskedEditExtender>
		    </td>
		</tr>
		<tr>
			<td colspan="2" class="h3">MAILING ADDRESS</td>
		</tr>
		<tr>
			<td class="left">Address 1*:</td>
			<td class="right"><asp:TextBox ID="tbAddress1" runat="server" MaxLength="40" Columns="40"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvAddress1" runat="server" ControlToValidate="tbAddress1"
					ErrorMessage="Address is required." Display="Dynamic"><asp:Image ID="Image18" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Address is required" /></asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="left">Address 2:</td>
			<td class="right"><asp:TextBox ID="tbAddress2" runat="server" MaxLength="40" Columns="40"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="left">Address 3:</td>
			<td class="right"><asp:TextBox ID="tbAddress3" runat="server" MaxLength="40" Columns="40"></asp:TextBox></td>
		</tr>
		<tr>
			<td class="left">City*:</td>
			<td class="right"><asp:TextBox ID="tbCity" runat="server" MaxLength="20" Columns="30"></asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="tbCity"
					ErrorMessage="City is required." Display="Dynamic"><asp:Image ID="Image15" runat="server"
					ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="City is required" /></asp:RequiredFieldValidator></td>
		</tr>
		<tr>
			<td class="left">State*:</td>
			<td class="right"><uc2:USStatesDropDownList ID="ddlState" runat="server" ShortStateName="True" UseValidator="true" />
				&nbsp;&nbsp;&nbsp;<span class="left">Zip&nbsp;Code*:</span><asp:TextBox ID="tbPostalCode" runat="server" MaxLength="20" Columns="10"></asp:TextBox>
				<asp:CustomValidator ID="cvPostalCode" runat="server" 
			        ErrorMessage="Zip Code is required." ClientValidationFunction="UPEU.ZipValidator" 
			        ControlToValidate="tbPostalCode" onservervalidate="cvZip_ServerValidate" 
			        ValidateEmptyText="True">
			            <asp:Image ID="Image19" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="Zip Code is required" />
			        </asp:CustomValidator></td>
		</tr>
		<%--<tr>
			<td class="left">
				Country:</td>
			<td class="right">
				<asp:TextBox ID="tbCountry" runat="server" MaxLength="30" Columns="30"></asp:TextBox></td>
			<td>&nbsp;</td>
		</tr>--%>
		<tr>
			<%--<td colspan="5" class="h3">PROMOTIONAL STUFF</td>--%>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2"><asp:CheckBox ID="cbSentMePromotions" runat="server" Text="&nbsp;I would like to hear from Sunbelt about products, services, and events." Checked="True" /></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		</table>
		</cc2:RoundedCornerPanel>
		
		<br />
		<cc2:RoundedCornerPanel ID="rcp4" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 660px">
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="right">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<td><cc1:captchacontrol id="CaptchaControl1" runat="server" ErrorImage="~/SiteControls/UserProfile/Images/icon_warn2.gif" CaptchaMaxTimeout="300"></cc1:captchacontrol></td>
					<td class="instruction" style="vertical-align:bottom; padding:0px 0px 12px 4px;">▪ Not case sensitive.</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr id="trUserAgreement" runat="server">
		    <td>
		        *<asp:CheckBox ID="chkUserAgreement" runat="server" Text="" />&nbsp;Yes, I have read and accept the 
		        <asp:HyperLink ID="hlUA" runat="server" Target="_blank" NavigateUrl="~/UserAgreement.htm" Text="Website User Agreement of Sunbelt Rentals"></asp:HyperLink>
		        <asp:CustomValidator ID="cvUserAgreement" runat="server" ErrorMessage="You must accept the User Agreement of Sunbelt Rentals to create an account." >
					<asp:Image ID="Image9" runat="server" ImageUrl="~/SiteControls/UserProfile/Images/icon_warn2.gif" ImageAlign="AbsMiddle" ToolTip="User Agreement is required" />
				</asp:CustomValidator>
		    </td>
		</tr>	 
		<tr><td>&nbsp;</td></tr>
		</table>
		</cc2:RoundedCornerPanel>
		
		<br />		
		<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="left" style="text-align:right; width:660px;">
				<asp:HyperLink ID="hlCancel" runat="server" ImageUrl="~/App_Themes/Main/Buttons/cancel_80x20_darkgray.gif" 
					Visible="False">Cancel</asp:HyperLink><asp:Literal ID="litCancel" runat="server" 
					Visible="False"></asp:Literal><asp:ImageButton ID="btnAddUser" runat="server" AlternateText="Create Account" 
					ImageUrl="~/App_Themes/Main/Buttons/create_account_100x20_darkred.gif" 
					OnClick="btnAddUser_Click" /><asp:ImageButton ID="btnUpdateUser" runat="server" AlternateText="Update" 
					ImageUrl="~/App_Themes/Main/Buttons/update_80x20_darkred.gif" OnClick="btnUpdateUser_Click" />
			</td>
		</tr>
	</table>
	<table cellpadding="0" cellspacing="0" style="width: 660px;">
		<tr>
			<td class="right" colspan="2"><hr style="width: 660px" /></td>
		</tr>
		<tr id="trEditLogin" runat="server">
			<td colspan="2" class="right"><asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="../../EditLogin.aspx">Change My Login</asp:HyperLink></td>
		</tr>
		<tr id="trAccountTools" runat="server">
			<td colspan="2" class="right"><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/AccountTools/Default.aspx">Account Tools</asp:HyperLink></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome1" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>
<div id="divEmailSent" runat="server">
	<cc2:RoundedCornerPanel ID="rcp6" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 400px">
		<uc1:UPSendVerifyEmail ID="UPSendVerifyEmail1" runat="server" />
	</cc2:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width: 100%;">
		<tr>
			<td class="right2" colspan="2"><hr /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome3" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>
<div id="divUserUpdated" runat="server">
	<cc2:RoundedCornerPanel ID="rcp5" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 400px">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="h2">PROFILE UPDATED</td>
		</tr>
		<tr>
			<td class="left"><br />Your user profile has been updated.<br /></td>
		</tr>
	</table>
	</cc2:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<tr>
			<td class="right2" colspan="2"><hr /></td>
		</tr>
		<tr id="trEditLogin2" runat="server">
			<td colspan="2" class="right"><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="../../EditLogin.aspx">Change My Login</asp:HyperLink></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/AccountTools/Default.aspx">Account Tools</asp:HyperLink></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome2" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>
<div id="divError" runat="server">
	<cc2:RoundedCornerPanel ID="rcp7" runat="server" SkinID="rcpGreyNoTitle" TableStyle="width: 400px">
		<br />An error has occurred. Please contact customer support.<br />
		Refer to error code: <asp:Label ID="lbErrorCode" runat="server" Text="lbErrorCode" Font-Bold="True"></asp:Label><br />&nbsp;
	</cc2:RoundedCornerPanel>
	<table cellpadding="0" cellspacing="0" style="width: 400px;">
		<tr>
			<td class="right2" colspan="2"><hr /></td>
		</tr>
		<tr>
			<td colspan="2" class="right"><asp:HyperLink ID="hlHome4" runat="server" NavigateUrl="~/Default.aspx">Home</asp:HyperLink></td>
		</tr>
	</table>
</div>
