﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Sunbelt.Tools;
using Sunbelt.BusinessObjects;


public partial class UserProfile_UPAccountType : System.Web.UI.UserControl
{
	public sealed class AccountCode
	{
		static public int OK = 0;
		static public int BAD_ACCOUNT = 1;
		static public int BAD_NAME = 2;
		static public int BAD_TAXID = 4;
		static public int BAD_DUNS = 8;
		static public int PARAMETERS_MISSING = 16;
	}
	
	public int AccountNumber
	{
		get { return ConvertTools.ToInt32(tbAccountNumber.Text.Trim()); }
	}
	public string CustomerName
	{
		get { return tbCompanyName.Text.Trim().ToUpper(); }
	}
    public int TaxIdNumber
    {
        get { return ConvertTools.ToInt32(tbTaxId.Text.Trim()); }
    }
    public int DunsIDNumber
    {
        get { return ConvertTools.ToInt32(tbDunsId.Text.Trim()); }
    }

	public event EventHandler<EventArgs> CashCustomerChecked;
	public event EventHandler<EventArgs> CreditCustomerChecked;
	
	protected void Page_Load(object sender, EventArgs e)
    {
		if (!IsPostBack)
		{
			radCashCustomer.Checked = true;
			UpdateDisplay();
		}
    }

	public int IsValidateCreditAccount()
	{
		if (radCreditCustomer.Checked)
		{
			//Make call here...
			return CustomerData.ValidateCustomerAccount(AccountNumber, CustomerName, TaxIdNumber, DunsIDNumber); 
		}
		return AccountCode.OK;
	}
	
	private void UpdateDisplay()
	{
		divCreditCustomer.Visible = radCreditCustomer.Checked;
		divCashCustomer.Visible = radCashCustomer.Checked;
	}

	protected override void OnPreRender(EventArgs e)
	{
		//Register the client javascript.
		SBNet.Tools.IncludeEntryControlsClientScript(this.Page);

		if (!Page.ClientScript.IsStartupScriptRegistered("UPAccountType"))
		{
           
            StringBuilder sbCode = new StringBuilder();

            sbCode.Append("var UPAT = {\r\n");

            sbCode.Append("WindowOnLoad:function() {\r\n");
            //sbCode.Append("alert('UPAT.WindowOnLoad')\r\n");
            sbCode.Append("  if(document.getElementById('" + tbAccountNumber.ClientID + "') != null)\r\n");
			sbCode.Append("  {\r\n");
			sbCode.Append("    SBNet.EC.NumericEntry(document.getElementById('" + tbAccountNumber.ClientID + "'), true);\r\n");
            sbCode.Append("    SBNet.EC.SafeTextEntry(document.getElementById('" + tbCompanyName.ClientID + "'), true);\r\n");
            sbCode.Append("    SBNet.EC.NumericEntry(document.getElementById('" + tbTaxId.ClientID + "'), true);\r\n");
            sbCode.Append("    SBNet.EC.NumericEntry(document.getElementById('" + tbDunsId.ClientID + "'), true);\r\n");
			sbCode.Append("  }\r\n");
			sbCode.Append("}\r\n");

			//sbCode.Append(",ValidateIds:function(sender, args){\r\n");
			//sbCode.Append("alert('validateIds')\r\n");
			//sbCode.Append("  var dunsId =  document.getElementById('" + tbDunsId.ClientID + "');\r\n");
			//sbCode.Append("  if(dunsId.value == \"\")\r\n");
			//sbCode.Append("    args.IsValid = false;\r\n");
			//sbCode.Append("  else\r\n");
			//sbCode.Append("    args.IsValid = true;\r\n");
			//sbCode.Append("}");

			sbCode.Append("}\r\n"); //End class

			sbCode.Append("if(typeof(Sys) != 'undefined')\r\n");
			sbCode.Append("  Sys.Application.add_load(UPAT.WindowOnLoad)\r\n");
			sbCode.Append("else\r\n");
			sbCode.Append("  SBNet.Util.AttachEvent(window, 'onload', UPAT.WindowOnLoad);\r\n");
			Page.ClientScript.RegisterStartupScript(this.GetType(), "UPAccountType", sbCode.ToString(), true);

		}
		base.OnPreRender(e);
	}

	protected void radCashCustomer_CheckedChanged(object sender, EventArgs e)
	{
		//cbCreditCustomer.Checked = false;
		UpdateDisplay();

		EventHandler<EventArgs> eh = CashCustomerChecked;
		if (eh != null)
			eh(this, EventArgs.Empty);
	}

	protected void radCreditCustomer_CheckedChanged(object sender, EventArgs e)
	{
		//cbCashCustomer.Checked = false;
		UpdateDisplay();

		EventHandler<EventArgs> eh = CreditCustomerChecked;
		if (eh != null)
			eh(this, EventArgs.Empty);
	}
	protected void cvDUNSNumber_ServerValidate(object source, ServerValidateEventArgs args)
	{
		bool b1  = (tbDunsId.Text.Trim() != string.Empty);
		bool b2 = (tbTaxId.Text.Trim() != string.Empty);
		args.IsValid = b1 || b2;
	}
	protected void cvTaxId_ServerValidate(object source, ServerValidateEventArgs args)
	{
		bool b1 = (tbDunsId.Text.Trim() != string.Empty);
		bool b2 = (tbTaxId.Text.Trim() != string.Empty);
		args.IsValid = b1 || b2;
	}
}