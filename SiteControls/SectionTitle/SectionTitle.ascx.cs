using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class controls_sectiontitle : System.Web.UI.UserControl
{
    private string title;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Page.Title = title;
    }

    public string Title
    {
        get { return title; }
        set
        {
            title = "Sunbelt Rentals : " + value;
            SectionTitle.Text = value;
        }
    }
}
