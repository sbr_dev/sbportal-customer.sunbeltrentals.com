﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using Sunbelt.Tools;

public partial class Test_GroupingControl : Sunbelt.Web.UserControl
{
    private string TimeStamp
    {
        set { SetViewState("TimeStamp", value); }
        get { return (string)GetViewState("TimeStamp", null); }
    }

    public DataTable dtData
    {
        set 
        { 
            Sunbelt.Tools.WebTools.SetSessionCache("reportdata", value, new TimeSpan(0, 5, 0));
            newDtData = true;
            SecondarySortingColumn = "";
        }
        get { return (DataTable)Sunbelt.Tools.WebTools.GetSessionCache("reportdata", null); }
    }

    public DataView dvSorted = null;

    public string GroupedByColumnName
    {
        set { SetViewState("colName", value); }
        get { return (string)GetViewState("colName", ""); }
    }

    public bool GroupData
    {
        get { return Convert.ToBoolean(hfGroupData.Value); }
    }

    public bool AllowColumnSorting
    {
        set { SetViewState("colSorting", value); }
        get { return (bool)GetViewState("colSorting", false); }
    }

    public bool ShowRows
    {
        set { SetViewState("ShowRows", value); }
        get { return (bool)GetViewState("ShowRows", false); }
    }

    public string HeaderRowClass
    {
        set { SetViewState("headerClass", value); }
        get { return (string)GetViewState("headerClass", ""); }
    }

    public string HeaderRowClassWithSorting
    {
        set { SetViewState("headerSort", value); }
        get { return (string)GetViewState("headerSort", ""); }
    }

    public virtual string ImageSortingUp
    {
        set { SetViewState("ImageUp", value); }
        get { return ResolveUrl((string)GetViewState("ImageUp", "")); }
    }

    public virtual string ImageSortingDown
    {
        set { SetViewState("ImageDown", value); }
        get { return ResolveUrl((string)GetViewState("ImageDown", "")); }
    }

    public virtual string ImageSortingNone
    {
        set { SetViewState("ImageNone", value); }
        get { return ResolveUrl((string)GetViewState("ImageNone", "")); }
    }

    private string SortingDirection
    {
        set { SetViewState("sortOrder", value); }
        get { return GetViewState("sortOrder", "").ToString(); }
    }

    public string RowClass
    {
        set { SetViewState("rowClass", value); }
        get { return (string)GetViewState("rowClass", ""); }
    }

    public string AlternatingRowClass
    {
        set { SetViewState("altClass", value); }
        get { return (string)GetViewState("altClass", ""); }
    }

    public string TableClass
    {
        set { SetViewState("tblClass", value); }
        get { return (string)GetViewState("tblClass", ""); }
    }

    public string SecondarySortingColumn
    {
        set 
        { 
            if ((string)GetViewState("secondSortingCol", "") != value)
                SortingDirection = "";

            SetViewState("secondSortingCol", value);
        }
        get { return (string)GetViewState("secondSortingCol", ""); }
    }

    public Unit Width
    {
        set { SetViewState("width", value); }
        get 
        { 
            string s = GetViewState("width", "0px").ToString();
            if (s.Contains("px"))
                s = s.Replace("px", "");

            return (Unit)Convert.ToInt32(s);
        }
    }

    public bool AllowPaging
    {
        set { SetViewState("allowPaging", value); }
        get { return Convert.ToBoolean(GetViewState("allowPaging", false)); }
    }

    public int PagingSize
    {
        set { SetViewState("pageSize", value); }
        get { return Convert.ToInt32(GetViewState("pageSize", 10)); }
    }
    protected int StartPageIndex
    {
        set { SetViewState("startPageIndex", value); }
        get { return Convert.ToInt32(GetViewState("startPageIndex", 0)); }
    }

    public string LastGroupedColumnName = "";
    public bool IsAlternatingColumn = false;
    public int GroupCount = 0;
    public bool newDtData = false;
    public bool blnEmptyStringGroupedCol = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        SBNet.Tools.IncludeSBNetClientScript(Page);
        RegisterClientScript();

        if (!IsPostBack)
        {
            chkGroupData.Checked = GroupData;
        }
        else
        {
            BindData();
        }

        if (GroupData)
        {
            ddlColumns.Style["display"] = "block";
            btnView.Style["display"] = "block";
            lnkShowAll.Style["display"] = "block";
            lnkHideAll.Style["display"] = "block";
        }
        else
        {
            ddlColumns.Style["display"] = "none";
            btnView.Style["display"] = "none";
            lnkShowAll.Style["display"] = "none";
            lnkHideAll.Style["display"] = "none";
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (Width != Unit.Pixel(0))
        {
            Panel pnl = (Panel)listView.FindControl("pnl");
            if (pnl != null)
                pnl.Width = Width;
        }
        
        BuildHeaders();
    }

    protected void BuildHeaders()
    {
        Literal lit = (Literal)listView.FindControl("litHeader");
        if (lit != null)
        {
            if (GroupData)
            {
                lit.Text = "<th></th>";
            }
            else
                lit.Text = "";

            if (newDtData)
            {
                SecondarySortingColumn = "";
                ddlColumns.Items.Clear();
            }

            if (AllowColumnSorting)
            {
                if (SortingDirection == "" && SecondarySortingColumn != "")
                    SortingDirection = "down";

                foreach (DataColumn dc in dtData.Columns)
                {
                    if (SortingDirection == "" || SortingDirection == "none")
                    {
                        lit.Text += "<th class='" + HeaderRowClassWithSorting
                            + "' onclick=\"Grouping.Prototype.SortByHeader('"
                            + dc.ColumnName + "')\" style=\"background-image: url('" + ImageSortingNone + "');background-position: right; background-repeat: no-repeat; padding-right: 10px;\">"
                            + dc.ColumnName + "</th>";   
                    }
                    else if (SortingDirection == "down")
                    {
                        if (dc.ColumnName == SecondarySortingColumn)
                        {
                            lit.Text += "<th class='" + HeaderRowClassWithSorting
                                + "' onclick=\"Grouping.Prototype.SortByHeader('"
                                + dc.ColumnName + "')\" style=\"background-image: url('" + ImageSortingDown + "');background-position: right; background-repeat: no-repeat; padding-right: 10px;\">"
                                + dc.ColumnName + "</th>";
                        }
                        else
                        {
                            lit.Text += "<th class='" + HeaderRowClassWithSorting
                            + "' onclick=\"Grouping.Prototype.SortByHeader('"
                            + dc.ColumnName + "')\" style=\"background-image: url('" + ImageSortingNone + "');background-position: right; background-repeat: no-repeat; padding-right: 10px;\">"
                            + dc.ColumnName + "</th>";
                        }
                    }
                    else if (SortingDirection == "up")
                    {
                        if (dc.ColumnName == SecondarySortingColumn)
                        {
                            lit.Text += "<th class='" + HeaderRowClassWithSorting
                                + "' onclick=\"Grouping.Prototype.SortByHeader('"
                                + dc.ColumnName + "')\" style=\"background-image: url('" + ImageSortingUp + "');background-position: right; background-repeat: no-repeat; padding-right: 10px;\">"
                                + dc.ColumnName + "</th>";
                        }
                        else
                        {
                            lit.Text += "<th class='" + HeaderRowClassWithSorting
                            + "' onclick=\"Grouping.Prototype.SortByHeader('"
                            + dc.ColumnName + "')\" style=\"background-image: url('" + ImageSortingNone + "');background-position: right; background-repeat: no-repeat; padding-right: 10px;\">"
                            + dc.ColumnName + "</th>";
                        }
                    }
                    if (newDtData)
                        ddlColumns.Items.Add(new ListItem(dc.ColumnName, dc.ColumnName));
                }
            }
            else
            {
                //ddlColumns.Items.Clear();
                foreach (DataColumn dc in dtData.Columns)
                {
                    lit.Text += "<th>" + dc.ColumnName + "</th>";
                    if (newDtData)
                        ddlColumns.Items.Add(new ListItem(dc.ColumnName, dc.ColumnName));
                }
            }
            
        }
    }

    protected void RegisterClientScript()
    {
        if (!Page.ClientScript.IsClientScriptIncludeRegistered("GroupingGridview.js"))
            Page.ClientScript.RegisterClientScriptInclude("GroupingGridview.js", ResolveUrl("GroupingGridview.js"));

        //Register the Report startup script.
        if (!Page.ClientScript.IsStartupScriptRegistered("Grouping"))
        {
            StringBuilder sbCode = new StringBuilder();
            sbCode.Append("Grouping.Prototype.Initialize('" + TableClass + "','" + HeaderRowClass + "','" + LinkButton1.UniqueID + "','"
                + hfShowGroupingIndex.ClientID + "');");
            
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Grouping", sbCode.ToString(), true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "Grouping", sbCode.ToString(), true);
        }
    }

    public void BindData()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(GroupedByColumnName);
        if (!string.IsNullOrEmpty(SecondarySortingColumn) && SecondarySortingColumn != GroupedByColumnName)
            dt.Columns.Add(SecondarySortingColumn);

        if (GroupData == true && !string.IsNullOrEmpty(GroupedByColumnName))
        {
            if (SecondarySortingColumn != "")
            {
                DataRow[] drs;
                if (SortingDirection == "down")
                    drs = dtData.Select("", GroupedByColumnName + " ASC, " + SecondarySortingColumn + " ASC");
                else if (SortingDirection == "none" || SortingDirection == "")
                    drs = dtData.Select("", GroupedByColumnName + " ASC");
                else
                    drs = dtData.Select("", GroupedByColumnName + " ASC, " + SecondarySortingColumn + " DESC");

                foreach (DataRow dr in drs)
                {
                    if (!string.IsNullOrEmpty(SecondarySortingColumn) && SecondarySortingColumn != GroupedByColumnName)
                        dt.Rows.Add(dr[GroupedByColumnName].ToString(), dr[SecondarySortingColumn].ToString());
                    else
                        dt.Rows.Add(dr[GroupedByColumnName].ToString());
                }
            }
            else
            {
                DataRow[] drs;
                drs = dtData.Select("", GroupedByColumnName + " ASC");

                foreach (DataRow dr in drs)
                {
                    if (!string.IsNullOrEmpty(SecondarySortingColumn))
                        dt.Rows.Add(dr[GroupedByColumnName].ToString(), dr[SecondarySortingColumn].ToString());
                    else
                        dt.Rows.Add(dr[GroupedByColumnName].ToString());
                }
            }

            listView.DataSource = dt;
            listView.DataBind();
        }
        else if (SecondarySortingColumn != "")
        {
            DataView dv;
            if (SortingDirection == "down")
                dv = new DataView(dtData, "", SecondarySortingColumn + " ASC", DataViewRowState.CurrentRows);
            else if (SortingDirection == "none" || SortingDirection == "")
                dv = new DataView(dtData, "", "", DataViewRowState.CurrentRows);
            else
                dv = new DataView(dtData, "", SecondarySortingColumn + " DESC", DataViewRowState.CurrentRows);

            listView.DataSource = dv;
            GroupedByColumnName = dv.Table.Columns[0].ColumnName;
            listView.DataBind();
        }
        else
        {
            chkGroupData.Checked = false;
            hfGroupData.Value = "false";
            ddlColumns.Style["display"] = "none";
            btnView.Style["display"] = "none";
            lnkHideAll.Style["display"] = "none";
            lnkShowAll.Style["display"] = "none";
            listView.DataSource = dtData;
            GroupedByColumnName = dtData.Columns[0].ColumnName; //Need to give it a column to bind to even though it will never display.
            listView.DataBind();
        }
        BindPager();
    }

    protected void BindPager()
    {
        if (AllowPaging)
        {
            int i = dtData.Rows.Count / PagingSize;
            pager.Text = "<table><tr><td> << </td><td> < </td>";
            for (int j = 0; j < i; j++)
            {
                LinkButton lnk = new LinkButton();
                lnk.Text = j.ToString();
                lnk.CommandArgument = j.ToString();
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                lnk.RenderControl(htw);
                pager.Text += "<td>" + sw.ToString() + "</td>";
            }
            pager.Text += "<td> > </td><td> >> </td></tr></table>";
        }
        else
            pager.Text = "";
    }

    public void DownloadGridview()
    {
        ShowRows = true;
        DisplayShowHideAll(false);
        BindData();
        BuildHeaders();

        IList<ListViewDataItem> items = listView.Items;
        foreach (ListViewDataItem lvi in items)
        {
            HtmlTableCell tc = (HtmlTableCell)lvi.FindControl("tdExtraCol");
            if (tc != null)
                tc.Visible = false;

        }

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        listView.RenderControl(htw);

        Response.Clear();

        HttpContext.Current.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Report.xls"));

        HttpContext.Current.Response.ContentType = "application/vnd.xls";//"text/comma-seperated-values";
        Response.Charset = "";

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        Response.Write(sw);

        Response.End();

        DisplayShowHideAll(true);
    }

    public void DisplayShowHideAll(bool blnShowControls)
    {
        lnkHideAll.Visible = blnShowControls;
        lnkShowAll.Visible = blnShowControls;
    }

    public void ClearViewState()
    {
        hfShowGroupingIndex.Value = "";
    }

    protected void listView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        //Set the column span for the grouped header
        HtmlTableCell th = (HtmlTableCell)e.Item.FindControl("GroupColumn");
        if (th != null)
        {
            if (dtData != null)
                th.ColSpan = dtData.Columns.Count;
        }

        if (GroupData)
        {
            Image img = (Image)e.Item.FindControl("imgExpandCollapse");
            if (img != null)
            {
                GroupCount++;  //This will be used to create our unique group names so we can hide/show them off of click
                if (ShowRows)
                    img.ImageUrl = ResolveUrl("~/Images/minus.png");
                else
                    img.ImageUrl = ResolveUrl("~/Images/plus.png");

                img.Attributes["onclick"] = "Grouping.Prototype.ShowHideGroup('" + img.ClientID + "','block" + GroupCount + "', false);";
            }

            //Set the Grouped items together.
            Label GroupedItem = (Label)e.Item.FindControl("lblGroupBy");
            if (GroupedItem != null)
            {
                Literal litCol = (Literal)e.Item.FindControl("litColumns");


                if (LastGroupedColumnName != GroupedItem.Text.Trim(' ') && dtData.Columns[GroupedByColumnName].DataType != System.Type.GetType("System.DateTime"))
                {
                    if (GroupedItem.Text.Contains("'"))
                        GroupedItem.Text = GroupedItem.Text.Replace("'", "");

                    LastGroupedColumnName = GroupedItem.Text.Trim(' ');

                    DataRow[] drDetails;
                    if (SecondarySortingColumn != "" && SortingDirection == "down")
                        drDetails = dtData.Select("[" + GroupedByColumnName + "]='" + GroupedItem.Text.TrimEnd(' ') + "'", SecondarySortingColumn + " ASC");
                    else if (SecondarySortingColumn != "" && SortingDirection == "up")
                        drDetails = dtData.Select("[" + GroupedByColumnName + "]='" + GroupedItem.Text.TrimEnd(' ') + "'", SecondarySortingColumn + " DESC");
                    else if (SecondarySortingColumn != "" && string.IsNullOrEmpty(SortingDirection) == true)
                        drDetails = dtData.Select("[" + GroupedByColumnName + "]='" + GroupedItem.Text.TrimEnd(' ') + "'", SecondarySortingColumn + " ASC");
                    else
                        drDetails = dtData.Select("[" + GroupedByColumnName + "]='" + GroupedItem.Text.TrimEnd(' ') + "'");
                    int count = 0;
                    foreach (DataRow dr in drDetails)
                    {
                        count++; //Count to find the last grouped row

                        if (IsAlternatingColumn)
                        {
                            if (ShowRows) //Set display for rows
                                litCol.Text += "<tr class='" + AlternatingRowClass + "' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                            else
                                litCol.Text += "<tr class='" + AlternatingRowClass + "' style='display: none;' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                        }
                        else
                        {
                            if (ShowRows) //Set display for rows
                                litCol.Text += "<tr class='dgMain' name='block" + GroupCount + "'><td>&nbsp;</td>";
                            else
                                litCol.Text += "<tr class='dgMain' style='display: none;' name='block" + GroupCount + "'><td>&nbsp;</td>";
                        }

                        for (int i = 0; i < dr.ItemArray.Length; i++)
                        {
                            if (drDetails.Length == count)
                            {
                                if (dr.ItemArray[i].ToString() == "")
                                    litCol.Text += "<td>&nbsp;</td>";
                                else
                                    litCol.Text += "<td>" + dr.ItemArray[i].ToString() + "</td>";
                            }
                            else
                            {
                                if (dr.ItemArray[i].ToString() == "")
                                    litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>&nbsp;</td>";
                                else
                                    litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>" + dr.ItemArray[i].ToString() + "</td>";
                            }
                        }
                        litCol.Text += "</tr>";

                        if (IsAlternatingColumn)
                            IsAlternatingColumn = false;
                        else
                            IsAlternatingColumn = true;
                    }
                }
                else if (dtData.Columns[GroupedByColumnName].DataType == System.Type.GetType("System.DateTime") && GroupedItem.Text.Trim(' ') != "")
                {
                    if (DateTimeTools.ToAcceptableDataViewString(ConvertTools.ToDateTime(LastGroupedColumnName, DateTime.MinValue))
                        != DateTimeTools.ToAcceptableDataViewString(ConvertTools.ToDateTime(GroupedItem.Text, DateTime.MinValue)))
                    {
                        LastGroupedColumnName = GroupedItem.Text;

                        DateTime dateTime = Convert.ToDateTime(GroupedItem.Text.TrimEnd(' '));
                        DataRow[] drDetails = dtData.Select("[" + GroupedByColumnName + "]>=#"
                            + dateTime.AddMinutes(-1).ToString(CultureInfo.CurrentCulture) + "# AND [" + GroupedByColumnName + "]<=#"
                            + dateTime.AddMinutes(1).ToString(CultureInfo.CurrentCulture) + "#");
                        int count = 0;
                        foreach (DataRow dr in drDetails)
                        {
                            count++; //Count to find the last grouped row

                            if (IsAlternatingColumn)
                            {
                                if (ShowRows) //Set display for rows
                                    litCol.Text += "<tr class='" + AlternatingRowClass + "' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                                else
                                    litCol.Text += "<tr class='" + AlternatingRowClass + "' style='display: none;' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                            }
                            else
                            {
                                if (ShowRows) //Set display for rows
                                    litCol.Text += "<tr class='dgMain' name='block" + GroupCount + "'><td>&nbsp;</td>";
                                else
                                    litCol.Text += "<tr class='dgMain' style='display: none;' name='block" + GroupCount + "'><td>&nbsp;</td>";
                            }

                            for (int i = 0; i < dr.ItemArray.Length; i++)
                            {
                                if (drDetails.Length == count)
                                {
                                    if (dr.ItemArray[i].ToString() == "")
                                        litCol.Text += "<td>&nbsp;</td>";
                                    else
                                        litCol.Text += "<td>" + dr.ItemArray[i].ToString() + "</td>";
                                }
                                else
                                {
                                    if (dr.ItemArray[i].ToString() == "")
                                        litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>&nbsp;</td>";
                                    else
                                        litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>" + dr.ItemArray[i].ToString() + "</td>";
                                }
                            }
                            litCol.Text += "</tr>";

                            if (IsAlternatingColumn)
                                IsAlternatingColumn = false;
                            else
                                IsAlternatingColumn = true;
                        }
                    }
                    else
                    {
                        e.Item.Visible = false;
                        IsAlternatingColumn = false;
                    }
                }
                else if (GroupedItem.Text.Trim(' ') == "" && blnEmptyStringGroupedCol == false)
                {
                    // Datatable select method will not work on a blank string, but since its sorted just loop through the current datatable
                    //and stop once you hit a new groupeditem.
                    blnEmptyStringGroupedCol = true;
                    for (int i = 0; i < dtData.Rows.Count; i++)
                    {
                        if (dtData.Rows[i][GroupedByColumnName] == System.DBNull.Value)
                        {
                            int count = 0;

                            count++; //Count to find the last grouped row

                            if (IsAlternatingColumn)
                            {
                                if (ShowRows) //Set display for rows
                                    litCol.Text += "<tr class='" + AlternatingRowClass + "' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                                else
                                    litCol.Text += "<tr class='" + AlternatingRowClass + "' style='display: none;' name=\"block" + GroupCount + "\"><td>&nbsp;</td>";
                            }
                            else
                            {
                                if (ShowRows) //Set display for rows
                                    litCol.Text += "<tr class='dgMain' name='block" + GroupCount + "'><td>&nbsp;</td>";
                                else
                                    litCol.Text += "<tr class='dgMain' style='display: none;' name='block" + GroupCount + "'><td>&nbsp;</td>";
                            }

                            for (int j = 0; j < dtData.Columns.Count; j++)
                            {
                                if (dtData.Rows[i].ItemArray[j].ToString() == "")
                                    litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>&nbsp;</td>";
                                else
                                    litCol.Text += "<td style='border-bottom: solid 1px #ece9d8;'>" + dtData.Rows[i].ItemArray[j].ToString() + "</td>";
                            }

                            litCol.Text += "</tr>";
                            if (IsAlternatingColumn)
                                IsAlternatingColumn = false;
                            else
                                IsAlternatingColumn = true;
                        }
                    }
                }
                else
                {
                    e.Item.Visible = false;
                    IsAlternatingColumn = false;
                }
            }
        }
        else
        {
            HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("GroupRow");
            if (tr != null)
                tr.Visible = false;

            if (AllowPaging && GroupCount >= StartPageIndex && GroupCount <= StartPageIndex + PagingSize)
            {
                Literal litCol = (Literal)e.Item.FindControl("litColumns");
                if (IsAlternatingColumn)
                    litCol.Text = "<tr class='" + AlternatingRowClass + "'>";
                else
                    litCol.Text = "<tr>";

                if (dvSorted == null)
                {
                    if (SecondarySortingColumn != "" && SortingDirection == "down")
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " ASC", DataViewRowState.CurrentRows);
                    else if (SecondarySortingColumn != "" && SortingDirection == "up")
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " DESC", DataViewRowState.CurrentRows);
                    else if (SecondarySortingColumn != "" && string.IsNullOrEmpty(SortingDirection) == true)
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " ASC", DataViewRowState.CurrentRows);
                    else
                        dvSorted = new DataView(dtData);
                }

                DataTable dtSortedTable = dvSorted.ToTable();

                for (int i = 0; i < dtSortedTable.Rows[GroupCount].ItemArray.Length; i++)
                {
                    litCol.Text += "<td>" + dtSortedTable.Rows[GroupCount].ItemArray[i].ToString() + "</td>";
                }
                litCol.Text += "</tr>";
            }
            else if (!AllowPaging)
            {
                Literal litCol = (Literal)e.Item.FindControl("litColumns");
                if (IsAlternatingColumn)
                    litCol.Text = "<tr class='" + AlternatingRowClass + "'>";
                else
                    litCol.Text = "<tr>";

                if (dvSorted == null)
                {
                    if (SecondarySortingColumn != "" && SortingDirection == "down")
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " ASC", DataViewRowState.CurrentRows);
                    else if (SecondarySortingColumn != "" && SortingDirection == "up")
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " DESC", DataViewRowState.CurrentRows);
                    else if (SecondarySortingColumn != "" && string.IsNullOrEmpty(SortingDirection) == true)
                        dvSorted = new DataView(dtData, "", SecondarySortingColumn + " ASC", DataViewRowState.CurrentRows);
                    else
                        dvSorted = new DataView(dtData);
                }

                DataTable dtSortedTable = dvSorted.ToTable();

                for (int i = 0; i < dtSortedTable.Rows[GroupCount].ItemArray.Length; i++)
                {
                    litCol.Text += "<td>" + dtSortedTable.Rows[GroupCount].ItemArray[i].ToString() + "</td>";
                }
                litCol.Text += "</tr>";
            }
            GroupCount++;

            if (IsAlternatingColumn)
                IsAlternatingColumn = false;
            else
                IsAlternatingColumn = true;
        }
    }

    protected void listViewLayoutCreated(object sender, EventArgs e)
    {

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        SecondarySortingColumn = Request.Params["__EVENTARGUMENT"];

        if (SortingDirection == "none")
            SortingDirection = "down";
        else if (SortingDirection == "down")
            SortingDirection = "up";
        else if (SortingDirection == "up")
            SortingDirection = "none";

        BindData();
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        ShowRows = true;
        ClearViewState();
        BindData();
    }

    protected void LinkButton2_Click1(object sender, EventArgs e)
    {
        ShowRows = false;
        ClearViewState();
        BindData();
    }
    protected void btnView_Click(object sender, ImageClickEventArgs e)
    {
        if (GroupData)
        {
            GroupedByColumnName = ddlColumns.SelectedValue;
            ClearViewState();
            BindData();
        }
        else
        {
            ClearViewState();
            BindData();
        }
    }
}
