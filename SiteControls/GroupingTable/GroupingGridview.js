﻿
Grouping = function()
{
	// Grouping detail stuff ////
	this.tbl = null;
    this.tblClass = null;
    this.tblHeader = null;
    this.tblHeaderClass = null;
    this.linkButton = null;
    this.hfShowGroupingIndex = null;
    this.ddlColumns = null;
    this.btnView = null;
    this.chkGroupData = null;
}

Grouping.Prototype = {

    Initialize:function(TableClass, TableHeaderClass, LinkButtonUniqueId, HfClientID)
    {
	    this.tbl = document.getElementById("tbl");
	    this.tblClass = TableClass;	
	    
	    this.tblHeader = document.getElementById("tblHeader");
	    this.tblHeaderClass = TableHeaderClass;
	    
	    this.linkButton = LinkButtonUniqueId;
	    this.hfShowGroupingIndex = document.getElementById(HfClientID);
	    
	    this.ddlColumns = SBNet.Util.FindASPXControl("SELECT", "ddlColumns");
	    this.btnView = SBNet.Util.FindASPXControl("INPUT", "btnView");
	    this.chkGroupData = SBNet.Util.FindASPXControl("INPUT", "chkGroupData");
	    
	    if (!SBNet.Util.isIE())
	    {
	        if (this.tbl != null)
	        {
	            this.tbl.className = this.tblClass;
	            this.tblHeader.className = this.tblHeaderClass;
	        }
	    }
	    else
	    {
	        if (this.tbl != null)
	        {
	            this.tbl.attributes["class"].value = this.tblClass;
	            this.tblHeader.attributes["class"].value = this.tblHeaderClass;
	        }
	    } 
	    
	    if (this.hfShowGroupingIndex != null)
	    {
	        if (this.hfShowGroupingIndex.value != "")
                this.ShowGroupOnPostBack();
        }
    },
    
    ShowGroupOnPostBack:function()
    {
        var strGroupingBlocks = this.hfShowGroupingIndex.value.split(";");
        for (var j=0; j < strGroupingBlocks.length; j++)
        {
            var objArray = strGroupingBlocks[j].split(",");
            if (objArray[0] != "")
                this.ShowHideGroup(objArray[0], objArray[1], true);       
        }
    },
    
    ShowHideGroup:function(imgObj, blockName, OnPostBack)
    {
        var trArray = document.getElementsByTagName("tr");
        var img = document.getElementById(imgObj);

        if (OnPostBack)
        {
            if (img != null)
            {
                img.src = SBNet.Util.webAddress.toLowerCase() + "Images/Minus.png";
                for(var i=0; i < trArray.length; i++)
                {
                    if (trArray[i].getAttribute("name") == blockName)
                    {
                        trArray[i].style.display = "";
                    }
                }
            }
        }
        else
        {
            if (img.src == SBNet.Util.webAddress.toLowerCase() + "Images/Minus.png")
            {
                img.src = SBNet.Util.webAddress.toLowerCase() + "Images/Plus.png";
                
                if (this.hfShowGroupingIndex != null)
                {
                    this.hfShowGroupingIndex.value = this.hfShowGroupingIndex.value.replace(imgObj + "," + blockName + ";", "");
                }
                
                for(i=0; i < trArray.length; i++)
                {
                    if (trArray[i].getAttribute("name") == blockName)
                    {
                        trArray[i].style.display = "none";
                    }
                }
            }
            else
            {
                img.src = SBNet.Util.webAddress.toLowerCase() + "Images/Minus.png";
                this.hfShowGroupingIndex.value += imgObj + "," + blockName + ";";
                for(i=0; i < trArray.length; i++)
                {
                    if (trArray[i].getAttribute("name") == blockName)
                    {
                        trArray[i].style.display = "";
                    }
                }
            }
        }
    },
    
    HideCol:function(rowNumber)
    {
        var Elements = this.tbl.getElementsByTagName('TD');
        for (var i=rowNumber-1;i<Elements.length;i=i+rowNumber)
        {
            Elements[i].style.display = "none";
        }
    },
    
    SortByHeader:function(headerName)
    {
        __doPostBack(this.linkButton, headerName);
    },
    
    ShowGroupingControls:function()
    {   
        var hfGroup = SBNet.Util.FindASPXControl("INPUT", "hfGroupData");
        var lnkShowAll = SBNet.Util.FindASPXControl("A", "lnkShowAll");
        var lnkHideAll = SBNet.Util.FindASPXControl("A", "lnkHideAll");
        hfGroup.value = this.chkGroupData.checked;
        if (this.chkGroupData.checked)
        {
            this.ddlColumns.style.display = "block";
            this.btnView.style.display = "block";
            this.ddlColumns.style.display = "block";
        }
        else
        {
            lnkShowAll.style.display = "none";
            this.ddlColumns.style.display = "none";
            lnkHideAll.style.display = "none";
        }
    }
}