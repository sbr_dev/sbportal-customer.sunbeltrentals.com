﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupingControl.ascx.cs" Inherits="Test_GroupingControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<script type="text/javascript" language="javascript">
//<![CDATA[
    
    function onUpdating()
    {
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 

        //  get the gridview element        
        var gridView = $get('<%= this.listView.ClientID %>');
        
        // make it visible
        updateProgressDiv.style.display = '';        
        
        // get the bounds of both the gridview and the progress div
        var gridViewBounds = Sys.UI.DomElement.getBounds(gridView);
        var updateProgressDivBounds = Sys.UI.DomElement.getBounds(updateProgressDiv);
        
        //  center of gridview
        var x = gridViewBounds.x + Math.round(gridViewBounds.width / 2) - Math.round(updateProgressDivBounds.width / 2);
        var y = gridViewBounds.y + Math.round(gridViewBounds.height / 2) - Math.round(updateProgressDivBounds.height / 2);        

        //    set the progress element to this position
        Sys.UI.DomElement.setLocation (updateProgressDiv, x, y);           
    }

    function onUpdated() 
    {
        // get the update progress div
        var updateProgressDiv = $get('updateProgressDiv'); 
        // make it invisible
        updateProgressDiv.style.display = 'none';
    }

//]]>    
</script>

<table cellpadding="2">
    <tr>
        <td><asp:CheckBox ID="chkGroupData" runat="server" onclick="javascript:Grouping.Prototype.ShowGroupingControls();" Text="Group Data" /></td>
        <td><asp:DropDownList ID="ddlColumns" runat="server" /></td>
        <td><asp:ImageButton ID="btnView" runat="server" 
                ImageUrl="~/App_Themes/Main/Buttons/view_60x20_darkred.gif" 
                onclick="btnView_Click" /></td>
        <td><asp:LinkButton ID="lnkShowAll" runat="server" onclick="LinkButton2_Click">Expand All</asp:LinkButton></td>
        <td><asp:LinkButton ID="lnkHideAll" runat="server" onclick="LinkButton2_Click1">Collapse All</asp:LinkButton></td>
    </tr>
</table>
<asp:UpdatePanel ID="upServiceCallRequest" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:ListView ID="listView" runat="server" 
            onitemdatabound="listView_ItemDataBound" 
            onlayoutcreated="listViewLayoutCreated" style="border-top: solid 1px gray;" 
            EnableViewState="False" >
            <LayoutTemplate>
                <asp:Panel ID="pnl" runat="server">
                    <table id="tbl" cellspacing="0" cellpadding="1" style="border: solid 1px gray;">
                        <tr id="tblHeader">
                            <asp:Literal ID="litHeader" runat="server" Text="" />
                        </tr>
                        <tr id="ItemPlaceHolder" runat="server"></tr>
                    </table>
                </asp:Panel>
            </LayoutTemplate>
            <ItemTemplate>
                <tr id="GroupRow" runat="server">
                    <td id="tdExtraCol" runat="server" style="border-bottom: solid 1px black;"><asp:Image ID="imgExpandCollapse" runat="server" ImageAlign="Baseline" ImageUrl="~/Images/Minus.png" style="cursor: pointer;" /></td>
                    <td id="GroupColumn" runat="server" style="border-bottom: solid 1px black; padding-top: 5px; font-weight: bold;">
                        <%=GroupedByColumnName.ToString()%>:&nbsp;<asp:Label ID="lblGroupBy" runat="server" Text='<%#Eval(GroupedByColumnName)%>' />
                        &nbsp;&nbsp;<asp:Label ID="lblCount" runat="server" Text="" />
                    </td>
                </tr>
                <asp:Literal ID="litColumns" runat="server" Text="" />
            </ItemTemplate>
            <EmptyDataTemplate>
                <asp:Panel ID="pnl" runat="server" Width="100%" style="text-align: center;">
                    There is no data available</asp:Panel>
            </EmptyDataTemplate>
        </asp:ListView>
        <asp:Literal ID="pager" runat="server" Text="" />
    </ContentTemplate>
</asp:UpdatePanel>
<Ajax:UpdatePanelAnimationExtender ID="upae" BehaviorID="animation" runat="server" TargetControlID="upServiceCallRequest">
    <Animations>
        <OnUpdating>
            <Parallel Duration="0">
                <ScriptAction Script="onUpdating();" />
                <FadeOut AnimationTarget="listView" MinimumOpacity=".6" />
            </Parallel>
        </OnUpdating>
        <OnUpdated>
            <Parallel duration="0">
                <ScriptAction Script="onUpdated();" />
                <FadeIn AnimationTarget="listView" MaximumOpacity ="1" />
            </Parallel>
        </OnUpdated>
    </Animations>
</Ajax:UpdatePanelAnimationExtender>
<div id="updateProgressDiv" style="display:none; background-color: #D9EBC5; width: 300px; border: solid 1px gray; ">
    <div align="center" style="margin-top:13px;">
        <asp:Image ID="img" runat="server" ImageUrl="~/Images/progressbar_long_green.gif" />
        <span class="updateProgressMessage">
    </div>
</div>

<asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" Visible="False">LinkButton</asp:LinkButton>
<input id="hfShowGroupingIndex" runat="server" type="hidden" />
<input id="hfGroupData" runat="server" type="hidden" value="false" />


